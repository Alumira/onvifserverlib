#pragma once
#include <string>
#include <vector>

namespace ONVIFSERVER
{
    namespace MESSAGES
    {
        const char* const AccessControl_Topic = "tns1:Device/AccessControl/";
        const char* const AccessControl_Accident = "Accident";
        const char* const AccessControl_Fault = "Fault";

        const char* const FireAlarm_Topic = "tns1:Device/FireAlarm/";
        const char* const FireAlarm_Alarm = "Alarm";
        const char* const FireAlarm_Fire = "Fire";
        const char* const FireAlarm_Fault = "Fault";

        const char* const GasAnalysis_Topic = "tns1:Device/GasAnalysis/";
        const char* const GasAnalysis_ConcentrationExceeded = "ConcentrationExceeded";
        
        const char* const Introscopy_Topic = "tns1:Device/Introscopy/";
        const char* const Introscopy_LimitExceeded = "LimitExceeded";

        const char* const NeutronProbing_Topic = "tns1:Device/NeutronProbing/";
        const char* const NeutronProbing_Detect = "Detect";

        const char* const RadiationMonitoring_Topic = "tns1:Device/RadiationMonitoring/";
        const char* const RadiationMonitoring_Detect = "Detect";

        const char* const SteamDetector_Topic = "tns1:Device/SteamDetector/";
        const char* const SteamDetector_Detect = "Detect";
        
        const char* const MetalDetector_Topic = "tns1:Device/MetalDetector/";
        const char* const MetalDetector_Detect = "Detect";

        const char* const VideoSurveillanceSystem_Topic = "tns1:Device/VideoSurveillanceSystem/";
        const char* const VideoSurveillanceSystem_ChannelDisconnect = "ChannelDisconnect";
        const char* const VideoSurveillanceSystem_ChannelConnect = "ChannelConnect";
        const char* const VideoSurveillanceSystem_RecordTurnOff = "RecordTurnOff";
        const char* const VideoSurveillanceSystem_RecordTurnOn = "RecordTurnOn";
        const char* const VideoSurveillanceSystem_MotionDetectorTrigger = "MotionDetectorTrigger";
        const char* const VideoSurveillanceSystem_ReadRecordRequest = "ReadRecordRequest";
        const char* const VideoSurveillanceSystem_DeleteRecordRequest = "DeleteRecordRequest";
        const char* const VideoSurveillanceSystem_EditDbRecordRequest = "EditDbRecordRequest";
        const char* const VideoSurveillanceSystem_CopyDbRecordRequest = "CopyDbRecordRequest";
        const char* const VideoSurveillanceSystem_Authorization = "Authorization";
        const char* const VideoSurveillanceSystem_ServerPowerOn = "ServerPowerOn";
        const char* const VideoSurveillanceSystem_ServerPowerOff = "ServerPowerOff";

        const char* const AxxonHDDrive_Topic = "axx:HDDrive/";
        const char* const AxxonHDDrive_Fault = "axx:Fault";
        const char* const AxxonHDDrive_RecordingFault = "axx:RecordingFault";
        const char* const AxxonHDDrive_DriveFull = "axx:DriveFull";

        const char* const AxxonVideoSurveillanceSystem_Topic = "axx:VideoSurveillanceSystem/";
        const char* const AxxonVideoSurveillanceSystem_ChannelDisconnect = "axx:ChannelDisconnect";
        const char* const AxxonVideoSurveillanceSystem_ChannelConnect = "axx:ChannelConnect";
        const char* const AxxonVideoSurveillanceSystem_ServerPowerOn = "axx:ServerPowerOn";
        const char* const AxxonVideoSurveillanceSystem_ServerPowerOff = "axx:ServerPowerOff";
    }

    struct MessageDescription
    {
        MessageDescription(const std::string& topic, const std::string& messageName, const std::vector<std::string>& params, bool hasSource = false) :
            _topic(topic), _messageName(messageName), _hasSource(hasSource), _params(params)
        {}

        std::string GetFullName() const
        {
            return _topic + _messageName;
        }
        
        std::string _topic;
        std::string _messageName;
        bool _hasSource;

        std::vector<std::string> _params;
    };
    
    class OnvifMessageDescriptor
    {
    public:
        static std::vector<MessageDescription> GetMessages()
        {
            std::vector<MessageDescription> msgs;

            std::vector<std::string> msgParams;

            msgParams.push_back("Name");
            msgParams.push_back("Comment");
            msgParams.push_back("Place");
            msgs.push_back(MessageDescription(MESSAGES::AccessControl_Topic, MESSAGES::AccessControl_Accident, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::AccessControl_Topic, MESSAGES::AccessControl_Fault, msgParams));

            msgParams.clear();
            msgParams.push_back("Category");
            msgParams.push_back("Zone");
            msgParams.push_back("Comment");
            msgs.push_back(MessageDescription(MESSAGES::FireAlarm_Topic, MESSAGES::FireAlarm_Alarm, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::FireAlarm_Topic, MESSAGES::FireAlarm_Fire, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::FireAlarm_Topic, MESSAGES::FireAlarm_Fault, msgParams));

            msgParams.clear();
            msgParams.push_back("Account");
            msgParams.push_back("Picture");
            msgParams.push_back("Result");
            msgs.push_back(MessageDescription(MESSAGES::Introscopy_Topic, MESSAGES::Introscopy_LimitExceeded, msgParams));

            msgParams.clear();
            msgParams.push_back("Place");
            msgParams.push_back("ConcentrationLimit");
            msgParams.push_back("GasType");
            msgParams.push_back("Concentration");
            msgParams.push_back("Comment");
            msgs.push_back(MessageDescription(MESSAGES::GasAnalysis_Topic, MESSAGES::GasAnalysis_ConcentrationExceeded, msgParams));

            msgParams.clear();
            msgParams.push_back("Picture");
            msgParams.push_back("Category");
            msgParams.push_back("Mesures");
            msgParams.push_back("Account");
            msgs.push_back(MessageDescription(MESSAGES::RadiationMonitoring_Topic, MESSAGES::RadiationMonitoring_Detect, msgParams));

            msgParams.clear();
            msgParams.push_back("Place");
            msgParams.push_back("ExplosiveType");
            msgParams.push_back("Location");
            msgParams.push_back("Comment");
            msgs.push_back(MessageDescription(MESSAGES::NeutronProbing_Topic, MESSAGES::NeutronProbing_Detect, msgParams));

            msgParams.clear();
            msgParams.push_back("Picture");
            msgParams.push_back("Account");
            msgs.push_back(MessageDescription(MESSAGES::MetalDetector_Topic, MESSAGES::MetalDetector_Detect, msgParams));

            msgParams.clear();
            msgParams.push_back("Mesures");
            msgParams.push_back("Picture");
            msgParams.push_back("Account");
            msgs.push_back(MessageDescription(MESSAGES::SteamDetector_Topic, MESSAGES::SteamDetector_Detect, msgParams));

            msgParams.clear();
            msgParams.push_back("Category");
            msgParams.push_back("Comment");
            msgParams.push_back("Priority");
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_ChannelDisconnect, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_ChannelConnect, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_RecordTurnOff, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_RecordTurnOn, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_MotionDetectorTrigger, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_ReadRecordRequest, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_DeleteRecordRequest, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_EditDbRecordRequest, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_CopyDbRecordRequest, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_Authorization, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_ServerPowerOn, msgParams));
            msgs.push_back(MessageDescription(MESSAGES::VideoSurveillanceSystem_Topic, MESSAGES::VideoSurveillanceSystem_ServerPowerOff, msgParams));

            //AxxonMesages
            /*msgParams.clear();
            msgParams.push_back("Server");
            msgParams.push_back("Disc");
            msgParams.push_back("Comment");

            msgs.push_back(MessageDescription(MESSAGES::AxxonHDDrive_Topic, MESSAGES::AxxonHDDrive_Fault, msgParams));

            msgParams.clear();
            msgParams.push_back("Comment");

            msgs.push_back(MessageDescription(MESSAGES::AxxonHDDrive_Topic, MESSAGES::AxxonHDDrive_RecordingFault, msgParams, true));
            msgs.push_back(MessageDescription(MESSAGES::AxxonVideoSurveillanceSystem_Topic, MESSAGES::AxxonVideoSurveillanceSystem_ChannelConnect, msgParams, true));
            msgs.push_back(MessageDescription(MESSAGES::AxxonVideoSurveillanceSystem_Topic, MESSAGES::AxxonVideoSurveillanceSystem_ChannelDisconnect, msgParams, true));
            msgs.push_back(MessageDescription(MESSAGES::AxxonVideoSurveillanceSystem_Topic, MESSAGES::AxxonVideoSurveillanceSystem_ServerPowerOn, msgParams, true));
            msgs.push_back(MessageDescription(MESSAGES::AxxonVideoSurveillanceSystem_Topic, MESSAGES::AxxonVideoSurveillanceSystem_ServerPowerOff, msgParams, true));*/

            return msgs;
        }
    };
}
