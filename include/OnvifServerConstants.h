#pragma once

namespace ONVIF_SERVER
{
    const char* const SERVER_LOCALHOST = "127.0.0.1";

    const char* const VIDEO_PORT = "900";
    const char* const VIDEO_ARCH_PORT = "900";
    const char* const VIDEO_GATE_PORT = "902";
    const char* const ONVIF_SERVER_PORT = "2250";
    const char* const ONVIF_SERVER_PROCESS_NAME = "ONVIFServer";
    const char* const TITLE = "ONVIF server";

    const char* const OBJECT_TYPE = "ONVIF_SERVER";

    namespace REACT
    {
        const char* const ACTION_SETUP = "SETUP";
        const char* const ACTION_DELETE = "DELETE";
    }

    namespace PARAM
    {
        const char* const port = "port";
        const char* const network_interface = "network_interface";
        const char* const rtsp_id = "rtsp_id";
		const char* const department_id = "department_id";
        const char* const is_proxy = "is_proxy";
        const char* const proxy_port = "proxy_port";
		const char* const is_version2 = "is_version2";
		const char* const isDhcpConfigurationEnabled = "isDhcpConfigurationEnabled";
		const char* const discovery_enabled = "discovery_enabled";

        namespace Misc
        {
            const char* const PERSON_PASSWD = "PERSON.passwd.%d";
            const char* const PERSON_LOGIN = "PERSON.login.%d";
			const char* const PERSON_ID = "PERSON.person_id.%d";
			const char* const RIGHTS_ID = "PERSON.rights_id.%d";
            const char* const PERSON_CAMS = "PERSON.cams.%d";
            const char* const PERSON_ARCH_HOURS = "PERSON.ah.%d";
			const char* const PERSON_ONVIF_LEVEL = "PERSON.ONVIF_level.%d";
            const char* const PERSON_COUNT = "PERSON.count";
        }
    }
}

namespace ONVIF_PROFILE
{
	const char* const OBJECT_TYPE = "ONVIF_PROFILE";

    const int START_DEFAULT_MULTICAST_AUDIO_PORT = 55000;
    const char* const DEFAULT_MULTICAST_IP = "224.0.0.7";

    namespace PARAM
    {
        const char* const is_multicast = "is_multicast";
    }
}
