#pragma once

#include <Windows.h>
#include <strsafe.h>

class RegUtils
{
	BOOL readonly;
	LONG openret;
	HKEY key;
	std::wstring skey;
	int m_addMaskOptions;
public:

	static const int x86RegistryBranch = KEY_WOW64_32KEY;
	static const int x64RegistryBranch = KEY_WOW64_64KEY;
	static const int x86registryRead = KEY_READ | x86RegistryBranch;
	static const int x86registryAllAccess = KEY_ALL_ACCESS | x86RegistryBranch;

	explicit RegUtils(LPCWSTR subkey, BOOL create = TRUE, HKEY hkey = nullptr, int addMaskOptions = x86registryAllAccess)
	{
		skey = subkey;
		m_addMaskOptions = addMaskOptions;
		readonly = FALSE;

		if (!m_addMaskOptions)
		{
			m_addMaskOptions |= KEY_ALL_ACCESS;
		}
		if ((m_addMaskOptions & x64RegistryBranch) != x64RegistryBranch)
		{
			m_addMaskOptions |= x86RegistryBranch;
		}

		REGSAM rights = m_addMaskOptions;
		openret = RegOpenKeyExW(hkey ? hkey : HKEY_LOCAL_MACHINE, skey.c_str(), 0, rights, &key);

		if (!hkey && !IsValid())
		{
			openret = RegOpenKeyExW(hkey ? hkey : HKEY_CURRENT_USER, skey.c_str(), 0, rights, &key);
		}

		if (!IsValid() && create)
		{
			CreateKey(skey.c_str(), hkey);
		}
	}

	~RegUtils()
	{
		if (IsValid())
			RegCloseKey(key);
	}

	BOOL IsValid() const { return openret == ERROR_SUCCESS; }

	HKEY GetKey() const
	{
		return key;
	}

	BOOL WriteString(LPCWSTR sval, LPCWSTR def)
	{
		if (readonly)
		{
			RegCloseKey(key);
			CreateKey(skey.c_str());
		}

		return RegSetValueExW(
			key,           // handle to key to set value for
			sval, // name of the value to set
			NULL,      // reserved
			REG_SZ,        // flag for value type
			(CONST BYTE*)def,  // address of value data
			(DWORD)(wcslen(def) * sizeof(wchar_t))// size of value data
			) == ERROR_SUCCESS;
	}

	std::wstring ReadString(LPCWSTR sval, LPCWSTR def = nullptr) const
	{
		std::wstring s;
		std::auto_ptr<BYTE> buffer;

		if (IsValid())
		{
			try
			{
				DWORD size = 0;
				
				auto status = RegQueryValueExW(key, sval, nullptr, nullptr, nullptr, &size);

				if (status != ERROR_SUCCESS)
				{
					return def && s.empty() ? def : s;
				}
				buffer.reset((BYTE*) ::operator new(size));

				status = RegQueryValueEx(key, sval, nullptr, nullptr, buffer.get(), &size);

				if (status == ERROR_SUCCESS && size)
				{
					s = std::wstring((wchar_t*)buffer.get());
				}
			}
			catch (...)
			{
			}
		}
		return def && s.empty() ? def : s;
	}

	BOOL CreateKey(LPCWSTR sval, HKEY hkey = nullptr)
	{
		DWORD cb;
		openret = RegCreateKeyExW(hkey ? hkey : HKEY_LOCAL_MACHINE, sval, 0, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS | m_addMaskOptions, nullptr, &key, &cb);
		if (!hkey && !IsValid())
			openret = RegCreateKeyExW(HKEY_CURRENT_USER, sval, 0, REG_NONE, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS | m_addMaskOptions, nullptr, &key, &cb);

		return IsValid();
	}
};

