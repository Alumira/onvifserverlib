﻿#include "HostNameUtils.h"

#include <boost/regex/v4/regex.hpp>
#include <boost/asio/ip/host_name.hpp>

std::string HostNameUtils::GetHostName()
{
	boost::system::error_code ec;
	std::string host = boost::asio::ip::host_name(ec);
	
	return host;
}

bool HostNameUtils::CheckHostName(const std::string& _hostName)
{
	boost::regex xRegEx("^[A-Za-z0-9][A-Za-z0-9 -]*$");

	boost::smatch xResults;
	return boost::regex_match(std::string(_hostName), xResults, xRegEx);
}

std::wstring HostNameUtils::GetDomainName()
{
	std::wstring domainName;

#ifdef _WINDOWS_DEF
	DWORD bufSize = MAX_PATH;
	WCHAR domainNameBuf[MAX_PATH];

	GetComputerNameEx(ComputerNameDnsDomain, domainNameBuf, &bufSize);

	domainName = std::wstring(domainNameBuf);
#endif //_WINDOWS_DEF
	return domainName;
}

std::wstring SystemInfoUtil::GetLocation()
{
#ifdef _WINDOWS_DEF
	GEOID myGEO = GetUserGeoID(GEOCLASS_NATION);
	int sizeOfBuffer = GetGeoInfo(myGEO, GEO_ISO2, NULL, 0, 0);
	wchar_t *buffer = new wchar_t[sizeOfBuffer];
	GetGeoInfo(myGEO, GEO_ISO2, buffer, sizeOfBuffer, 0);
	return std::wstring(buffer);
#else
	return std::wstring();
#endif //_WINDOWS_DEF
}
