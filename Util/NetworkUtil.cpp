#include "NetworkUtil.h"

#ifdef _WINDOWS_DEF
#include <winsock2.h>
#include <lmerr.h>
#include <iphlpapi.h>
#include <iptypes.h>
#include <ws2tcpip.h>
#include "RegUtils.h"

#pragma comment(lib, "IPHLPAPI.lib")

// headers for Linux
#else
#include <netpacket/packet.h>
#include <net/if.h>
#include <netdb.h>
#include <ifaddrs.h>

#endif

#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/regex/v4/regex.hpp>

#include <numeric>
#include <ItvSdk/include/IErrorService.h>
#include <Util/Log.h>
#include "StringUtils.h"

#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

#ifndef _WINDOWS_DEF
std::wstring charToWString(const char* str)
{
	auto stdstr = std::string(str);
	return std::wstring(stdstr.begin(), stdstr.end());
}

struct AdapterInfo
{
	std::wstring ipv4;
	std::wstring ipv6;
	std::string mac;
};

#endif

AdapterDescriptorSP NetworkUtil::FindAdapter(std::string adapterIp)
{
    auto adapters = GetAdapters();
	auto wadapterIp = StringUtils::getwstring(adapterIp);
    auto foundAdapterPtr = std::find_if(adapters.begin(), adapters.end(), [&](const AdapterDescriptorSP& ad) { return ad->m_ipv4 == wadapterIp; });
    return foundAdapterPtr != adapters.end() ? *foundAdapterPtr : AdapterDescriptorSP();
}

std::string NetworkUtil::ConvertPhysicalAddressToString(unsigned char* p_Byte, int iSize)
{
    std::string strRetValue;

#ifdef _WINDOWS_DEF
    char cAux[3];
    for (int i = 0; i<iSize; i++)
    {
        sprintf_s(cAux, "%02X", p_Byte[i]);
        strRetValue.append(cAux);
        if (i < (iSize - 1))
            strRetValue.append("-");
    }
#endif // _WINDOWS_DEF
// TODO implement ConvertPhysicalAddressToString for Linux
    return strRetValue;
}

std::vector<AdapterDescriptorSP> NetworkUtil::GetAdapters()
{
	std::vector<AdapterDescriptorSP> adapters;

#ifdef _WINDOWS_DEF
	DWORD size = 0;
	auto rv = GetAdaptersAddresses(AF_UNSPEC, GAA_FLAG_INCLUDE_PREFIX, nullptr, nullptr, &size);
	if (rv != ERROR_BUFFER_OVERFLOW) 
	{
		fprintf(stderr, "GetAdaptersAddresses() failed...");
		return adapters;
	}

	auto adapter_addresses = static_cast<IP_ADAPTER_ADDRESSES*>(MALLOC(size));
	rv = GetAdaptersAddresses(AF_UNSPEC, GAA_FLAG_INCLUDE_PREFIX, nullptr, adapter_addresses, &size);
	if (rv != ERROR_SUCCESS) 
	{
		fprintf(stderr, "GetAdaptersAddresses() failed...");
		FREE(adapter_addresses);
		return adapters;
	}

	for (auto aa = adapter_addresses; aa; aa = aa->Next)
	{
		if (aa->IfType == IF_TYPE_SOFTWARE_LOOPBACK) continue;

		AdapterDescriptorSP adapter(new AdapterDescriptor());
		adapter->dhcp_enabled = (aa->Flags & IP_ADAPTER_DHCP_ENABLED) != 0;

		adapter->m_regAdapterName = aa->FriendlyName;
		adapter->m_adapterName = aa->AdapterName;

        adapter->m_physicalAddress = ConvertPhysicalAddressToString(aa->PhysicalAddress, aa->PhysicalAddressLength);

		for (auto ua = aa->FirstUnicastAddress; ua; ua = ua->Next) 
		{
			size = BUFSIZ;
			long errorcode = WSAAddressToString(ua->Address.lpSockaddr, ua->Address.iSockaddrLength, NULL, aa->FriendlyName, &size);
			if (errorcode != 0)
			{
				printf("WSAAddressToString() failed ...");
				break;
			}

			if (ua->Address.lpSockaddr->sa_family == AF_INET)
				adapter->m_ipv4 = aa->FriendlyName;
			else if (ua->Address.lpSockaddr->sa_family == AF_INET6)
				adapter->m_ipv6 = aa->FriendlyName;
		}

		auto dnsServers = readNameServerRegister(aa->AdapterName);

		for (auto dns = aa->FirstDnsServerAddress; dns; dns = dns->Next)
		{
			if (dns->Address.lpSockaddr->sa_family != AF_INET)
			{
				continue;
			}

			size = BUFSIZ;
			long errorcode = WSAAddressToString(dns->Address.lpSockaddr, dns->Address.iSockaddrLength, nullptr, aa->FriendlyName, &size);
			if (errorcode != 0)
			{
				printf("WSAAddressToString() failed ...");
				break;
			}

			bool isStatic = dnsServers.find(aa->FriendlyName) != std::string::npos;
			adapter->m_dns.emplace_back(DnsInfo(aa->FriendlyName, isStatic));
		}

		adapters.push_back(adapter);
	}

	FREE(adapter_addresses);

	ParseAdapters(adapters);

#else
// Implementation for Linux
	struct ifaddrs *ifaddr;

	if (getifaddrs(&ifaddr) == -1) 
	{
		fprintf(stderr, "getifaddrs() failed...");
		return adapters;
	}

	std::map<std::wstring, AdapterInfo> tempAdapterInfo;

	for (auto ifaCurrent = ifaddr; ifaCurrent != NULL; ifaCurrent = ifaCurrent->ifa_next) 
	{
		if (ifaCurrent->ifa_addr == NULL)
			continue;

		// ignore loopback interface
		if (ifaCurrent->ifa_flags & IFF_LOOPBACK)
			continue;

		int family = ifaCurrent->ifa_addr->sa_family;

		auto adapterName = charToWString(ifaCurrent->ifa_name);
			
		char host[NI_MAXHOST];
			
		if (family == AF_INET || family == AF_INET6)
		{
			int getnameinfoRes = getnameinfo(ifaCurrent->ifa_addr,
					(family == AF_INET) ? sizeof(struct sockaddr_in) :
										  sizeof(struct sockaddr_in6),
					host, NI_MAXHOST,
					NULL, 0, NI_NUMERICHOST);
			
			if (getnameinfoRes != 0) 
			{
				fprintf(stderr, "getnameinfo() failed...");
				continue;
			}

			if (family == AF_INET)
				tempAdapterInfo[adapterName].ipv4 = charToWString(host);
			else
				tempAdapterInfo[adapterName].ipv6 = charToWString(host);
		}
		else if (family == AF_PACKET) 
		{
			struct sockaddr_ll* physical = reinterpret_cast<sockaddr_ll*>(ifaCurrent->ifa_addr);

			std::string mac;

			for (int octet = 0; octet < 6; ++octet)
			{
				mac = mac + (boost::format("%02X%s") % static_cast<int>(physical->sll_addr[octet]) 
													 % std::string(octet < 5 ? ":" : "")).str();
			}

			tempAdapterInfo[adapterName].mac = mac;
		}
	}

	for (auto adapterTmp : tempAdapterInfo)
	{
		AdapterDescriptorSP adapter(new AdapterDescriptor());
		adapter->m_ipv4 = adapterTmp.second.ipv4;
		adapter->m_ipv6 = adapterTmp.second.ipv6;
		adapter->m_physicalAddress = adapterTmp.second.mac;
		adapter->m_regAdapterName = adapterTmp.first;

		adapters.push_back(adapter);
	}

	freeifaddrs(ifaddr);

#endif
	return adapters;
}

void NetworkUtil::ParseAdapters(std::vector<AdapterDescriptorSP>& _adapters)
{
#ifdef _WINDOWS_DEF
	ULONG ulAdapterInfoSize = sizeof(IP_ADAPTER_INFO);
    auto pAdapterInfo = reinterpret_cast<IP_ADAPTER_INFO*>(new char[ulAdapterInfoSize]);

    if (GetAdaptersInfo(pAdapterInfo, &ulAdapterInfoSize) == ERROR_BUFFER_OVERFLOW) // out of buff
    {
        delete[] pAdapterInfo;
        pAdapterInfo = reinterpret_cast<IP_ADAPTER_INFO*>(new char[ulAdapterInfoSize]);
    }

    auto pAdapterPtr = pAdapterInfo;
    if (GetAdaptersInfo(pAdapterInfo, &ulAdapterInfoSize) == ERROR_SUCCESS)
    {
        do
        {
			auto adInfo = std::find_if(_adapters.begin(), _adapters.end(), [&](const AdapterDescriptorSP& ad) { return ad->m_adapterName == pAdapterInfo->AdapterName; });

			if(adInfo != _adapters.end())
			{
				//Get default gateway
				auto gatew = pAdapterInfo->GatewayList;
				(*adInfo)->m_gateways.push_back(gatew.IpAddress.String);

				auto ngatew = pAdapterInfo->GatewayList.Next;
				while (ngatew)
				{
					(*adInfo)->m_gateways.push_back(ngatew->IpAddress.String);
					ngatew = ngatew->Next;
				}
			}

            pAdapterInfo = pAdapterInfo->Next;
        } while (pAdapterInfo);
    }

    delete[] pAdapterPtr;

#endif //_WINDOWS_DEF
}

#ifdef _WINDOWS_DEF
std::wstring NetworkUtil::readNameServerRegister(const std::string &adapterName)
{
    std::string regPath = "SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\Interfaces\\" + adapterName;

	std::wstring wregPath = StringUtils::getwstring(regPath);
	std::wstring keyName = StringUtils::getwstring("NameServer");

    auto dnsStr = RegUtils(wregPath.c_str(), false, HKEY_LOCAL_MACHINE, KEY_WOW64_64KEY | KEY_ALL_ACCESS).ReadString(keyName.c_str());
    return dnsStr;
}
#endif //_WINDOWS_DEF

void NetworkUtil::SetSearchDomainsList(const std::vector<std::string> &searchList)
{
#ifdef _WINDOWS_DEF
	std::string regPath = "SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters";
	auto wregPath = StringUtils::getwstring(regPath);

	std::string joinedString = boost::algorithm::join(searchList, ",");
	auto wjoinedString = StringUtils::getwstring(joinedString);

	std::wstring keyName = StringUtils::getwstring("SearchList");

	RegUtils(wregPath.c_str(), false, HKEY_LOCAL_MACHINE, RegUtils::x64RegistryBranch | KEY_ALL_ACCESS).WriteString(keyName.c_str(), wjoinedString.c_str());
#endif //_WINDOWS_DEF
}

std::vector<std::string> NetworkUtil::GetSearchDomainsList()
{
	std::vector<std::string> searchList;

#ifdef _WINDOWS_DEF
	std::string regPath = "SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters";
	auto wregPath = StringUtils::getwstring(regPath);

	std::wstring keyName = StringUtils::getwstring("SearchList");

	auto listStr = RegUtils(wregPath.c_str(), false, HKEY_LOCAL_MACHINE, RegUtils::x64RegistryBranch | KEY_ALL_ACCESS).ReadString(keyName.c_str());

	if (!listStr.empty()) 
	{
		boost::split(searchList, listStr, boost::is_any_of(","), boost::token_compress_on);
	}
#endif //_WINDOWS_DEF
	return searchList;
}

void NetworkUtil::SetAdapterIpv4(ITV8::ILogger* logger, const std::string& adapterIp, const std::string& newAdapterIp)
{
	auto adapters = GetAdapters();
	auto wnewAdapterIp = StringUtils::getwstring(newAdapterIp);
	auto foundAdapterPtr = std::find_if(adapters.begin(), adapters.end(), [&](const AdapterDescriptorSP& ad) { return ad->m_ipv4 == StringUtils::getwstring(adapterIp); });
	auto foundAdapter = foundAdapterPtr != adapters.end() ? *foundAdapterPtr : AdapterDescriptorSP();

	if(foundAdapter)
	{
		std::string gateway = foundAdapter->m_gateways.empty() ? "" : foundAdapter->m_gateways.front();
		auto dns = foundAdapter->m_dns.empty() ? std::wstring() : foundAdapter->m_dns.front().dnsAddress;

#ifdef _WINDOWS_DEF
		executeCommandLine(logger, (boost::format("netsh interface ipv4 set address name=\"%1%\" source=static addr=%2% gateway=%3% gwmetric=10") % StringUtils::getstring(foundAdapter->m_regAdapterName) % newAdapterIp % gateway).str());
		//executeCommandLine((boost::format("netsh interface ipv4 set dnsservers \"%1%\" static %2%") % foundAdapter->m_regAdapterName % dns).str());
#endif //_WINDOWS_DEF
	}
}

std::wstring NetworkUtil::GetAddress(const std::string& interfaceToken)
{
	auto adapters = GetAdapters();
	auto winterfaceToken = StringUtils::getwstring(interfaceToken);
	auto foundAdapterPtr = std::find_if(adapters.begin(), adapters.end(), [&](const AdapterDescriptorSP& ad) { return ad->m_regAdapterName == winterfaceToken; });
	auto foundAdapter = foundAdapterPtr != adapters.end() ? *foundAdapterPtr : AdapterDescriptorSP();

	if (foundAdapter)
	{
		return foundAdapter->m_ipv4;
	}
	return std::wstring();
}

void NetworkUtil::EnableAdapterDhcp(ITV8::ILogger* logger, const std::string& adapterToken)
{
#ifdef _WINDOWS_DEF
	executeCommandLine(logger, (boost::format("netsh interface ipv4 set address name=\"%1%\" source=dhcp") % adapterToken).str());
#endif //_WINDOWS_DEF
}

void NetworkUtil::SetAdapterDnsDhcp(ITV8::ILogger* logger, const std::string& adapterToken)
{
#ifdef _WINDOWS_DEF
	executeCommandLine(logger, (boost::format("netsh interface ipv4 set dnsservers name=\"%1%\" source=dhcp") % adapterToken).str());
#endif //_WINDOWS_DEF
}

bool NetworkUtil::SetDNSAdapterInf(ITV8::ILogger* logger, const std::string& adapterToken, const std::vector<DnsInfo>& dnsIps)
{
    std::wstring settingAdapterIp;
    BOOST_FOREACH(auto newIp, dnsIps)
    {
        if (settingAdapterIp.length())
        {
            settingAdapterIp += StringUtils::getwstring(",");
        }
        settingAdapterIp += newIp.dnsAddress;
    }

	if (settingAdapterIp.empty()) settingAdapterIp = StringUtils::getwstring("8.8.8.8");
#ifdef _WINDOWS_DEF
	return executeCommandLine(logger, (boost::format("netsh interface ipv4 set dnsservers \"%1%\" static %2%") % adapterToken % StringUtils::getstring(settingAdapterIp)).str());
#else
	return false;
#endif //_WINDOWS_DEF
}

#ifdef _WINDOWS_DEF
void NetworkUtil::regSetDNS(const std::string& adapterName, const std::string& newDns)
{
    std::string regPath = "SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\Interfaces\\" + adapterName;
	auto wregPath = StringUtils::getwstring(regPath);
	auto wnewDns = StringUtils::getwstring(newDns);
	auto wkey = StringUtils::getwstring("NameServer");
	RegUtils(wregPath.c_str(), false, HKEY_LOCAL_MACHINE, RegUtils::x64RegistryBranch | KEY_ALL_ACCESS).WriteString(wkey.c_str(), wnewDns.c_str());
}
#endif //_WINDOWS_DEF

void NetworkUtil::SetDefaultGatewayInf(ITV8::ILogger* logger, const std::string& adapterIp, std::vector<std::string> gatews)
{
	auto adapterInfo = FindAdapter(adapterIp);
	if (!adapterInfo)
	{
		auto lMsg = (boost::format("requested adapter not found: %1%") % adapterIp.c_str()).str();
		ONVIF_LOG_BASE(logger, ITV8::LOG_ERROR, lMsg.c_str());

		return;
	}

	auto gwIp = gatews.front();
#ifdef _WINDOWS_DEF
	executeCommandLine(logger, (boost::format("netsh interface ipv4 set address name=\"%1%\" source=static addr=%2% gateway=%3% gwmetric=0") % StringUtils::getstring(adapterInfo->m_regAdapterName) % StringUtils::getstring(adapterInfo->m_ipv4) % gwIp).str());
#endif //_WINDOWS_DEF
}

bool NetworkUtil::executeCommandLine(ITV8::ILogger* logger, std::string cmdLine)
{
#ifdef _WINDOWS_DEF
	PROCESS_INFORMATION processInformation = { 0 };
	STARTUPINFO startupInfo = { 0 };
	startupInfo.cb = sizeof(startupInfo);

	auto wcmdLine = StringUtils::getwstring(cmdLine);

	// Create the process
	BOOL result = CreateProcess(NULL, const_cast<wchar_t *>(wcmdLine.c_str()),
		NULL, NULL, FALSE,
		NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW,
		NULL, NULL, &startupInfo, &processInformation);

	if (!result)
	{
		DWORD dwError = GetLastError();

		auto lMsg = (boost::format("failed at CreateProcess: %1%, error %2%") % cmdLine.c_str() % dwError).str();
		ONVIF_LOG_BASE(logger, ITV8::LOG_ERROR, lMsg.c_str());

		return false;
	}

	// Successfully created the process.  Wait for it to finish.
	WaitForSingleObject(processInformation.hProcess, INFINITE);

	DWORD exitCode;

	// Get the exit code.
	result = GetExitCodeProcess(processInformation.hProcess, &exitCode);

	// Close the handles.
	CloseHandle(processInformation.hProcess);
	CloseHandle(processInformation.hThread);

	if (!result)
	{
		// Could not get exit code.
		auto lMsg = (boost::format("Executed command but couldn't get exit code: %1%") % cmdLine.c_str()).str();
		ONVIF_LOG_BASE(logger, ITV8::LOG_ERROR, lMsg.c_str());

		return false;
	}

	if (exitCode)
	{
		// Bad result code
		auto lMsg = (boost::format("Executed command returned exit code: %1% - %2%") % cmdLine.c_str() % exitCode).str();
		ONVIF_LOG_BASE(logger, ITV8::LOG_ERROR, lMsg.c_str());

		return false;
	}

	// We succeeded.
	return true;
#else
	return false;
#endif //_WINDOWS_DEF
}


