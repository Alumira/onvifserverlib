#pragma once

#include <sstream>

/// Logs a message at specified level to logger. This macro checks for
/// validity of passed logger pointer and level.
#define ONVIF_LOG_BASE(logger, level, formatString) \
	do \
	{ \
		ITV8::ILogger* const mylogger = logger; \
		if (!mylogger || mylogger->GetLogLevel() > level) \
			break; \
		try \
		{ \
			std::ostringstream s; \
			s << formatString; \
			mylogger->Log(level, ITV8_LINEINFO, s.str().c_str()); \
		} \
		catch (const std::exception& e) \
		{ \
			mylogger->Log(level, ITV8_LINEINFO, "Unexpected exception while formatting log message"); \
			mylogger->Log(level, ITV8_LINEINFO, e.what()); \
			mylogger->Log(level, ITV8_LINEINFO, #formatString); \
		} \
	} while(false) 

#define ONVIF_LOG_ERROR(logger, formatString) \
        ONVIF_LOG_BASE(logger, ITV8::LOG_ERROR, formatString.c_str()) 

#define ONVIF_LOG_INFO(logger, formatString) \
        ONVIF_LOG_BASE(logger, ITV8::LOG_INFO, formatString.c_str()) 

#define ONVIF_LOG_WARN(logger, formatString) \
        ONVIF_LOG_BASE(logger, ITV8::LOG_WARNING, formatString.c_str()) 
