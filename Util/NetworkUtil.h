#pragma once
#include <vector>
#include <memory>

namespace ITV8 {
	struct ILogger;
}

struct DnsInfo
{
    DnsInfo(std::wstring address, bool _isManual) : dnsAddress(address), isManual(_isManual) {}
    std::wstring dnsAddress;
    bool isManual;
};

class AdapterDescriptor
{
public:
    std::string m_index;
    std::vector<DnsInfo> m_dns;
    std::wstring m_ipv4;
	std::string m_ipv4Prefix;
	std::wstring m_ipv6;
	bool dhcp_enabled;
    std::vector<std::string> m_gateways;
    std::wstring m_regAdapterName;
	std::string m_adapterName;
    std::string m_physicalAddress;
};
typedef std::shared_ptr<AdapterDescriptor> AdapterDescriptorSP;

class NetworkUtil
{
public:
    static AdapterDescriptorSP FindAdapter(std::string adapterIp);
    static std::vector<AdapterDescriptorSP> GetAdapters();

    static bool SetDNSAdapterInf(ITV8::ILogger* logger, const std::string& adapterToken, const std::vector<DnsInfo>& dnsIps);
	static void SetAdapterIpv4(ITV8::ILogger* logger, const std::string& adapterIp, const std::string& newAdapterIp);
	static void EnableAdapterDhcp(ITV8::ILogger* logger, const std::string& adapterToken);
	static std::wstring GetAddress(const std::string& interfaceToken);
	static void SetAdapterDnsDhcp(ITV8::ILogger* logger, const std::string& adapterToken);
    static void SetDefaultGatewayInf(ITV8::ILogger* logger, const std::string& adapterIp, std::vector<std::string> gatews);
	static void SetSearchDomainsList(const std::vector<std::string>& searchList);
	static std::vector<std::string> GetSearchDomainsList();

private:
	static void ParseAdapters(std::vector<AdapterDescriptorSP>& _adapters);
	static bool executeCommandLine(ITV8::ILogger* logger, std::string cmdLine);
    static std::string ConvertPhysicalAddressToString(unsigned char* p_Byte, int iSize);

#ifdef _WINDOWS_DEF
	static std::wstring readNameServerRegister(const std::string& adapterName);
	static void regSetDNS(const std::string& adapterName, const std::string& newDns);
#endif //_WINDOWS_DEF
};
