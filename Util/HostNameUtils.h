﻿#pragma once
#include <string>

class HostNameUtils
{
public:
	static std::string GetHostName();
	static bool CheckHostName(const std::string& _hostName);
	static std::wstring GetDomainName();
};

class SystemInfoUtil
{
public:
	static std::wstring GetLocation();
};