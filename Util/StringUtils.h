#pragma once
#include <vector>

#ifndef _WINDOWS_DEF
#include <locale>
#include <codecvt>
#endif

class StringUtils
{
public:
#ifdef _WINDOWS_DEF
	static bool safe_mbstowcs(const std::string &mbs, std::wstring *wcs)
	{
		assert(wcs);

		// First, determine the length of the destination buffer.
		size_t wcs_length;

		errno_t err;
		if ((err = mbstowcs_s(&wcs_length, nullptr, 0, mbs.c_str(), _TRUNCATE)) != 0) {
			return false;
		}
		assert(wcs_length > 0);

		std::vector<wchar_t> wcs_v(wcs_length);

		if ((err = mbstowcs_s(NULL, &wcs_v[0], wcs_length, mbs.c_str(), _TRUNCATE)) != 0)
		{
			return false;
		}

		*wcs = &wcs_v[0];
		return true;
	}
#else
	static bool safe_mbstowcs(const std::string &mbs, std::wstring *wcs)
	{
		assert(wcs);

		try
		{
			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
			*wcs = converter.from_bytes(mbs);
			return true;
		}
		catch(const std::exception& e)
		{
			return false;
		}
	}
#endif

	static std::wstring getwstring(const std::string &mbs)
	{
		std::wstring resStr;
		safe_mbstowcs(mbs, &resStr);
		return resStr;
	}

#ifdef _WINDOWS_DEF
	static bool safe_wcstombs(const std::wstring &wcs, std::string *mbs) 
	{
		assert(mbs);

		// First, determine the length of the destination buffer.
		size_t mbs_length;

		errno_t err;
		if ((err = wcstombs_s(&mbs_length, NULL, 0, wcs.c_str(), _TRUNCATE)) != 0) {
			return false;
		}
		assert(mbs_length > 0);

		std::vector<char> mbs_v(mbs_length);

		if ((err = wcstombs_s(NULL, &mbs_v[0], mbs_length, wcs.c_str(),
			_TRUNCATE)) != 0) {
			return false;
		}

		*mbs = &mbs_v[0];
		return true;
	}
#else
	static bool safe_wcstombs(const std::wstring &wcs, std::string *mbs) 
	{
		assert(mbs);

		try
		{
			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
			*mbs = converter.to_bytes(wcs);
			return true;
		}
		catch(const std::exception& e)
		{
			return false;
		}
	}
#endif

	static std::string getstring(const std::wstring &mbs)
	{
		std::string resStr;
		safe_wcstombs(mbs, &resStr);
		return resStr;
	}
};
