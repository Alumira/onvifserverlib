#pragma once
#include <boost/format.hpp>
#include <boost/thread/shared_mutex.hpp>
#include "Interface/IRTSPInfo.h"

struct ProtocolInfo
{
	ProtocolInfo(std::string _name, bool _isEnabled, int _port) : name(_name), port(_port), isEnabled(_isEnabled) {}

	std::string name;
	int port;
	bool isEnabled;
};
typedef std::shared_ptr<ProtocolInfo> ProtocolInfoSP;

struct CoreSettingsValues
{
	CoreSettingsValues() :
		m_port(0), m_proxy_port(0), m_use_proxy(false), m_discoveryEnabled(false), m_rtsp_port(0), m_isEnabledMulticast(false), m_is_version2(false), m_isDhcpConfigurationEnabled(true)
	{
	}

	int m_port;
    int m_proxy_port;
    bool m_use_proxy;
	std::string m_network_interface;
    std::string m_NatIpAddress;
	std::string m_model;
	std::string m_brand;
	std::string m_guid;
	bool m_discoveryEnabled;
	std::string m_id;
	int m_rtsp_port;
	std::string m_rtsp_id;
	std::string m_multicast_rtsp_port;
	bool m_isEnabledMulticast;
	bool m_is_version2;
    bool m_isDhcpConfigurationEnabled;
};
typedef std::shared_ptr<CoreSettingsValues> CoreSettingsValuesSP;

class CoreSettings
{
public:
	CoreSettings(const std::string& brand, const std::string& model, const std::string& version);
    ~CoreSettings();


	CoreSettings(const CoreSettings& other)
		: ModuleIpAdress(other.ModuleIpAdress),
		  m_port(other.m_port),
          m_proxy_port(other.m_proxy_port),
          m_use_proxy(other.m_use_proxy),
		  m_uuid(other.m_uuid),
		  m_id(other.m_id),
		  rtsp_id(other.rtsp_id),
		  m_brand(other.m_brand),
		  m_model(other.m_model),
		  m_version(other.m_version),
		  m_rtsp_info(other.m_rtsp_info),
		  m_isEnabledMulticast(other.m_isEnabledMulticast),
		  m_is_version2(other.m_is_version2),
          m_isDhcpConfigurationEnabled(other.m_isDhcpConfigurationEnabled),
		  m_discoveryEnabled(other.m_discoveryEnabled),
		  m_protocols(other.m_protocols),
		  m_scopes(other.m_scopes), m_NatIpAddress(other.m_NatIpAddress)
	{
	}

	CoreSettings& operator=(const CoreSettings& other)
	{
		if (this == &other)
			return *this;
		ModuleIpAdress = other.ModuleIpAdress;
		m_port = other.m_port;
        m_proxy_port = other.m_proxy_port;
        m_use_proxy = other.m_use_proxy;
		m_uuid = other.m_uuid;
		m_id = other.m_id;
		rtsp_id = other.rtsp_id;
		m_brand = other.m_brand;
		m_model = other.m_model;
		m_version = other.m_version;
		m_rtsp_info = other.m_rtsp_info;
		m_isEnabledMulticast = other.m_isEnabledMulticast;
		m_is_version2 = other.m_is_version2;
        m_isDhcpConfigurationEnabled = other.m_isDhcpConfigurationEnabled;
		m_discoveryEnabled = other.m_discoveryEnabled;
		m_protocols = other.m_protocols;
		m_scopes = other.m_scopes;
        m_NatIpAddress = other.m_NatIpAddress;
		return *this;
	}

	void SaveSettings(CoreSettingsValuesSP setupMsg, IRTSPInfoSP rtspInfo);

	std::string GetModuleIpAdress() const;
    std::string GetNatIpAddress() const;
    std::string GetServicesAddress() const;
    std::string GetServiceUri() const;
    std::string GetBaseUri() const;
    void SetIpAdress(const std::string& ipAdress);
    void SetNatIpAdress(const std::string& ipAdress);

    int GetPort() const;
    int GetInternalPort() const;
    bool IsProxy() const;

    IRTSPInfoSP GetRtspInfo() const;

	std::string GetMainUuid() const;
    std::string GetHardwareId() const;
	std::string GetUUID() const;

	std::string GetId() const;

	std::string GetRtspId() const;

	bool IsMulticastEnabled() const;
	bool IsVersion2() const;
    bool IsDhcpConfigurationEnabled() const;

	bool IsDiscoveryEnabled() const;
	void SetDiscoveryMode(bool isEnabled);
	
	std::string GetBrand() const;
    std::string GetManufacturer() const;
    std::string GetVersion() const;
	std::string GetModel() const;

	std::string GetName() const;

	//Network protocols
	void SetNetworkProtocol(std::string _name, bool _isEnabled);
	std::vector<ProtocolInfoSP> GetNetworkProtocols() const;

	//Scopes
	void SetScopes(const std::vector<std::string>& newScopes);
	void RemoveScopes(const std::vector<std::string>& scopes);
	std::vector<std::string> GetScopes() const;

private:
	std::string ModuleIpAdress;
	int m_port;
	int m_proxy_port;
	bool m_use_proxy;
	std::string m_uuid;
	std::string m_id;
	std::string rtsp_id;
	std::string m_brand;
	std::string m_model;
	std::string m_version;
	IRTSPInfoSP m_rtsp_info;
	bool m_isEnabledMulticast;
	bool m_is_version2;
	bool m_isDhcpConfigurationEnabled;
	bool m_discoveryEnabled;
	std::vector<ProtocolInfoSP> m_protocols;
	std::vector<std::string> m_scopes;
	std::string m_NatIpAddress;
	//Scopes
	boost::shared_mutex m_scopes_Lock;
	
	void initScopes();
};

typedef std::shared_ptr<CoreSettings> CoreSettingsSP;
