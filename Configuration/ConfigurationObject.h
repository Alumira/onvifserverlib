#pragma once
#include <string>

const int STRING_LEN_CONSTRAINT = 64;

class ConfigurationObject
{
public:
    virtual ~ConfigurationObject()
    {
    }

    std::string GetToken() const
    {
        return token;
    }

    ConfigurationObject& operator=(const ConfigurationObject& other)
    {
        if (this == &other)
            return *this;

        token = other.token;
        name = other.name;
        return *this;
    }

    void SetName(const std::string& _name)
    {
        if (_name.length() > STRING_LEN_CONSTRAINT)
        {
            name = _name.substr(0, STRING_LEN_CONSTRAINT);
            return;
        }

        name = _name;
    }

    virtual std::string GetName() const
    {
        return name;
    }

    void SetToken(const std::string& _token)
    {
        if (_token.length() > STRING_LEN_CONSTRAINT)
        {
            token = _token.substr(0, STRING_LEN_CONSTRAINT);
            return;
        }

        token = _token;
    }

private:
    std::string token;

protected:
    std::string name;
};
