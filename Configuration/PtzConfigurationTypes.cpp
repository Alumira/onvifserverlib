#include "PtzConfigurationTypes.h"
#include <boost/foreach.hpp>
#include <algorithm>

PresetConfigurationSP PtzConfiguration::AddPreset(const std::string& preset_name)
{
    auto presetConfigIt = std::find_if(m_presets.begin(), m_presets.end(), [preset_name](const std::pair<uint64_t, PresetConfigurationSP>& p)
    {
        return p.second->GetName() == preset_name;
    });

    PresetConfigurationSP presetConfig;

    if (presetConfigIt == m_presets.end())
    {
        uint64_t id = getNextPresetId();
        presetConfig = PresetConfigurationSP(new PresetConfiguration(id, GetToken()));
        presetConfig->SetName(preset_name);
        presetConfig->m_position = m_currentPosition;
    }
    else
    {
        presetConfig = presetConfigIt->second;
    }

    m_presets[presetConfig->m_id] = presetConfig;
    return presetConfig;
}

void PtzConfiguration::RemovePreset(const std::string& preset_token)
{
    auto presetIt = std::find_if(m_presets.begin(), m_presets.end(), [&](const std::pair<uint64_t, PresetConfigurationSP> &  preset) {return preset.second->GetToken() == preset_token; });
    if (presetIt != m_presets.end())
    {
        m_presets.erase(presetIt);
    }
}

void PtzConfiguration::AddPresets(const std::vector<PresetConfigurationSP>& presetsV)
{
    BOOST_FOREACH(auto preset, presetsV)
    {
        m_presets[preset->m_id] = preset;
    }
}
