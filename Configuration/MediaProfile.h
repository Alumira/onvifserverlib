#pragma once
#include "AudioConfigurationTypes.h"
#include "PtzConfigurationTypes.h"
#include "VideoConfigurationTypes.h"
#include "VideoAnalyticsConfigurationTypes.h"

#include <boost/thread/mutex.hpp>

struct ProfileSettings
{
	ProfileSettings() : m_is_multicast(false), m_isFixed(false) {}
	
	std::string m_guid;
	std::string m_name;
	std::string m_obj_id;
	std::string m_cam_id;
	std::string m_vstream_id;
	std::string m_mic_id;
	std::string m_astream_id;
	std::string m_analytics_id;
	std::string m_metadata_id;
	std::string m_telemetry_id;
	bool m_is_multicast;
	bool m_isFixed;
};
typedef std::shared_ptr<ProfileSettings> ProfileSettingsSP;

class MediaProfile : public ConfigurationObject
{
public:
    MediaProfile() : m_isMulticast(false), m_isFixed(true)
    {}

	MediaProfile(const MediaProfile& other)
		: ConfigurationObject(other), obj_id(other.obj_id),
		  m_isMulticast(other.m_isMulticast),
		  m_isFixed(other.m_isFixed),
		  videoSrc(other.videoSrc),
		  encoderConfig(other.encoderConfig),
		  audioSrc(other.audioSrc),
		  audioEncoderConfig(other.audioEncoderConfig),
		  videoAnalyticsConfig(other.videoAnalyticsConfig),
		  metadataConfig(other.metadataConfig),
		  ptzConfig(other.ptzConfig)
	{
	}

	MediaProfile& operator=(const MediaProfile& other)
	{
		if (this == &other)
			return *this;
		obj_id = other.obj_id;
		ptzConfig = other.ptzConfig;
		m_isMulticast = other.m_isMulticast;
		m_isFixed = other.m_isFixed;
		videoSrc = other.videoSrc;
		encoderConfig = other.encoderConfig;
		audioSrc = other.audioSrc;
		audioEncoderConfig = other.audioEncoderConfig;
		videoAnalyticsConfig = other.videoAnalyticsConfig;
		metadataConfig = other.metadataConfig;
		return *this;
	}

    virtual ~MediaProfile()
    {
    }

    bool IsMulticast() const
    {
        return m_isMulticast;
    }

    std::string obj_id;

    bool m_isMulticast;
    bool m_isFixed;
    void SetVideoSourceConfiguration(VideoSourceConfigurationSP vSourceConfig, bool calc = true);
    void ResetUseCount() const;

    void SetAudioSourceConfiguration(const AudioSourceConfigurationSP& aSourceConfig, bool calc = true);

    VideoSourceConfigurationSP GetVideoSourceConfiguration() const
    {
        return videoSrc;
    }

    void SetEncoderConfiguration(EncoderConfigurationSP _encConfig, bool calc = true);
    void SetAudioEncoderConfiguration(AudioEncoderConfigurationSP _encConfig, bool calc = true);
	void SetPtzConfiguration(PtzConfigurationSP _ptzConfig, bool calc = true);

    const EncoderConfigurationSP& GetEncoderConfiguration() const
    {
        return encoderConfig;
    }

    const AudioSourceConfigurationSP& GetAudioSourceConfiguration() const
    {
        return audioSrc;
    }

    const AudioEncoderConfigurationSP& GetAudioEncoderConfiguration() const
    {
        return audioEncoderConfig;
    }

	const PtzConfigurationSP& GetPtzConfiguration() const
	{
		return ptzConfig;
	}

	const VideoAnalyticsConfigurationSP& GetVideoAnalyticsConfiguration() const
	{
		return videoAnalyticsConfig;
	}
	void SetVideoAnalyticsConfiguration(VideoAnalyticsConfigurationSP vAnalyticsConfig, bool calc = true);

	const MetadataConfigurationSP& GetMetadataConfiguration() const
	{
		return metadataConfig;
	}
	void SetMetadataConfiguration(MetadataConfigurationSP vMetadataConfig, bool calc = true);

    std::string PrintConfiguration() const;

private:
    VideoSourceConfigurationSP videoSrc;
    EncoderConfigurationSP encoderConfig;
    AudioSourceConfigurationSP audioSrc;
    AudioEncoderConfigurationSP audioEncoderConfig;
	VideoAnalyticsConfigurationSP videoAnalyticsConfig;
	MetadataConfigurationSP metadataConfig;
	PtzConfigurationSP ptzConfig;

	mutable boost::mutex m_Lock;
};

typedef std::shared_ptr<MediaProfile> MediaProfileSP;

typedef std::map<std::string, MediaProfileSP> ProfilesMap;
