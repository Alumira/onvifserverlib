#pragma once
#include <vector>
#include <Configuration/ConfigurationObject.h>

class RelayOutput : public ConfigurationObject
{
public:
	enum RelayState
	{
		on,
		off
	};

	enum RelayMode
	{
		Bistable,
		Monostable
	};

	RelayOutput(const std::string& obj_id, const std::string& token) : 
			m_obj_id(obj_id), 
			m_state(off), m_idleState(on), m_mode(Bistable), m_delayTime(0)
	{
        SetToken(token);
	}

	std::string GetId() const
	{
		return m_obj_id;
	}

	void SetCurrentState(RelayState state)
	{
		m_state = state;
	}

	RelayState GetState() const
	{
		return m_state;
	}

	void SetIdleState(RelayState state)
	{
		m_idleState = state;
	}

	RelayState GetIdleState() const
	{
		return m_idleState;
	}

	void SetMode(RelayMode mode)
	{
		m_mode = mode;
	}

	RelayMode GetMode() const
	{
		return m_mode;
	}

	int64_t GetDelayTime() const
	{
		return m_delayTime;
	}

	void SetDelayTime(int64_t delayTime)
	{
		m_delayTime = delayTime;
	}

private:
    const std::string m_obj_id;
	RelayState m_state;

	RelayState m_idleState;
	RelayMode m_mode;
	int64_t m_delayTime;
};
typedef std::shared_ptr<RelayOutput> RelayOutputSP;
typedef std::vector<RelayOutputSP> RelayOutputSetType;

class RayOutput : public ConfigurationObject
{
public:
	RayOutput(const std::string& obj_id, const std::string& token, bool idleState) : 
			m_obj_id(obj_id), m_idleState(idleState), m_currentState(idleState)
	{
        SetToken(token);
	}

	void SetIdleState(int state) 
	{
		m_idleState = state;
	}

	int GetIdleState() const
	{
		return m_idleState;
	}

	bool GetCurrentState() const
	{
		return m_currentState;
	}

	std::string GetId() const
	{
		return m_obj_id;
	}

	void SetCurrentState(bool state)
	{
		m_currentState = state;
	}

private:
	const std::string m_obj_id;
	int m_idleState;
	bool m_currentState;
};
typedef std::shared_ptr<RayOutput> RayOutputSP;
typedef std::vector<RayOutputSP> RayOutputSetType;