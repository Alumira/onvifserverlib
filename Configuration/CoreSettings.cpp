#include "CoreSettings.h"
#include <Util/NetworkUtil.h>
#include <Util/HostNameUtils.h>

#include <boost/foreach.hpp>
#include <boost/range/algorithm/remove_if.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread/locks.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include "Util/StringUtils.h"

CoreSettings::CoreSettings(const std::string& brand, const std::string& model, const std::string& version) :
	m_port(9999), m_proxy_port(9998)
	, m_use_proxy(false)
	, m_brand(brand), m_model(model)
	, m_version(version)
	, m_isEnabledMulticast(false)
	, m_is_version2(false)
	, m_isDhcpConfigurationEnabled(true)
	, m_discoveryEnabled(false)
{
}

CoreSettings::~CoreSettings()
{
}

std::string CoreSettings::GetModuleIpAdress() const
{
	return ModuleIpAdress;
}

std::string CoreSettings::GetNatIpAddress() const
{
    if(!m_NatIpAddress.empty())
        return m_NatIpAddress;

    return ModuleIpAdress;
}

void CoreSettings::SetIpAdress(const std::string& ipAdress)
{
	ModuleIpAdress = ipAdress;
}

void CoreSettings::SetNatIpAdress(const std::string& ipAdress)
{
    m_NatIpAddress = ipAdress;
}

int CoreSettings::GetPort() const
{
	return m_use_proxy ? m_proxy_port : m_port;
}

int CoreSettings::GetInternalPort() const
{
    return m_port;
}

bool CoreSettings::IsProxy() const
{
    return m_use_proxy;
}

IRTSPInfoSP CoreSettings::GetRtspInfo() const
{
	return m_rtsp_info;
}

std::string CoreSettings::GetServicesAddress() const
{
	return (boost::format("%1%:%2%") % GetNatIpAddress() % GetPort()).str();
}

std::string CoreSettings::GetServiceUri() const
{
    return (boost::format("http://%1%/onvif/services") % GetServicesAddress()).str();
}

std::string CoreSettings::GetBaseUri() const
{
    return (boost::format("http://%1%") % GetServicesAddress()).str();
}

std::string CoreSettings::GetMainUuid() const
{
	return m_uuid;
}

std::string CoreSettings::GetHardwareId() const
{
    auto adapters = NetworkUtil::GetAdapters();
    auto foundAdapterPtr = std::find_if(adapters.begin(), adapters.end(), [&](const AdapterDescriptorSP& ad) { return ad->m_ipv4 == StringUtils::getwstring(GetModuleIpAdress()); });
    auto adapterInfo = (foundAdapterPtr != adapters.end() ? *foundAdapterPtr : AdapterDescriptorSP());
    
    std::string hId = m_uuid;
    if (adapterInfo)
    {
        hId = adapterInfo->m_physicalAddress;
    }

    return hId;
}

std::string CoreSettings::GetUUID() const
{
	auto str = m_uuid;
	str.erase(boost::remove_if(str, boost::is_any_of("{}")), str.end());
	boost::to_lower(str);

	return "urn:uuid:" + str;
}

void CoreSettings::initScopes()
{
	std::vector<std::string> changedScopes;
	changedScopes.push_back("onvif://www.onvif.org/name/");
	changedScopes.push_back("onvif://www.onvif.org/firmware/");
	changedScopes.push_back("onvif://www.onvif.org/hardware/");

	RemoveScopes(changedScopes);
	
	std::vector<std::string> fixedScopes;
	
	fixedScopes.push_back("onvif://www.onvif.org/Profile/Streaming");
	fixedScopes.push_back("onvif://www.onvif.org/Profile/G");
    if (IsVersion2())
    {
        fixedScopes.push_back("onvif://www.onvif.org/Profile/T");
    }
	fixedScopes.push_back("onvif://www.onvif.org/type/video_encoder");
	fixedScopes.push_back("onvif://www.onvif.org/type/audio_encoder");
	fixedScopes.push_back("onvif://www.onvif.org/hardware/" + GetVersion());
	fixedScopes.push_back("onvif://www.onvif.org/firmware/" + GetVersion());
	fixedScopes.push_back("onvif://www.onvif.org/name/" + GetName());
	fixedScopes.push_back("onvif://www.onvif.org/manufacturer/" + GetManufacturer());
	fixedScopes.push_back("onvif://www.onvif.org/model/" + GetName());
	fixedScopes.push_back("onvif://www.onvif.org/location/" + StringUtils::getstring(SystemInfoUtil::GetLocation()));

	SetScopes(fixedScopes);
}

void CoreSettings::SetScopes(const std::vector<std::string>& newScopes)
{
	boost::unique_lock<boost::shared_mutex> l(m_scopes_Lock);

	BOOST_FOREACH(auto scope, newScopes)
	{
		if (std::find_if(m_scopes.begin(), m_scopes.end(), [&](const std::string& _sc) { return _sc.find(scope) != std::string::npos; }) == m_scopes.end())
		{
			m_scopes.push_back(scope);
		}
	}
}

std::vector<std::string> CoreSettings::GetScopes() const
{
	return m_scopes;
}

void CoreSettings::RemoveScopes(const std::vector<std::string>& scopes)
{
	boost::unique_lock<boost::shared_mutex> l(m_scopes_Lock);

	BOOST_FOREACH(auto remScope, scopes)
	{
		auto it = std::find_if(m_scopes.begin(), m_scopes.end(), [&](const std::string& sc) { return sc.find(remScope) != std::string::npos; });
		if (it != m_scopes.end())
		{
			m_scopes.erase(it);
		}
	}
}

std::string CoreSettings::GetId() const
{
	return m_id;
}

std::string CoreSettings::GetRtspId() const
{
	return rtsp_id;
}

bool CoreSettings::IsMulticastEnabled() const
{
	return m_isEnabledMulticast;
}

bool CoreSettings::IsVersion2() const
{
	return m_is_version2;
}

bool CoreSettings::IsDhcpConfigurationEnabled() const
{
    return m_isDhcpConfigurationEnabled;
}

bool CoreSettings::IsDiscoveryEnabled() const
{
	return m_discoveryEnabled;
}

std::string CoreSettings::GetBrand() const
{
	return m_brand;
}

std::string CoreSettings::GetManufacturer() const
{
    std::string s = m_brand;
    boost::replace_all(s, " ", "%20");

    return s;
}

std::string CoreSettings::GetVersion() const
{
	return m_version;
}

std::string CoreSettings::GetModel() const
{
	return m_model;
}

std::string CoreSettings::GetName() const
{
	std::string s = m_model;
	boost::replace_all(s, " ", "%20");

	return s;
}

void CoreSettings::SetDiscoveryMode(bool isEnabled)
{
	m_discoveryEnabled = isEnabled;
}

void CoreSettings::SaveSettings(CoreSettingsValuesSP setupMsg, IRTSPInfoSP rtspInfo)
{
    m_port = setupMsg->m_port;
    m_proxy_port = setupMsg->m_proxy_port;
    m_use_proxy = setupMsg->m_use_proxy;

    if(!setupMsg->m_network_interface.empty())
    {
		if(setupMsg->m_network_interface != "ANY")
		{
			ModuleIpAdress = StringUtils::getstring(NetworkUtil::GetAddress(setupMsg->m_network_interface));
		}
		else
		{
			ModuleIpAdress = setupMsg->m_network_interface;
		}
    }

    m_NatIpAddress = setupMsg->m_NatIpAddress;

	m_model = setupMsg->m_model;
	m_brand = setupMsg->m_brand;

    if(!setupMsg->m_guid.empty())
    {
        m_uuid = setupMsg->m_guid;
    }
    else
    {
        m_uuid = "a618da35-e8ba-4a78-a120-8efa93413c96";
    }

	m_discoveryEnabled = setupMsg->m_discoveryEnabled;
    m_id = setupMsg->m_id;

	m_rtsp_info = rtspInfo;

    rtsp_id = setupMsg->m_rtsp_id;

    m_isEnabledMulticast = setupMsg->m_isEnabledMulticast;
    m_is_version2 = setupMsg->m_is_version2;
    m_isDhcpConfigurationEnabled = setupMsg->m_isDhcpConfigurationEnabled;

	//Save network settings
	m_protocols.clear();

	auto http_proto = ProtocolInfoSP(new ProtocolInfo("HTTP", true, GetPort()));
	m_protocols.push_back(http_proto);

	auto rtp_proto = ProtocolInfoSP(new ProtocolInfo("RTSP", true, setupMsg->m_rtsp_port));
	m_protocols.push_back(rtp_proto);

	initScopes();
}

std::vector<ProtocolInfoSP> CoreSettings::GetNetworkProtocols() const
{
	return m_protocols;
}

void CoreSettings::SetNetworkProtocol(std::string _name, bool _isEnabled)
{
	auto it = std::find_if(m_protocols.begin(), m_protocols.end(), [&](const ProtocolInfoSP& pi) { return pi->name == _name; });
	if(it != m_protocols.end())
	{
		(*it)->isEnabled = _isEnabled;
	}
}
