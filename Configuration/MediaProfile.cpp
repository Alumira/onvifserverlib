#include "MediaProfile.h"
#include <boost/format.hpp>

void MediaProfile::SetVideoSourceConfiguration(VideoSourceConfigurationSP vSourceConfig, bool calc)
{
	boost::mutex::scoped_lock guard(m_Lock); 

    if (videoSrc == vSourceConfig)
    {
        return;
    }

    if (videoSrc && calc)
    {
        videoSrc->DecUseCount();
    }

    videoSrc = vSourceConfig;

    if (videoSrc && calc)
    {
        videoSrc->IncUseCount();
    }
}

void MediaProfile::ResetUseCount() const
{
	boost::mutex::scoped_lock guard(m_Lock);

    if (encoderConfig)
    {
        encoderConfig->IncUseCount();
    }

    if (videoSrc)
    {
        videoSrc->IncUseCount();
    }

    if(audioSrc)
    {
        audioSrc->IncUseCount();
    }

    if (audioEncoderConfig)
    {
        audioEncoderConfig->IncUseCount();
    }
}

void MediaProfile::SetAudioSourceConfiguration(const AudioSourceConfigurationSP& aSourceConfig, bool calc)
{
	boost::mutex::scoped_lock guard(m_Lock);

    if (audioSrc == aSourceConfig)
    {
        return;
    }

    if (audioSrc && calc)
    {
        audioSrc->DecUseCount();
    }

    audioSrc = aSourceConfig;

    if (audioSrc && calc)
    {
        audioSrc->IncUseCount();
    }
}

void MediaProfile::SetEncoderConfiguration(EncoderConfigurationSP _encConfig, bool calc)
{
	boost::mutex::scoped_lock guard(m_Lock);

    if (encoderConfig == _encConfig)
    {
        return;
    }

    if (encoderConfig && calc)
    {
        encoderConfig->DecUseCount();
    }

    encoderConfig = _encConfig;

    if (encoderConfig && calc)
    {
        encoderConfig->IncUseCount();
    }
}

void MediaProfile::SetAudioEncoderConfiguration(AudioEncoderConfigurationSP _encConfig, bool calc)
{
	boost::mutex::scoped_lock guard(m_Lock);

    if (audioEncoderConfig == _encConfig)
    {
        return;
    }

    if (audioEncoderConfig && calc)
    {
        audioEncoderConfig->DecUseCount();
    }

    audioEncoderConfig = _encConfig;

    if (audioEncoderConfig && calc)
    {
        audioEncoderConfig->IncUseCount();
    }
}

void MediaProfile::SetPtzConfiguration(PtzConfigurationSP _ptzConfig, bool calc)
{
	boost::mutex::scoped_lock guard(m_Lock);

	if (ptzConfig == _ptzConfig)
	{
		return;
	}

	if (ptzConfig && calc)
	{
		ptzConfig->DecUseCount();
	}

	ptzConfig = _ptzConfig;

	if (ptzConfig && calc)
	{
		ptzConfig->IncUseCount();
	}
}

void MediaProfile::SetVideoAnalyticsConfiguration(VideoAnalyticsConfigurationSP _vAnalyticsConfig, bool calc)
{
	boost::mutex::scoped_lock guard(m_Lock);

	if (videoAnalyticsConfig == _vAnalyticsConfig)
	{
		return;
	}

	if (videoAnalyticsConfig && calc)
	{
		videoAnalyticsConfig->DecUseCount();
	}

	videoAnalyticsConfig = _vAnalyticsConfig;

	if (videoAnalyticsConfig && calc)
	{
		videoAnalyticsConfig->IncUseCount();
	}
}

void MediaProfile::SetMetadataConfiguration(MetadataConfigurationSP _vMetadataConfig, bool calc)
{
	boost::mutex::scoped_lock guard(m_Lock);

	if (metadataConfig == _vMetadataConfig)
	{
		return;
	}

	if (metadataConfig && calc)
	{
		metadataConfig->DecUseCount();
	}

	metadataConfig = _vMetadataConfig;

	if (metadataConfig && calc)
	{
		metadataConfig->IncUseCount();
	}
}

std::string MediaProfile::PrintConfiguration() const
{
    return (boost::format("Profile: id<%1%> token<%2%> name<%3%> video_source<%4%> video_encoder_config<%5%> audio_source<%6%> audio_encoder_config<%7%> metadata_config<%8%> ptz_config<%9%> analytics_config<%10%>\t") 
        % obj_id % GetToken() % GetName() 
        % (videoSrc ? videoSrc->GetToken() : "-") 
        % (encoderConfig ? encoderConfig->GetToken() : "-")
        % (audioSrc ? audioSrc->GetToken() : "-")
        % (audioEncoderConfig ? audioEncoderConfig->GetToken() : "-")
        % (metadataConfig ? metadataConfig->GetToken() : "-")
        % (ptzConfig ? ptzConfig->GetToken() : "-")
        % (videoAnalyticsConfig ? videoAnalyticsConfig->GetToken() : "-")).str();
}
