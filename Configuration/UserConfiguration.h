#pragma once
#include <vector>
#include <memory>

enum UserLevel
{
	Administrator = 0,
	Operator = 1,
	User = 2,
	Anonymous = 3,
	Extended = 4
};

class UserConfiguration
{
public:
	UserConfiguration(const std::string& _username, UserLevel level, const std::string& _userpassw, const std::string& _userTextPassword = "") :
		allowArchHours(0), username(_username), userpassw(_userpassw), m_level(level), m_textPassword(_userTextPassword)
	{
	}

	void SetPersonId(const std::string& _person_id)
	{
		person_id = _person_id;
	}

	std::string GetPersonId() const
	{
		return person_id;
	}

	void SetRightsId(const std::string& _rights_id)
	{
		rights_id = _rights_id;
	}

	std::string GetRightsId() const
	{
		return rights_id;
	}

    bool CheckUserCredentials(const std::string& _username, const std::string& _userpassw) const
    {
        return userpassw == _userpassw && username == _username;
    }

    std::string GetUsername() const 
    {
        return username;
    }

    std::string GetUserpassw() const
    {
        return userpassw;
    }

	std::string GetUserTextpassw() const
	{
		return m_textPassword;
	}

	UserLevel /*UserConfiguration::*/GetLevel() const
	{
		return m_level;
	}

	int allowArchHours;
    std::string allowedCams;

private:
    std::string username;
    std::string userpassw;
	UserLevel m_level;
	std::string m_textPassword;
	std::string person_id;
	std::string rights_id;
};

typedef std::shared_ptr<UserConfiguration> UserConfigurationSP;
typedef std::vector<UserConfigurationSP> UserConfigurationList;
