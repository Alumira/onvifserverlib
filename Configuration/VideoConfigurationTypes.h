#pragma once
#include <string>
#include <numeric>
#include "PtzConfigurationTypes.h"
#include "MulticastConfigurationTypes.h"
#include <boost/timer.hpp>
#include <Interface/UseCounter.h>
#include "VideoAnalyticsConfigurationTypes.h"
#include <boost/math/special_functions/round.hpp>

const int GovLengthRangeMin = 0;
const int GovLengthRangeMax = 50;
const int GovLengthDefault = 8;

const int EncodingIntervalRangeMin = 1;
const int EncodingIntervalRangeMax = 1;
const int EncodingIntervalDefault = 1;

const int QualityRangeMin = 1;
const int QualityRangeMax = 5;

const int BrightnessRangeMin = 0;
const int BrightnessRangeMax = 10;

const int ContrastRangeMin = 0;
const int ContrastRangeMax = 10;

const int ColorSaturationRangeMin = 0;
const int ColorSaturationRangeMax = 10;

const int FpsRangeMin = 4;
const int FpsRangeMax = 25;

const int BpsRangeMin = 1;
const int BpsRangeMax = 30000;

class ResolutionInfo
{
public:
    struct Resolution
    {
        Resolution(int _id, int w, int h) : width(w), height(h), id(_id) {}

        int width;
        int height;
        int id;
    };
    typedef std::shared_ptr<Resolution> ResolutionSP;

    ResolutionInfo()
    {
        m_resolutions.push_back(ResolutionSP(new Resolution(0, 320, 240)));
        m_resolutions.push_back(ResolutionSP(new Resolution(1, 640, 480)));
        m_resolutions.push_back(ResolutionSP(new Resolution(2, 704, 576)));

        //m_currentResolution = m_resolutions[0];
    }

    bool Contains(int w, int h)
    {
        return std::find_if(m_resolutions.begin(), m_resolutions.end(), [&](const ResolutionSP& res) {return res->width == w && res->height == h; }) != m_resolutions.end();
    }

    void SetResolution(int width, int height)
    {
        m_currentResolution = ResolutionSP(new Resolution(0, width, height));

        m_resolutions.clear();
        m_resolutions.push_back(m_currentResolution);
    }

    const ResolutionSP& CurrentResolution() const
    {
        return m_currentResolution;
    }

    std::vector<ResolutionSP> m_resolutions;
    ResolutionSP m_currentResolution;
};

class StreamDynamicParams
{
public:
	explicit StreamDynamicParams() : width(0), height(0), fps(0), bps(0)
	{

	}

	explicit StreamDynamicParams(const std::string& _codec, int _w, int _h, double _fps, double _bps) :
		codec(_codec), width(_w), height(_h), fps(_fps), bps(_bps)
	{

	}

	std::string codec;
	int width;
	int height;
	double fps;
	double bps;
};

class StreamStaticParams
{
public:
	explicit StreamStaticParams() : GovLength(GovLengthDefault), FrameRateLimit(FpsRangeMax), Quality(float(QualityRangeMin))
	{
	}

	explicit StreamStaticParams(int _GovLength, int _FrameRateLimit, float _Quality) :
		GovLength(_GovLength), FrameRateLimit(_FrameRateLimit), Quality(_Quality)
	{
	}

	bool SetGovLength(int _govLength)
	{
		if(_govLength < GovLengthRangeMin || _govLength > GovLengthRangeMax)
		{
			return false;
		}

		GovLength = _govLength;
		return true;
	}

	int GetGovLength() const
	{
		return GovLength;
	}

	bool SetQuality(const float quality)
	{
		if (quality < float(QualityRangeMin) || quality >float(QualityRangeMax))
		{
			return false;
		}

		Quality = quality;
		return true;
	}

	float GetQuality() const
	{
		return Quality;
	}

	bool SetFrameRateLimit(int _FrameRateLimit)
	{
		if (_FrameRateLimit < FpsRangeMin || _FrameRateLimit > FpsRangeMax)
		{
			return false;
		}

		FrameRateLimit = _FrameRateLimit;
		return true;
	}

	int GetFrameRateLimit() const
	{
		return FrameRateLimit;
	}

	std::string GetSupportedFramerates() const
	{
		std::string frameRates;
		for (int i = FpsRangeMax; i > FpsRangeMin; --i)
		{
			frameRates += boost::lexical_cast<std::string>(i) + " ";
		}

		frameRates += boost::lexical_cast<std::string>(FpsRangeMin);

		return frameRates;
	}

private:
	int GovLength;
	int FrameRateLimit;
	float Quality;
};

typedef std::shared_ptr<StreamStaticParams> StreamStaticParamsSP;

class EncoderConfiguration : public UseCounter, public ConfigurationObject
{
public:
    EncoderConfiguration(const std::string& t) : CHECK_UPDATE_STREAM_PERIOD(10.0)
		, m_started(false)
		, BitrateLimit(BpsRangeMax)
    {
        SetToken(t);        
        m_staticParams.reset(new StreamStaticParams());
    }

    void SaveStreamDynamicParams(const StreamDynamicParams& dynamic_params)
    {
        m_updateTimer.restart();

		m_started = true;

        codec = dynamic_params.codec;
        m_resolutionsInfo.SetResolution(dynamic_params.width, dynamic_params.height);

		BitrateLimit = int(ceil(dynamic_params.bps));
    }

	void SaveStreamStaticParams(const StreamStaticParamsSP& staticParams)
    {
		m_staticParams = staticParams;
    }

	StreamStaticParamsSP GetStreamStaticParams() const
    {
		return m_staticParams;
    }

	bool SetBitrateLimit(int _bps)
    {
		if(_bps < BpsRangeMin || _bps > BpsRangeMax)
		{
			return false;
		}
    	
    	BitrateLimit = _bps;
		return true;
    }

	int GetBitrateLimit() const
	{
		return BitrateLimit;
	}

     bool NeedsUpdate() const
    {
        return !m_started || (m_updateTimer.elapsed() > CHECK_UPDATE_STREAM_PERIOD);
    }

    std::string GetName() const override
    {
        if(!name.empty())
        {
			return name;
        }
    	
    	if(streamId.empty())
        {
            return "default";
        }

        return streamId;
    }

    bool IsSet() const
    {
        return m_resolutionsInfo.CurrentResolution() 
                && m_resolutionsInfo.CurrentResolution()->width 
                && m_resolutionsInfo.CurrentResolution()->height; 
    }

	std::string streamId;
	std::string codec;
	ResolutionInfo m_resolutionsInfo;

	MulticastParamsPtr multicastParams;
	std::string internal_params;

private:
    const double CHECK_UPDATE_STREAM_PERIOD;
    boost::timer m_updateTimer;
	bool m_started;
	int BitrateLimit;
	StreamStaticParamsSP m_staticParams;
};
typedef std::shared_ptr<EncoderConfiguration> EncoderConfigurationSP;

struct BoundsRange
{
    BoundsRange(int width, int height)
    {
        _widthRange = std::pair<int, int>(width, width);
        _heightRange = std::pair<int, int>(height, height);

        _xRange = std::pair<int, int>(0, 0);
        _yRange = std::pair<int, int>(0, 0);
    }

    bool CompareBounds(int width, int height, int x, int y) const
    {
        return width >= _widthRange.first && width <= _widthRange.second
            && height >= _heightRange.first && height <= _heightRange.second
            && x >= _xRange.first && x <= _xRange.second
            && y >= _yRange.first && y <= _yRange.second;
    }

    std::pair<int, int> _widthRange;
    std::pair<int, int> _heightRange;
    std::pair<int, int> _xRange;
    std::pair<int, int> _yRange;
};
typedef std::shared_ptr<BoundsRange> BoundsRangeSP;

class VideoSource : public ConfigurationObject
{
public:
    VideoSource() : cam_port("900"), m_width(0), m_height(0), m_FocusOn(false), m_isAutoIrisOn(false)
	, m_brightness(float(BrightnessRangeMin)), m_contrast(float(ContrastRangeMin)), m_colorSaturation(float(ColorSaturationRangeMin)), m_started(false)
    {
    }

    bool IsAutoFocusOn() const
    {
        return m_FocusOn;
    }

    void SetAutoFocus(bool isOn)
    {
        m_FocusOn = isOn;
    }

    bool IsAutoIrisOn() const
    {
        return m_isAutoIrisOn;
    }

    void SetAutoIris(bool isOn)
    {
        m_isAutoIrisOn = isOn;
    }

    void SetPictureParams(int width, int height)
    {
        m_width = width;
        m_height = height;

        m_bounds.reset(new BoundsRange(m_width, m_height));
    }

	bool IsStarted() const
	{
		return m_started;
	}

	void MarkAsStarted()
	{
		m_started = true;
	}

	void MarkAsStopped()
	{
		m_started = false;
	}

    int GetWidth() const
    {
        return m_width;
    }

    int GetHeight() const
    {
        return m_height;
    }

	float GetBrightness() const
    {
		return m_brightness;
    }

	float GetContrast() const
	{
		return m_contrast;
	}

	float GetColorSaturation() const
	{
		return m_colorSaturation;
	}

	bool SetBrightness(float brightness)
    {
		if(brightness < BrightnessRangeMin || brightness > BrightnessRangeMax)
		{
			return false;
		}

		m_brightness = brightness;
		return true;
    }

	bool SetContrast(float contrast)
	{
		if (contrast < ContrastRangeMin || contrast > ContrastRangeMax)
		{
			return false;
		}

		m_contrast = contrast;
		return true;
	}

	bool SetColorSaturation(float colorSaturation)
	{
		if (colorSaturation < ColorSaturationRangeMin || colorSaturation > ColorSaturationRangeMax)
		{
			return false;
		}

		m_colorSaturation = colorSaturation;
		return true;
	}

    std::string camId;
    std::string camName;
    std::string slave_id;
    std::string slave_ip;
    std::string cam_port;
    std::string stream_client;
    BoundsRangeSP m_bounds;

private:
    int m_width;
    int m_height;
    bool m_FocusOn;
    bool m_isAutoIrisOn;
	float m_brightness;
	float m_contrast;
	float m_colorSaturation;
	bool m_started;
};
typedef std::shared_ptr<VideoSource> VideoSourceSP;

class VideoSourceConfiguration : public UseCounter, public ConfigurationObject
{
public:
    VideoSourceConfiguration() {}

    std::map<std::string, EncoderConfigurationSP> m_encoders;
    VideoSourceSP videoSource;

    EncoderConfigurationSP findEncoderConfig(const std::string& streamid);

    PtzConfigurationSP m_ptzConfiguration;

    PtzConfigurationSP getPtzConfig() const
    {
        return m_ptzConfiguration;
    }

	VideoAnalyticsConfigurationSP GetVideoAnalyticsConfiguration() const
    {
		return m_videoAnalyticsConfiguration;
    }

	void SetVideoAnalyticsConfiguration(VideoAnalyticsConfigurationSP _videoAnalyticsConfiguration)
	{
		m_videoAnalyticsConfiguration = _videoAnalyticsConfiguration;
	}

	MetadataConfigurationSP GetMetadataConfiguration() const
	{
		return m_metadataConfiguration;
	}

	void SetMetadataConfiguration(MetadataConfigurationSP _metadataConfiguration)
	{
		m_metadataConfiguration = _metadataConfiguration;
	}

private:
	VideoAnalyticsConfigurationSP m_videoAnalyticsConfiguration;
	MetadataConfigurationSP m_metadataConfiguration;
};
typedef std::shared_ptr<VideoSourceConfiguration> VideoSourceConfigurationSP;

#ifdef VSCFG_ORDERED_MAP

template <typename Key, typename T>
class StrictlyOrderedMap
{
public:
    typedef typename std::vector<std::pair<Key, T>>::iterator iterator;
    typedef typename std::vector<std::pair<Key, T>>::const_iterator const_iterator;

    iterator begin() { return data.begin(); }
    iterator end() { return data.end(); }

    const_iterator begin() const { return data.begin(); }
    const_iterator end() const { return data.end(); }

    std::pair<iterator, bool> insert(std::pair<Key, T>&& value)
    {
        auto it = std::find_if(data.begin(), data.end(), [&value](const std::pair<Key, T>& item) { return value.first == item.first; });

        if (it != data.end())
            return { it, false };

        data.push_back(std::move(value));

        return { std::prev(data.end()), true };
    }

    void swap(StrictlyOrderedMap& other)
    {
        std::swap(*this, other);
    }

    iterator find(const Key& value)
    {
        return std::find_if(data.begin(), data.end(), [&value](const auto& item) { return value == item.first; });
    }

    const_iterator find(const Key& key) const
    {
        return std::find_if(data.begin(), data.end(), [&key](const auto& item) { return key == item.first; });
    }

    T& operator[](const Key& key)
    {
        auto it = std::find_if(data.begin(), data.end(), [&key](const auto& item) { return key == item.first; });
        if (it != data.end())
            return it->second;

        data.push_back({ key, T() });

        return std::prev(data.end())->second;
    }

    void erase(iterator it)
    {
        data.erase(it);
    }

private:
    std::vector<std::pair<Key, T>> data;
};

typedef StrictlyOrderedMap<std::string, VideoSourceConfigurationSP> VSourceConfigMap;
#else
typedef std::map<std::string, VideoSourceConfigurationSP> VSourceConfigMap;
#endif

typedef std::map<std::string, VideoSourceSP> VSourceMap;
