#pragma once
#include <stdint.h>
#include <Interface/UseCounter.h>
#include <boost/lexical_cast.hpp>
#include <map>
#include <vector>
#include <algorithm>
#include <memory>
#include "ConfigurationObject.h"

struct PtzPosition
{
    float x;
    float y;
    float zoom;

    PtzPosition(float _x, float _y, float _zoom) : x(_x), y(_y), zoom(_zoom) {}
    PtzPosition() : x(0), y(0), zoom(0) {}
};

class PresetConfiguration : public ConfigurationObject
{
public:
    PresetConfiguration(uint64_t id, std::string ptzToken) : m_id(id)
    {
        SetToken(ptzToken + "." + boost::lexical_cast<std::string>(id));
    }

    uint64_t m_id;
    PtzPosition m_position;
};

typedef std::shared_ptr<PresetConfiguration> PresetConfigurationSP;
typedef std::map<uint64_t, PresetConfigurationSP> PresetsMap;

class PtzConfiguration : public UseCounter, public ConfigurationObject
{
public:
    static const uint64_t MAX_PRESETS_NUM = 255;
    static const int64_t DEFAULT_PTZ_TIMEOUT = 10000;

    PtzConfiguration(const std::string& _obj_id) : m_PTZTimeout(DEFAULT_PTZ_TIMEOUT), obj_id(_obj_id)
    {
        SetName(_obj_id);
    }

    const PresetsMap& GetPresets() const
    {
        return m_presets;
    }

    PresetConfigurationSP GetPreset(const std::string& presetToken)
    {
        auto presetIt = std::find_if(m_presets.begin(), m_presets.end(), [&](const std::pair<uint64_t, PresetConfigurationSP> &  preset) {return preset.second->GetToken() == presetToken; });
        if(presetIt != m_presets.end())
        {
            return presetIt->second;
        }

        return PresetConfigurationSP();
    }

    PresetConfigurationSP AddPreset(const std::string& preset_name);
    void RemovePreset(const std::string& preset_token);

    void AddPresets(const std::vector<PresetConfigurationSP>& presetsV);

    std::string GetConfigurationToken() const
    {
        return GetToken() + "_config";
    }

    bool IsAbsoluteCoordsSupported() const
    {
        //TODO: get from driver is absolute coords supported for camera
        return true;
    }

    void SetPtzTimeout(int64_t _timeout)
    {
        m_PTZTimeout = _timeout;
    }

    int64_t GetPtzTimeout() const
    {
        return m_PTZTimeout;
    }

    void SetCurrentPosition(const PtzPosition& ptzPos)
    {
        m_currentPosition = ptzPos;
    }

	std::string GetId() const
    {
		return obj_id;
    }

private:
    PresetsMap m_presets;
    int64_t m_PTZTimeout;
    PtzPosition m_currentPosition;
	std::string obj_id;

    uint64_t getNextPresetId()
    {
        for (unsigned int i = 1; i < MAX_PRESETS_NUM; ++i)
        {
            if (!m_presets[i] || (m_presets[i] && m_presets[i]->GetName().empty()))
            {
                return i;
            }
        }

        return MAX_PRESETS_NUM;
    }
};

typedef std::shared_ptr<PtzConfiguration> PtzConfigurationSP;
