#pragma once
#include <boost/thread/shared_mutex.hpp>
#include "VideoConfigurationTypes.h"
#include "AudioConfigurationTypes.h"
#include "IOConfigurationTypes.h"
#include "RecordingConfigurationTypes.h"

typedef boost::shared_lock<boost::shared_mutex> ReadLock_t;
typedef boost::unique_lock<boost::shared_mutex> WriteLock_t;

class OnvifDeviceConfiguration : boost::noncopyable
{
public:
    OnvifDeviceConfiguration();
    ~OnvifDeviceConfiguration();

    boost::shared_mutex& getLock() const { return m_Lock; }

	void SaveProfileSettings(ProfileSettingsSP msg);
	void SaveRecordingSettings(const std::vector<RecordingConfigurationSettings> recConfigs);

	void SaveVideoSettings(VSourceMap& sources, VSourceConfigMap& sourceConfigurations);
	void SaveAudioSettings(ASources& audiosourcesMap, ASourceConfigs& audioSourceConfigurationsMap);
	void SaveRelaysSettings(RelayOutputSetType& relaysMap);
	void SaveRaysSettings(RayOutputSetType& raysMap);

	void DeleteProfile(const std::string& obj_id);

	void AddRecJob(RecordingJobSP newJob);
	void DeleteJob(RecordingJobSP newJob);

	void UpdateCamEncoderParams(const std::string& id, MulticastParamsPtr params);
	void UpdateCamEncoderInternalParams(const std::string& id, const std::string& paramsString);

	void UpdateAudioEncoderParams(const std::string &id, MulticastParamsPtr params);
	void UpdateImagingParams(const VideoSourceSP& videoSource);
	void RefreshOlxaParams(const std::string &id, int samplerate);
	void RefreshRayParams(const std::string& id, int type);
	void SaveDynamicParams(const std::string& stream_id, const StreamDynamicParams& dynamic_params);
	void SaveStaticParams(const std::string& stream_id, const StreamStaticParamsSP& static_params);

    //Get profiles config
	ProfilesMap GetProfiles() const
	{
        ReadLock_t l(getLock());
	    return m_profies;
	}

	MediaProfileSP FindProfile(const std::string& profileToken) const;
	MediaProfileSP FindProfileByName(const std::string& profileName) const;
  
	//Get video config 
	//Video source
	VSourceMap GetVSources() const
	{
		return m_sources;
	}

	VideoSourceSP GetVSource(const std::string&  vSourceToken) const;
	VideoSourceSP FindVideoSourceByCamId(const std::string& camId) const;

	//Video configuration
	VSourceConfigMap GetVSourceConfigurations() const
	{
		return m_sourceConfigurations;
	}
	
	VideoSourceConfigurationSP FindVideoSourceConfiguration(const std::string& vSourceConfigurationToken) const;
	VideoSourceConfigurationSP FindVideoSourceConfigurationByCamId(const std::string& camId) const; 
	VideoSourceConfigurationSP FindVideoSourceConfigurationByPTZ(const std::string& ptzConfigurationToken) const;
	PtzConfigurationSP FindPtzConfiguration(const std::string& ptzConfigurationToken) const;
	VideoSourceConfigurationSP FindVideoSourceConfigurationByEncoder(const std::string& encoderConfigurationToken) const;

	//Encoder configuration
	EncoderConfigurationSP FindVideoEncoderConfig(const std::string& encoderConfigurationToken) const;

	//Get Audio config  
	//Audio source
	ASources GetASources() const
	{
		return m_audiosources;
	}

	AudioSourceSP GetASource(const std::string& aSourceToken) const;
	AudioSourceSP FindAudioSourceByMicId(const std::string& micId) const;

	//Audio configuration
	const ASourceConfigs GetASourceConfigurations() const
	{
		return m_audioSourceConfigurations;
	}
	
	AudioSourceConfigurationSP findAudioSource(const std::string& micId) const;
	AudioSourceConfigurationSP FindAudioSourceConfiguration(const std::string& aSourceConfigurationToken) const;

	//Audio Encoder configuration
	AudioEncoderConfigurationSP FindAudioEncoderConfig(const std::string& encoderConfigurationToken) const;
    VideoSourceConfigurationSP FindVideoSourceConfigurationByMetadataConfigurationToken(
        const std::string& metadataConfigurationToken);

    //Relay get config
	const RelayOutputSetType GetRelaysConfigurations() const
	{
		return m_relayConfigurations;
	}

	RelayOutputSP findRelayConfiguration(const std::string& token) const;

	//Ray get config
	RayOutputSetType GetRaysConfigurations() const
	{
		return m_rayConfigurations;
	}

	RayOutputSP findRayConfiguration(const std::string& token) const;

	//Recording configuration
	RecordingsSetType GetRecordingConfigurations() const
	{
		return m_recordingConfigurations;
	}

	RecordingConfigurationSP findRecordingConfig(const std::string& recordingConfigurationToken) const;
    RecordingConfigurationSP findRecordingBySourceId(const std::string& sourceToken) const;
    RecordingConfigurationSP findRecordingConfigByCam(const std::string& camId) const;

	//Recording jobs configuration
	RecordingsJobSetType GetRecordingsJobs() const
	{
		return m_recordingJobs;
	}

	RecordingJobSP findRecordingJobByToken(const std::string& cs) const;

	//Analytics configuration
	VideoAnalyticsConfigurationSP FindVideoAnalyticsConfiguration(const std::string& token) const;
	VideoAnalyticsConfigurationSP FindVideoAnalyticsConfigurationByCam(const std::string& camId) const;
	MetadataConfigurationSP FindMetadataConfiguration(const std::string& token) const;
	MetadataConfigurationSP FindMetadataConfigurationByCam(const std::string& camId) const;

	//Network 
	std::string GetHostName() const;
	bool SetHostName(const std::string& hostName);

	void SetRtspTimeout(uint64_t _timeout);
	uint64_t GetRtspTimeout() const;

    std::string PrintProfiles() const;

private:
	ProfilesMap m_profies;

	VSourceMap m_sources;
	VSourceConfigMap m_sourceConfigurations;

	ASources m_audiosources;
	ASourceConfigs m_audioSourceConfigurations;

	RelayOutputSetType m_relayConfigurations;
	RayOutputSetType m_rayConfigurations;

	RecordingsSetType m_recordingConfigurations;
	RecordingsJobSetType m_recordingJobs;

	//Network 
	std::string m_hostname;

	uint64_t rtspTimeout;

    mutable boost::shared_mutex m_Lock;
};

typedef std::shared_ptr<OnvifDeviceConfiguration> OnvifDeviceConfigurationSP;
typedef std::weak_ptr<OnvifDeviceConfiguration> OnvifDeviceConfigurationWP;
