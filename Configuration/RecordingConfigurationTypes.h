#pragma once
#include "MediaProfile.h"
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/foreach.hpp>
#include <deque>

enum RecordingTrackType
{
    videoTrackType,
    audioTrackType,
    metadataTrackType,
    allTrackType
};

class RecordingTrack : public ConfigurationObject
{
public:
    RecordingTrack(RecordingTrackType type, const std::string& token) : m_type(type)
    {
        SetToken(token);
    }

    RecordingTrackType GetType() const
    {
        return m_type;
    }

private:
    const RecordingTrackType m_type;
};
typedef std::shared_ptr<RecordingTrack> RecordingTrackSP;
typedef std::vector<RecordingTrackSP> RecordingTracksSetType;

struct RecordingConfigurationSettings
{
	std::string m_cam_id;
	std::string m_mic_id;
    std::string m_access_point;
    std::string m_device_ap;
};

class RecordingConfiguration
{
public:
    struct SourceInfo
    {
        std::string SourceId;
        std::string Name;
        std::string Location;
        std::string Description;
        std::string Address;
    };

    RecordingConfiguration(VideoSourceSP vSource, AudioSourceSP aSource, 
        const std::string& access_point = "", const std::string& device_ap = "")

        : m_videoSource(vSource)
        , m_audioSource(aSource)
        , m_retentionTime(0)
        , m_access_point(access_point)
        , m_device_ap(device_ap)
    {
		if (vSource)
		{
			m_tracks.push_back(RecordingTrackSP(new RecordingTrack(videoTrackType, vSource->camId + ".v")));
			
			m_sourceInfo.Name = vSource->camName;
			m_sourceInfo.SourceId = vSource->GetToken();
			m_sourceInfo.Description = vSource->GetName();
		}

		if(m_audioSource)
		{
			m_tracks.push_back(RecordingTrackSP(new RecordingTrack(audioTrackType, m_audioSource->GetMicId() + ".a")));
		}
    }

    void SetArchDepthParams(time_t start, time_t stop)
    {
        m_startArchTime = start;
        m_stopArchTime = stop;
    }

    std::string GetToken() const
    {
        return m_videoSource->camId;
    }

    VideoSourceSP GetVSource() const
    {
        return m_videoSource;
    }

	AudioSourceSP GetASource() const
	{
		return m_audioSource;
	}

    std::string GetAccessPoint()
    {
        return m_access_point;
    }

    std::string DeviceAP()
    {
        return m_device_ap;
    }

    RecordingTrackSP FindTrack(const std::string& token) const
    {
        auto foundtr = std::find_if(m_tracks.begin(), m_tracks.end(), [&](const RecordingTrackSP& tr) { return tr->GetToken() == token; });
        return foundtr == m_tracks.end() ? RecordingTrackSP() : *foundtr;
    }

	RecordingTrackSP FindTrackType(RecordingTrackType type) const
	{
		auto foundtr = std::find_if(m_tracks.begin(), m_tracks.end(), [&](const RecordingTrackSP& tr) { return tr->GetType() == type; });
		return foundtr == m_tracks.end() ? RecordingTrackSP() : *foundtr;
	}

    const RecordingTracksSetType& GetTracks() const 
    {
        return m_tracks;
    }

    //TODO: send intellect parameter "������� �� �����" to cam 
    void SetRetentionTime(uint64_t maximum_retention_time)
    {
        m_retentionTime = maximum_retention_time;
    }
    uint64_t GetRetentionTime() const
    {
        return m_retentionTime;
    }

	bool Matches(const std::vector<RecordingTrackType>& tracksFilter)
    {
		bool matches = true;
    	BOOST_FOREACH(auto fl, tracksFilter)
	    {
		    if(fl == allTrackType)
		    {
				return true;
		    }

    		if(!FindTrackType(fl))
		    {
				return false;
		    }
	    }
		return matches;
    }

	time_t m_startArchTime;
	time_t m_stopArchTime;
    SourceInfo m_sourceInfo;

private:
    VideoSourceSP m_videoSource;
	AudioSourceSP m_audioSource;
    RecordingTracksSetType m_tracks;
    
    //TODO: implement parameter usage
    uint64_t m_retentionTime;
    std::string m_access_point;
    std::string m_device_ap;
};

typedef std::shared_ptr<RecordingConfiguration> RecordingConfigurationSP;

typedef std::deque<RecordingConfigurationSP> RecordingsSetType;

namespace RecordingState
{
    const char* const RecordingState_Active = "Active";
    const char* const RecordingState_Idle = "Idle";
};

class RecordingJob
{
public:
    RecordingJob() : m_state(RecordingState::RecordingState_Active), priority(1)
    {
	    m_token = to_string(boost::uuids::random_generator()());
    }

    RecordingJob(const std::string& token, const std::string& state) : m_state(state), priority(1), m_token(token)
    {
    }

    std::string GetToken() const
    {
        return m_token;
    }

    MediaProfileSP m_profile;
    RecordingConfigurationSP m_recordingConfig;
    std::string m_state;
    int priority;

private:
    std::string m_token;
};
typedef std::shared_ptr<RecordingJob> RecordingJobSP;
typedef std::vector<RecordingJobSP> RecordingsJobSetType;