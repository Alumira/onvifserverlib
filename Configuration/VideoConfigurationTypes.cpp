#include "VideoConfigurationTypes.h"
#include <boost/foreach.hpp>

EncoderConfigurationSP VideoSourceConfiguration::findEncoderConfig(const std::string& streamid)
{
    BOOST_FOREACH(auto encConfig, m_encoders)
        {
            if (encConfig.second->streamId == streamid)
            {
                return encConfig.second;
            }
        }

    return EncoderConfigurationSP();
}
