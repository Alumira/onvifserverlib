#include "VideoConfiguration.h"
#include <boost/foreach.hpp>
#include <Util/HostNameUtils.h>
#include "Util/StringUtils.h"
#include <boost/format.hpp>

OnvifDeviceConfiguration::OnvifDeviceConfiguration() : rtspTimeout(65000)
{
	m_hostname = StringUtils::getstring(HostNameUtils::GetDomainName());
}

OnvifDeviceConfiguration::~OnvifDeviceConfiguration()
{
}

void OnvifDeviceConfiguration::SaveProfileSettings(ProfileSettingsSP sets)
{
    boost::upgrade_lock<boost::shared_mutex> l(getLock());
    
    std::string guid = sets->m_guid;
    auto profile = m_profies.find(guid);

    MediaProfileSP newPr;
    if(profile == m_profies.end())
    {
        newPr = MediaProfileSP(new MediaProfile());
        newPr->SetToken(guid);
    }
    else
    {
        newPr = profile->second;
    }

    if(!sets->m_cam_id.empty())
    {
        newPr->SetEncoderConfiguration(nullptr);
        newPr->SetVideoSourceConfiguration(FindVideoSourceConfigurationByCamId(sets->m_cam_id));

        if (newPr->GetVideoSourceConfiguration())
        {
            newPr->SetEncoderConfiguration(newPr->GetVideoSourceConfiguration()->findEncoderConfig(sets->m_vstream_id));
        }
    }
    else
    {
        newPr->SetVideoSourceConfiguration(nullptr);
        newPr->SetEncoderConfiguration(nullptr);
    }

    //Audio
    if (!sets->m_mic_id.empty())
    {
        newPr->SetAudioEncoderConfiguration(nullptr);
        newPr->SetAudioSourceConfiguration(findAudioSource(sets->m_mic_id));

        if (newPr->GetAudioSourceConfiguration())
        {
            if(!sets->m_astream_id.empty())
            {
                newPr->SetAudioEncoderConfiguration(newPr->GetAudioSourceConfiguration()->findAudioEncoderConfig(sets->m_astream_id));
            }
        }
    }
    else
    {
        newPr->SetAudioSourceConfiguration(nullptr);
        newPr->SetAudioEncoderConfiguration(nullptr);
    }

	//Analytics
	newPr->SetVideoAnalyticsConfiguration(nullptr);
	if (!sets->m_analytics_id.empty())
	{
		auto vSourceConfig = newPr->GetVideoSourceConfiguration();
		
		if (vSourceConfig)
		{
			newPr->SetVideoAnalyticsConfiguration(vSourceConfig->GetVideoAnalyticsConfiguration());
		}
	}
	else
	{
		newPr->SetVideoAnalyticsConfiguration(nullptr);
	}

	//Metadata
	newPr->SetMetadataConfiguration(nullptr);
	if (!sets->m_metadata_id.empty())
	{
		auto vSourceConfig = newPr->GetVideoSourceConfiguration();

		if (vSourceConfig)
		{
			newPr->SetMetadataConfiguration(vSourceConfig->GetMetadataConfiguration());
		}
		else
		{
			BOOST_FOREACH(auto vConfig, m_sourceConfigurations)
			{
				if(auto metadataConfig = vConfig.second->GetMetadataConfiguration())
				{
					if(metadataConfig->GetId() == sets->m_metadata_id)
					{
						newPr->SetMetadataConfiguration(metadataConfig);
						break;
					}
				}
			}
		}
	}
	else
	{
		newPr->SetMetadataConfiguration(nullptr);
	}
    
    newPr->SetName(sets->m_name);
    newPr->obj_id = sets->m_obj_id;

    if(!sets->m_telemetry_id.empty() && newPr->GetVideoSourceConfiguration())
    {
        newPr->SetPtzConfiguration(newPr->GetVideoSourceConfiguration()->getPtzConfig());
    }
	else
	{
		newPr->SetPtzConfiguration(nullptr);
	}

    newPr->m_isMulticast = sets->m_is_multicast;
    newPr->m_isFixed = sets->m_isFixed;

    {
        boost::upgrade_to_unique_lock<boost::shared_mutex> wl(l);
        m_profies[newPr->GetToken()] = newPr;
    }
}

void OnvifDeviceConfiguration::SaveVideoSettings(VSourceMap& sources, VSourceConfigMap& sourceConfigurations)
{
	WriteLock_t l(getLock());

	m_sourceConfigurations.swap(sourceConfigurations);
	m_sources.swap(sources);
}

void OnvifDeviceConfiguration::SaveAudioSettings(ASources& audiosourcesMap, ASourceConfigs& audioSourceConfigurationsMap)
{
    WriteLock_t l(getLock());

    m_audioSourceConfigurations.swap(audioSourceConfigurationsMap);
    m_audiosources.swap(audiosourcesMap);
}

void OnvifDeviceConfiguration::SaveRelaysSettings(RelayOutputSetType& relaysMap)
{
	WriteLock_t l(getLock());
	m_relayConfigurations.swap(relaysMap);
}

void OnvifDeviceConfiguration::SaveRaysSettings(RayOutputSetType& raysMap)
{
	WriteLock_t l(getLock());
	m_rayConfigurations.swap(raysMap);
}

void OnvifDeviceConfiguration::SaveRecordingSettings(const std::vector<RecordingConfigurationSettings> recConfigs)
{
	boost::upgrade_lock<boost::shared_mutex> l(getLock());
	RecordingsSetType recsSet;
    RecordingsJobSetType recJobs;
	
	BOOST_FOREACH(auto rec, recConfigs)
	{
		auto vs = FindVideoSourceByCamId(rec.m_cam_id);
		auto as = FindAudioSourceByMicId(rec.m_mic_id);

        auto recConfig = RecordingConfigurationSP(new RecordingConfiguration(vs, as, rec.m_access_point, rec.m_device_ap));
	    recsSet.push_back(recConfig);
        
        auto newJob = RecordingJobSP(new RecordingJob(recConfig->GetToken() + ".job", RecordingState::RecordingState_Idle));
        newJob->m_recordingConfig = recConfig;
        recJobs.push_back(newJob);
	}

	boost::upgrade_to_unique_lock<boost::shared_mutex> wl(l);
	m_recordingConfigurations.swap(recsSet);
    m_recordingJobs.swap(recJobs);
}

VideoSourceConfigurationSP OnvifDeviceConfiguration::FindVideoSourceConfigurationByCamId(const std::string& camId) const
{
	ReadLock_t l(getLock());
    BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
    {
        if(vSourceConfig.second->videoSource->camId == camId)
        {
            return vSourceConfig.second;
        }
    }

    return VideoSourceConfigurationSP();
}

AudioSourceConfigurationSP OnvifDeviceConfiguration::findAudioSource(const std::string& micId) const
{
    BOOST_FOREACH(auto aSourceConfig, m_audioSourceConfigurations)
    {
        if (aSourceConfig->GetAudioSource()->GetMicId() == micId)
        {
            return aSourceConfig;
        }
    }

    return AudioSourceConfigurationSP();
}

void OnvifDeviceConfiguration::SaveDynamicParams(const std::string& stream_id, const StreamDynamicParams& dynamic_params)
{
    WriteLock_t l(getLock());
    
    BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
    {
        auto encConfig = vSourceConfig.second->findEncoderConfig(stream_id);
        if (encConfig)
        {
        	encConfig->SaveStreamDynamicParams(dynamic_params);
        }

        if(vSourceConfig.second->videoSource->stream_client == stream_id)
        {
            vSourceConfig.second->videoSource->SetPictureParams(dynamic_params.width, dynamic_params.height);
        }
    }
}

void OnvifDeviceConfiguration::SaveStaticParams(const std::string& stream_id, const StreamStaticParamsSP& static_params)
{
	WriteLock_t l(getLock());

	BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
	{
		auto encConfig = vSourceConfig.second->findEncoderConfig(stream_id);
		if (encConfig)
		{
			encConfig->SaveStreamStaticParams(static_params);
		}
	}
}

void OnvifDeviceConfiguration::DeleteProfile(const std::string& obj_id)
{
    boost::upgrade_lock<boost::shared_mutex> l(getLock());

    auto it = std::find_if(m_profies.begin(), m_profies.end(), [obj_id](const std::pair<std::string, MediaProfileSP> & profile) { return profile.second->obj_id == obj_id; });

    if(it == m_profies.end())
    {
		assert(false);
    	return;
    }
	
	it->second->SetVideoSourceConfiguration(nullptr);
    it->second->SetEncoderConfiguration(nullptr);

    it->second->SetAudioSourceConfiguration(nullptr);
    it->second->SetAudioEncoderConfiguration(nullptr);
    
    {
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(l);
        m_profies.erase(it);
    }
}

MediaProfileSP OnvifDeviceConfiguration::FindProfile(const std::string& profileToken) const
{
    ReadLock_t l(getLock());
    
    auto profile = m_profies.find(profileToken);
    if (profile == m_profies.end())
    {
        return MediaProfileSP();
    }

    return profile->second;
}

MediaProfileSP OnvifDeviceConfiguration::FindProfileByName(const std::string& profileName) const
{
    ReadLock_t l(getLock());

    auto it = std::find_if(m_profies.begin(), m_profies.end(), [profileName](const std::pair<std::string, MediaProfileSP> & profile) { return profile.second->GetName() == profileName; });
    return it != m_profies.end() ? it->second : MediaProfileSP();
}

VideoSourceConfigurationSP OnvifDeviceConfiguration::FindVideoSourceConfiguration(const std::string& vSourceConfigurationToken) const
{
    ReadLock_t l(getLock());

    auto videoSourceCfg = m_sourceConfigurations.find(vSourceConfigurationToken);
    if (videoSourceCfg == m_sourceConfigurations.end())
    {
        return VideoSourceConfigurationSP();
    }

    return videoSourceCfg->second;
}

AudioSourceConfigurationSP OnvifDeviceConfiguration::FindAudioSourceConfiguration(const std::string& aSourceConfigurationToken) const
{
    ReadLock_t l(getLock());

    auto audioSourceCfg = std::find_if(m_audioSourceConfigurations.begin(), m_audioSourceConfigurations.end(), [&](const AudioSourceConfigurationSP& aSource)
    {
        return aSource->GetToken() == aSourceConfigurationToken;
    });

    return audioSourceCfg != m_audioSourceConfigurations.end() ? *audioSourceCfg : AudioSourceConfigurationSP();
}

VideoSourceSP OnvifDeviceConfiguration::GetVSource(const std::string&  vSourceToken) const
{
    ReadLock_t l(getLock());

    auto videoSource = m_sources.find(vSourceToken);
    if (videoSource == m_sources.end())
    {
        return VideoSourceSP();
    }

    return videoSource->second;
}

VideoSourceSP OnvifDeviceConfiguration::FindVideoSourceByCamId(const std::string&  camId) const
{
	ReadLock_t l(getLock());

	auto videoSourceIt = std::find_if(m_sources.begin(), m_sources.end(), 
		[camId](const std::pair<std::string, VideoSourceSP>& p) { return p.second->camId == camId; });

	if (videoSourceIt != m_sources.end())
	{
		return (*videoSourceIt).second;
	}

	return VideoSourceSP();
}

AudioSourceSP OnvifDeviceConfiguration::GetASource(const std::string&  aSourceToken) const
{
    ReadLock_t l(getLock());

    auto aSourceRes = std::find_if(m_audiosources.begin(), m_audiosources.end(), [&](const AudioSourceSP& aSrc)
    {
        return aSrc->GetToken() == aSourceToken;
    });

    return aSourceRes != m_audiosources.end() ? *aSourceRes : AudioSourceSP();
}

AudioSourceSP OnvifDeviceConfiguration::FindAudioSourceByMicId(const std::string& micId) const
{
	ReadLock_t l(getLock());

	auto audioSourceIt = std::find_if(m_audiosources.begin(), m_audiosources.end(),
		[micId](const AudioSourceSP& p) { return p->GetMicId() == micId; });

	if (audioSourceIt != m_audiosources.end())
	{
		return *audioSourceIt;
	}

	return AudioSourceSP();
}

VideoSourceConfigurationSP OnvifDeviceConfiguration::FindVideoSourceConfigurationByPTZ(const std::string& ptzConfigurationToken) const
{
    ReadLock_t l(getLock());

    BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
    {
        if(vSourceConfig.second->getPtzConfig() && vSourceConfig.second->getPtzConfig()->GetConfigurationToken() == ptzConfigurationToken)
        {
            return vSourceConfig.second;
        }
    }

    return VideoSourceConfigurationSP(); 
}

PtzConfigurationSP OnvifDeviceConfiguration::FindPtzConfiguration(const std::string& ptzConfigurationToken) const
{
	ReadLock_t l(getLock());

	BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
	{
		if (vSourceConfig.second->getPtzConfig() && vSourceConfig.second->getPtzConfig()->GetConfigurationToken() == ptzConfigurationToken)
		{
			return vSourceConfig.second->getPtzConfig();
		}
	}

	return PtzConfigurationSP();
}

VideoSourceConfigurationSP OnvifDeviceConfiguration::FindVideoSourceConfigurationByEncoder(const std::string& encoderConfigurationToken) const
{
	ReadLock_t l(getLock());

	BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
	{
		BOOST_FOREACH(auto enc, vSourceConfig.second->m_encoders)
		{
			if (encoderConfigurationToken == enc.second->GetToken())
				return vSourceConfig.second;
		}
	}

	return VideoSourceConfigurationSP();
}

EncoderConfigurationSP OnvifDeviceConfiguration::FindVideoEncoderConfig(const std::string& encoderConfigurationToken) const
{
    ReadLock_t l(getLock());

    BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
    {
        auto cfg = vSourceConfig.second->m_encoders.find(encoderConfigurationToken);
        if(cfg != vSourceConfig.second->m_encoders.end())
        {
            return cfg->second; 
        }
    }

    return EncoderConfigurationSP();
}

AudioEncoderConfigurationSP OnvifDeviceConfiguration::FindAudioEncoderConfig(const std::string& encoderConfigurationToken) const
{
    ReadLock_t l(getLock());

    BOOST_FOREACH(auto aSourceConfig, m_audioSourceConfigurations)
    {
        if (aSourceConfig->GetAudioEncoderConfig()->GetToken() == encoderConfigurationToken)
        {
            return aSourceConfig->GetAudioEncoderConfig();
        }
    }

    return AudioEncoderConfigurationSP();
}

VideoSourceConfigurationSP OnvifDeviceConfiguration::FindVideoSourceConfigurationByMetadataConfigurationToken(const std::string& metadataConfigurationToken)
{
    ReadLock_t l(getLock());

    BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
    {
        if (vSourceConfig.second->GetMetadataConfiguration() && vSourceConfig.second->GetMetadataConfiguration()->GetToken() == metadataConfigurationToken)
        {
            return vSourceConfig.second;
        }
    }

    return VideoSourceConfigurationSP();
}

RecordingConfigurationSP OnvifDeviceConfiguration::findRecordingConfig(const std::string& recordingConfigurationToken) const
{
    ReadLock_t l(getLock());

    auto recConfRes = std::find_if(m_recordingConfigurations.begin(), m_recordingConfigurations.end(), [&](const RecordingConfigurationSP& recConf)
    {
        return recConf->GetToken() == recordingConfigurationToken;
    });

    return recConfRes != m_recordingConfigurations.end() ? *recConfRes : RecordingConfigurationSP();
}

RecordingConfigurationSP OnvifDeviceConfiguration::findRecordingBySourceId(const std::string& sourceToken) const
{
    ReadLock_t l(getLock());

    auto recConfRes = std::find_if(m_recordingConfigurations.begin(), m_recordingConfigurations.end(), [&](const RecordingConfigurationSP& recConf)
    {
        return recConf->GetVSource()->GetToken() == sourceToken;
    });

    return recConfRes != m_recordingConfigurations.end() ? *recConfRes : RecordingConfigurationSP();
}

RecordingConfigurationSP OnvifDeviceConfiguration::findRecordingConfigByCam(const std::string& camId) const
{
    ReadLock_t l(getLock());

    auto recConfRes = std::find_if(m_recordingConfigurations.begin(), m_recordingConfigurations.end(), [&](const RecordingConfigurationSP& recConf)
    {
        return recConf->GetVSource() && recConf->GetVSource()->camId == camId;
    });

    return recConfRes != m_recordingConfigurations.end() ? *recConfRes : RecordingConfigurationSP();
}

void OnvifDeviceConfiguration::AddRecJob(RecordingJobSP newJob)
{
	WriteLock_t l(getLock());
    m_recordingJobs.push_back(newJob);
}

void OnvifDeviceConfiguration::DeleteJob(RecordingJobSP newJob)
{
	WriteLock_t l(getLock());
    m_recordingJobs.erase(std::remove(m_recordingJobs.begin(), m_recordingJobs.end(), newJob), m_recordingJobs.end());
}

RecordingJobSP OnvifDeviceConfiguration::findRecordingJobByToken(const std::string& cs) const
{
    ReadLock_t l(getLock());

    auto recJobRes = std::find_if(m_recordingJobs.begin(), m_recordingJobs.end(), [&](const RecordingJobSP& recJob)
    {
        return recJob->GetToken() == cs;
    });

    return recJobRes != m_recordingJobs.end() ? *recJobRes : RecordingJobSP();
}

void OnvifDeviceConfiguration::UpdateCamEncoderParams(const std::string &id, MulticastParamsPtr params)
{
	WriteLock_t l(getLock());
	BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
	{
		auto encConfig = vSourceConfig.second->findEncoderConfig(id);
		if (encConfig)
		{
			encConfig->multicastParams = params;
		}
	}
}

void OnvifDeviceConfiguration::UpdateCamEncoderInternalParams(const std::string& id, const std::string& paramsString)
{
	WriteLock_t l(getLock());
	BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
	{
		auto encConfig = vSourceConfig.second->findEncoderConfig(id);
		if (encConfig)
		{
			encConfig->internal_params = paramsString;
		}
	}
}

void OnvifDeviceConfiguration::UpdateAudioEncoderParams(const std::string &id, MulticastParamsPtr params)
{
	WriteLock_t l(getLock());

    BOOST_FOREACH(auto aSourceConfig, m_audioSourceConfigurations)
    {
        auto encConfig = aSourceConfig->GetAudioEncoderConfig();
        if (encConfig)
        {
            encConfig->multicastParams = params;
        }
    }
}

void OnvifDeviceConfiguration::UpdateImagingParams(const VideoSourceSP& _videoSource)
{
	WriteLock_t l(getLock());

	auto videoSourceIt = std::find_if(m_sources.begin(), m_sources.end(),
		[_videoSource](const std::pair<std::string, VideoSourceSP>& p) { return p.second->camId == _videoSource->camId; });

	if (videoSourceIt != m_sources.end())
	{
		auto vSourcePtr = (*videoSourceIt).second;
		*vSourcePtr = *_videoSource;
	}
}

void OnvifDeviceConfiguration::RefreshOlxaParams(const std::string &id, int samplerate)
{
	WriteLock_t l(getLock());

	BOOST_FOREACH(auto aSourceConfig, m_audioSourceConfigurations)
	{
		auto encConfig = aSourceConfig->GetAudioEncoderConfig();
		if (encConfig)
		{
			encConfig->SetSamplerate(samplerate);
		}
	}
}

void OnvifDeviceConfiguration::RefreshRayParams(const std::string& id, int type)
{
	WriteLock_t l(getLock());

	BOOST_FOREACH(auto rayConf, m_rayConfigurations)
	{
		if (rayConf->GetId() == id)
		{
			rayConf->SetIdleState(type);
			break;
		}
	}
}

VideoAnalyticsConfigurationSP OnvifDeviceConfiguration::FindVideoAnalyticsConfiguration(const std::string& token) const
{
	ReadLock_t l(getLock());

	BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
	{
		auto analyticsConfig = vSourceConfig.second->GetVideoAnalyticsConfiguration();
		if (analyticsConfig && analyticsConfig->GetToken() == token)
		{
			return analyticsConfig;
		}
	}

	return VideoAnalyticsConfigurationSP();
}

VideoAnalyticsConfigurationSP OnvifDeviceConfiguration::FindVideoAnalyticsConfigurationByCam(const std::string& camId) const
{
	ReadLock_t l(getLock());

	BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
	{
		auto analyticsConfig = vSourceConfig.second->GetVideoAnalyticsConfiguration();
		if (analyticsConfig && vSourceConfig.second->videoSource->camId == camId)
		{
			return analyticsConfig;
		}
	}

	return VideoAnalyticsConfigurationSP();
}

MetadataConfigurationSP OnvifDeviceConfiguration::FindMetadataConfiguration(const std::string& token) const
{
	ReadLock_t l(getLock());

	BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
	{
		auto metadataConfig = vSourceConfig.second->GetMetadataConfiguration();
		if (metadataConfig && metadataConfig->GetToken() == token)
		{
			return metadataConfig;
		}
	}

	return MetadataConfigurationSP();
}

MetadataConfigurationSP OnvifDeviceConfiguration::FindMetadataConfigurationByCam(const std::string& camId) const
{
	ReadLock_t l(getLock());

	BOOST_FOREACH(auto vSourceConfig, m_sourceConfigurations)
	{
		auto metadataConfig = vSourceConfig.second->GetMetadataConfiguration();
		if (metadataConfig && vSourceConfig.second->videoSource->camId == camId)
		{
			return metadataConfig;
		}
	}

	return MetadataConfigurationSP();
}

RayOutputSP OnvifDeviceConfiguration::findRayConfiguration(const std::string& token) const
{
	ReadLock_t l(getLock());

	auto rayConf = std::find_if(m_rayConfigurations.begin(), m_rayConfigurations.end(), [&](const RayOutputSP& raySp)
	{
		return raySp->GetToken() == token;
	});

	return rayConf != m_rayConfigurations.end() ? *rayConf : RayOutputSP();
}

RelayOutputSP OnvifDeviceConfiguration::findRelayConfiguration(const std::string& token) const
{
	ReadLock_t l(getLock());

	auto relayConf = std::find_if(m_relayConfigurations.begin(), m_relayConfigurations.end(), [&](const RelayOutputSP& relaySp)
	{
		return relaySp->GetToken() == token;
	});

	return relayConf != m_relayConfigurations.end() ? *relayConf : RelayOutputSP();
}

std::string OnvifDeviceConfiguration::GetHostName() const
{
	if(m_hostname.empty())
	{
		return HostNameUtils::GetHostName();
	}

	return m_hostname;
}

bool OnvifDeviceConfiguration::SetHostName(const std::string& hostName)
{
	if (!HostNameUtils::CheckHostName(hostName))
	{
		return false;
	}

	m_hostname = hostName;
	return true;
}

void OnvifDeviceConfiguration::SetRtspTimeout(uint64_t _timeout)
{
	rtspTimeout = _timeout;
}

uint64_t OnvifDeviceConfiguration::GetRtspTimeout() const
{
	return rtspTimeout;
}

std::string OnvifDeviceConfiguration::PrintProfiles() const
{
    std::string lMsg;

    ReadLock_t l(getLock());

    BOOST_FOREACH(const auto profile, m_profies)
    {
        lMsg += (boost::format("Profile: id<%1%> token<%2%> name<%3%>\n")
            % profile.second->obj_id % profile.second->GetToken() % profile.second->GetName()).str();
    }

    return lMsg;
}
