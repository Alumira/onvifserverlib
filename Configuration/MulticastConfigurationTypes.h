#pragma once
#include <string>
#include <memory>

class MulticastParams
{
public:
    MulticastParams(std::string _destinationIp, int _port) : destinationIp(_destinationIp), port(_port)
    {}

    int GetRealPort() const
    {
        return port;
    }

    int GetIntellectPort() const
    {
        return port / 2;
    }

    std::string GetDestinationIp() const
    {
        return destinationIp;
    }

private:
    std::string destinationIp;
    int port; // real RTSP port, i.e. port == 2*intellect_settings_port
};
typedef std::shared_ptr<MulticastParams> MulticastParamsPtr;
