#pragma once
#include <Interface/UseCounter.h>
#include <Interface/MessageFilter.h>

const std::string TRACKER_TOPIC = "tns1:VideoAnalytics/nn:ObjectTracker";
const std::string TRACKER_NAME = "nn:ObjectTracker";
const std::string SCHEMA_URL = "http://www.onvif.org/ver10/schema";

class TrackerModule : public ConfigurationObject
{
public:
	explicit TrackerModule(const std::string& id, const std::string& guid) :
		m_topic(TRACKER_TOPIC),
		m_id(id), m_MinObjectWidth(0), m_MinObjectHeight(0), m_MaxObjectWidth(0), m_MaxObjectHeight(0), m_schema(SCHEMA_URL)
	{
        SetToken(guid);
        SetName(TRACKER_NAME);
	}

	std::string GetType() const
	{
		return m_topic;
	}

	float GetMinObjectWidth() const
	{
		return m_MinObjectWidth;
	}

	float GetMinObjectHeight() const
	{
		return m_MinObjectHeight;
	}

	float GetMaxObjectWidth() const
	{
		return m_MaxObjectWidth;
	}

	float GetMaxObjectHeight() const
	{
		return m_MaxObjectHeight;
	}

	std::string GetSchema() const
	{
		return m_schema;
	}

private:
	const std::string m_topic;
	const std::string m_id;

	float m_MinObjectWidth;
	float m_MinObjectHeight;
	float m_MaxObjectWidth;
	float m_MaxObjectHeight;
	
	const std::string m_schema;
};
typedef std::shared_ptr<TrackerModule> TrackerModuleSP;

class TrackerRule : public ConfigurationObject
{
public:
	explicit TrackerRule(const std::string& id, const std::string& guid) :
		m_id(id)
	{
        SetToken(guid);
	}

private:
	const std::string m_id;
};
typedef std::shared_ptr<TrackerRule> TrackerRuleSP;

class VideoAnalyticsConfiguration : public UseCounter, public ConfigurationObject
{
public:
    VideoAnalyticsConfiguration(const std::string& t, const std::string& id) : m_id(id)
    {
        SetToken(t);
        SetName(GetToken() + "." + m_id);
    }

	std::string GetId() const
	{
		return m_id;
	}

	std::vector<TrackerModuleSP> GetModules() const
    {
		return m_trackers;
    }

	std::vector<TrackerRuleSP> GetRules() const
	{
		return m_trackerRules;
	}

private:
	std::string m_id;
	std::vector<TrackerModuleSP> m_trackers;
	std::vector<TrackerRuleSP> m_trackerRules;
};
typedef std::shared_ptr<VideoAnalyticsConfiguration> VideoAnalyticsConfigurationSP;

class MetadataConfiguration : public UseCounter, public ConfigurationObject
{
public:
	MetadataConfiguration(const std::string& t, const std::string& id) : 
        m_id(id), m_includeAnalytics(true), m_includeEvents(true)
	{
        SetToken(t);
        SetName(GetToken() + "." + m_id);
	}

	std::string GetId() const
	{
		return m_id;
	}

	bool GetAnalyticsFlag() const
	{
		return m_includeAnalytics;
	}

	void SetAnalyticsFlag(bool _isIncludeAnalytics)
	{
		m_includeAnalytics = _isIncludeAnalytics;
	}

    bool GetEventsFlag() const
    {
        return m_includeEvents;
    }

    void SetEventsFlag(bool _isIncludeEvents)
    {
        m_includeEvents = _isIncludeEvents;
    }

    std::vector<FilterTypeSP> GetFilter() const
	{
        return m_filter;
	}

    void SetFilter(const std::vector<FilterTypeSP>& filter)
	{
        m_filter = filter;
	}

	MulticastParamsPtr multicastParams;

private:
	std::string m_id;
	bool m_includeAnalytics;
    bool m_includeEvents;
    std::vector<FilterTypeSP> m_filter;
};
typedef std::shared_ptr<MetadataConfiguration> MetadataConfigurationSP;
