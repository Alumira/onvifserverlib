#pragma once
#include <vector>
#include <Interface/UseCounter.h>
#include "MulticastConfigurationTypes.h"
#include <Configuration/ConfigurationObject.h>

class AudioSource : public ConfigurationObject
{
public:
    AudioSource(const std::string& _token, const std::string& _micId, const std::string& _micName, const std::string& _slaveIp, const std::string& _slaveId)
        : micId(_micId), slaveId(_slaveId), slaveIp(_slaveIp)
    {
        SetToken(_token);
        SetName(_micName);
    }
    
    std::string GetMicId() const
    {
        return micId;
    }

private:
    std::string micId;
    std::string slaveId;
    std::string slaveIp;
};

typedef std::shared_ptr<AudioSource> AudioSourceSP;

class AudioEncoderConfiguration : public UseCounter, public ConfigurationObject
{
public:
    AudioEncoderConfiguration(const std::string& _token, const std::string& _streamId, const std::string& parent_id, int _samplerate = 8000)
        : m_samplerate(_samplerate), m_streamId(_streamId), m_parent_id(parent_id)
    {
        SetToken(_token);
    }

    std::string GetStreamId() const
    {
        return m_streamId;
    }

    int GetSamplerate() const
    {
        return m_samplerate / 1000; 
    }

	void SetSamplerate(int samplerate)
    {
		m_samplerate = samplerate;
    }

	std::string GetParentId() const
    {
		return m_parent_id;
    }

    MulticastParamsPtr multicastParams;

private:
    int m_samplerate;
    std::string m_streamId;
	std::string m_parent_id;
};

typedef std::shared_ptr<AudioEncoderConfiguration> AudioEncoderConfigurationSP;

class AudioSourceConfiguration : public UseCounter, public ConfigurationObject
{
public:
    AudioSourceConfiguration(const AudioSourceSP& _audioSource, const AudioEncoderConfigurationSP& _audioEncoderConfig) :
        m_audioSource(_audioSource), m_audioEncoderConfig(_audioEncoderConfig)
    {
        SetToken(m_audioSource->GetToken() + "_config");
    }

    const AudioSourceSP& GetAudioSource() const
    {
        return m_audioSource;
    }

    AudioEncoderConfigurationSP findAudioEncoderConfig(const std::string& streamid) const
    {
        if(m_audioEncoderConfig->GetStreamId() == streamid)
        {
            return m_audioEncoderConfig;
        }
        return AudioEncoderConfigurationSP();
    }

    const AudioEncoderConfigurationSP& GetAudioEncoderConfig() const
    {
        return m_audioEncoderConfig;
    }

private:
    AudioSourceSP m_audioSource;
    AudioEncoderConfigurationSP m_audioEncoderConfig;
};

typedef std::shared_ptr<AudioSourceConfiguration> AudioSourceConfigurationSP;

typedef std::vector<AudioSourceConfigurationSP> ASourceConfigs;
typedef std::vector<AudioSourceSP> ASources;
