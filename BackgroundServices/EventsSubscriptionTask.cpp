#include "EventsSubscriptionTask.h"
#include <ItvSdk/include/IErrorService.h>
#include <boost/foreach.hpp>
#include <Interface/FilterProcessor.h>
#include "Util/Log.h"
#include "SoapProxy/SoapProxyClasses.h"
#include <ServicesImpl/DeviceBindingServiceImpl.h>

EventsSubscriptionTaskSP EventsSubscriptionTask::Create(ITV8::ILogger* logger, ISubscriptionManagerSP subscrManager, const time_t& terminationTime, const std::vector<FilterTypeSP>& filter, NotificationConsumerBindingProxySP consumerProxy)
{
    return EventsSubscriptionTaskSP(new EventsSubscriptionTask(generateNewUuid(), logger, subscrManager, terminationTime, filter, consumerProxy));
}

EventsSubscriptionTask::~EventsSubscriptionTask()
{
    {
        boost::mutex::scoped_lock lock(m_consumer_thread_lock);
        m_consumer_running = false;
        m_consumer_condition.notify_one();
    }

    m_consumer_thread.join();
}

void EventsSubscriptionTask::SetTerminationTime(const time_t& tss)
{
    if(m_terminationTime < tss)
        m_terminationTime = tss;
}

void EventsSubscriptionTask::Wait(LONG64 waittimeout)
{
    boost::system_time const timeout = boost::get_system_time() + boost::posix_time::milliseconds(waittimeout);
    
    boost::mutex::scoped_lock lock(m_waitmutex);
    if (m_waitcondition.timed_wait(lock, timeout, [&]() -> bool
		{
			auto cur_tssTime = SoapProxy::GetCurTime();
			return cur_tssTime > m_terminationTime || GetEventsNum() || m_canceled;
		}))
    {
        //finish ok
		auto lMsg = (boost::format("Task %1% finished") % m_GUID.c_str()).str();
        ONVIF_LOG_INFO(m_logger, lMsg);
    }
    else
    {
        //timeout return
		auto lMsg = (boost::format("Task %1% exited by timeout") % m_GUID.c_str()).str();
        ONVIF_LOG_ERROR(m_logger, lMsg);
    }
}

EventsSubscriptionTask::EventsSubscriptionTask(const std::string& guid, ITV8::ILogger* logger, ISubscriptionManagerSP subscrManager, time_t terminationTime, std::vector<FilterTypeSP> filter, NotificationConsumerBindingProxySP consumerProxy) :
    IBackgroundTask(guid)
    , IMessageSubscription(filter)
    , m_subscrManager(subscrManager)
    , m_terminationTime(terminationTime)
    , m_logger(logger)
    , m_canceled(false)
    , m_consumerProxy(consumerProxy)
    , m_consumer_running(false)
{
    if(m_consumerProxy)
    {
        m_consumer_running = true;
        m_consumer_thread = boost::thread([&]() { this->consumer_thread_run(); });
    }
}

void EventsSubscriptionTask::Start(soap* soap)
{
    if(auto sm = m_subscrManager.lock())
    {
        IBackgroundTask::Start(soap);

        _this = shared_from_this();
        sm->attach(_this);
		sm->SendPropertyEventsStates(soap, _this);

		auto lMsg = (boost::format("Task %1% started") % m_GUID.c_str()).str();
        ONVIF_LOG_INFO(m_logger, lMsg);
    }
    else
    {
        IBackgroundTask::Stop();
    }
}

void EventsSubscriptionTask::Stop()
{
    m_consumer_running = false;
    IBackgroundTask::Stop();
}

void EventsSubscriptionTask::Cancel()
{
    boost::unique_lock<boost::mutex> ul(m_waitmutex);
    m_canceled = true;
    m_waitcondition.notify_one();
}

EventsSubscriptionSetType EventsSubscriptionTask::PopEvents(int maxResultsNum)
{
	boost::mutex::scoped_lock guard(m_Lock);

    EventsSubscriptionSetType retIntervals;
    while((maxResultsNum < 0 || int(retIntervals.size()) < maxResultsNum) && m_vResultEvents.size() > 0)
    {
        retIntervals.push_back(m_vResultEvents.front());
        m_vResultEvents.pop_front();
    }
    
    return retIntervals;
}

int EventsSubscriptionTask::GetEventsNum() const
{
    return m_vResultEvents.size();
}

void EventsSubscriptionTask::OnReceive(MessageHolderTypeSP message)
{
    if (m_interrupt)
    {
        return;
    }

	auto cur_tssTime = SoapProxy::GetCurTime();

    if (cur_tssTime > m_terminationTime)
    {
        doInterrupt(STOPPED);
    }

    auto fProcessor = FilterProcessor(m_filter);
    if(fProcessor.Filter(message))
	{
		boost::mutex::scoped_lock guard(m_Lock);

		if (m_consumerProxy)
		{
            m_consumerQueue.push(message, 0);

            boost::mutex::scoped_lock lock(m_consumer_thread_lock);
		    m_consumer_condition.notify_one();
		}
		else
		{
			m_vResultEvents.push_back(message);
		}
		m_waitcondition.notify_all();
	}
}

void EventsSubscriptionTask::doInterrupt(SearchResult reason)
{
    IBackgroundTask::doInterrupt(reason);

    if (auto sm = m_subscrManager.lock())
    {
        sm->detach(_this);
    }

	_this.reset();
}

void EventsSubscriptionTask::consumer_thread_run()
{
    boost::mutex::scoped_lock lock(m_consumer_thread_lock);
    while (m_consumer_running)
    {
        m_consumer_condition.wait(lock, [=] { return m_consumerQueue.popReady() || !m_consumer_running; });
        if (m_consumer_running)
        {
            lock.unlock();
            MessageHolderTypeSP msg;
            if(m_consumerQueue.pop(msg, 0))
            {
                m_consumerProxy->NotifyMessage(msg);
            }
            lock.lock();
        }
    }
}
