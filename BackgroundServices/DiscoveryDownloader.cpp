#include "DiscoveryDownloader.h"

#include <string>
#include <boost/format.hpp>
#include "Util/StringUtils.h"
#include "Util/Log.h"

DiscoveryDownloader::DiscoveryDownloader(ITV8::ILogger* logger, const std::string& ip, const std::string& nat_ip, const std::string& port, const std::string& uuid)
    : m_ip(ip), m_port(port), m_uuid(uuid), m_logger(logger), m_lpfnStart(nullptr), m_lpfnStop(nullptr), m_lpfnScopes(nullptr)
{
#ifdef _WINDOWS_DEF
	m_hDll = nullptr;
#endif //_WINDOWS_DEF

	Init();
}

DiscoveryDownloader::~DiscoveryDownloader()
{
#ifdef _WINDOWS_DEF
	if (m_hDll)
    {
        Stop();
        UnloadDll(m_hDll);
    }
#endif //_WINDOWS_DEF
}

void DiscoveryDownloader::Init()
{
#ifdef _WINDOWS_DEF
	wchar_t path[MAX_PATH];
    GetModuleFileName(nullptr, path, MAX_PATH);

	auto pathStr = std::wstring(path);

    auto nPos = pathStr.rfind('\\');
	pathStr = pathStr.substr(0, nPos);

	pathStr += '\\';

	std::wstring resStr;
	std::string dllName(DISCOVERY_DLL_NAME);
	StringUtils::safe_mbstowcs(dllName, &resStr);
	pathStr += resStr;

    m_hDll = LoadDll(pathStr.c_str());

    if (m_hDll)
    {
        m_lpfnStart =
            reinterpret_cast<InitDiscoveryFunc>(::GetProcAddress(m_hDll, "InitDiscovery"));

        m_lpfnStop =
            reinterpret_cast<StopDiscoveryFunc>(::GetProcAddress(m_hDll, "StopDiscovery"));

		m_lpfnScopes =
			reinterpret_cast<SetDiscoveryScopesFunc>(::GetProcAddress(m_hDll, "SetDiscoveryScopes"));
    }
    else
    {
		auto lMsg = (boost::format("Failed to find %1%") % StringUtils::getstring(pathStr)).str();
		ONVIF_LOG_ERROR(m_logger, lMsg);
    }
#else
	auto lMsg = (boost::format("Loading dll is not implemented!")).str();
    ONVIF_LOG_ERROR(m_logger, lMsg);
#endif //_WINDOWS_DEF
}

void DiscoveryDownloader::Start(const std::vector<std::string>& newScopes) const
{
    if(m_lpfnStart)
    {
        m_lpfnStart(m_ip, m_nat_ip, m_port, m_uuid, newScopes);
    }
}

void DiscoveryDownloader::Stop() const
{
    if (m_lpfnStop)
    {
        m_lpfnStop();
    }
}

void DiscoveryDownloader::SetScopes(const std::vector<std::string>& newScopes) const
{
	if (m_lpfnScopes)
	{
		m_lpfnScopes(newScopes);
	}
}

#ifdef _WINDOWS_DEF
HMODULE DiscoveryDownloader::LoadDll(LPCWSTR lpszDll) const
{
    HMODULE hDll = nullptr;
    if (!(hDll = ::LoadLibrary(lpszDll)))
    {
        LPVOID lpMsgBuf;
        ::FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER |
            FORMAT_MESSAGE_FROM_SYSTEM |
            FORMAT_MESSAGE_IGNORE_INSERTS,
            nullptr,
            GetLastError(),
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
            (LPTSTR)&lpMsgBuf,
            0,
            nullptr
            );

		auto lMsg = (boost::format("Unable to load %1%\t") % StringUtils::getstring(lpszDll)).str();
        ONVIF_LOG_ERROR(m_logger, lMsg);

		::LocalFree(lpMsgBuf);

        return nullptr;
    }

    return hDll;
}

BOOL DiscoveryDownloader::UnloadDll(HMODULE hDll) const
{
    if (hDll)
    {
        if (!::FreeLibrary(hDll))
        {
            LPVOID lpMsgBuf;
            ::FormatMessage(
                FORMAT_MESSAGE_ALLOCATE_BUFFER |
                FORMAT_MESSAGE_FROM_SYSTEM |
                FORMAT_MESSAGE_IGNORE_INSERTS,
                nullptr,
                GetLastError(),
                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                (LPTSTR)&lpMsgBuf,
                0,
                NULL
                );

			auto lMsg = (boost::format("Unable to unload dll")).str();
            ONVIF_LOG_ERROR(m_logger, lMsg);

			::LocalFree(lpMsgBuf);

            return FALSE;
        }

        return TRUE;
    }

    return FALSE;
}
#endif //_WINDOWS_DEF