#pragma once
#include <boost/thread/thread.hpp>
#include <boost/smart_ptr/scoped_ptr.hpp>
#include <boost/asio/ip/tcp.hpp>
#include "ProxyConnection.h"

namespace ITV8 {
    struct ILogger;
}

class ProxyService
{
public:
    ProxyService(const CoreSettingsSP _coreSetts, ITV8::ILogger* logger);
    ~ProxyService();

    void Stop();
    void start_accept();
    void handle_accept(ProxyConnectionSP new_connection, const boost::system::error_code& error);
    void Restart();

private:
    int m_proxyPort;
    CoreSettingsSP _coreSettings;

    boost::asio::io_service m_ioService;
    boost::shared_ptr<boost::asio::ip::tcp::acceptor> acceptor;
    boost::scoped_ptr<boost::thread> thread;
    ITV8::ILogger* m_logger;
    std::string m_description;
};

typedef std::shared_ptr<ProxyService> ProxyServiceSP;
