#pragma once
#include <Interface/IBackgroundTask.h>
#include <Interface/ISubscriptionManager.h>

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <Interface/IMessageSubscription.h>
#include <ServicesImpl/DeviceBindingServiceImpl.h>
#include <Interface/MessageFilter.h>
#include "TaskAsyncQueue.h"

namespace ITV8 {
	struct ILogger;
}

class EventsSubscriptionTask;
typedef std::shared_ptr<EventsSubscriptionTask> EventsSubscriptionTaskSP;

typedef std::deque<MessageHolderTypeSP> EventsSubscriptionSetType;

class EventsSubscriptionTask : public IBackgroundTask, public IMessageSubscription, public std::enable_shared_from_this<EventsSubscriptionTask>
{
public:
    static EventsSubscriptionTaskSP Create(ITV8::ILogger* logger, ISubscriptionManagerSP subscrManager, const time_t& terminationTime, const std::vector<FilterTypeSP>& filter, NotificationConsumerBindingProxySP consumerProxy);
    ~EventsSubscriptionTask();

    void Start(soap* soap) override;
    void Stop() override;

    void Cancel() override;

    EventsSubscriptionSetType PopEvents(int maxResultsNum = -1);
    int GetEventsNum() const;

    void OnReceive(MessageHolderTypeSP message) override;
	time_t GetTerminationTime() const
    {
        return m_terminationTime;
    }

    void SetTerminationTime(const time_t& tss);
    void Wait(LONG64 timeout);
private:
    EventsSubscriptionTask(const std::string& guid, ITV8::ILogger* logger, ISubscriptionManagerSP subscrManager, time_t terminationTime, std::vector<FilterTypeSP> filter, NotificationConsumerBindingProxySP consumerProxy);

	void doInterrupt(SearchResult reason) override;
    void consumer_thread_run();

    ISubscriptionManagerWP m_subscrManager;

    EventsSubscriptionSetType m_vResultEvents;

	time_t m_terminationTime;
	ITV8::ILogger* m_logger;
    
	mutable boost::mutex m_Lock;

    mutable boost::mutex m_waitmutex;
    mutable boost::condition_variable m_waitcondition;
    bool m_canceled;
	NotificationConsumerBindingProxySP m_consumerProxy;
    ONVIF::TaskAsyncQueue<MessageHolderTypeSP> m_consumerQueue;
    boost::thread m_consumer_thread;
    bool m_consumer_running;
    boost::mutex m_consumer_thread_lock;
    boost::condition_variable m_consumer_condition;

	EventsSubscriptionTaskSP _this;
};

