#pragma once
#include <boost/smart_ptr/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/basic_streambuf_fwd.hpp>
#include <deque>
#include <unordered_map>
#include <boost/lexical_cast.hpp>
#include "Configuration/CoreSettings.h"

namespace ITV8 {
    struct ILogger;
}

class ProxyConnection;
typedef boost::shared_ptr<ProxyConnection> ProxyConnectionSP;

typedef std::deque< std::vector<char> > message_queue_t;
typedef std::shared_ptr<boost::asio::ip::tcp::socket> SocketSp;

struct ConnectionParams
{
    ConnectionParams(const std::string& ip, const int& port) : _port(port), _ip(ip)
    {
        if(_ip.empty() || ip == "ANY")
        {
            _ip = "0.0.0.0";
        }

        const auto ip_addr(boost::asio::ip::address::from_string(_ip));
        _endpoint = boost::asio::ip::tcp::endpoint(ip_addr, _port);
    }

    ConnectionParams(const std::string& ip, const std::string& port) : _port(0), _ip(ip)
    {
        try
        {
            _port = boost::lexical_cast<int>(port);
        }
        catch(...)
        { }

        if (_ip.empty() || _ip == "ANY")
        {
            _ip = "0.0.0.0";
        }

        const auto ip_addr(boost::asio::ip::address::from_string(_ip));
        _endpoint = boost::asio::ip::tcp::endpoint(ip_addr, _port);
    }
    
    int _port;
    std::string _ip;
    boost::asio::ip::tcp::endpoint _endpoint;

    SocketSp _socket;
    message_queue_t _write_msgs;
    boost::asio::streambuf readbuf;
};

typedef std::shared_ptr<ConnectionParams> ConnectionParamsSP;

struct ConnectionPair
{
    ConnectionPair(ConnectionParamsSP src, ConnectionParamsSP dst) : _source(src), _dest(dst)
    {
        _dispatch = _dest == nullptr;
    }
    
    ConnectionParamsSP _source;
    ConnectionParamsSP _dest;
    bool _dispatch;
};
typedef std::shared_ptr<ConnectionPair> ConnectionPairSP;

class ProxyConnection
    : public boost::enable_shared_from_this<ProxyConnection>
{
public:

    static ProxyConnectionSP create(boost::asio::io_service& io_service, CoreSettingsSP _coreSetts, ITV8::ILogger* logger)
    {
        return ProxyConnectionSP(new ProxyConnection(io_service, _coreSetts, logger));
    }

    ~ProxyConnection();

    boost::asio::ip::tcp::socket& socket()
    {
        return *(m_client_connection->_socket.get());
    }

    void write(const std::string& data);

    void start();

private:
    ProxyConnection(boost::asio::io_service& io_service, CoreSettingsSP _coreSetts, ITV8::ILogger* logger);
    std::unordered_map<std::string, std::string> parseHeaders(const std::string& h);
    void startProxy(ConnectionPairSP _connections);
    void start_read_proxy(const boost::system::error_code& error, ConnectionPairSP _connections);
    void read_next_magic_bytes(ConnectionPairSP _connection);
    void start_read_data();

    void closeConnection(ConnectionParamsSP _connection);

    void read_http_packet(ConnectionPairSP _connections);
    void handle_read_magic_bytes(const boost::system::error_code& error, ConnectionPairSP _connections);

    void handle_read_http_header(const boost::system::error_code& error, size_t bytes_transferred, ConnectionPairSP _connections);
    void handle_read_http_body(const boost::system::error_code& error, size_t bytes_transferred, size_t body_length, ConnectionPairSP _connections);

    void closeAllConnections(const std::string& lMsg);
    void handle_connect(const boost::system::error_code& error, ConnectionPairSP _connections);
    void handle_write(const boost::system::error_code& error, size_t bytes_transferred, ConnectionParamsSP _connection);
    void write_packet(const std::vector<char> &packet, ConnectionPairSP _connection);
    void schedule_write_packet(ConnectionParamsSP _connection);

    ConnectionParamsSP m_rtsp_connection;
    ConnectionParamsSP m_onvif_connection;
    ConnectionParamsSP m_client_connection;

	boost::asio::streambuf::mutable_buffers_type mutableBuffer;
    ITV8::ILogger* m_logger;
    std::string m_description;
    bool _is_closing;
};
