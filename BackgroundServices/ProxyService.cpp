#include "ProxyService.h"
#include <Util/Log.h>
#include <ItvSdk/include/IErrorService.h>

namespace asio = boost::asio;
using asio::ip::tcp;

ProxyService::ProxyService(const CoreSettingsSP coreSetts, ITV8::ILogger* logger) : m_proxyPort(coreSetts->GetPort()), _coreSettings(coreSetts), m_logger(logger)
{
    m_description = "[ProxyService]";

    auto ip = coreSetts->GetModuleIpAdress();
    if (ip.empty() || ip == "ANY")
    {
        ip = "0.0.0.0";
    }

    const auto ip_addr(asio::ip::address::from_string(ip));
    const auto endpoint = tcp::endpoint(ip_addr, m_proxyPort);
    
    try
    {
        acceptor.reset(new tcp::acceptor(m_ioService, endpoint));
    }
    catch(const std::exception& exp)
    {
        auto lMsg = (boost::format("%1% acceptor failed with exception: %2%") % m_description % exp.what()).str();
        ONVIF_LOG_BASE(m_logger, ITV8::LOG_ERROR, lMsg.c_str());
    }
}

ProxyService::~ProxyService()
{
    Stop();
}

void ProxyService::Stop()
{
    if (!thread) return;
    m_ioService.stop();
    thread->join();
    m_ioService.reset();
    thread.reset();
}

void ProxyService::start_accept()
{
    if(!acceptor)
    {
        return;
    }

    auto new_connection =
        ProxyConnection::create(acceptor->get_io_service(), _coreSettings, m_logger);

    // Asynchronously wait to accept a new client
    //
    acceptor->async_accept(new_connection->socket(),
        boost::bind(&ProxyService::handle_accept, this, new_connection,
            boost::asio::placeholders::error));
}

void ProxyService::handle_accept(ProxyConnectionSP new_connection, const boost::system::error_code& error)
{
    if (!error) {
        new_connection->start();
    }

    start_accept();
}

void ProxyService::Restart()
{
    Stop();

    start_accept();

    thread.reset(new boost::thread(
        boost::bind(&asio::io_service::run, &m_ioService)
        ));
}
