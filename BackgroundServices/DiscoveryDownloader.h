#pragma once

#ifdef _WINDOWS_DEF
#include <windows.h>
#endif //_WINDOWS_DEF

#ifdef DEBUG
#define DISCOVERY_DLL_NAME "OnvifServer.Discovery-D.dll"
#else
#define DISCOVERY_DLL_NAME "OnvifServer.Discovery.dll"
#endif

#include <vector>
#include <ItvSdk/include/IErrorService.h>
#include <memory>

typedef void( * InitDiscoveryFunc)(std::string, std::string, std::string, std::string, std::vector<std::string>);
typedef void( * StopDiscoveryFunc)(void);
typedef void( * SetDiscoveryScopesFunc)(const std::vector<std::string>&);

class DiscoveryDownloader
{
public:
    DiscoveryDownloader(ITV8::ILogger* logger, const std::string& ip, const std::string& nat_ip, const std::string& port, const std::string& uuid);
    virtual ~DiscoveryDownloader();

    void Start(const std::vector<std::string>& newScopes) const;
    void Stop() const;
	void SetScopes(const std::vector<std::string>& newScopes) const;

private:
    std::string m_ip;
    std::string m_nat_ip;
    std::string m_port;
    std::string m_uuid;
	ITV8::ILogger* m_logger;

    void Init();

#ifdef _WINDOWS_DEF
	HMODULE LoadDll(LPCWSTR lpszDll) const;
	BOOL UnloadDll(HMODULE hDll) const;

    HMODULE m_hDll;
#endif //_WINDOWS_DEF

    InitDiscoveryFunc m_lpfnStart;
    StopDiscoveryFunc m_lpfnStop;
	SetDiscoveryScopesFunc m_lpfnScopes;
};

typedef std::weak_ptr<DiscoveryDownloader> DiscoveryDownloaderWPtr;
typedef  std::shared_ptr<DiscoveryDownloader> DiscoveryDownloaderPtr;



