#include "TaskCommander.h"

TaskCommander::TaskCommander()
{
}

TaskCommander::~TaskCommander()
{
}

TaskResult TaskCommander::AddTask(IBackgroundTaskSP newTask, soap* soap)
{
	TaskResult result;
	
	if(newTask)
    {
        newTask->Start(soap);
        result.GUID = newTask->GetGUID();
        result.ErrorMessage.clear();

	    boost::mutex::scoped_lock guard(m_Lock);
	    m_searchTasksMap[newTask->GetGUID()] = newTask;
        
	    return result;
    }

    result.GUID.clear();
    result.ErrorMessage = "Error creating task";

	return result;
}

IBackgroundTaskSP TaskCommander::GetTask(const std::string& taskGuid)
{
    boost::mutex::scoped_lock guard(m_Lock);

    auto task = m_searchTasksMap.find(taskGuid);
    return task != m_searchTasksMap.end() ? task->second : IBackgroundTaskSP();
}

void TaskCommander::RemoveTask(const std::string& taskGuid)
{
    boost::mutex::scoped_lock guard(m_Lock);

    const auto tsk = m_searchTasksMap.find(taskGuid);

    if(tsk == m_searchTasksMap.end())
    {
        return;
    }

    tsk->second->Stop();
    m_searchTasksMap.erase(tsk);
}
