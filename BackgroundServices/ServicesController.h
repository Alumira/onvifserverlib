#pragma once
#include <ServicesImpl/DeviceBindingServiceImpl.h>
#include <ServicesImpl/Media2BindingServiceImpl.h>
#include <BackgroundServices/HttpService.h>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/thread.hpp>
#include "ProxyService.h"

typedef  std::unique_ptr<onvif_generated_::DeviceBindingServiceImpl> DeviceBindingServicePtr;
typedef  std::unique_ptr<onvif_generated_::Media2BindingServiceImpl> Media2BindingServicePtr;

class ServicesController
{
public:
	explicit ServicesController()
		: m_stop(false)
	{
	}

	~ServicesController();
    void CreateServices();
	void do_work(SoapWrapperSP soapObj, std::vector<onvif_generated_::IServiceImplSP> services);
    void start(onvif_generated_::IntellectDataSP data, TaskCommanderSP taskCommander);
    void stop();
	void runDiscovery(onvif_generated_::IntellectDataSP data);
	void run(onvif_generated_::IntellectDataSP data, TaskCommanderSP taskCommander);
	void RestartDiscovery(onvif_generated_::IntellectDataSP data, TaskCommanderSP taskCommander);
private:
	boost::mutex mutex;
	boost::condition_variable cond;
    
	boost::mutex discoverymutex;
	DiscoveryDownloaderPtr m_discovery;
    boost::thread m_mainThread;
	boost::thread m_discoveryThread;
    mutable bool m_stop;
    HttpServiceSP m_httpService;
    ProxyServiceSP m_proxyservice;
};
