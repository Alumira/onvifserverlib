#pragma once
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <deque>

#ifndef _WINDOWS_DEF
typedef unsigned int    UINT;
#define INFINITE        0xFFFFFFFF
#endif

namespace ONVIF {

    namespace Detail {

        template <class T> class TaskAsyncQueueSizeLimiter
        {
        public:
            virtual ~TaskAsyncQueueSizeLimiter() {};
            virtual void onPush(T& item)     = 0;
            virtual void onPop(T& item)      = 0;
            virtual void clear()             = 0;
            virtual bool isOverflow() const  = 0;
        };

        template <class T> class TaskAsyncQueueSizeLimiterDefault : public TaskAsyncQueueSizeLimiter<T>
        {
        public:
            TaskAsyncQueueSizeLimiterDefault(size_t maxSize_) : m_size(0), m_maxSize(maxSize_) {}

            void onPush(T& item)     override { ++m_size;   }
            void onPop(T& item)      override { --m_size;   }
            void clear()             override { m_size = 0; }
            bool isOverflow() const  override { return m_size >= m_maxSize; }

            void setMaxSize(size_t maxSize)   { m_maxSize = maxSize; }

        private:
            size_t m_size;
            size_t m_maxSize;
        };
    } // namespace Detail

    template <class T> class TaskAsyncQueue
    {
    public:
       
        typedef Detail::TaskAsyncQueueSizeLimiter<T> SizeLimiter;


        explicit TaskAsyncQueue(size_t maxSize = 10) 
            : m_complete(false)
            , m_sizeLimiter(new Detail::TaskAsyncQueueSizeLimiterDefault<T>(maxSize))
        {

        }

        explicit TaskAsyncQueue(SizeLimiter* sizeLimiter) 
            : m_complete(false)
            , m_sizeLimiter(sizeLimiter)
        {

        }

        ~TaskAsyncQueue()
        {
            boost::mutex::scoped_lock l(m_mutex);
            m_queue.clear();
        }

        void setMaxSize(size_t maxSize)
        {
            boost::mutex::scoped_lock l(m_mutex);
            if (auto p = dynamic_cast<Detail::TaskAsyncQueueSizeLimiterDefault<T>*>(m_sizeLimiter.get()))
            {
                p->setMaxSize(maxSize);
            }
        }

        bool pushReady() const
        {
            boost::mutex::scoped_lock l(m_mutex);
            return !m_sizeLimiter->isOverflow();
        }

        bool push(T val, UINT timeoutMs = INFINITE)
        {
            boost::mutex::scoped_lock l(m_mutex);
            if (timeoutMs == INFINITE)
            {
                m_conditionPush.wait(l, [this] () { return !m_sizeLimiter->isOverflow() || m_complete; });
            }
            else if (timeoutMs > 0)
            {
                m_conditionPush.timed_wait(l, boost::posix_time::milliseconds(timeoutMs), [this] () { return !m_sizeLimiter->isOverflow() || m_complete; });
            }

            if (m_sizeLimiter->isOverflow() || m_complete)
            {
                return false;
            }
            m_sizeLimiter->onPush(val);
            m_queue.push_back(val);
            m_conditionPop.notify_one();
            return true;
        }

        bool popReady() const
        {
            boost::mutex::scoped_lock l(m_mutex);
            return !m_queue.empty();
        }

        bool pop(T& val, UINT timeoutMs = INFINITE)
        {
            boost::mutex::scoped_lock l(m_mutex);
            if (timeoutMs == INFINITE)
            {
                m_conditionPop.wait(l, [this] () { return !m_queue.empty() || m_complete; });
            }
            else if (timeoutMs > 0)
            {
                m_conditionPop.timed_wait(l, boost::posix_time::milliseconds(timeoutMs), [this] () { return !m_queue.empty() || m_complete; });
            }

            if (m_queue.empty())
            {
                return false;
            }
            val = m_queue.front();
            m_queue.pop_front();
            m_sizeLimiter->onPop(val);
            m_conditionPush.notify_one();
            return true;
        }

        bool get(T& val, UINT timeoutMs = INFINITE) const
        {
            boost::mutex::scoped_lock l(m_mutex);
            if (timeoutMs == INFINITE)
            {
                m_conditionPop.wait(l, [this] () { return !m_queue.empty() || m_complete; });
            }
            else if (timeoutMs > 0)
            {
                m_conditionPop.timed_wait(l, boost::posix_time::milliseconds(timeoutMs), [this] () { return !m_queue.empty() || m_complete; });
            }

            if (!m_queue.empty())
            {
                val = m_queue.front();
                return true;
            }
            return false;
        }

        void forEach(const std::function<bool(const T&)>& func) const
        {
            boost::mutex::scoped_lock l(m_mutex);
            for (auto it = m_queue.begin(); it != m_queue.end(); ++it)
            {
                if (!func(*it))
                {
                    break;
                }
            }
        }

        void finish()
        {
            boost::mutex::scoped_lock l(m_mutex);
            m_complete = true;
            m_conditionPop.notify_all();
            m_conditionPush.notify_all();
        }

        bool isComplete() const
        {
            boost::mutex::scoped_lock l(m_mutex);
            return m_complete;
        }

        bool isFinished() const
        {
            boost::mutex::scoped_lock l(m_mutex);
            return m_complete && m_queue.empty();
        }

        bool isOverflow() const
        {
            boost::mutex::scoped_lock l(m_mutex);
            return m_sizeLimiter->isOverflow();
        }

        void resetCompleteFlag()
        {
            boost::mutex::scoped_lock l(m_mutex);
            m_complete = false;
        }

        void reset()
        {
            boost::mutex::scoped_lock l(m_mutex);
            m_queue.clear();
            m_complete = false;
            m_sizeLimiter->clear();
            m_conditionPop.notify_all();
            m_conditionPush.notify_all();
        }

        size_t size() const
        {
            boost::mutex::scoped_lock l(m_mutex);
            return m_queue.size();
        }

        void swap(TaskAsyncQueue<T>& that)
        {
            TaskAsyncQueue<T>* that_1 = max(this, &that);
            TaskAsyncQueue<T>* that_2 = min(this, &that);

            boost::mutex::scoped_lock l(that_1->m_mutex);
            {
                boost::mutex::scoped_lock l(that_2->m_mutex);
                m_queue.swap(that.m_queue);
                std::swap(m_complete, that.m_complete);
                m_sizeLimiter.swap(that.m_sizeLimiter);
                that_2->m_conditionPop.notify_all();
                that_2->m_conditionPush.notify_all();
            }
            that_1->m_conditionPop.notify_all();
            that_1->m_conditionPush.notify_all();
        }

        void checkSizeLimiter(std::function<void(const SizeLimiter*)> func) const
        {
            boost::mutex::scoped_lock l(m_mutex);
            func(m_sizeLimiter.get());
        }

        void updateSizeLimiter(std::function<void(SizeLimiter*)> func)
        {
            boost::mutex::scoped_lock l(m_mutex);
            func(m_sizeLimiter.get());
        }

    private:
        mutable boost::mutex m_mutex;
        mutable boost::condition_variable m_conditionPush;
        mutable boost::condition_variable m_conditionPop;
        std::deque<T> m_queue;
        bool m_complete;
        std::unique_ptr<SizeLimiter> m_sizeLimiter;
    };

} //namespace ONVIF