#pragma once
#include <Interface/IBackgroundTask.h>
#include <map>

struct TaskResult
{
    TaskResult(){}
    TaskResult(std::string _GUID, std::string _ErrorMessage) : GUID(_GUID), ErrorMessage(_ErrorMessage) {}
            
    std::string GUID;
    std::string ErrorMessage;
};

class TaskCommander
{
public:
    TaskCommander();
    ~TaskCommander();

	TaskResult AddTask(IBackgroundTaskSP newTask, soap* soap);
    IBackgroundTaskSP GetTask(const std::string& taskGuid);
    void RemoveTask(const std::string& taskGuid);

private:
    mutable boost::mutex m_Lock;
    std::map<std::string, IBackgroundTaskSP> m_searchTasksMap;
};

typedef std::shared_ptr<TaskCommander> TaskCommanderSP;
