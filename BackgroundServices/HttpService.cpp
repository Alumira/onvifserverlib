#include "HttpService.h"

int HttpService::getSnapshot(soap* soap, onvif_generated_::IntellectData* intellect_data, const HttpQuery& query)
{
    //TODO: check authentication problem (MEDIA-6-1-1-v14.12 SNAPSHOT URI)
    /*if (auto auth_result = intellect_data->GetSecurity()->CheckAuthentication(soap, ISecurityServer::READ_MEDIA))
    {
        return auth_result;
    }*/

    auto cam = query.GeFieldValue("cam.id");
    auto stream = query.GeFieldValue("stream.id");
    if (cam.empty() || stream.empty())
    {
		assert(false);
    	return 404;
    }

	auto res = intellect_data->processorFactory->createGetSnapshotProcessor(cam, stream, soap)->DoWork();
    return res;
}

int HttpService::http_get(struct soap *soap)
{
    onvif_generated_::IntellectData* serverData = static_cast<onvif_generated_::IntellectData*>(soap->user);
    if(!serverData)
    {
        return SOAP_ERR;
    }

    auto query = HttpQuery(soap->path);

    if(query.CheckCommand("image.jpg"))
    {
        return getSnapshot(soap, serverData, query);
    }
    
	auto lMsg = (boost::format("[HttpService] request is not supported %1%") % soap->path).str();
	ONVIF_LOG_BASE(serverData->m_logger, ITV8::LOG_ERROR, lMsg.c_str());

	return 404;
}
