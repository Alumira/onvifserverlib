#pragma once
#include <Interface/IServiceImpl.h>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>

class HttpQuery
{
public:
    HttpQuery(std::string uri) : m_Uri(uri)
    {
        parseUri();
        parseQueryFields();
    }

    std::string GeFieldValue(std::string name) const
    {
        boost::to_lower(name);
        
        auto it = m_queryFields.find(name);
        if (it == m_queryFields.end())
        {
            return "";
        }
        return it->second.c_str();
    }

    bool CheckCommand(const std::string& cmd) const
    {
        return m_Uri.find(cmd) != std::string::npos;
    }

    std::string GetSubUri() const
    {
        if(m_sub_uris.size())
        {
            return m_sub_uris.back();
        }
        return "";
    }

private:
    std::map<std::string, std::string> m_queryFields;
    const std::string m_Uri;
    std::vector<std::string> m_sub_uris;

    void parseUri()
    {
        try
        {
            m_sub_uris.clear();
            boost::split(m_sub_uris, m_Uri, boost::is_any_of("/\\"), boost::token_compress_on);
        }
        catch(...)
        {
        }
    }

    void parseQueryFields()
    {
        boost::smatch sResults;
        std::string::const_iterator it = m_Uri.begin();
        while (boost::regex_search(it, m_Uri.end(), sResults,
            boost::regex("[\\?|&]([\\w|\\.]+?)=([^&]+)")))
        {
            std::string key = sResults[1].str();
            std::string value = sResults[2].str();
            boost::algorithm::to_lower(key);
            m_queryFields[key] = value;
            it = sResults[2].second;
        }
    }
};

class HttpService
{
public:
    HttpService() {};
    static int getSnapshot(soap* soap, onvif_generated_::IntellectData* intellect_data, const HttpQuery& query);
    static int http_get(struct soap *soap);
};

typedef std::shared_ptr<HttpService> HttpServiceSP;
