#include "ProxyConnection.h"
#include <boost/bind.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/lexical_cast.hpp>
#include <memory>
#include <Util/Log.h>
#include <ItvSdk/include/IErrorService.h>

ProxyConnection::ProxyConnection(boost::asio::io_service& io_service, CoreSettingsSP _coreSetts, ITV8::ILogger* logger): mutableBuffer(nullptr, 0), _is_closing(false)
{
    auto rtsp_ip = _coreSetts->GetRtspInfo()->GetIp();
    auto onvif_ip = _coreSetts->GetModuleIpAdress();

    m_client_connection = std::make_shared<ConnectionParams>(onvif_ip, _coreSetts->GetPort());
    m_client_connection->_socket = std::make_shared<boost::asio::ip::tcp::socket>(io_service);

    m_rtsp_connection = std::make_shared<ConnectionParams>(rtsp_ip, _coreSetts->GetRtspInfo()->GetRtspPort());
    m_rtsp_connection->_socket = std::make_shared<boost::asio::ip::tcp::socket>(io_service);

    m_onvif_connection = std::make_shared<ConnectionParams>(onvif_ip, _coreSetts->GetInternalPort());
    m_onvif_connection->_socket = std::make_shared<boost::asio::ip::tcp::socket>(io_service);

    m_logger = logger;

    m_description = (boost::format("[ProxyConnection][%1%]") % this).str();

    auto lMsg = (boost::format("%1% created new connection with onvif ip = %2%:%3%, rtsp ip = %4%:%5%") % 
        m_description % onvif_ip % _coreSetts->GetPort() % rtsp_ip % _coreSetts->GetRtspInfo()->GetRtspPort()).str();

    ONVIF_LOG_BASE(m_logger, ITV8::LOG_INFO, lMsg.c_str());
}

ProxyConnection::~ProxyConnection()
{
    closeAllConnections("");
}

void ProxyConnection::closeConnection(ConnectionParamsSP _connection)
{
    if (!_connection || !_connection->_socket->is_open()) return;

    try
    {
        boost::system::error_code ec;
        _connection->_socket->shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
        _connection->_socket->close(ec);
    }
    catch (std::exception& exp)
    {
        auto lMsg = (boost::format("%1% closeConnection failed with error: %2%") % m_description % exp.what()).str();
        ONVIF_LOG_BASE(m_logger, ITV8::LOG_ERROR, lMsg.c_str());
    }
}

std::string buffer_to_string(const boost::asio::streambuf &buffer, std::size_t n)
{
    using boost::asio::buffers_begin;

    auto bufs = buffer.data();
    std::string result(buffers_begin(bufs), buffers_begin(bufs) + n);
    return result;
}

void ProxyConnection::start()
{
    read_next_magic_bytes(std::make_shared<ConnectionPair>(m_client_connection, nullptr));
}

void ProxyConnection::startProxy(ConnectionPairSP _connections)
{
    auto source = _connections->_source;
    source->_socket->async_connect(source->_endpoint,
        boost::bind(&ProxyConnection::handle_connect, shared_from_this(),
            boost::asio::placeholders::error,
            _connections));
}

void ProxyConnection::start_read_proxy(const boost::system::error_code& error, ConnectionPairSP _connections)
{
    read_next_magic_bytes(_connections);
}

void ProxyConnection::read_next_magic_bytes(ConnectionPairSP _connections)
{
    auto source = _connections->_source;

    if (source->readbuf.size() < 4) {
        async_read(*(source->_socket.get()), source->readbuf, boost::asio::transfer_at_least(4),
            boost::bind(&ProxyConnection::handle_read_magic_bytes, shared_from_this(),
                boost::asio::placeholders::error, _connections));

        return;
    }

    handle_read_magic_bytes(boost::system::error_code(), _connections);
}

void ProxyConnection::handle_read_magic_bytes(const boost::system::error_code& error, ConnectionPairSP _connections)
{
    auto source = _connections->_source;

    if (error)
    {
        auto lMsg = (boost::format("%1% handle read failed with error: %2%") % m_description % error.message().c_str()).str();
		closeAllConnections("");
        return;
    }

    BOOST_ASSERT(source->readbuf.size() >= 4);

    std::vector<char> magic(4);
    buffer_copy(boost::asio::buffer(magic), source->readbuf.data());

    if (magic[0] == 0x24)
    {
        // RTCP
        if(_connections->_dispatch)
        {
            _connections->_dest = m_rtsp_connection;
        }

        uint16_t length = /*ntohl*/(uint16_t(magic[2]) << 8 | (uint16_t(magic[3]) & 0xFF));

        handle_read_http_body(error, 0, length + 4, _connections);
        return;
    }

    read_http_packet(_connections);
}

void ProxyConnection::closeAllConnections(const std::string &lMsg)
{
    if (_is_closing) return;

    if (!lMsg.empty())
        ONVIF_LOG_BASE(m_logger, ITV8::LOG_ERROR, lMsg.c_str());

    _is_closing = true;

    closeConnection(m_client_connection);
    closeConnection(m_rtsp_connection);
    closeConnection(m_onvif_connection);
}

void ProxyConnection::write_packet(const std::vector<char> &packet, ConnectionPairSP _connections)
{
	auto dest = _connections->_dest;
    const auto write_in_progress = !dest->_write_msgs.empty();
	dest->_write_msgs.push_back(packet);

	// initiate read-back loop
	if (_connections->_dispatch)
	{
        if (!dest->_socket->is_open())
        {
            startProxy(std::make_shared<ConnectionPair>(_connections->_dest, _connections->_source));
            return;
        }
	}

    if (!write_in_progress)
    {
        schedule_write_packet(dest);
    }
}

void ProxyConnection::handle_connect(const boost::system::error_code& error, ConnectionPairSP _connections)
{
    if (error)
    {
        auto lMsg = (boost::format("%1% handle_connect failed with error: %2%") % m_description % error.message().c_str()).str();
        closeAllConnections(lMsg);
        return;
    }

    start_read_proxy(error, _connections);
    schedule_write_packet(_connections->_source);
}

void ProxyConnection::schedule_write_packet(ConnectionParamsSP _connection)
{
    if (!_connection->_write_msgs.empty())
    {
        boost::asio::async_write(*(_connection->_socket.get()),
            boost::asio::buffer(_connection->_write_msgs.front()),
            boost::bind(&ProxyConnection::handle_write, shared_from_this(),
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred,
                _connection));
    }
}

void ProxyConnection::handle_write(const boost::system::error_code& error,
    size_t bytes_transferred, ConnectionParamsSP _connection)
{
    if (error)
    {
        auto lMsg = (boost::format("%1% handle_write failed with error: %2%") % m_description % error.message().c_str()).str();
        closeAllConnections(lMsg);
        return;
    }

    _connection->_write_msgs.pop_front();
    schedule_write_packet(_connection);
}

void ProxyConnection::read_http_packet(ConnectionPairSP _connections)
{
    auto source = _connections->_source;
    
    using boost::asio::buffers_begin;
    auto bufs = source->readbuf.data();
    const auto http_delimeter = std::string("\r\n\r\n");
    const auto end_of_input = buffers_begin(bufs) + source->readbuf.size();
    const auto search_result = std::search(buffers_begin(bufs), end_of_input, http_delimeter.begin(), http_delimeter.end());
    if (end_of_input == search_result)
    {
        async_read_until(*(source->_socket.get()), source->readbuf, "\r\n\r\n",
            boost::bind(&ProxyConnection::handle_read_http_header, shared_from_this(),
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred, _connections));

        return;
    }

    handle_read_http_header(boost::system::error_code(), std::distance(buffers_begin(bufs), search_result + 4), _connections);
}

void ProxyConnection::handle_read_http_header(const boost::system::error_code& error, size_t headers_bytes, ConnectionPairSP _connections)
{
    auto source = _connections->_source;

    if (error)
    {
        auto lMsg = (boost::format("%1% handle_read_http_header failed with error: %2%") % m_description % error.message().c_str()).str();
        closeAllConnections(lMsg);
        return;
    }

    std::string headers_str = buffer_to_string(source->readbuf, headers_bytes);

	if (_connections->_dispatch)
	{
		const std::string rtsp_substr = "/rtspproxy";
		const auto rtsp_substr_pos = headers_str.find(rtsp_substr);
		if (rtsp_substr_pos != std::string::npos)
		{
			_connections->_dest = m_rtsp_connection;
		}
		else
		{
			_connections->_dest = m_onvif_connection;
		}
	}

    {
        std::vector<char> packet(headers_bytes);
        std::size_t bytes_copied = buffer_copy(
            boost::asio::buffer(packet), // target's output sequence
            source->readbuf.data());
        source->readbuf.consume(bytes_copied); // Remove data that was read.
        write_packet(packet, _connections);
    }

    auto headers = parseHeaders(headers_str);
    auto pair_it = headers.find("Content-Length");
    if (pair_it != headers.end())
    {
        const auto body_length = boost::lexical_cast<int>(pair_it->second);
        handle_read_http_body(error, 0, body_length, _connections);
        return;
    }

    read_next_magic_bytes(_connections);
}

void ProxyConnection::handle_read_http_body(const boost::system::error_code& error,
    size_t bytes_transferred,
    size_t body_length, 
    ConnectionPairSP _connections)
{
    auto source = _connections->_source;
    source->readbuf.commit(bytes_transferred);

    if (error)
    {
        auto lMsg = (boost::format("%1% handle_read_http_body failed with error: %2%") % m_description % error.message().c_str()).str();
        closeAllConnections(lMsg);
        return;
    }

    const auto to_write = std::min(source->readbuf.size(), body_length);
    if (to_write > 0) {
        std::vector<char> packet(to_write);
        const std::size_t bytes_copied = buffer_copy(
            boost::asio::buffer(packet), // target's output sequence
            source->readbuf.data());

        source->readbuf.consume(bytes_copied); // Remove data that was read.
        write_packet(packet, _connections);
    }

    const auto to_read = body_length - to_write;
    if (to_read > 0) {
        mutableBuffer = source->readbuf.prepare(1024);
        source->_socket->async_read_some(mutableBuffer,
            boost::bind(&ProxyConnection::handle_read_http_body, shared_from_this(),
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred,
                to_read, _connections));
        return;
    }

    read_next_magic_bytes(_connections);
}

std::unordered_map<std::string, std::string> ProxyConnection::parseHeaders(const std::string &str)
{
    typedef boost::split_iterator<std::string::const_iterator> string_split_iterator;

    std::unordered_map<std::string, std::string> hm;
    for (string_split_iterator it =
        make_split_iterator(str, first_finder("\r\n", boost::is_equal()));
        it != string_split_iterator();
        ++it)
    {
        std::vector<std::string> split_vector(2);
        boost::split(split_vector, *it, boost::is_any_of(":"), boost::token_compress_on);
        if (split_vector.size() != 2) continue;
        boost::trim(split_vector[0]);
        boost::trim(split_vector[1]);
        hm.insert(std::make_pair(split_vector[0], split_vector[1]));
    }

    return hm;
}