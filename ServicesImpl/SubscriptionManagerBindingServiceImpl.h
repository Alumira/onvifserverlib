#pragma once

#include "onvif_generated_SubscriptionManagerBindingService.h"
#include <Interface/IServiceImpl.h>
#include <BackgroundServices/TaskCommander.h>

namespace onvif_generated_ {
    class SubscriptionManagerBindingServiceImpl : public SubscriptionManagerBindingService, public IServiceImpl
    {
    public:
        SubscriptionManagerBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander);
        SubscriptionManagerBindingService* copy() override { return new SubscriptionManagerBindingServiceImpl(*this); }

        int Renew(_wsnt__Renew* wsnt__Renew, _wsnt__RenewResponse& wsnt__RenewResponse) override;
        int Unsubscribe(_wsnt__Unsubscribe* wsnt__Unsubscribe, _wsnt__UnsubscribeResponse& wsnt__UnsubscribeResponse) override;

        //IServiceImpl
        int Dispatch(struct soap* soapPtr) override
        {
            return SubscriptionManagerBindingService::dispatch(soapPtr);
        }

    private:
        TaskCommanderSP m_taskCommander;
    };

}
