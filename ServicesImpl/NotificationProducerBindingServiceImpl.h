#pragma once
#include "onvif_generated_NotificationProducerBindingService.h"
#include <Interface/IServiceImpl.h>
#include <BackgroundServices/TaskCommander.h>

namespace onvif_generated_ {
    class NotificationProducerBindingServiceImpl : public NotificationProducerBindingService, public IServiceImpl
    {
    public:
        NotificationProducerBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander);
        NotificationProducerBindingService* copy() override { return new NotificationProducerBindingServiceImpl(*this); }

        int Subscribe(_wsnt__Subscribe* wsnt__Subscribe, _wsnt__SubscribeResponse& wsnt__SubscribeResponse) override;
        int GetCurrentMessage(_wsnt__GetCurrentMessage* wsnt__GetCurrentMessage, _wsnt__GetCurrentMessageResponse& wsnt__GetCurrentMessageResponse) override;

        //IServiceImpl
        int Dispatch(struct soap* soapPtr) override
        {
            return NotificationProducerBindingService::dispatch(soapPtr);
        }

    private:
        TaskCommanderSP m_taskCommander;
    };

}
