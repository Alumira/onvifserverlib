#include "MediaBindingServiceImpl.h"
#include <boost/foreach.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include "SoapProxy/EventsBuildUtil.h"

namespace onvif_generated_ {
	MediaBindingServiceImpl::MediaBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander)
            : MediaBindingService(soapObj->GetSoap())
			, IServiceImpl(data)
            , m_taskCommander(taskCommander)
	{
		
	}

    int MediaBindingServiceImpl::GetServiceCapabilities(_trt__GetServiceCapabilities* trt__GetServiceCapabilities, _trt__GetServiceCapabilitiesResponse& trt__GetServiceCapabilitiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }

        trt__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities(this->soap, GetCoreSettings());
        
        return SOAP_OK;
    }

	int MediaBindingServiceImpl::GetVideoSources(_trt__GetVideoSources* trt__GetVideoSources, _trt__GetVideoSourcesResponse& trt__GetVideoSourcesResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        BOOST_FOREACH(auto src,GetConfiguration()->GetVSourceConfigurations())
        {
            updateEncoderParams(src.second);
        }
	    
        ReadLock_t l(GetConfiguration()->getLock());
        BOOST_FOREACH(auto vSource, GetConfiguration()->GetVSourceConfigurations())
        {
            if (checkVideoSourceCodec(vSource.second))
            {
                tt__VideoSource* source = GetProxy()->createVideoSource(soap, vSource.second->videoSource, vSource.second);
                trt__GetVideoSourcesResponse.VideoSources.push_back(source);
            }
        }

		return SOAP_OK;
	}

    int MediaBindingServiceImpl::GetAudioSources(_trt__GetAudioSources* trt__GetAudioSources, _trt__GetAudioSourcesResponse& trt__GetAudioSourcesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        {
            ReadLock_t l(GetConfiguration()->getLock());

            BOOST_FOREACH(auto src,GetConfiguration()->GetASources())
            {
                auto aSourceConfig = GetConfiguration()->findAudioSource(src->GetMicId());

                tt__AudioSource* source = GetProxy()->createAudioSource(soap, src, aSourceConfig);
                trt__GetAudioSourcesResponse.AudioSources.push_back(source);
            }
        }

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::GetAudioOutputs(_trt__GetAudioOutputs* trt__GetAudioOutputs, _trt__GetAudioOutputsResponse& trt__GetAudioOutputsResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
	    
        OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:AudioOutputNotSupported", nullptr, true);
        return SOAP_FAULT;
	}

    int MediaBindingServiceImpl::RemoveVideoEncoderConfiguration(_trt__RemoveVideoEncoderConfiguration* trt__RemoveVideoEncoderConfiguration, _trt__RemoveVideoEncoderConfigurationResponse& trt__RemoveVideoEncoderConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt__RemoveVideoEncoderConfiguration->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__RemoveVideoEncoderConfiguration->ProfileToken);
        }

        if(!profile->GetEncoderConfiguration())
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->SetEncoderConfiguration(nullptr, false);
        //newProfile->SetVideoSourceConfiguration(nullptr, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::RemoveVideoSourceConfiguration(_trt__RemoveVideoSourceConfiguration* trt__RemoveVideoSourceConfiguration, _trt__RemoveVideoSourceConfigurationResponse& trt__RemoveVideoSourceConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }
        
        auto profile = GetConfiguration()->FindProfile(trt__RemoveVideoSourceConfiguration->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__RemoveVideoSourceConfiguration->ProfileToken);
        }

        auto vSourceConfig = profile->GetVideoSourceConfiguration();
        if(vSourceConfig == nullptr)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->SetVideoSourceConfiguration(nullptr, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::RemoveAudioEncoderConfiguration(_trt__RemoveAudioEncoderConfiguration* trt__RemoveAudioEncoderConfiguration, _trt__RemoveAudioEncoderConfigurationResponse& trt__RemoveAudioEncoderConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt__RemoveAudioEncoderConfiguration->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__RemoveAudioEncoderConfiguration->ProfileToken);
        }

        if (!profile->GetAudioEncoderConfiguration())
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->SetAudioEncoderConfiguration(nullptr, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::RemoveAudioSourceConfiguration(_trt__RemoveAudioSourceConfiguration* trt__RemoveAudioSourceConfiguration, _trt__RemoveAudioSourceConfigurationResponse& trt__RemoveAudioSourceConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt__RemoveAudioSourceConfiguration->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__RemoveAudioSourceConfiguration->ProfileToken);
        }

        auto aSourceConfig = profile->GetAudioSourceConfiguration();
        if (aSourceConfig == nullptr)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->SetAudioSourceConfiguration(nullptr, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::RemovePTZConfiguration(_trt__RemovePTZConfiguration* trt__RemovePTZConfiguration, _trt__RemovePTZConfigurationResponse& trt__RemovePTZConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt__RemovePTZConfiguration->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__RemovePTZConfiguration->ProfileToken);
        }

        if (!profile->GetPtzConfiguration())
        {
            OnvifUtil::ONVIF_Fault(soap, " ter:ActionNotSupported", "ter:NoConfig");
            return SOAP_FAULT;
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->SetPtzConfiguration(PtzConfigurationSP());

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_OK;
    }

	int MediaBindingServiceImpl::RemoveVideoAnalyticsConfiguration(_trt__RemoveVideoAnalyticsConfiguration* trt__RemoveVideoAnalyticsConfiguration, _trt__RemoveVideoAnalyticsConfigurationResponse& trt__RemoveVideoAnalyticsConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto profile = GetConfiguration()->FindProfile(trt__RemoveVideoAnalyticsConfiguration->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, trt__RemoveVideoAnalyticsConfiguration->ProfileToken);
		}

		MediaProfileSP newProfile(new MediaProfile(*profile.get()));
		newProfile->SetVideoAnalyticsConfiguration(nullptr, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::RemoveMetadataConfiguration(_trt__RemoveMetadataConfiguration* trt__RemoveMetadataConfiguration, _trt__RemoveMetadataConfigurationResponse& trt__RemoveMetadataConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto profile = GetConfiguration()->FindProfile(trt__RemoveMetadataConfiguration->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, trt__RemoveMetadataConfiguration->ProfileToken);
		}

		MediaProfileSP newProfile(new MediaProfile(*profile.get()));
		newProfile->SetMetadataConfiguration(nullptr, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::DeleteProfile(_trt__DeleteProfile* trt__DeleteProfile, _trt__DeleteProfileResponse& trt__DeleteProfileResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt__DeleteProfile->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__DeleteProfile->ProfileToken);
        }

		{
			MediaProfileSP newProfile(new MediaProfile(*profile.get()));
			newProfile->m_isMulticast = false;
			GetProcessorFactory()->createUpdateRtspProcessor(GetCoreSettings(), newProfile)->DoWork();
		}
        
		auto delete_profile = GetProcessorFactory()->createDeleteProfileProcessor(profile);
		delete_profile->DoWork();

        return SOAP_OK;
    }

	int MediaBindingServiceImpl::GetVideoSourceConfigurations(_trt__GetVideoSourceConfigurations* trt__GetVideoSourceConfigurations, _trt__GetVideoSourceConfigurationsResponse& trt__GetVideoSourceConfigurationsResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        BOOST_FOREACH(auto vSource, GetConfiguration()->GetVSourceConfigurations())
        {
            updateEncoderParams(vSource.second);
        }

        ReadLock_t l(GetConfiguration()->getLock());
        BOOST_FOREACH(auto vSource, GetConfiguration()->GetVSourceConfigurations())
        {
            if (checkVideoSourceCodec(vSource.second))
            {
                tt__VideoSourceConfiguration* sourcecfg = GetProxy()->createVideoSourceConfiguration(soap, vSource.second);
                trt__GetVideoSourceConfigurationsResponse.Configurations.push_back(sourcecfg);
            }
        }

		return SOAP_OK;
	}

    int MediaBindingServiceImpl::GetVideoEncoderConfigurations(_trt__GetVideoEncoderConfigurations* trt__GetVideoEncoderConfigurations, _trt__GetVideoEncoderConfigurationsResponse& trt__GetVideoEncoderConfigurationsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        BOOST_FOREACH(auto vSource,GetConfiguration()->GetVSourceConfigurations())
        {
            updateEncoderParams(vSource.second);
        }
        
        ReadLock_t l(GetConfiguration()->getLock());
        BOOST_FOREACH(auto vSource,GetConfiguration()->GetVSourceConfigurations())
        {
            BOOST_FOREACH(auto vEncoder, vSource.second->m_encoders)
            {
                if (vEncoder.second->IsSet() && checkCodec(vEncoder.second->codec))
                {
                    trt__GetVideoEncoderConfigurationsResponse.Configurations.push_back(GetProxy()->createVideoEncoderConfiguration(soap, vEncoder.second));
                }
            }
        }

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::GetAudioSourceConfigurations(_trt__GetAudioSourceConfigurations* trt__GetAudioSourceConfigurations, _trt__GetAudioSourceConfigurationsResponse& trt__GetAudioSourceConfigurationsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());

        BOOST_FOREACH(auto src,GetConfiguration()->GetASourceConfigurations())
        {
            tt__AudioSourceConfiguration* sourcecfg = GetProxy()->createAudioSourceConfiguration(soap, src);
            trt__GetAudioSourceConfigurationsResponse.Configurations.push_back(sourcecfg);
        }

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::GetAudioEncoderConfigurations(_trt__GetAudioEncoderConfigurations* trt__GetAudioEncoderConfigurations, _trt__GetAudioEncoderConfigurationsResponse& trt__GetAudioEncoderConfigurationsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());
        BOOST_FOREACH(auto aSource,GetConfiguration()->GetASourceConfigurations())
        {
            auto aEncoder = aSource->GetAudioEncoderConfig();
            if(aEncoder)
            {
                trt__GetAudioEncoderConfigurationsResponse.Configurations.push_back(GetProxy()->createAudioEncoderConfiguration(soap, aEncoder));
            }
        }

        return SOAP_OK;
    }

	int MediaBindingServiceImpl::GetVideoAnalyticsConfigurations(_trt__GetVideoAnalyticsConfigurations* trt__GetVideoAnalyticsConfigurations, _trt__GetVideoAnalyticsConfigurationsResponse& trt__GetVideoAnalyticsConfigurationsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		ReadLock_t l(GetConfiguration()->getLock());
		BOOST_FOREACH(auto vSourceConfig,GetConfiguration()->GetVSourceConfigurations())
		{
			auto analyticsConfig = vSourceConfig.second->GetVideoAnalyticsConfiguration();
			if (analyticsConfig)
			{
				trt__GetVideoAnalyticsConfigurationsResponse.Configurations.push_back(GetProxy()->createVideoAnalyticsConfiguration(soap, analyticsConfig));
			}
		}

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::GetMetadataConfigurations(_trt__GetMetadataConfigurations* trt__GetMetadataConfigurations, _trt__GetMetadataConfigurationsResponse& trt__GetMetadataConfigurationsResponse)
    {
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		ReadLock_t l(GetConfiguration()->getLock());
		BOOST_FOREACH(auto vSourceConfig,GetConfiguration()->GetVSourceConfigurations())
		{
			auto config = vSourceConfig.second->GetMetadataConfiguration();
			if (config)
			{
				trt__GetMetadataConfigurationsResponse.Configurations.push_back(GetProxy()->createMetadataConfiguration(soap, config));
			}
		}

		return SOAP_OK;
    }

    int MediaBindingServiceImpl::GetVideoSourceConfiguration(_trt__GetVideoSourceConfiguration* trt__GetVideoSourceConfiguration, _trt__GetVideoSourceConfigurationResponse& trt__GetVideoSourceConfigurationResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
	    
	    auto videoSourceCfg = GetConfiguration()->FindVideoSourceConfiguration(trt__GetVideoSourceConfiguration->ConfigurationToken);
	    
		if (!videoSourceCfg)
		{
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		trt__GetVideoSourceConfigurationResponse.Configuration = GetProxy()->createVideoSourceConfiguration(soap, videoSourceCfg);
		return SOAP_OK;
	}

    int MediaBindingServiceImpl::GetVideoEncoderConfiguration(_trt__GetVideoEncoderConfiguration* trt__GetVideoEncoderConfiguration, _trt__GetVideoEncoderConfigurationResponse& trt__GetVideoEncoderConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        EncoderConfigurationSP encConfig;
        
        BOOST_FOREACH(auto vSourceConfig,GetConfiguration()->GetVSourceConfigurations())
        {
            auto cfg = vSourceConfig.second->m_encoders.find(trt__GetVideoEncoderConfiguration->ConfigurationToken);
            if (cfg != vSourceConfig.second->m_encoders.end())
            {
                updateEncoderParams(vSourceConfig.second);
                encConfig = cfg->second;
                break;
            }
        }

        if(!encConfig)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        trt__GetVideoEncoderConfigurationResponse.Configuration = GetProxy()->createVideoEncoderConfiguration(this->soap, encConfig);
        return SOAP_OK;
    }

    int MediaBindingServiceImpl::GetAudioSourceConfiguration(_trt__GetAudioSourceConfiguration* trt__GetAudioSourceConfiguration, _trt__GetAudioSourceConfigurationResponse& trt__GetAudioSourceConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto audioSourceCfg = GetConfiguration()->FindAudioSourceConfiguration(trt__GetAudioSourceConfiguration->ConfigurationToken);

        if (!audioSourceCfg)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        trt__GetAudioSourceConfigurationResponse.Configuration = GetProxy()->createAudioSourceConfiguration(soap, audioSourceCfg);
        return SOAP_OK;
    }

    int MediaBindingServiceImpl::GetAudioEncoderConfiguration(_trt__GetAudioEncoderConfiguration* trt__GetAudioEncoderConfiguration, _trt__GetAudioEncoderConfigurationResponse& trt__GetAudioEncoderConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        AudioEncoderConfigurationSP encConfig = GetConfiguration()->FindAudioEncoderConfig(trt__GetAudioEncoderConfiguration->ConfigurationToken);
        if (!encConfig)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        trt__GetAudioEncoderConfigurationResponse.Configuration = GetProxy()->createAudioEncoderConfiguration(this->soap, encConfig);
        return SOAP_OK;
    }

	int MediaBindingServiceImpl::GetVideoAnalyticsConfiguration(_trt__GetVideoAnalyticsConfiguration* trt__GetVideoAnalyticsConfiguration, _trt__GetVideoAnalyticsConfigurationResponse& trt__GetVideoAnalyticsConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		auto vAnalyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(trt__GetVideoAnalyticsConfiguration->ConfigurationToken);
		if (!vAnalyticsConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, " ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		trt__GetVideoAnalyticsConfigurationResponse.Configuration = GetProxy()->createVideoAnalyticsConfiguration(this->soap, vAnalyticsConfig);

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::GetMetadataConfiguration(_trt__GetMetadataConfiguration* trt__GetMetadataConfiguration, _trt__GetMetadataConfigurationResponse& trt__GetMetadataConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		auto metadataConfig = GetConfiguration()->FindMetadataConfiguration(trt__GetMetadataConfiguration->ConfigurationToken);
		if (!metadataConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, " ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		trt__GetMetadataConfigurationResponse.Configuration = GetProxy()->createMetadataConfiguration(this->soap, metadataConfig);

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::CreateProfile(_trt__CreateProfile* trt__CreateProfile, _trt__CreateProfileResponse& trt__CreateProfileResponse)
	{
        return defaultImplementation();

        /*if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        if(trt__CreateProfile->Token)
        {
            auto profile = GetConfiguration()->FindProfile(*trt__CreateProfile->Token);
            if (profile)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ProfileExists");
                return SOAP_FAULT;
            }
        }

		{
			auto profile = GetConfiguration()->FindProfileByName(trt__CreateProfile->Name);
			if (profile)
			{
				trt__CreateProfileResponse.Profile = GetProxy()->getProfile(soap, profile);
				return SOAP_OK;
			}
		}
	    
	    MediaProfileSP coreprofile(new MediaProfile);
        coreprofile->SetName(trt__CreateProfile->Name);

        auto uuid = boost::uuids::to_string(boost::uuids::random_generator()());
        coreprofile->SetToken(trt__CreateProfile->Token ? *trt__CreateProfile->Token : uuid);

		GetProcessorFactory()->createCreateProfileProcessor(coreprofile, m_data->m_coreSettings)->DoWork();

        trt__CreateProfileResponse.Profile = GetProxy()->getProfile(soap, coreprofile);
        return SOAP_OK;*/
	}

	int MediaBindingServiceImpl::GetProfile(_trt__GetProfile* trt__GetProfile, _trt__GetProfileResponse& trt__GetProfileResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
	    
	    auto profile = GetConfiguration()->FindProfile(trt__GetProfile->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__GetProfile->ProfileToken);
        }

        updateEncoderParams(profile->GetVideoSourceConfiguration());
		trt__GetProfileResponse.Profile = GetProxy()->getProfile(soap, profile);

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::GetProfiles(_trt__GetProfiles* trt__GetProfiles, _trt__GetProfilesResponse& trt__GetProfilesResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result; 
        }

	    BOOST_FOREACH(const auto vProfile,GetConfiguration()->GetProfiles())
		{
            updateEncoderParams(vProfile.second->GetVideoSourceConfiguration());

            if (vProfile.second->GetVideoSourceConfiguration() && !checkVideoSourceCodec(vProfile.second->GetVideoSourceConfiguration()))
            {
                continue;
            }
            if (vProfile.second->GetEncoderConfiguration() && !checkCodec(vProfile.second->GetEncoderConfiguration()->codec))
            {
                continue;
            }

            tt__Profile * prof = GetProxy()->getProfile(soap, vProfile.second);
		    trt__GetProfilesResponse.Profiles.push_back(prof);
		}

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::AddVideoEncoderConfiguration(_trt__AddVideoEncoderConfiguration* trt__AddVideoEncoderConfiguration, _trt__AddVideoEncoderConfigurationResponse& trt__AddVideoEncoderConfigurationResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }
	    
	    EncoderConfigurationSP encoder;
        MediaProfileSP searchedProfile;

	    {
            ReadLock_t l(GetConfiguration()->getLock());

            auto profile = GetConfiguration()->FindProfile(trt__AddVideoEncoderConfiguration->ProfileToken);
            if (!profile)
            {
                return no_profile(this->soap, trt__AddVideoEncoderConfiguration->ProfileToken);
            }

            EncoderConfigurationSP encConf;
            BOOST_FOREACH(auto vSourceConfig,GetConfiguration()->GetVSourceConfigurations())
            {
                auto encConfIt = vSourceConfig.second->m_encoders.find(trt__AddVideoEncoderConfiguration->ConfigurationToken);
                if (encConfIt != vSourceConfig.second->m_encoders.end())
                {
                    encConf = encConfIt->second;
                    break;
                }
            }

            if (!encConf)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }

            encoder = encConf;
            searchedProfile = profile;
        }

        MediaProfileSP newProfile(new MediaProfile(*searchedProfile.get()));
        newProfile->SetEncoderConfiguration(encoder, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::AddVideoSourceConfiguration(_trt__AddVideoSourceConfiguration* trt__AddVideoSourceConfiguration, _trt__AddVideoSourceConfigurationResponse& trt__AddVideoSourceConfigurationResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }
	    
	    auto videoSrcConfig = GetConfiguration()->FindVideoSourceConfiguration(trt__AddVideoSourceConfiguration->ConfigurationToken);
        if (!videoSrcConfig)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        auto profile = GetConfiguration()->FindProfile(trt__AddVideoSourceConfiguration->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__AddVideoSourceConfiguration->ProfileToken);
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->SetVideoSourceConfiguration(videoSrcConfig, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

	    return SOAP_OK;
	}

    int MediaBindingServiceImpl::AddAudioEncoderConfiguration(_trt__AddAudioEncoderConfiguration* trt__AddAudioEncoderConfiguration, _trt__AddAudioEncoderConfigurationResponse& trt__AddAudioEncoderConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        AudioEncoderConfigurationSP encoder;
        MediaProfileSP searchedProfile;

        {
            ReadLock_t l(GetConfiguration()->getLock());

            auto profile = GetConfiguration()->FindProfile(trt__AddAudioEncoderConfiguration->ProfileToken);
            if (!profile)
            {
                return no_profile(this->soap, trt__AddAudioEncoderConfiguration->ProfileToken);
            }

            AudioEncoderConfigurationSP encConf;
            BOOST_FOREACH(auto aSourceConfig,GetConfiguration()->GetASourceConfigurations())
            {
                auto encConfIt = aSourceConfig->GetAudioEncoderConfig();
                if (encConfIt->GetToken() == trt__AddAudioEncoderConfiguration->ConfigurationToken)
                {
                    encConf = encConfIt;
                    break;
                }
            }

            if (!encConf)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }

            encoder = encConf;
            searchedProfile = profile;
        }

        MediaProfileSP newProfile(new MediaProfile(*searchedProfile.get()));
        newProfile->SetAudioEncoderConfiguration(encoder, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::AddAudioSourceConfiguration(_trt__AddAudioSourceConfiguration* trt__AddAudioSourceConfiguration, _trt__AddAudioSourceConfigurationResponse& trt__AddAudioSourceConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto audioSrcConfig = GetConfiguration()->FindAudioSourceConfiguration(trt__AddAudioSourceConfiguration->ConfigurationToken);
        if (!audioSrcConfig)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        auto profile = GetConfiguration()->FindProfile(trt__AddAudioSourceConfiguration->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__AddAudioSourceConfiguration->ProfileToken);
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->SetAudioSourceConfiguration(audioSrcConfig, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::AddPTZConfiguration(_trt__AddPTZConfiguration* trt__AddPTZConfiguration, _trt__AddPTZConfigurationResponse& trt__AddPTZConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }
        
        auto profile = GetConfiguration()->FindProfile(trt__AddPTZConfiguration->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__AddPTZConfiguration->ProfileToken);
        }

        auto vSourceConfig = GetConfiguration()->FindVideoSourceConfigurationByPTZ(trt__AddPTZConfiguration->ConfigurationToken);
        if (!vSourceConfig)
        {
            OnvifUtil::ONVIF_Fault(soap, " ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        auto profileVSourceConfig = profile->GetVideoSourceConfiguration();

        if (profileVSourceConfig && (!profileVSourceConfig->getPtzConfig() || profileVSourceConfig->getPtzConfig()->GetConfigurationToken() != trt__AddPTZConfiguration->ConfigurationToken))
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:Action", "ter:ConfigurationConflict");
            return SOAP_FAULT;
        }

        if(!profileVSourceConfig)
        {
            profileVSourceConfig = vSourceConfig;
        }

        PtzConfigurationSP ptzConf = profileVSourceConfig->getPtzConfig();
        
        if(!ptzConf)
        {
            OnvifUtil::ONVIF_Fault(soap, " ter:ActionNotSupported", "ter:PTZNotSupported");
            return SOAP_FAULT;
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->SetPtzConfiguration(ptzConf);
        newProfile->SetVideoSourceConfiguration(profileVSourceConfig, false);
        
		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_OK;
    }

	int MediaBindingServiceImpl::AddVideoAnalyticsConfiguration(_trt__AddVideoAnalyticsConfiguration* trt__AddVideoAnalyticsConfiguration, _trt__AddVideoAnalyticsConfigurationResponse& trt__AddVideoAnalyticsConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto profile = GetConfiguration()->FindProfile(trt__AddVideoAnalyticsConfiguration->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, trt__AddVideoAnalyticsConfiguration->ProfileToken);
		}

		auto vAnalyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(trt__AddVideoAnalyticsConfiguration->ConfigurationToken);
		if (!vAnalyticsConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, " ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		MediaProfileSP newProfile(new MediaProfile(*profile.get()));
		newProfile->SetVideoAnalyticsConfiguration(vAnalyticsConfig, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::AddMetadataConfiguration(_trt__AddMetadataConfiguration* trt__AddMetadataConfiguration, _trt__AddMetadataConfigurationResponse& trt__AddMetadataConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto profile = GetConfiguration()->FindProfile(trt__AddMetadataConfiguration->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, trt__AddMetadataConfiguration->ProfileToken);
		}

		auto vMetadataConfig = GetConfiguration()->FindMetadataConfiguration(trt__AddMetadataConfiguration->ConfigurationToken);
		if (!vMetadataConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, " ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		MediaProfileSP newProfile(new MediaProfile(*profile.get()));
		newProfile->SetMetadataConfiguration(vMetadataConfig, false);

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::GetCompatibleVideoEncoderConfigurations(_trt__GetCompatibleVideoEncoderConfigurations* trt__GetCompatibleVideoEncoderConfigurations, _trt__GetCompatibleVideoEncoderConfigurationsResponse& trt__GetCompatibleVideoEncoderConfigurationsResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
	    
        VideoSourceConfigurationSP vSource;
	    {
            ReadLock_t l(GetConfiguration()->getLock());

            auto profile = GetConfiguration()->FindProfile(trt__GetCompatibleVideoEncoderConfigurations->ProfileToken);
            if (!profile)
            {
                return no_profile(this->soap, trt__GetCompatibleVideoEncoderConfigurations->ProfileToken);
            }

            vSource = profile->GetVideoSourceConfiguration();

            if (!vSource)
            {
                return SOAP_OK;
            }
        }

        updateEncoderParams(vSource);

        ReadLock_t l(GetConfiguration()->getLock());
	    BOOST_FOREACH(auto vEncoder, vSource->m_encoders)
		{
			if(vEncoder.second->IsSet() && checkCodec(vEncoder.second->codec))
			{
                trt__GetCompatibleVideoEncoderConfigurationsResponse.Configurations.push_back(GetProxy()->createVideoEncoderConfiguration(soap, vEncoder.second));
			}
		}

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::GetCompatibleVideoSourceConfigurations(_trt__GetCompatibleVideoSourceConfigurations* trt__GetCompatibleVideoSourceConfigurations, _trt__GetCompatibleVideoSourceConfigurationsResponse& trt__GetCompatibleVideoSourceConfigurationsResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
	    
        BOOST_FOREACH(auto vSource, GetConfiguration()->GetVSourceConfigurations())
        {
            updateEncoderParams(vSource.second);
        }

        ReadLock_t l(GetConfiguration()->getLock());
        BOOST_FOREACH(auto vSource, GetConfiguration()->GetVSourceConfigurations())
        {
            if (checkVideoSourceCodec(vSource.second))
            {
                tt__VideoSourceConfiguration* sourcecfg = GetProxy()->createVideoSourceConfiguration(soap, vSource.second);
                trt__GetCompatibleVideoSourceConfigurationsResponse.Configurations.push_back(sourcecfg);
            }
        }

		return SOAP_OK;
	}

    int MediaBindingServiceImpl::GetCompatibleAudioEncoderConfigurations(_trt__GetCompatibleAudioEncoderConfigurations* trt__GetCompatibleAudioEncoderConfigurations, _trt__GetCompatibleAudioEncoderConfigurationsResponse& trt__GetCompatibleAudioEncoderConfigurationsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        AudioSourceConfigurationSP aSource;
        {
            ReadLock_t l(GetConfiguration()->getLock());

            auto profile = GetConfiguration()->FindProfile(trt__GetCompatibleAudioEncoderConfigurations->ProfileToken);
            if (!profile)
            {
                return no_profile(this->soap, trt__GetCompatibleAudioEncoderConfigurations->ProfileToken);
            }

            aSource = profile->GetAudioSourceConfiguration();

            if (!aSource)
            {
                return SOAP_OK;
            }
        }

        ReadLock_t l(GetConfiguration()->getLock());
        if(aSource->GetAudioEncoderConfig())
        {
            trt__GetCompatibleAudioEncoderConfigurationsResponse.Configurations.push_back(GetProxy()->createAudioEncoderConfiguration(soap, aSource->GetAudioEncoderConfig()));
        }

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::GetCompatibleAudioSourceConfigurations(_trt__GetCompatibleAudioSourceConfigurations* trt__GetCompatibleAudioSourceConfigurations, _trt__GetCompatibleAudioSourceConfigurationsResponse& trt__GetCompatibleAudioSourceConfigurationsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());

        BOOST_FOREACH(auto aSource,GetConfiguration()->GetASourceConfigurations())
        {
            tt__AudioSourceConfiguration* sourcecfg = GetProxy()->createAudioSourceConfiguration(soap, aSource);
            trt__GetCompatibleAudioSourceConfigurationsResponse.Configurations.push_back(sourcecfg);
        }

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::GetCompatibleVideoAnalyticsConfigurations(_trt__GetCompatibleVideoAnalyticsConfigurations* trt__GetCompatibleVideoAnalyticsConfigurations, _trt__GetCompatibleVideoAnalyticsConfigurationsResponse& trt__GetCompatibleVideoAnalyticsConfigurationsResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

		auto profile = GetConfiguration()->FindProfile(trt__GetCompatibleVideoAnalyticsConfigurations->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, trt__GetCompatibleVideoAnalyticsConfigurations->ProfileToken);
		}

		auto vSourceConfig = profile->GetVideoSourceConfiguration();

		if(vSourceConfig && vSourceConfig->GetVideoAnalyticsConfiguration())
		{
			trt__GetCompatibleVideoAnalyticsConfigurationsResponse.Configurations.push_back(GetProxy()->createVideoAnalyticsConfiguration(this->soap, vSourceConfig->GetVideoAnalyticsConfiguration()));
		}
	    
	    return SOAP_OK;
	}

	int MediaBindingServiceImpl::GetCompatibleMetadataConfigurations(_trt__GetCompatibleMetadataConfigurations* trt__GetCompatibleMetadataConfigurations, _trt__GetCompatibleMetadataConfigurationsResponse& trt__GetCompatibleMetadataConfigurationsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		auto profile = GetConfiguration()->FindProfile(trt__GetCompatibleMetadataConfigurations->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, trt__GetCompatibleMetadataConfigurations->ProfileToken);
		}

		auto vSourceConfig = profile->GetVideoSourceConfiguration();

		if (vSourceConfig && vSourceConfig->GetMetadataConfiguration())
		{
			trt__GetCompatibleMetadataConfigurationsResponse.Configurations.push_back(GetProxy()->createMetadataConfiguration(this->soap, vSourceConfig->GetMetadataConfiguration()));
		}
		else if(!vSourceConfig)
		{
			BOOST_FOREACH(auto vSrcConf,GetConfiguration()->GetVSourceConfigurations())
			{
				if (auto metaConf = vSrcConf.second->GetMetadataConfiguration())
				{
					trt__GetCompatibleMetadataConfigurationsResponse.Configurations.push_back(GetProxy()->createMetadataConfiguration(this->soap, metaConf));
				}
			}
		}

		return SOAP_OK;
	}

    int MediaBindingServiceImpl::SetVideoSourceConfiguration(_trt__SetVideoSourceConfiguration* trt__SetVideoSourceConfiguration, _trt__SetVideoSourceConfigurationResponse& trt__SetVideoSourceConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }
        
        VideoSourceConfigurationSP config;
        if (trt__SetVideoSourceConfiguration->Configuration != nullptr)
        {
            config = GetConfiguration()->FindVideoSourceConfiguration(trt__SetVideoSourceConfiguration->Configuration->token);
            if (!config)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }

            tt__IntRectangle *bounds = trt__SetVideoSourceConfiguration->Configuration->Bounds;
            if (bounds)
            {
                if (config->videoSource && config->videoSource->m_bounds &&
                    !config->videoSource->m_bounds->CompareBounds(bounds->width, bounds->height, bounds->x, bounds->y))
                {
                    OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
                    return SOAP_FAULT;
                }
            }
        }

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::SetVideoEncoderConfiguration(_trt__SetVideoEncoderConfiguration* trt__SetVideoEncoderConfiguration, _trt__SetVideoEncoderConfigurationResponse& trt__SetVideoEncoderConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        BOOST_FOREACH(auto vSourceConfig,GetConfiguration()->GetVSourceConfigurations())
        {
            auto cfg = vSourceConfig.second->m_encoders.find(trt__SetVideoEncoderConfiguration->Configuration->token);
            if(cfg != vSourceConfig.second->m_encoders.end())
            {
                updateEncoderParams(vSourceConfig.second);
            }
        }

        //ReadLock_t l(GetConfiguration()->getLock());
        
        auto vEncoderConfig = GetConfiguration()->FindVideoEncoderConfig(trt__SetVideoEncoderConfiguration->Configuration->token);

        if(!vEncoderConfig)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        //TODO: send new params to core
        if(vEncoderConfig->codec != convertEncodingName(trt__SetVideoEncoderConfiguration->Configuration->Encoding))
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
            return SOAP_FAULT;
        }

        bool error = false;
        if(trt__SetVideoEncoderConfiguration->Configuration->Resolution
            && !vEncoderConfig->m_resolutionsInfo.Contains(trt__SetVideoEncoderConfiguration->Configuration->Resolution->Width, 
                                                        trt__SetVideoEncoderConfiguration->Configuration->Resolution->Height))
        {
            error = true;
        }

		auto vSourceConfig = GetConfiguration()->FindVideoSourceConfigurationByEncoder(vEncoderConfig->GetToken());
		bool isCam = vSourceConfig && vSourceConfig->videoSource->camId == vEncoderConfig->streamId;
		EncoderConfigurationSP newEnc(new EncoderConfiguration(*vEncoderConfig.get()));

		bool changed = false;

        if(trt__SetVideoEncoderConfiguration->Configuration->H264)
        {
            auto config = trt__SetVideoEncoderConfiguration->Configuration->H264;
            if(config->H264Profile != tt__H264Profile__Main)
            {
                error = true;
            }
			else if (vEncoderConfig->GetStreamStaticParams()->GetGovLength() != config->GovLength)
			{
				changed = true;
				error |= !vEncoderConfig->GetStreamStaticParams()->SetGovLength(config->GovLength);
			}
        }

        if(trt__SetVideoEncoderConfiguration->Configuration->MPEG4)
        {
            auto config = trt__SetVideoEncoderConfiguration->Configuration->MPEG4;
			if(config->Mpeg4Profile != tt__Mpeg4Profile__SP)
			{
				error = true;
			}
			else if(config->GovLength && vEncoderConfig->GetStreamStaticParams()->GetGovLength() != config->GovLength)
			{
				changed = true;
				error |= !vEncoderConfig->GetStreamStaticParams()->SetGovLength(config->GovLength);
			}
        }

		float quality = trt__SetVideoEncoderConfiguration->Configuration->Quality;
		if(quality && vEncoderConfig->GetStreamStaticParams()->GetQuality() != quality)
		{
			changed = true;
			error |= !vEncoderConfig->GetStreamStaticParams()->SetQuality(quality);
		}

		auto rateControl = trt__SetVideoEncoderConfiguration->Configuration->RateControl;
		if(rateControl)
		{
			if (rateControl->FrameRateLimit && (vEncoderConfig->GetStreamStaticParams()->GetFrameRateLimit() != rateControl->FrameRateLimit))
			{
				changed = true;
				error |= !vEncoderConfig->GetStreamStaticParams()->SetFrameRateLimit(rateControl->FrameRateLimit);
			}
		}

        if(trt__SetVideoEncoderConfiguration->Configuration->Multicast 
            && trt__SetVideoEncoderConfiguration->Configuration->Multicast->Address->IPv4Address)
        {
            MulticastParamsPtr newParams(new MulticastParams(*trt__SetVideoEncoderConfiguration->Configuration->Multicast->Address->IPv4Address
                                    , trt__SetVideoEncoderConfiguration->Configuration->Multicast->Port));
            
            if(newParams->GetDestinationIp() != vEncoderConfig->multicastParams->GetDestinationIp() || newParams->GetRealPort() != vEncoderConfig->multicastParams->GetRealPort())
            {
				newEnc->multicastParams = newParams;
				changed = true;
            }
        }

		if(changed)
		{
			GetProcessorFactory()->createUpdateVideoEncoderProcessor(newEnc, isCam)->DoWork();
		}

		if (trt__SetVideoEncoderConfiguration->Configuration->Multicast
			&& trt__SetVideoEncoderConfiguration->Configuration->Multicast->Address->IPv4Address)
		{
			BOOST_FOREACH(auto profile, GetConfiguration()->GetProfiles())
			{
				if (profile.second->GetEncoderConfiguration() == vEncoderConfig)
				{
					MediaProfileSP newProfile(new MediaProfile(*profile.second.get()));
					newProfile->m_isMulticast = trt__SetVideoEncoderConfiguration->Configuration->Multicast->AutoStart; 

						GetProcessorFactory()->createUpdateRtspProcessor(GetCoreSettings(), newProfile)->DoWork();
					}
				}
			}

        if(error)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
            return SOAP_FAULT;
        }
        
        return SOAP_OK;
    }

    int MediaBindingServiceImpl::SetAudioSourceConfiguration(_trt__SetAudioSourceConfiguration* trt__SetAudioSourceConfiguration, _trt__SetAudioSourceConfigurationResponse& trt__SetAudioSourceConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        AudioSourceSP aSource;
        if (trt__SetAudioSourceConfiguration->Configuration != nullptr)
        {
            aSource = GetConfiguration()->GetASource(trt__SetAudioSourceConfiguration->Configuration->SourceToken);
            if (!aSource)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }

            auto aSourceConfig = GetConfiguration()->FindAudioSourceConfiguration(trt__SetAudioSourceConfiguration->Configuration->token);
            if(!trt__SetAudioSourceConfiguration->Configuration->token.empty() && !aSourceConfig)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }
        }


        return SOAP_OK;
    }

    int MediaBindingServiceImpl::SetAudioEncoderConfiguration(_trt__SetAudioEncoderConfiguration* trt__SetAudioEncoderConfiguration, _trt__SetAudioEncoderConfigurationResponse& trt__SetAudioEncoderConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto aEncoderConfig = GetConfiguration()->FindAudioEncoderConfig(trt__SetAudioEncoderConfiguration->Configuration->token);

        if (!aEncoderConfig)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        //TODO: send new params to core
        if (trt__SetAudioEncoderConfiguration->Configuration->Encoding != tt__AudioEncoding__G711)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
            return SOAP_FAULT;
        }

        bool error = false;

        if(trt__SetAudioEncoderConfiguration->Configuration->Bitrate && trt__SetAudioEncoderConfiguration->Configuration->Bitrate != 128)
        {
            error = true;
        }

        if(trt__SetAudioEncoderConfiguration->Configuration->SampleRate && !GetProxy()->CheckAudioSampleRate(aEncoderConfig, trt__SetAudioEncoderConfiguration->Configuration->SampleRate))
        {
            error = true;
        }

		if (error)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
			return SOAP_FAULT;
		}

		AudioEncoderConfigurationSP newEnc(new AudioEncoderConfiguration(*aEncoderConfig.get()));

		if (trt__SetAudioEncoderConfiguration->Configuration->Multicast
            && trt__SetAudioEncoderConfiguration->Configuration->Multicast->Address->IPv4Address)
        {
            MulticastParamsPtr newParams(new MulticastParams(*trt__SetAudioEncoderConfiguration->Configuration->Multicast->Address->IPv4Address
                , trt__SetAudioEncoderConfiguration->Configuration->Multicast->Port));

            if (newParams->GetDestinationIp() != aEncoderConfig->multicastParams->GetDestinationIp() || newParams->GetRealPort() != aEncoderConfig->multicastParams->GetRealPort())
            {
                newEnc->multicastParams = newParams;
            }
        }

		if (trt__SetAudioEncoderConfiguration->Configuration->SampleRate)
		{
			newEnc->SetSamplerate(trt__SetAudioEncoderConfiguration->Configuration->SampleRate * 1000);
		}
         
		GetProcessorFactory()->createUpdateAudioEncoderProcessor(newEnc)->DoWork();

		/*if (trt__SetAudioEncoderConfiguration->Configuration->Multicast
			&& trt__SetAudioEncoderConfiguration->Configuration->Multicast->Address->IPv4Address)
		{
			BOOST_FOREACH(auto profile, GetConfiguration()->GetProfiles())
			{
				if (profile.second->GetAudioEncoderConfiguration() == aEncoderConfig)
				{
					MediaProfileSP newProfile(new MediaProfile(*profile.second.get()));
					newProfile->m_isMulticast = trt__SetAudioEncoderConfiguration->Configuration->Multicast->AutoStart;

					GetProcessorFactory()->createUpdateRtspProcessor(GetCoreSettings(), newProfile)->DoWork();
				}
			}
		}*/

        return SOAP_OK;
    }

	int MediaBindingServiceImpl::SetVideoAnalyticsConfiguration(_trt__SetVideoAnalyticsConfiguration* trt__SetVideoAnalyticsConfiguration, _trt__SetVideoAnalyticsConfigurationResponse& trt__SetVideoAnalyticsConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto vAnalyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(trt__SetVideoAnalyticsConfiguration->Configuration->token);
		if (!vAnalyticsConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, " ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::SetMetadataConfiguration(_trt__SetMetadataConfiguration* trt__SetMetadataConfiguration, _trt__SetMetadataConfigurationResponse& trt__SetMetadataConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto metadataConfig = GetConfiguration()->FindMetadataConfiguration(trt__SetMetadataConfiguration->Configuration->token);
		if (!metadataConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, " ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		if(trt__SetMetadataConfiguration->Configuration && trt__SetMetadataConfiguration->Configuration->Analytics)
		{
			metadataConfig->SetAnalyticsFlag(*trt__SetMetadataConfiguration->Configuration->Analytics);
		}

        if (trt__SetMetadataConfiguration->Configuration)
        {
            if (trt__SetMetadataConfiguration->Configuration->Analytics)
            {
                metadataConfig->SetAnalyticsFlag(*trt__SetMetadataConfiguration->Configuration->Analytics);
            }

            metadataConfig->SetEventsFlag(trt__SetMetadataConfiguration->Configuration->Events != nullptr);
            if (trt__SetMetadataConfiguration->Configuration->Events)
            {
                std::vector<FilterTypeSP> filter;
                if (trt__SetMetadataConfiguration->Configuration->Events->Filter)
                {
                    BOOST_FOREACH(auto txt, trt__SetMetadataConfiguration->Configuration->Events->Filter->__any)
                    {
                        if (auto tpe = FilterType::ConvertType(txt.name))
                        {
                            filter.push_back(FilterTypeSP(new FilterType(tpe, txt.text)));
                        }
                    }
                }
                metadataConfig->SetFilter(filter);
            }
        }

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::GetVideoSourceConfigurationOptions(_trt__GetVideoSourceConfigurationOptions* trt__GetVideoSourceConfigurationOptions, _trt__GetVideoSourceConfigurationOptionsResponse& trt__GetVideoSourceConfigurationOptionsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
        
        VideoSourceConfigurationSP config;
        if (trt__GetVideoSourceConfigurationOptions->ConfigurationToken != nullptr)
        {
            config = GetConfiguration()->FindVideoSourceConfiguration(*trt__GetVideoSourceConfigurationOptions->ConfigurationToken);
            if (!config)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }
        }

        if (config == nullptr && trt__GetVideoSourceConfigurationOptions->ProfileToken != nullptr)
        {
            auto profile = GetConfiguration()->FindProfile(*trt__GetVideoSourceConfigurationOptions->ProfileToken);
            if (!profile)
            {
                return no_profile(this->soap, *trt__GetVideoSourceConfigurationOptions->ProfileToken);
            }

            config = profile->GetVideoSourceConfiguration();
        }

        if(config == nullptr)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        trt__GetVideoSourceConfigurationOptionsResponse.Options = soap_new_tt__VideoSourceConfigurationOptions(this->soap);
        trt__GetVideoSourceConfigurationOptionsResponse.Options->VideoSourceTokensAvailable.push_back(config->videoSource->GetToken());

        trt__GetVideoSourceConfigurationOptionsResponse.Options->BoundsRange = soap_new_tt__IntRectangleRange(this->soap);
        
        BoundsRangeSP bounds = config->videoSource->m_bounds;
        if(!bounds)
        {
            bounds = BoundsRangeSP(new BoundsRange(0, 0));
        }

        trt__GetVideoSourceConfigurationOptionsResponse.Options->BoundsRange->WidthRange = soap_new_set_tt__IntRange(this->soap, bounds->_widthRange.first, bounds->_widthRange.second);
        trt__GetVideoSourceConfigurationOptionsResponse.Options->BoundsRange->HeightRange = soap_new_set_tt__IntRange(this->soap, bounds->_heightRange.first, bounds->_heightRange.second);
        trt__GetVideoSourceConfigurationOptionsResponse.Options->BoundsRange->XRange = soap_new_set_tt__IntRange(this->soap, bounds->_xRange.first, bounds->_xRange.second);
        trt__GetVideoSourceConfigurationOptionsResponse.Options->BoundsRange->YRange = soap_new_set_tt__IntRange(this->soap, bounds->_yRange.first, bounds->_yRange.second);

        return SOAP_OK;
    }

	int MediaBindingServiceImpl::GetVideoEncoderConfigurationOptions(_trt__GetVideoEncoderConfigurationOptions* trt__GetVideoEncoderConfigurationOptions, _trt__GetVideoEncoderConfigurationOptionsResponse& trt__GetVideoEncoderConfigurationOptionsResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

		BOOST_FOREACH(auto src, GetConfiguration()->GetVSourceConfigurations())
		{
			updateEncoderParams(src.second);
		}
	    
        ReadLock_t l(GetConfiguration()->getLock());

	    EncoderConfigurationSP config;
		if (trt__GetVideoEncoderConfigurationOptions->ProfileToken != nullptr)
		{
			auto profile = GetConfiguration()->FindProfile(*trt__GetVideoEncoderConfigurationOptions->ProfileToken);
			if (!profile)
			{
                return no_profile(this->soap, *trt__GetVideoEncoderConfigurationOptions->ProfileToken);
			}

			config = profile->GetEncoderConfiguration();
		}

		if (config == nullptr && trt__GetVideoEncoderConfigurationOptions->ConfigurationToken != nullptr 
            && !(*trt__GetVideoEncoderConfigurationOptions->ConfigurationToken).empty())
		{
            config = GetConfiguration()->FindVideoEncoderConfig(*trt__GetVideoEncoderConfigurationOptions->ConfigurationToken);
		    
			if (!config)
			{
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
				return SOAP_FAULT;
			}
		}

		trt__GetVideoEncoderConfigurationOptionsResponse.Options = soap_new_tt__VideoEncoderConfigurationOptions(this->soap);

        trt__GetVideoEncoderConfigurationOptionsResponse.Options->QualityRange = 
                soap_new_set_tt__IntRange(this->soap, QualityRangeMin, QualityRangeMax);

        //Count available encoders
        int h264 = 0;
        int mjpeg = 0;
        int mpeg4 = 0;

	    BOOST_FOREACH(auto vSource,GetConfiguration()->GetVSourceConfigurations())
        {
            BOOST_FOREACH(auto vEncoder, vSource.second->m_encoders)
            {
                if (vEncoder.second->codec == "H264")
                {
                    h264++;
                }
                else if (vEncoder.second->codec == "MJPEG")
                {
                    mjpeg++;
                }
                else if (vEncoder.second->codec == "MPEG4")
                {
                    mpeg4++;
                }
            }
        }

        //H264
        if ((config && config->codec == "H264") || (!config && h264))
		{
		    trt__GetVideoEncoderConfigurationOptionsResponse.Options->H264 = soap_new_tt__H264Options(this->soap);

            GetProxy()->fillResolutions(this->soap, trt__GetVideoEncoderConfigurationOptionsResponse.Options->H264->ResolutionsAvailable,
                config ? config->m_resolutionsInfo : ResolutionInfo());
		    
            trt__GetVideoEncoderConfigurationOptionsResponse.Options->H264->GovLengthRange = 
                                soap_new_set_tt__IntRange(this->soap, GovLengthRangeMin, GovLengthRangeMax);

        	trt__GetVideoEncoderConfigurationOptionsResponse.Options->H264->FrameRateRange = 
                                soap_new_set_tt__IntRange(this->soap, FpsRangeMin, FpsRangeMax);

            trt__GetVideoEncoderConfigurationOptionsResponse.Options->H264->EncodingIntervalRange = 
                                soap_new_set_tt__IntRange(this->soap, EncodingIntervalRangeMin, EncodingIntervalRangeMax);

            trt__GetVideoEncoderConfigurationOptionsResponse.Options->H264->H264ProfilesSupported.push_back(tt__H264Profile__Main);
		}

	    //MPEG-4
        if ((config && config->codec == "MPEG4") || (!config && mpeg4))
        {
            trt__GetVideoEncoderConfigurationOptionsResponse.Options->MPEG4 = soap_new_tt__Mpeg4Options(this->soap);

            GetProxy()->fillResolutions(this->soap, trt__GetVideoEncoderConfigurationOptionsResponse.Options->MPEG4->ResolutionsAvailable,
                config ? config->m_resolutionsInfo : ResolutionInfo());

            trt__GetVideoEncoderConfigurationOptionsResponse.Options->MPEG4->Mpeg4ProfilesSupported.push_back(tt__Mpeg4Profile__SP);
            
        	trt__GetVideoEncoderConfigurationOptionsResponse.Options->MPEG4->FrameRateRange = 
                            soap_new_set_tt__IntRange(this->soap, FpsRangeMin, FpsRangeMax);

            trt__GetVideoEncoderConfigurationOptionsResponse.Options->MPEG4->GovLengthRange = 
                            soap_new_set_tt__IntRange(this->soap, GovLengthRangeMin, GovLengthRangeMax);

            trt__GetVideoEncoderConfigurationOptionsResponse.Options->MPEG4->EncodingIntervalRange =
                            soap_new_set_tt__IntRange(this->soap, EncodingIntervalRangeMin, EncodingIntervalRangeMax);
        }

	    //JPEG
        if ((config && config->codec == "MJPEG") || (!config && mjpeg))
        {
            trt__GetVideoEncoderConfigurationOptionsResponse.Options->JPEG = soap_new_tt__JpegOptions(this->soap);

            GetProxy()->fillResolutions(this->soap, trt__GetVideoEncoderConfigurationOptionsResponse.Options->JPEG->ResolutionsAvailable,
                config ? config->m_resolutionsInfo : ResolutionInfo());

            trt__GetVideoEncoderConfigurationOptionsResponse.Options->JPEG->EncodingIntervalRange =
                soap_new_set_tt__IntRange(this->soap, EncodingIntervalRangeMin, EncodingIntervalRangeMax);
            
        	trt__GetVideoEncoderConfigurationOptionsResponse.Options->JPEG->FrameRateRange =
                soap_new_set_tt__IntRange(this->soap, FpsRangeMin, FpsRangeMax);
        }

		return SOAP_OK;
	}

    int MediaBindingServiceImpl::GetAudioSourceConfigurationOptions(_trt__GetAudioSourceConfigurationOptions* trt__GetAudioSourceConfigurationOptions, _trt__GetAudioSourceConfigurationOptionsResponse& trt__GetAudioSourceConfigurationOptionsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        AudioSourceConfigurationSP config;
        if (trt__GetAudioSourceConfigurationOptions->ConfigurationToken != nullptr)
        {
            config = GetConfiguration()->FindAudioSourceConfiguration(*trt__GetAudioSourceConfigurationOptions->ConfigurationToken);
            if (!config)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }
        }

        /*if (config == nullptr && trt__GetAudioSourceConfigurationOptions->ProfileToken == nullptr)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }*/

        if (config == nullptr && trt__GetAudioSourceConfigurationOptions->ProfileToken != nullptr)
        {
            auto profile = GetConfiguration()->FindProfile(*trt__GetAudioSourceConfigurationOptions->ProfileToken);
            if (!profile)
            {
                return no_profile(this->soap, *trt__GetAudioSourceConfigurationOptions->ProfileToken);
            }

            config = profile->GetAudioSourceConfiguration();
        }

        trt__GetAudioSourceConfigurationOptionsResponse.Options = soap_new_tt__AudioSourceConfigurationOptions(this->soap);

        BOOST_FOREACH(auto aSourceConfig,GetConfiguration()->GetASources())
        {
            trt__GetAudioSourceConfigurationOptionsResponse.Options->InputTokensAvailable.push_back(aSourceConfig->GetToken());
        }

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::GetAudioEncoderConfigurationOptions(_trt__GetAudioEncoderConfigurationOptions* trt__GetAudioEncoderConfigurationOptions, _trt__GetAudioEncoderConfigurationOptionsResponse& trt__GetAudioEncoderConfigurationOptionsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

		AudioEncoderConfigurationSP encConf;
		if(trt__GetAudioEncoderConfigurationOptions->ConfigurationToken)
        {
			encConf = GetConfiguration()->FindAudioEncoderConfig(*trt__GetAudioEncoderConfigurationOptions->ConfigurationToken);
        }

		if(trt__GetAudioEncoderConfigurationOptions->ProfileToken)
		{
			auto profile = GetConfiguration()->FindProfile(*trt__GetAudioEncoderConfigurationOptions->ProfileToken);
			if(!profile)
			{
                return no_profile(this->soap, *trt__GetAudioEncoderConfigurationOptions->ProfileToken);
			}

			encConf = profile->GetAudioEncoderConfiguration();
		}
		
        trt__GetAudioEncoderConfigurationOptionsResponse.Options = soap_new_tt__AudioEncoderConfigurationOptions(this->soap);
		
		auto opt = GetProxy()->CreateAudioEncoderConfigurationOption(this->soap, encConf);
        trt__GetAudioEncoderConfigurationOptionsResponse.Options->Options.push_back(opt);

        return SOAP_OK;
    }

	int MediaBindingServiceImpl::GetMetadataConfigurationOptions(_trt__GetMetadataConfigurationOptions* trt__GetMetadataConfigurationOptions, _trt__GetMetadataConfigurationOptionsResponse& trt__GetMetadataConfigurationOptionsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		trt__GetMetadataConfigurationOptionsResponse.Options = soap_new_tt__MetadataConfigurationOptions(this->soap);
		trt__GetMetadataConfigurationOptionsResponse.Options->PTZStatusFilterOptions = soap_new_tt__PTZStatusFilterOptions(this->soap);
		trt__GetMetadataConfigurationOptionsResponse.Options->PTZStatusFilterOptions->PanTiltPositionSupported = OnvifUtil::BoolValue(this->soap, false);
		trt__GetMetadataConfigurationOptionsResponse.Options->PTZStatusFilterOptions->PanTiltStatusSupported = false;
		trt__GetMetadataConfigurationOptionsResponse.Options->PTZStatusFilterOptions->ZoomPositionSupported = OnvifUtil::BoolValue(this->soap, false);
		trt__GetMetadataConfigurationOptionsResponse.Options->PTZStatusFilterOptions->ZoomStatusSupported = false;

		return SOAP_OK;
	}

	int MediaBindingServiceImpl::GetGuaranteedNumberOfVideoEncoderInstances(_trt__GetGuaranteedNumberOfVideoEncoderInstances* trt__GetGuaranteedNumberOfVideoEncoderInstances, _trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse& trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
        
        VideoSourceConfigurationSP vSource = GetConfiguration()->FindVideoSourceConfiguration(trt__GetGuaranteedNumberOfVideoEncoderInstances->ConfigurationToken);
        if (!vSource)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

		int totalNum = 0;

        BOOST_FOREACH(auto vEncoder, vSource->m_encoders)
        {
            if(vEncoder.second->codec == "H264")
            {
				if (!trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.H264)
				{
					trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.H264 = soap_new_int(this->soap);
					*trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.H264 = 0;
				}

            	*trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.H264 = *trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.H264 + 1;
				totalNum++;
            }
            else if(vEncoder.second->codec == "MJPEG")
            {
				if (!trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.JPEG)
				{
					trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.JPEG = soap_new_int(this->soap);
					*trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.JPEG = 0;
				}
            	
            	*trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.JPEG = *trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.JPEG + 1;
				totalNum++;
            }
            else if (vEncoder.second->codec == "MPEG4")
            {
				if (!trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.MPEG4)
				{
					trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.MPEG4 = soap_new_int(this->soap);
					*trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.MPEG4 = 0;
				}
            	
            	*trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.MPEG4 = *trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.MPEG4 + 1;
				totalNum++;
            }
        }

		trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse.TotalNumber = totalNum;

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::GetStreamUri(_trt__GetStreamUri* trt__GetStreamUri, _trt__GetStreamUriResponse& trt__GetStreamUriResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
	    
	    auto profile = GetConfiguration()->FindProfile(trt__GetStreamUri->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__GetStreamUri->ProfileToken);
        }

		VideoSourceConfigurationSP vSource = profile->GetVideoSourceConfiguration();
        auto aSource = profile->GetAudioSourceConfiguration();
        if(!vSource && !aSource)
        {
            return incomplete_configuration(soap, profile);
        }

        if(vSource && !profile->GetEncoderConfiguration() && !profile->GetMetadataConfiguration()
            /*|| (aSource && !profile->GetAudioEncoderConfiguration())*/)
        {
            return incomplete_configuration(soap, profile);
        }

        bool isMulticast = false;
        if(trt__GetStreamUri->StreamSetup)
        {
            isMulticast = trt__GetStreamUri->StreamSetup->Stream == tt__StreamType__RTP_Multicast;

            if(isMulticast || profile->m_isMulticast)
            {
                MediaProfileSP newProfile(new MediaProfile(*profile.get()));
                newProfile->m_isMulticast = true;

				GetProcessorFactory()->createUpdateRtspProcessor(GetCoreSettings(), newProfile)->DoWork();
            }
        }

        trt__GetStreamUriResponse.MediaUri = soap_new_tt__MediaUri(this->soap);

        auto setts = GetCoreSettings();
	    if(trt__GetStreamUri->StreamSetup 
            && trt__GetStreamUri->StreamSetup->Transport
            && trt__GetStreamUri->StreamSetup->Transport->Protocol == tt__TransportProtocol__HTTP && GetCoreSettings()->IsProxy())
        {
            trt__GetStreamUriResponse.MediaUri->Uri = setts->GetBaseUri() + "/rtspproxy/" +
                setts->GetRtspInfo()->GetProfileUnicastUriName(profile);
        }
        else
        {
            trt__GetStreamUriResponse.MediaUri->Uri = setts->GetRtspInfo()->GetUri(profile, isMulticast);
        }

		return SOAP_OK;
	}

    int MediaBindingServiceImpl::StartMulticastStreaming(_trt__StartMulticastStreaming* trt__StartMulticastStreaming, _trt__StartMulticastStreamingResponse& trt__StartMulticastStreamingResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt__StartMulticastStreaming->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__StartMulticastStreaming->ProfileToken);
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->m_isMulticast = true;

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

		newProfile->m_isMulticast = false;
		GetProcessorFactory()->createUpdateRtspProcessor(GetCoreSettings(), newProfile)->DoWork();

		newProfile->m_isMulticast = true;
		GetProcessorFactory()->createUpdateRtspProcessor(GetCoreSettings(), newProfile)->DoWork();

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::StopMulticastStreaming(_trt__StopMulticastStreaming* trt__StopMulticastStreaming, _trt__StopMulticastStreamingResponse& trt__StopMulticastStreamingResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt__StopMulticastStreaming->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__StopMulticastStreaming->ProfileToken);
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->m_isMulticast = false;

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

		GetProcessorFactory()->createUpdateRtspProcessor(GetCoreSettings(), newProfile)->DoWork();

        return SOAP_OK;
    }

    int MediaBindingServiceImpl::SetSynchronizationPoint(_trt__SetSynchronizationPoint* trt__SetSynchronizationPoint, _trt__SetSynchronizationPointResponse& trt__SetSynchronizationPointResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

		GetSubscriptionManager()->SendPropertyEventsStates(this->soap, IMessageSubscriptionPtr());
        return SOAP_OK;
    }

    int MediaBindingServiceImpl::GetSnapshotUri(_trt__GetSnapshotUri* trt__GetSnapshotUri, _trt__GetSnapshotUriResponse& trt__GetSnapshotUriResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt__GetSnapshotUri->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt__GetSnapshotUri->ProfileToken);
        }

        if(!profile->GetVideoSourceConfiguration() || !profile->GetEncoderConfiguration())
        {
            return incomplete_configuration(soap, profile);
        }
	    
	    trt__GetSnapshotUriResponse.MediaUri = soap_new_tt__MediaUri(this->soap);
		trt__GetSnapshotUriResponse.MediaUri->InvalidAfterConnect = false;
		trt__GetSnapshotUriResponse.MediaUri->InvalidAfterReboot = false;
		trt__GetSnapshotUriResponse.MediaUri->Timeout = 0;
        trt__GetSnapshotUriResponse.MediaUri->Uri = "http://" + GetCoreSettings()->GetServicesAddress() + "/image.jpg?cam.id="
            + profile->GetVideoSourceConfiguration()->videoSource->camId
            + "&stream.id=" + profile->GetEncoderConfiguration()->streamId;
		return SOAP_OK;
	}

    trt__Capabilities* MediaBindingServiceImpl::GetServiceCapabilities(struct soap* soapObj, CoreSettingsSP coreSettings)
    {
        auto out_capabilities = soap_new_trt__Capabilities(soapObj);
        
        out_capabilities->Rotation = OnvifUtil::BoolValue(soapObj, false);

        out_capabilities->ProfileCapabilities = soap_new_trt__ProfileCapabilities(soapObj);
        out_capabilities->ProfileCapabilities->MaximumNumberOfProfiles = soap_new_int(soapObj);
        *out_capabilities->ProfileCapabilities->MaximumNumberOfProfiles = 10000;

        out_capabilities->StreamingCapabilities = soap_new_trt__StreamingCapabilities(soapObj);
        out_capabilities->StreamingCapabilities->RTPMulticast = OnvifUtil::BoolValue(soapObj, coreSettings->IsMulticastEnabled());
        out_capabilities->StreamingCapabilities->RTP_USCORERTSP_USCORETCP = OnvifUtil::BoolValue(soapObj, true);
        out_capabilities->StreamingCapabilities->RTP_USCORETCP = OnvifUtil::BoolValue(soapObj, true);

        return out_capabilities;
    }

    void MediaBindingServiceImpl::updateEncoderParams(VideoSourceConfigurationSP vSourceConfig)
	{
        if(!vSourceConfig)
        {
            return;
        }

        auto it = std::find_if(vSourceConfig->m_encoders.begin(), vSourceConfig->m_encoders.end(),
            [](const std::pair<std::string, EncoderConfigurationSP>& enc) { return enc.second->NeedsUpdate(); });

        if(it != vSourceConfig->m_encoders.end())
        {
			GetProcessorFactory()->createGetStreamParamsProcessor(vSourceConfig->videoSource)->DoWork();
        }
	}

    bool MediaBindingServiceImpl::checkCodec(const std::string& codec)
	{
        return codec == "H264" || codec == "MPEG4" || codec == "MJPEG";
	}

    bool MediaBindingServiceImpl::checkVideoSourceCodec(const VideoSourceConfigurationSP& vSourceConfig) const
    {
        BOOST_FOREACH(auto vEncoder, vSourceConfig->m_encoders)
        {
            if (vEncoder.second->IsSet() && MediaBindingServiceImpl::checkCodec(vEncoder.second->codec))
            {
                return true;
            }
        }

        return false;
    }

	MessageHolderTypeSP MediaBindingServiceImpl::CreateProfileChangedEvent(struct soap* soap, MediaProfileSP profile)
	{
		auto msg = soap_new_wsnt__NotificationMessageHolderType(soap);

		msg->Topic = soap_new_wsnt__TopicExpressionType(soap);
		msg->Topic->__any = "tns1:Configuration/Profile";
		msg->Topic->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;

		_tt__Message * tt_msg = soap_new__tt__Message(soap);

		tt_msg->PropertyOperation = soap_new_tt__PropertyOperation(soap);
		*tt_msg->PropertyOperation = tt__PropertyOperation__Changed;

		auto cur_tssTime = SoapProxy::GetCurTime();
		tt_msg->UtcTime = *SoapProxy::convertTime(soap, cur_tssTime);

		tt_msg->Source = soap_new_tt__ItemList(soap);

        simple_item_type prop_vToken;
		prop_vToken.Name = "Token";
		prop_vToken.Value = profile->GetToken();
		tt_msg->Source->SimpleItem.push_back(prop_vToken);

		tt_msg->Data = soap_new_tt__ItemList(soap);

        element_item_type elementItem;
		elementItem.Name = "Configuration";

		auto info = SoapProxy::getProfile(soap, profile);

		elementItem.__any.set(info, SOAP_TYPE_tt__Profile);
		elementItem.__any.name = "tt:Profile";

		tt_msg->Data->ElementItem.push_back(elementItem);

		msg->Message.__any.set(tt_msg, SOAP_TYPE__tt__Message);

		return std::make_shared<MessageHolderType>(msg, nullptr);
	}

	MessageHolderTypeSP MediaBindingServiceImpl::CreateVEncoderConfigurationChangedEvent(struct soap* soap, EncoderConfigurationSP config)
	{
		auto msg = soap_new_wsnt__NotificationMessageHolderType(soap);

		msg->Topic = soap_new_wsnt__TopicExpressionType(soap);
		msg->Topic->__any = "tns1:Configuration/VideoEncoderConfiguration";
		msg->Topic->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;

		_tt__Message * tt_msg = soap_new__tt__Message(soap);

		auto cur_tssTime = SoapProxy::GetCurTime();
		tt_msg->UtcTime = *SoapProxy::convertTime(soap, cur_tssTime);

		tt_msg->Source = soap_new_tt__ItemList(soap);

        simple_item_type prop_vToken;
		prop_vToken.Name = "Token";
		prop_vToken.Value = config->GetToken();
		tt_msg->Source->SimpleItem.push_back(prop_vToken);

		tt_msg->Data = soap_new_tt__ItemList(soap);

        element_item_type elementItem;
		elementItem.Name = "Configuration";

		auto info = SoapProxy::createVideoEncoderConfiguration(soap, config);

		elementItem.__any.set(info, SOAP_TYPE_tt__VideoEncoderConfiguration);
		elementItem.__any.name = "tt:VideoEncoderConfiguration";

		tt_msg->Data->ElementItem.push_back(elementItem);

		msg->Message.__any.set(tt_msg, SOAP_TYPE__tt__Message);

		return std::make_shared<MessageHolderType>(msg, nullptr);
	}

    std::string MediaBindingServiceImpl::convertEncodingName(tt__VideoEncoding& soapEncodingName)
    {
        switch (soapEncodingName)
        {
        case tt__VideoEncoding__H264:
            return "H264";
        case tt__VideoEncoding__MPEG4:
            return "MPEG4";
        case tt__VideoEncoding__JPEG:
            return "MJPEG";
        default: break;
        }
        return "";
    }
}
