#pragma once

#include "onvif_generated_ImagingBindingService.h"
#include <Interface/IServiceImpl.h>
#include <SoapProxy/SoapUtil.h>

namespace onvif_generated_ {
	class ImagingBindingServiceImpl : public ImagingBindingService, public IServiceImpl
	{
	public:
        ImagingBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data);

        ImagingBindingService* copy() override { return new ImagingBindingServiceImpl(*this); }

	    int GetServiceCapabilities(_timg2__GetServiceCapabilities* timg2__GetServiceCapabilities, _timg2__GetServiceCapabilitiesResponse& timg2__GetServiceCapabilitiesResponse) override;
        int GetImagingSettings(_timg2__GetImagingSettings* timg2__GetImagingSettings, _timg2__GetImagingSettingsResponse& timg2__GetImagingSettingsResponse) override;
        int SetImagingSettings(_timg2__SetImagingSettings* timg2__SetImagingSettings, _timg2__SetImagingSettingsResponse& timg2__SetImagingSettingsResponse) override;
        int GetOptions(_timg2__GetOptions* timg2__GetOptions, _timg2__GetOptionsResponse& timg2__GetOptionsResponse) override;
        int Move(_timg2__Move* timg2__Move, _timg2__MoveResponse& timg2__MoveResponse) override;
        int Stop(_timg2__Stop* timg2__Stop, _timg2__StopResponse& timg2__StopResponse) override;
		int GetStatus(_timg2__GetStatus* timg2__GetStatus, _timg2__GetStatusResponse& timg2__GetStatusResponse) override;
        int GetMoveOptions(_timg2__GetMoveOptions* timg2__GetMoveOptions, _timg2__GetMoveOptionsResponse& timg2__GetMoveOptionsResponse) override;
	    int GetPresets(_timg2__GetPresets* timg2__GetPresets, _timg2__GetPresetsResponse& timg2__GetPresetsResponse) override { return defaultImplementation(); }
	    int GetCurrentPreset(_timg2__GetCurrentPreset* timg2__GetCurrentPreset, _timg2__GetCurrentPresetResponse& timg2__GetCurrentPresetResponse) override { return defaultImplementation(); }
	    int SetCurrentPreset(_timg2__SetCurrentPreset* timg2__SetCurrentPreset, _timg2__SetCurrentPresetResponse& timg2__SetCurrentPresetResponse) override { return defaultImplementation(); }

        //IServiceImpl
	    int Dispatch(struct soap* soapPtr) override
        {
            return ImagingBindingService::dispatch(soapPtr);
        }

        static timg2__Capabilities* GetServiceCapabilities(struct soap* soapObj);

        static std::string GetUri()
        {
            return "/imaging_service";
        }

	private:
        int defaultImplementation() override
        {
            if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
            {
                return auth_result;
            }

			OnvifUtil::ONVIF_Fault(this->soap, "ter:ActionNotSupported", "ter:NotImplemented", "This optinal method is not implemented", true);
            return SOAP_FAULT;
        }
	};
}
