#pragma once

#include "onvif_generated_RuleEngineBindingService.h"
#include <Interface/IServiceImpl.h>
#include <SoapProxy/SoapUtil.h>

namespace onvif_generated_ {
	class RuleEngineBindingServiceImpl : public RuleEngineBindingService, public IServiceImpl
	{
	public:
        RuleEngineBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data);

        RuleEngineBindingService* copy() override { return new RuleEngineBindingServiceImpl(*this); }

		int GetSupportedRules(_tanl2__GetSupportedRules* tanl2__GetSupportedRules, _tanl2__GetSupportedRulesResponse& tanl2__GetSupportedRulesResponse) override;
		int CreateRules(_tanl2__CreateRules* tanl2__CreateRules, _tanl2__CreateRulesResponse& tanl2__CreateRulesResponse) override;
		int DeleteRules(_tanl2__DeleteRules* tanl2__DeleteRules, _tanl2__DeleteRulesResponse& tanl2__DeleteRulesResponse) override;
		int GetRules(_tanl2__GetRules* tanl2__GetRules, _tanl2__GetRulesResponse& tanl2__GetRulesResponse) override;
		int ModifyRules(_tanl2__ModifyRules* tanl2__ModifyRules, _tanl2__ModifyRulesResponse& tanl2__ModifyRulesResponse) override;

		int GetRuleOptions(_tanl2__GetRuleOptions* tanl2__GetRuleOptions, _tanl2__GetRuleOptionsResponse& tanl2__GetRuleOptionsResponse) override;
		
		//IServiceImpl
	    int Dispatch(struct soap* soapPtr) override
        {
            return RuleEngineBindingService::dispatch(soapPtr);
        }

	private:
        int defaultImplementation() override
        {
            if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
            {
                return auth_result;
            }

			OnvifUtil::ONVIF_Fault(this->soap, "ter:ActionNotSupported", "ter:NotImplemented", "This optinal method is not implemented", true);
            return SOAP_FAULT;
        }
	};
}
