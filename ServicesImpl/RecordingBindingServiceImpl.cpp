#include "RecordingBindingServiceImpl.h"
#include <boost/foreach.hpp>
#include <SoapProxy/SoapUtil.h>
#include "SoapProxy/EventsBuildUtil.h"
#include "SoapProxy/StructDefines.h"

namespace onvif_generated_ {
    RecordingBindingServiceImpl::RecordingBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data) :
            RecordingBindingService(soapObj->GetSoap()), IServiceImpl(data)
	{
	}

    int RecordingBindingServiceImpl::GetServiceCapabilities(_trc__GetServiceCapabilities* trc__GetServiceCapabilities, _trc__GetServiceCapabilitiesResponse& trc__GetServiceCapabilitiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }
        
        trc__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities(this->soap);
        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::GetRecordings(_trc__GetRecordings* trc__GetRecordings, _trc__GetRecordingsResponse& trc__GetRecordingsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());

        BOOST_FOREACH(auto recordingConfig,GetConfiguration()->GetRecordingConfigurations())
        {
            tt__GetRecordingsResponseItem * item = GetProxy()->createRecordingItemInfo(this->soap, recordingConfig);
            trc__GetRecordingsResponse.RecordingItem.push_back(item);
        }

        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::SetRecordingConfiguration(_trc__SetRecordingConfiguration* trc__SetRecordingConfiguration, _trc__SetRecordingConfigurationResponse& trc__SetRecordingConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());

        auto recordingConf = GetConfiguration()->findRecordingConfig(trc__SetRecordingConfiguration->RecordingToken);
        if (!recordingConf)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecording");
            return SOAP_FAULT;
        }

        recordingConf->SetRetentionTime(trc__SetRecordingConfiguration->RecordingConfiguration->MaximumRetentionTime);

        auto srcInfo = trc__SetRecordingConfiguration->RecordingConfiguration->Source;
        if(srcInfo)
        {
            //TODO: save params in database
            
            recordingConf->m_sourceInfo.Name = srcInfo->Name;
            recordingConf->m_sourceInfo.SourceId = srcInfo->SourceId;
            recordingConf->m_sourceInfo.Description = srcInfo->Description;
            recordingConf->m_sourceInfo.Location = srcInfo->Location;
            recordingConf->m_sourceInfo.Address = srcInfo->Address;
        }

        sendRecordingConfigurationSetEvent(recordingConf);

        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::GetRecordingConfiguration(_trc__GetRecordingConfiguration* trc__GetRecordingConfiguration, _trc__GetRecordingConfigurationResponse& trc__GetRecordingConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());

        auto recordingConf = GetConfiguration()->findRecordingConfig(trc__GetRecordingConfiguration->RecordingToken);
        if(!recordingConf)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecording");
            return SOAP_FAULT;
        }

        trc__GetRecordingConfigurationResponse.RecordingConfiguration = GetProxy()->createRecordingConfiguration(this->soap, recordingConf);
        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::GetRecordingOptions(_trc__GetRecordingOptions* trc__GetRecordingOptions, _trc__GetRecordingOptionsResponse& trc__GetRecordingOptionsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());

        auto recordingConf = GetConfiguration()->findRecordingConfig(trc__GetRecordingOptions->RecordingToken);
        if (!recordingConf)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecording");
            return SOAP_FAULT;
        }

        trc__GetRecordingOptionsResponse.Options = soap_new_trc__RecordingOptions(this->soap);
        trc__GetRecordingOptionsResponse.Options->Job = soap_new_trc__JobOptions(this->soap);
        trc__GetRecordingOptionsResponse.Options->Job->Spare = soap_new_int(this->soap);
        *trc__GetRecordingOptionsResponse.Options->Job->Spare = 1;
        
        std::string sources;
        BOOST_FOREACH(auto profile,GetConfiguration()->GetProfiles())
        {
            if(profile.second->GetVideoSourceConfiguration() 
                && profile.second->GetVideoSourceConfiguration()->videoSource == recordingConf->GetVSource()
                && profile.second->GetEncoderConfiguration())
            {
                if(!sources.empty())
                {
                    sources += " ";
                }
                
                sources += profile.second->GetToken();
            }
        }

        if (!sources.empty())
        {
            trc__GetRecordingOptionsResponse.Options->Job->CompatibleSources = soap_new_std__string(this->soap);
            *trc__GetRecordingOptionsResponse.Options->Job->CompatibleSources = sources;
        }

        trc__GetRecordingOptionsResponse.Options->Track = soap_new_trc__TrackOptions(this->soap);
        trc__GetRecordingOptionsResponse.Options->Track->SpareTotal = soap_new_int(this->soap);
        *trc__GetRecordingOptionsResponse.Options->Track->SpareTotal = 20;

        trc__GetRecordingOptionsResponse.Options->Track->SpareVideo = soap_new_int(this->soap);
        *trc__GetRecordingOptionsResponse.Options->Track->SpareVideo = 10;

        trc__GetRecordingOptionsResponse.Options->Track->SpareAudio = soap_new_int(this->soap);
        *trc__GetRecordingOptionsResponse.Options->Track->SpareAudio = 10;

        trc__GetRecordingOptionsResponse.Options->Track->SpareMetadata = soap_new_int(this->soap);
        *trc__GetRecordingOptionsResponse.Options->Track->SpareMetadata = 0;

        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::GetTrackConfiguration(_trc__GetTrackConfiguration* trc__GetTrackConfiguration, _trc__GetTrackConfigurationResponse& trc__GetTrackConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());

        auto recordingConf = GetConfiguration()->findRecordingConfig(trc__GetTrackConfiguration->RecordingToken);
        if (!recordingConf)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecording");
            return SOAP_FAULT;
        }

        auto track = recordingConf->FindTrack(trc__GetTrackConfiguration->TrackToken);
        if(!track)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoTrack");
            return SOAP_FAULT;
        }

        trc__GetTrackConfigurationResponse.TrackConfiguration = GetProxy()->createTrackConfiguration(this->soap, track, recordingConf);
        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::SetTrackConfiguration(_trc__SetTrackConfiguration* trc__SetTrackConfiguration, _trc__SetTrackConfigurationResponse& trc__SetTrackConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());

        auto recordingConf = GetConfiguration()->findRecordingConfig(trc__SetTrackConfiguration->RecordingToken);
        if (!recordingConf)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecording");
            return SOAP_FAULT;
        }

        auto track = recordingConf->FindTrack(trc__SetTrackConfiguration->TrackToken);
        if (!track)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoTrack");
            return SOAP_FAULT;
        }

        sendTrackConfigurationSetEvent(recordingConf, track);

        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::CreateRecordingJob(_trc__CreateRecordingJob* trc__CreateRecordingJob, _trc__CreateRecordingJobResponse& trc__CreateRecordingJobResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto recordingConf = GetConfiguration()->findRecordingConfig(trc__CreateRecordingJob->JobConfiguration->RecordingToken);
        if (!recordingConf)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecording");
            return SOAP_FAULT;
        }

        auto newJob = RecordingJobSP(new RecordingJob());
        newJob->m_recordingConfig = recordingConf;

        BOOST_FOREACH(auto src, trc__CreateRecordingJob->JobConfiguration->Source)
        {
            if(src->SourceToken->Type == SoapProxy::SchemaProfileUrl)
            {
                auto profile = GetConfiguration()->FindProfile(src->SourceToken->Token);
                if(!profile)
                {
                    profile = GetConfiguration()->FindProfileByName(src->SourceToken->Token);
                }
                newJob->m_profile = profile;
            }
        }

        newJob->m_state = trc__CreateRecordingJob->JobConfiguration->Mode;
        newJob->priority = trc__CreateRecordingJob->JobConfiguration->Priority;
        
		GetProcessorFactory()->createAddJobProcessor(newJob)->DoWork();

        trc__CreateRecordingJobResponse.JobToken = newJob->GetToken();
        trc__CreateRecordingJobResponse.JobConfiguration = GetProxy()->CreateRecordingJobConfiguration(this->soap, newJob);

		sendJobStateEvent(newJob, tt__PropertyOperation__Changed);

        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::DeleteRecordingJob(_trc__DeleteRecordingJob* trc__DeleteRecordingJob, _trc__DeleteRecordingJobResponse& trc__DeleteRecordingJobResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto job = GetConfiguration()->findRecordingJobByToken(trc__DeleteRecordingJob->JobToken);
        if (!job)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecordingJob");
            return SOAP_FAULT;
        }

		GetProcessorFactory()->createDeleteJobProcessor(job)->DoWork();

        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::GetRecordingJobs(_trc__GetRecordingJobs* trc__GetRecordingJobs, _trc__GetRecordingJobsResponse& trc__GetRecordingJobsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        BOOST_FOREACH(auto job,GetConfiguration()->GetRecordingsJobs())
        {
            trc__GetRecordingJobsResponse.JobItem.push_back(GetProxy()->CreateRecordingJobConfigurationItem(this->soap, job));
        }

        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::SetRecordingJobConfiguration(_trc__SetRecordingJobConfiguration* trc__SetRecordingJobConfiguration, _trc__SetRecordingJobConfigurationResponse& trc__SetRecordingJobConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());

        auto job = GetConfiguration()->findRecordingJobByToken(trc__SetRecordingJobConfiguration->JobToken);
        if (!job)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecordingJob");
            return SOAP_FAULT;
        }

        BOOST_FOREACH(auto src, trc__SetRecordingJobConfiguration->JobConfiguration->Source)
        {
            if (src->SourceToken->Type == SoapProxy::SchemaProfileUrl)
            {
                auto profile = GetConfiguration()->FindProfileByName(src->SourceToken->Token);
                job->m_profile = profile;
                break;
            }
        }

        job->m_state = trc__SetRecordingJobConfiguration->JobConfiguration->Mode;
        job->priority = trc__SetRecordingJobConfiguration->JobConfiguration->Priority;

        trc__SetRecordingJobConfigurationResponse.JobConfiguration = GetProxy()->CreateRecordingJobConfiguration(this->soap, job);

        sendRecordingJobConfigurationEvent(job, tt__PropertyOperation__Changed);

        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::GetRecordingJobConfiguration(_trc__GetRecordingJobConfiguration* trc__GetRecordingJobConfiguration, _trc__GetRecordingJobConfigurationResponse& trc__GetRecordingJobConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto job = GetConfiguration()->findRecordingJobByToken(trc__GetRecordingJobConfiguration->JobToken);
        if (!job)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecordingJob");
            return SOAP_FAULT;
        }

        trc__GetRecordingJobConfigurationResponse.JobConfiguration = GetProxy()->CreateRecordingJobConfiguration(this->soap, job);

        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::SetRecordingJobMode(_trc__SetRecordingJobMode* trc__SetRecordingJobMode, _trc__SetRecordingJobModeResponse& trc__SetRecordingJobModeResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto job = GetConfiguration()->findRecordingJobByToken(trc__SetRecordingJobMode->JobToken);
        if (!job)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecordingJob");
            return SOAP_FAULT;
        }

        //TODO: send react to video
        job->m_state = trc__SetRecordingJobMode->Mode;

        sendJobStateEvent(job, tt__PropertyOperation__Changed);

        return SOAP_OK;
    }

    int RecordingBindingServiceImpl::GetRecordingJobState(_trc__GetRecordingJobState* trc__GetRecordingJobState, _trc__GetRecordingJobStateResponse& trc__GetRecordingJobStateResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto job = GetConfiguration()->findRecordingJobByToken(trc__GetRecordingJobState->JobToken);
        if(!job)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecordingJob");
            return SOAP_FAULT;
        }

        trc__GetRecordingJobStateResponse.State = GetProxy()->createRecordingJobStateInformation(this->soap, job);

        return SOAP_OK;
    }

    trc__Capabilities* RecordingBindingServiceImpl::GetServiceCapabilities(::soap* soapObj)
    {
        auto _capabilities = soap_new_trc__Capabilities(soapObj);
        _capabilities->DynamicRecordings = OnvifUtil::BoolValue(soapObj, false);
        _capabilities->DynamicTracks = OnvifUtil::BoolValue(soapObj, false);
        _capabilities->MetadataRecording = OnvifUtil::BoolValue(soapObj, false);
        _capabilities->Options = OnvifUtil::BoolValue(soapObj, true);
        _capabilities->Encoding = soap_new_std__string(soapObj);
        *_capabilities->Encoding = "H264 JPEG MPEG4 G711";

        return _capabilities;
    }

    void RecordingBindingServiceImpl::sendRecordingConfigurationSetEvent(RecordingConfigurationSP recordingConf) const
    {
        if(!GetSubscriptionManager())
        {
            return;
        }

        auto msg = soap_new_wsnt__NotificationMessageHolderType(this->soap);
        
        msg->Topic = soap_new_wsnt__TopicExpressionType(this->soap);
        msg->Topic->__any = "tns1:RecordingConfig/RecordingConfiguration";
        msg->Topic->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;

        _tt__Message * tt_msg = soap_new__tt__Message(this->soap);

		auto cur_tssTime = SoapProxy::GetCurTime();
        tt_msg->UtcTime = *GetProxy()->convertTime(this->soap, cur_tssTime);

        tt_msg->Source = soap_new_tt__ItemList(this->soap);

		GetProxy()->addSimpleItem(&tt_msg->Source->SimpleItem, "RecordingToken", recordingConf->GetToken());

        tt_msg->Data = soap_new_tt__ItemList(this->soap);

        element_item_type elementItem;
        elementItem.Name = "Configuration";

        auto recConfInfo = GetProxy()->createRecordingConfiguration(this->soap, recordingConf);

        /*soap_dom_element config;
        config.set(recConfInfo, SOAP_TYPE_tt__RecordingConfiguration);
        config.name = "tt:RecordingConfiguration";*/

        elementItem.__any.set(recConfInfo, SOAP_TYPE_tt__RecordingConfiguration);
		elementItem.__any.name = "tt:RecordingConfiguration";

        tt_msg->Data->ElementItem.push_back(elementItem);

        msg->Message.__any.set(tt_msg, SOAP_TYPE__tt__Message);

        auto evt = std::make_shared<MessageHolderType>(msg, recordingConf->GetVSource());
        GetSubscriptionManager()->PushMessage(evt);
    }

    void RecordingBindingServiceImpl::sendTrackConfigurationSetEvent(RecordingConfigurationSP recordingConf, RecordingTrackSP Track) const
    {
        if (!GetSubscriptionManager())
        {
            return;
        }

        auto msg = soap_new_wsnt__NotificationMessageHolderType(this->soap);

        msg->Topic = soap_new_wsnt__TopicExpressionType(this->soap);
        msg->Topic->__any = "tns1:RecordingConfig/TrackConfiguration";
        msg->Topic->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;

        _tt__Message * tt_msg = soap_new__tt__Message(this->soap);

		auto cur_tssTime = SoapProxy::GetCurTime();
        tt_msg->UtcTime = *GetProxy()->convertTime(this->soap, cur_tssTime);

        tt_msg->Source = soap_new_tt__ItemList(this->soap);

		GetProxy()->addSimpleItem(&tt_msg->Source->SimpleItem, "RecordingToken", recordingConf->GetToken());
		GetProxy()->addSimpleItem(&tt_msg->Source->SimpleItem, "TrackToken", Track->GetToken());

        tt_msg->Data = soap_new_tt__ItemList(this->soap);

        element_item_type elementItem;
        elementItem.Name = "Configuration";

        auto recConfInfo = GetProxy()->createTrackConfiguration(this->soap, Track, recordingConf);

        /*soap_dom_element config;
        config.set(recConfInfo, SOAP_TYPE_tt__TrackConfiguration);
        config.name = "tt:TrackConfiguration";*/

        elementItem.__any.set(recConfInfo, SOAP_TYPE_tt__TrackConfiguration);
		elementItem.__any.name = "tt:TrackConfiguration";

        tt_msg->Data->ElementItem.push_back(elementItem);

        msg->Message.__any.set(tt_msg, SOAP_TYPE__tt__Message);

        auto evt = std::make_shared<MessageHolderType>(msg, recordingConf->GetVSource());
        GetSubscriptionManager()->PushMessage(evt);
    }

    void RecordingBindingServiceImpl::sendJobStateEvent(RecordingJobSP recordingJob, tt__PropertyOperation propOperation) const
    {
        if (!GetSubscriptionManager())
        {
            return;
        }

        const auto tt_msg = CreateJobStateEvent(this->soap, recordingJob, propOperation);
        GetSubscriptionManager()->PushMessage(EventsBuildUtil::CreateMessageEvent(this->soap, "tns1:RecordingConfig/JobState", GetCoreSettings()->GetServiceUri() + GetUri(), recordingJob->m_recordingConfig->GetVSource(),
            tt_msg));
    }

    _tt__Message * RecordingBindingServiceImpl::CreateJobStateEvent(struct soap* soap, RecordingJobSP recordingJob, tt__PropertyOperation propOperation)
    {
        _tt__Message * tt_msg = soap_new__tt__Message(soap);

		auto cur_tssTime = SoapProxy::GetCurTime();
        tt_msg->UtcTime = *SoapProxy::convertTime(soap, cur_tssTime);

        tt_msg->PropertyOperation = soap_new_tt__PropertyOperation(soap);
        *tt_msg->PropertyOperation = propOperation;

        tt_msg->Source = soap_new_tt__ItemList(soap);

        simple_item_type prop_vToken;
        prop_vToken.Name = "RecordingJobToken";
        prop_vToken.Value = recordingJob->GetToken();
        tt_msg->Source->SimpleItem.push_back(prop_vToken);

        tt_msg->Data = soap_new_tt__ItemList(soap);

        simple_item_type prop_state;
        prop_state.Name = "State";
        prop_state.Value = recordingJob->m_state;
        tt_msg->Data->SimpleItem.push_back(prop_state);

        element_item_type elementItem;
        elementItem.Name = "Information";

        auto info = SoapProxy::createRecordingJobStateInformation(soap, recordingJob);

        /*soap_dom_element config;
        config.set(info, SOAP_TYPE_tt__RecordingJobStateInformation);
        config.name = "tt:RecordingJobStateInformation";*/

        elementItem.__any.set(info, SOAP_TYPE_tt__RecordingJobStateInformation);
		elementItem.__any.name = "tt:RecordingJobStateInformation";

        tt_msg->Data->ElementItem.push_back(elementItem);

        return tt_msg;
    }

    void RecordingBindingServiceImpl::sendRecordingJobConfigurationEvent(const RecordingJobSP& job, tt__PropertyOperation propOperation) const
    {
        if (!GetSubscriptionManager())
        {
            return;
        }

        auto msg = soap_new_wsnt__NotificationMessageHolderType(this->soap);

        msg->Topic = soap_new_wsnt__TopicExpressionType(this->soap);
        msg->Topic->__any = "tns1:RecordingConfig/RecordingJobConfiguration";
        msg->Topic->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;

        _tt__Message * tt_msg = soap_new__tt__Message(this->soap);

        auto cur_tssTime = SoapProxy::GetCurTime();
        tt_msg->UtcTime = *GetProxy()->convertTime(this->soap, cur_tssTime);

        tt_msg->PropertyOperation = soap_new_tt__PropertyOperation(this->soap);
        *tt_msg->PropertyOperation = propOperation;

        tt_msg->Source = soap_new_tt__ItemList(this->soap);

		GetProxy()->addSimpleItem(&tt_msg->Source->SimpleItem, "RecordingJobToken", job->GetToken());

        tt_msg->Data = soap_new_tt__ItemList(this->soap);

        element_item_type elementItem;
        elementItem.Name = "Configuration";

        auto info = GetProxy()->CreateRecordingJobConfiguration(this->soap, job);

        /*soap_dom_element config;
        config.set(info, SOAP_TYPE_tt__RecordingJobConfiguration);
        config.name = "tt:RecordingJobConfiguration";*/

        elementItem.__any.set(info, SOAP_TYPE_tt__RecordingJobConfiguration);
		elementItem.__any.name = "tt:RecordingJobConfiguration";

        tt_msg->Data->ElementItem.push_back(elementItem);

        msg->Message.__any.set(tt_msg, SOAP_TYPE__tt__Message);

        auto evt = std::make_shared<MessageHolderType>(msg, job->m_recordingConfig->GetVSource());
        GetSubscriptionManager()->PushMessage(evt);
    }

    soap_dom_element RecordingBindingServiceImpl::createRecordingConfigEventsDescription(struct soap* soap)
    {
        auto recConfigDescr = soap_new_tns1__RecordingConfigEventDescription(soap);
        recConfigDescr->RecordingConfig.__anyAttribute.name = "wstop:topic";
        recConfigDescr->RecordingConfig.__anyAttribute.text = soap_bool2s(soap, true);

        //tns1:RecordingConfig/RecordingConfiguration 
        recConfigDescr->RecordingConfig.RecordingConfiguration.__anyAttribute.name = "wstop:topic";
        recConfigDescr->RecordingConfig.RecordingConfiguration.__anyAttribute.text = soap_bool2s(soap, true);

        {
            tt__MessageDescription * _MessageDescription_RecEvt = soap_new_tt__MessageDescription(soap);
            _MessageDescription_RecEvt->IsProperty = OnvifUtil::BoolValue(soap, false);

            _MessageDescription_RecEvt->Source = soap_new_tt__ItemListDescription(soap);

            simple_item_description_type sourceDescr;
            sourceDescr.Name = "RecordingToken";
            sourceDescr.Type = "tt:RecordingReference";
            _MessageDescription_RecEvt->Source->SimpleItemDescription.push_back(sourceDescr);

            _MessageDescription_RecEvt->Data = soap_new_tt__ItemListDescription(soap);

            elent_item_description_type elemDescr;
            elemDescr.Name = "Configuration";
            elemDescr.Type = "tt:RecordingConfiguration";
            _MessageDescription_RecEvt->Data->ElementItemDescription.push_back(elemDescr);

            recConfigDescr->RecordingConfig.RecordingConfiguration.MessageDescription = _MessageDescription_RecEvt;
        }

        //tns1:RecordingConfig/TrackConfiguration 
        recConfigDescr->RecordingConfig.TrackConfiguration.__anyAttribute.name = "wstop:topic";
        recConfigDescr->RecordingConfig.TrackConfiguration.__anyAttribute.text = soap_bool2s(soap, true);

        {
            tt__MessageDescription * _MessageDescription_TrackEvt = soap_new_tt__MessageDescription(soap);
            _MessageDescription_TrackEvt->IsProperty = OnvifUtil::BoolValue(soap, false);

            _MessageDescription_TrackEvt->Source = soap_new_tt__ItemListDescription(soap);

            simple_item_description_type sourceDescr;
            sourceDescr.Name = "RecordingToken";
            sourceDescr.Type = "tt:RecordingReference";
            _MessageDescription_TrackEvt->Source->SimpleItemDescription.push_back(sourceDescr);

            simple_item_description_type trackTokenDescr;
            trackTokenDescr.Name = "TrackToken";
            trackTokenDescr.Type = "tt:TrackReference";
            _MessageDescription_TrackEvt->Source->SimpleItemDescription.push_back(trackTokenDescr);

            _MessageDescription_TrackEvt->Data = soap_new_tt__ItemListDescription(soap);

            elent_item_description_type elemDescr;
            elemDescr.Name = "Configuration";
            elemDescr.Type = "tt:TrackConfiguration";
            _MessageDescription_TrackEvt->Data->ElementItemDescription.push_back(elemDescr);

            recConfigDescr->RecordingConfig.TrackConfiguration.MessageDescription = _MessageDescription_TrackEvt;
        }

        //tns1:RecordingConfig/JobState 
        recConfigDescr->RecordingConfig.JobState.__anyAttribute.name = "wstop:topic";
        recConfigDescr->RecordingConfig.JobState.__anyAttribute.text = soap_bool2s(soap, true);

        {
            tt__MessageDescription * _MessageDescription_TrackEvt = soap_new_tt__MessageDescription(soap);
            _MessageDescription_TrackEvt->IsProperty = OnvifUtil::BoolValue(soap, true);

            _MessageDescription_TrackEvt->Source = soap_new_tt__ItemListDescription(soap);

            simple_item_description_type sourceDescr;
            sourceDescr.Name = "RecordingJobToken";
            sourceDescr.Type = "tt:RecordingJobReference";
            _MessageDescription_TrackEvt->Source->SimpleItemDescription.push_back(sourceDescr);

            _MessageDescription_TrackEvt->Data = soap_new_tt__ItemListDescription(soap);

            simple_item_description_type stateDescr;
            stateDescr.Name = "State";
            stateDescr.Type = "xsd:string";
            _MessageDescription_TrackEvt->Data->SimpleItemDescription.push_back(stateDescr);

            elent_item_description_type elemDescr;
            elemDescr.Name = "Information";
            elemDescr.Type = "tt:RecordingJobStateInformation";
            _MessageDescription_TrackEvt->Data->ElementItemDescription.push_back(elemDescr);

            recConfigDescr->RecordingConfig.JobState.MessageDescription = _MessageDescription_TrackEvt;
        }

        //tns1:RecordingConfig/RecordingJobConfiguration 
        recConfigDescr->RecordingConfig.RecordingJobConfiguration.__anyAttribute.name = "wstop:topic";
        recConfigDescr->RecordingConfig.RecordingJobConfiguration.__anyAttribute.text = soap_bool2s(soap, true);

        {
            tt__MessageDescription * _MessageDescription_TrackEvt = soap_new_tt__MessageDescription(soap);
            _MessageDescription_TrackEvt->IsProperty = OnvifUtil::BoolValue(soap, false);

            _MessageDescription_TrackEvt->Source = soap_new_tt__ItemListDescription(soap);

            simple_item_description_type sourceDescr;
            sourceDescr.Name = "RecordingJobToken";
            sourceDescr.Type = "tt:RecordingJobReference";
            _MessageDescription_TrackEvt->Source->SimpleItemDescription.push_back(sourceDescr);

            _MessageDescription_TrackEvt->Data = soap_new_tt__ItemListDescription(soap);

            elent_item_description_type elemDescr;
            elemDescr.Name = "Configuration";
            elemDescr.Type = "tt:RecordingJobConfiguration";
            _MessageDescription_TrackEvt->Data->ElementItemDescription.push_back(elemDescr);

            recConfigDescr->RecordingConfig.RecordingJobConfiguration.MessageDescription = _MessageDescription_TrackEvt;
        }

        soap_dom_element msgDescr;
        msgDescr.set(recConfigDescr, SOAP_TYPE_tns1__RecordingConfigEventDescription);

        return msgDescr;
    }
}

