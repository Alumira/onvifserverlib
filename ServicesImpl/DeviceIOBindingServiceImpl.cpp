#include "DeviceIOBindingServiceImpl.h"
#include "onvif_generated_H.h"
#include "Media2BindingServiceImpl.h"
#include <SoapProxy/EventsBuildUtil.h>

#include <boost/foreach.hpp>

namespace onvif_generated_ {
    DeviceIOBindingServiceImpl::DeviceIOBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data) :
            DeviceIOBindingService(soapObj->GetSoap()), IServiceImpl(data)
	{
	}

    int DeviceIOBindingServiceImpl::GetServiceCapabilities(_tmd__GetServiceCapabilities* tmd__GetServiceCapabilities, _tmd__GetServiceCapabilitiesResponse& tmd__GetServiceCapabilitiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }

        tmd__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities(this->soap, GetConfiguration());
        return SOAP_OK;
    }

	int DeviceIOBindingServiceImpl::GetRelayOutputOptions(_tmd__GetRelayOutputOptions* tmd__GetRelayOutputOptions, _tmd__GetRelayOutputOptionsResponse& tmd__GetRelayOutputOptionsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
		{
			return auth_result;
		}

		BOOST_FOREACH(auto relay,GetConfiguration()->GetRelaysConfigurations())
		{
			if(tmd__GetRelayOutputOptions->RelayOutputToken && *tmd__GetRelayOutputOptions->RelayOutputToken != relay->GetToken())
			{
				continue;
			}

			auto options = soap_new_tmd__RelayOutputOptions(this->soap);
			options->token = relay->GetToken();
			options->Mode.push_back(tt__RelayMode__Bistable);
			options->DelayTimes = OnvifUtil::StringValue(this->soap, "0 10");
			
			tmd__GetRelayOutputOptionsResponse.RelayOutputOptions.push_back(options);
		}

		return SOAP_OK;
	}

	int DeviceIOBindingServiceImpl::GetAudioSources(tmd__Get* tmd__GetAudioSources, tmd__GetResponse& tmd__GetAudioSourcesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        {
            ReadLock_t l(GetConfiguration()->getLock());

            BOOST_FOREACH(auto src,GetConfiguration()->GetASources())
            {
                tmd__GetAudioSourcesResponse.Token.push_back(src->GetToken());
            }
        }

        return SOAP_OK;
    }

	int DeviceIOBindingServiceImpl::GetAudioOutputs(tmd__Get* tmd__GetAudioOutputs,
		tmd__GetResponse& tmd__GetAudioOutputsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:AudioOutputNotSupported", nullptr, true);
		return SOAP_FAULT;
	}

	int DeviceIOBindingServiceImpl::GetVideoSources(tmd__Get* tmd__GetVideoSources, tmd__GetResponse& tmd__GetVideoSourcesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        BOOST_FOREACH(auto src, GetConfiguration()->GetVSourceConfigurations())
        {
            updateEncoderParams(src.second);
        }

        ReadLock_t l(GetConfiguration()->getLock());
        BOOST_FOREACH(auto src, GetConfiguration()->GetVSourceConfigurations())
        {
            if(checkVideoSourceCodec(src.second))
            {
				tmd__GetVideoSourcesResponse.Token.push_back(src.second->videoSource->GetToken());
            }
        }

        return SOAP_OK;
    }

	int DeviceIOBindingServiceImpl::GetVideoOutputs(_tmd__GetVideoOutputs* tmd__GetVideoOutputs,
		_tmd__GetVideoOutputsResponse& tmd__GetVideoOutputsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		return SOAP_OK;
	}

	int DeviceIOBindingServiceImpl::GetVideoSourceConfiguration(_tmd__GetVideoSourceConfiguration* tmd__GetVideoSourceConfiguration, _tmd__GetVideoSourceConfigurationResponse& tmd__GetVideoSourceConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto videoSourceCfg = GetConfiguration()->FindVideoSourceConfiguration(tmd__GetVideoSourceConfiguration->VideoSourceToken);

        if (!videoSourceCfg)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        tmd__GetVideoSourceConfigurationResponse.VideoSourceConfiguration = GetProxy()->createVideoSourceConfiguration(soap, videoSourceCfg);
        return SOAP_OK;
    }

	int DeviceIOBindingServiceImpl::GetVideoOutputConfiguration(
		_tmd__GetVideoOutputConfiguration* tmd__GetVideoOutputConfiguration,
		_tmd__GetVideoOutputConfigurationResponse& tmd__GetVideoOutputConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoVideoOutput");
		return SOAP_FAULT;
	}

	int DeviceIOBindingServiceImpl::GetAudioSourceConfiguration(_tmd__GetAudioSourceConfiguration* tmd__GetAudioSourceConfiguration, _tmd__GetAudioSourceConfigurationResponse& tmd__GetAudioSourceConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto audioSourceCfg = GetConfiguration()->FindAudioSourceConfiguration(tmd__GetAudioSourceConfiguration->AudioSourceToken);

        if (!audioSourceCfg)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        tmd__GetAudioSourceConfigurationResponse.AudioSourceConfiguration = GetProxy()->createAudioSourceConfiguration(soap, audioSourceCfg);
        return SOAP_OK;
    }

	int DeviceIOBindingServiceImpl::GetAudioOutputConfiguration(
		_tmd__GetAudioOutputConfiguration* tmd__GetAudioOutputConfiguration,
		_tmd__GetAudioOutputConfigurationResponse& tmd__GetAudioOutputConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoAudioOutput");
		return SOAP_FAULT;
	}

	int DeviceIOBindingServiceImpl::SetVideoSourceConfiguration(
		_tmd__SetVideoSourceConfiguration* tmd__SetVideoSourceConfiguration,
		_tmd__SetVideoSourceConfigurationResponse& tmd__SetVideoSourceConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		VideoSourceConfigurationSP config;
		if (tmd__SetVideoSourceConfiguration->Configuration != nullptr)
		{
			config = GetConfiguration()->FindVideoSourceConfiguration(tmd__SetVideoSourceConfiguration->Configuration->token);
			if (!config)
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
				return SOAP_FAULT;
			}
		}

		tt__IntRectangle *bounds = tmd__SetVideoSourceConfiguration->Configuration->Bounds;
		if (bounds)
		{
			if (!config->videoSource->m_bounds->CompareBounds(bounds->width, bounds->height, bounds->x, bounds->y))
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
				return SOAP_FAULT;
			}
		}

		if (!tmd__SetVideoSourceConfiguration->Configuration->Name.empty())
		{
			config->videoSource->camName = tmd__SetVideoSourceConfiguration->Configuration->Name;
		}

		auto evt = Media2BindingServiceImpl::CreateConfigurationChangedEvent(this->soap, config->GetToken(), soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__VideoSource));
		GetSubscriptionManager()->PushMessage(evt);

		return SOAP_OK;
	}

	int DeviceIOBindingServiceImpl::SetVideoOutputConfiguration(
		_tmd__SetVideoOutputConfiguration* tmd__SetVideoOutputConfiguration,
		_tmd__SetVideoOutputConfigurationResponse& tmd__SetVideoOutputConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoVideoOutput");
		return SOAP_FAULT;
	}

	int DeviceIOBindingServiceImpl::SetAudioSourceConfiguration(
		_tmd__SetAudioSourceConfiguration* tmd__SetAudioSourceConfiguration,
		_tmd__SetAudioSourceConfigurationResponse& tmd__SetAudioSourceConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		AudioSourceSP aSource;
		if (tmd__SetAudioSourceConfiguration->Configuration != nullptr)
		{
			aSource = GetConfiguration()->GetASource(tmd__SetAudioSourceConfiguration->Configuration->SourceToken);
			if (!aSource)
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
				return SOAP_FAULT;
			}

			auto aSourceConfig = GetConfiguration()->FindAudioSourceConfiguration(tmd__SetAudioSourceConfiguration->Configuration->token);
			if (!tmd__SetAudioSourceConfiguration->Configuration->token.empty() && !aSourceConfig)
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
				return SOAP_FAULT;
			}

			//TODO: send name of object (microphone) to core 
			if (!tmd__SetAudioSourceConfiguration->Configuration->Name.empty())
			{
				aSource->SetName(tmd__SetAudioSourceConfiguration->Configuration->Name);
			}

			auto evt = Media2BindingServiceImpl::CreateConfigurationChangedEvent(this->soap, aSourceConfig->GetToken(), soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__AudioSource));
			GetSubscriptionManager()->PushMessage(evt);
		}

		return SOAP_OK;
	}

	int DeviceIOBindingServiceImpl::SetAudioOutputConfiguration(
		_tmd__SetAudioOutputConfiguration* tmd__SetAudioOutputConfiguration,
		_tmd__SetAudioOutputConfigurationResponse& tmd__SetAudioOutputConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoAudioOutput");
		return SOAP_FAULT;
	}

	int DeviceIOBindingServiceImpl::GetVideoSourceConfigurationOptions(_tmd__GetVideoSourceConfigurationOptions* tmd__GetVideoSourceConfigurationOptions, _tmd__GetVideoSourceConfigurationOptionsResponse& tmd__GetVideoSourceConfigurationOptionsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        VideoSourceConfigurationSP config;
        if (!tmd__GetVideoSourceConfigurationOptions->VideoSourceToken.empty())
        {
            config = GetConfiguration()->FindVideoSourceConfiguration(tmd__GetVideoSourceConfigurationOptions->VideoSourceToken);
            if (!config)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }
        }

        if (config == nullptr)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        tmd__GetVideoSourceConfigurationOptionsResponse.VideoSourceConfigurationOptions = soap_new_tt__VideoSourceConfigurationOptions(this->soap);
        tmd__GetVideoSourceConfigurationOptionsResponse.VideoSourceConfigurationOptions->VideoSourceTokensAvailable.push_back(config->videoSource->GetToken());

        tmd__GetVideoSourceConfigurationOptionsResponse.VideoSourceConfigurationOptions->BoundsRange = soap_new_tt__IntRectangleRange(this->soap);

        BoundsRangeSP bounds = config->videoSource->m_bounds;
        if (!bounds)
        {
            bounds = BoundsRangeSP(new BoundsRange(0, 0));
        }

        tmd__GetVideoSourceConfigurationOptionsResponse.VideoSourceConfigurationOptions->BoundsRange->WidthRange = soap_new_set_tt__IntRange(this->soap, bounds->_widthRange.first, bounds->_widthRange.second);
        tmd__GetVideoSourceConfigurationOptionsResponse.VideoSourceConfigurationOptions->BoundsRange->HeightRange = soap_new_set_tt__IntRange(this->soap, bounds->_heightRange.first, bounds->_heightRange.second);
        tmd__GetVideoSourceConfigurationOptionsResponse.VideoSourceConfigurationOptions->BoundsRange->XRange = soap_new_set_tt__IntRange(this->soap, bounds->_xRange.first, bounds->_xRange.second);
        tmd__GetVideoSourceConfigurationOptionsResponse.VideoSourceConfigurationOptions->BoundsRange->YRange = soap_new_set_tt__IntRange(this->soap, bounds->_yRange.first, bounds->_yRange.second);

        return SOAP_OK;
    }

	int DeviceIOBindingServiceImpl::GetVideoOutputConfigurationOptions(
		_tmd__GetVideoOutputConfigurationOptions* tmd__GetVideoOutputConfigurationOptions,
		_tmd__GetVideoOutputConfigurationOptionsResponse& tmd__GetVideoOutputConfigurationOptionsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoVideoOutput");
		return SOAP_FAULT;
	}

	int DeviceIOBindingServiceImpl::GetAudioSourceConfigurationOptions(_tmd__GetAudioSourceConfigurationOptions* tmd__GetAudioSourceConfigurationOptions, _tmd__GetAudioSourceConfigurationOptionsResponse& tmd__GetAudioSourceConfigurationOptionsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        AudioSourceConfigurationSP config;
        if (!tmd__GetAudioSourceConfigurationOptions->AudioSourceToken.empty())
        {
            config = GetConfiguration()->FindAudioSourceConfiguration(tmd__GetAudioSourceConfigurationOptions->AudioSourceToken);
            if (!config)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }
        }

        tmd__GetAudioSourceConfigurationOptionsResponse.AudioSourceOptions = soap_new_tt__AudioSourceConfigurationOptions(this->soap);

        BOOST_FOREACH(auto aSourceConfig,GetConfiguration()->GetASources())
        {
            tmd__GetAudioSourceConfigurationOptionsResponse.AudioSourceOptions->InputTokensAvailable.push_back(aSourceConfig->GetToken());
        }

        return SOAP_OK;
    }

	int DeviceIOBindingServiceImpl::GetAudioOutputConfigurationOptions(
		_tmd__GetAudioOutputConfigurationOptions* tmd__GetAudioOutputConfigurationOptions,
		_tmd__GetAudioOutputConfigurationOptionsResponse& tmd__GetAudioOutputConfigurationOptionsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoAudioOutput");
		return SOAP_FAULT;
	}

	int DeviceIOBindingServiceImpl::GetRelayOutputs(_tds__GetRelayOutputs* tds__GetRelayOutputs, _tds__GetRelayOutputsResponse& tds__GetRelayOutputsResponse)
    {
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		ReadLock_t l(GetConfiguration()->getLock());

		BOOST_FOREACH(auto relayConf,GetConfiguration()->GetRelaysConfigurations())
		{
			tds__GetRelayOutputsResponse.RelayOutputs.push_back(GetProxy()->createRelayOutput(this->soap, relayConf));
		}

		return SOAP_OK;
    }

	int DeviceIOBindingServiceImpl::SetRelayOutputSettings(_tmd__SetRelayOutputSettings* tmd__SetRelayOutputSettings, _tmd__SetRelayOutputSettingsResponse& tmd__SetRelayOutputSettingsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		if(!tmd__SetRelayOutputSettings->RelayOutput)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		auto conf = GetConfiguration()->findRelayConfiguration(tmd__SetRelayOutputSettings->RelayOutput->token);

		if (!conf)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

	    if(tmd__SetRelayOutputSettings->RelayOutput->Properties->Mode == tt__RelayMode__Monostable)
	    {
			conf->SetMode(RelayOutput::Monostable);
	    }
		else if(tmd__SetRelayOutputSettings->RelayOutput->Properties->Mode == tt__RelayMode__Bistable)
		{
			conf->SetMode(RelayOutput::Bistable);
		}

		if(tmd__SetRelayOutputSettings->RelayOutput->Properties->IdleState == tt__RelayIdleState__open)
		{
			conf->SetIdleState(RelayOutput::on);
		}
		else if(tmd__SetRelayOutputSettings->RelayOutput->Properties->IdleState == tt__RelayIdleState__closed)
		{
			conf->SetIdleState(RelayOutput::off);
		}

		conf->SetDelayTime(tmd__SetRelayOutputSettings->RelayOutput->Properties->DelayTime);

		return SOAP_OK;
	}

	int DeviceIOBindingServiceImpl::SetRelayOutputState(_tds__SetRelayOutputState* tds__SetRelayOutputState, _tds__SetRelayOutputStateResponse& tds__SetRelayOutputStateResponse)
	{
		std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);

    	if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto conf = GetConfiguration()->findRelayConfiguration(tds__SetRelayOutputState->RelayOutputToken);

		if (!conf)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		IIOProcessor::ActionCommand action = tds__SetRelayOutputState->LogicalState == tt__RelayLogicalState__inactive ? IIOProcessor::action_off : IIOProcessor::action_on;

		auto releProc = GetProcessorFactory()->createIOProcessor(user_id);
		releProc->ReleCommand(conf, action);

		return SOAP_OK;
	}

	int DeviceIOBindingServiceImpl::GetDigitalInputs(_tmd__GetDigitalInputs* tmd__GetDigitalInputs, _tmd__GetDigitalInputsResponse& tmd__GetDigitalInputsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		ReadLock_t l(GetConfiguration()->getLock());

		BOOST_FOREACH(auto rayConf,GetConfiguration()->GetRaysConfigurations())
		{
			tmd__GetDigitalInputsResponse.DigitalInputs.push_back(GetProxy()->createRayOutput(this->soap, rayConf));
		}

		return SOAP_OK;
	}

	int DeviceIOBindingServiceImpl::GetDigitalInputConfigurationOptions(_tmd__GetDigitalInputConfigurationOptions* tmd__GetDigitalInputConfigurationOptions, _tmd__GetDigitalInputConfigurationOptionsResponse& tmd__GetDigitalInputConfigurationOptionsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
		{
			return auth_result;
		}

		if(tmd__GetDigitalInputConfigurationOptions->Token)
		{
			auto conf = GetConfiguration()->findRayConfiguration(*tmd__GetDigitalInputConfigurationOptions->Token);

			if(!conf)
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
				return SOAP_FAULT;
			}
		}

		tmd__GetDigitalInputConfigurationOptionsResponse.DigitalInputOptions = soap_new_tmd__DigitalInputConfigurationOptions(this->soap);
		tmd__GetDigitalInputConfigurationOptionsResponse.DigitalInputOptions->IdleState.push_back(tt__DigitalIdleState__open);
		tmd__GetDigitalInputConfigurationOptionsResponse.DigitalInputOptions->IdleState.push_back(tt__DigitalIdleState__closed);

		return SOAP_OK;
	}

	int DeviceIOBindingServiceImpl::SetDigitalInputConfigurations(_tmd__SetDigitalInputConfigurations* tmd__SetDigitalInputConfigurations, _tmd__SetDigitalInputConfigurationsResponse& tmd__SetDigitalInputConfigurationsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		BOOST_FOREACH(auto dInput, tmd__SetDigitalInputConfigurations->DigitalInputs)
		{
			auto conf = GetConfiguration()->findRayConfiguration(dInput->token);

			if (!conf)
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
				return SOAP_FAULT;
			}

			RayOutputSP newRayOutput(new RayOutput(*conf.get()));
			newRayOutput->SetIdleState(*dInput->IdleState == tt__DigitalIdleState__closed ? 0 : 1);

			GetProcessorFactory()->createUpdateRayProcessor(newRayOutput)->DoWork();
		}

		return SOAP_OK;
	}

	_tmd__Capabilities* DeviceIOBindingServiceImpl::GetServiceCapabilities(struct soap* soapObj, OnvifDeviceConfigurationSP deviceConfig)
    {
        auto serviceCaps = soap_new__tmd__Capabilities(soapObj);
        serviceCaps->AudioOutputs = 0;
        serviceCaps->AudioSources = deviceConfig->GetASources().size();
        serviceCaps->DigitalInputOptions = false;
        serviceCaps->DigitalInputs = deviceConfig->GetRaysConfigurations().size();
        serviceCaps->RelayOutputs = deviceConfig->GetRelaysConfigurations().size();
        serviceCaps->SerialPorts = 0;
        serviceCaps->VideoOutputs = 0;
        serviceCaps->VideoSources = deviceConfig->GetVSources().size();

        return serviceCaps;
    }

    void DeviceIOBindingServiceImpl::updateEncoderParams(VideoSourceConfigurationSP vSourceConfig)
    {
        if (!vSourceConfig)
        {
            return;
        }

        auto it = std::find_if(vSourceConfig->m_encoders.begin(), vSourceConfig->m_encoders.end(),
            [](const std::pair<std::string, EncoderConfigurationSP>& enc) { return enc.second->NeedsUpdate(); });

        if (it != vSourceConfig->m_encoders.end())
        {
			GetProcessorFactory()->createGetStreamParamsProcessor(vSourceConfig->videoSource)->DoWork();
        }
    }

    bool DeviceIOBindingServiceImpl::checkVideoSourceCodec(const VideoSourceConfigurationSP& vSourceConfig) const
    {
    	BOOST_FOREACH(auto vEncoder, vSourceConfig->m_encoders)
        {
            if ((GetCoreSettings()->IsVersion2() && Media2BindingServiceImpl::checkCodec(vEncoder.second->codec))
                || !GetCoreSettings()->IsVersion2())
            {
                return true;
            }
        }

        return false;
    }

	MessageHolderTypeSP DeviceIOBindingServiceImpl::CreateRelayEvent(struct soap* soap, RelayOutputSP relayConfig, tt__PropertyOperation propOperation)
	{
		auto msg = soap_new_wsnt__NotificationMessageHolderType(soap);

		msg->Topic = soap_new_wsnt__TopicExpressionType(soap);
		msg->Topic->__any = "tns1:Device/Trigger/Relay";
		msg->Topic->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;

		_tt__Message * tt_msg = soap_new__tt__Message(soap);

		auto cur_tssTime = SoapProxy::GetCurTime();
		tt_msg->UtcTime = *SoapProxy::convertTime(soap, cur_tssTime);

		tt_msg->PropertyOperation = soap_new_tt__PropertyOperation(soap);
		*tt_msg->PropertyOperation = propOperation;

		tt_msg->Source = soap_new_tt__ItemList(soap);

        simple_item_type prop_vToken;
		prop_vToken.Name = "RelayToken";
		prop_vToken.Value = relayConfig->GetToken();
		tt_msg->Source->SimpleItem.push_back(prop_vToken);

		tt_msg->Data = soap_new_tt__ItemList(soap);

        simple_item_type prop_state;
		prop_state.Name = "LogicalState";

		if(relayConfig->GetState() == RelayOutput::on)
		{
			prop_state.Value = soap_tt__RelayLogicalState2s(soap, tt__RelayLogicalState__active);
		}
		else if(relayConfig->GetState() == RelayOutput::off)
		{
			prop_state.Value = soap_tt__RelayLogicalState2s(soap, tt__RelayLogicalState__inactive);
		}
		
		tt_msg->Data->SimpleItem.push_back(prop_state);

		msg->Message.__any.set(tt_msg, SOAP_TYPE__tt__Message);

		return std::make_shared<MessageHolderType>(msg, nullptr);
	}

	MessageHolderTypeSP DeviceIOBindingServiceImpl::CreateRayEvent(struct soap* soap, RayOutputSP rayConfig, tt__PropertyOperation propOperation)
	{
		auto msg = soap_new_wsnt__NotificationMessageHolderType(soap);

		msg->Topic = soap_new_wsnt__TopicExpressionType(soap);
		msg->Topic->__any = "tns1:Device/Trigger/DigitalInput";
		msg->Topic->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;

		_tt__Message * tt_msg = soap_new__tt__Message(soap);

		auto cur_tssTime = SoapProxy::GetCurTime();
		tt_msg->UtcTime = *SoapProxy::convertTime(soap, cur_tssTime);

		tt_msg->PropertyOperation = soap_new_tt__PropertyOperation(soap);
		*tt_msg->PropertyOperation = propOperation;

		tt_msg->Source = soap_new_tt__ItemList(soap);

        simple_item_type prop_vToken;
		prop_vToken.Name = "InputToken";
		prop_vToken.Value = rayConfig->GetToken();
		tt_msg->Source->SimpleItem.push_back(prop_vToken);

		tt_msg->Data = soap_new_tt__ItemList(soap);

        simple_item_type prop_state;
		prop_state.Name = "LogicalState";

		prop_state.Value = soap_bool2s(soap, rayConfig->GetCurrentState());

		tt_msg->Data->SimpleItem.push_back(prop_state);

		msg->Message.__any.set(tt_msg, SOAP_TYPE__tt__Message);

		return std::make_shared<MessageHolderType>(msg, nullptr);
	}

	soap_dom_element DeviceIOBindingServiceImpl::createRelayEventsDescription(struct soap* soap)
	{
		tt__MessageDescription * _MessageDescription_RelayEvt = soap_new_tt__MessageDescription(soap);
		_MessageDescription_RelayEvt->IsProperty = OnvifUtil::BoolValue(soap, true);

		_MessageDescription_RelayEvt->Source = soap_new_tt__ItemListDescription(soap);

        simple_item_description_type sourceDescr;
		sourceDescr.Name = "RelayToken";
		sourceDescr.Type = "tt:ReferenceToken";
		_MessageDescription_RelayEvt->Source->SimpleItemDescription.push_back(sourceDescr);

		_MessageDescription_RelayEvt->Data = soap_new_tt__ItemListDescription(soap);

        simple_item_description_type elemDescr;
		elemDescr.Name = "LogicalState";
		elemDescr.Type = "tt:RelayLogicalState";
		_MessageDescription_RelayEvt->Data->SimpleItemDescription.push_back(elemDescr);

		soap_dom_element msgDescr(soap);
		msgDescr.set(_MessageDescription_RelayEvt, SOAP_TYPE_tt__MessageDescription);
		msgDescr.name = "tt:MessageDescription";

		soap_dom_element relayEvtDescrRelayEl = EventsBuildUtil::AddHolder(soap, msgDescr, "Relay", true);
		soap_dom_element relayEvtDescrTriggerEl = EventsBuildUtil::AddHolder(soap, relayEvtDescrRelayEl, "Trigger", false);
		soap_dom_element relayEvtDescrDeviceEl = EventsBuildUtil::AddHolder(soap, relayEvtDescrTriggerEl, "tns1:Device", false);

		return relayEvtDescrDeviceEl;
	}

	soap_dom_element DeviceIOBindingServiceImpl::createRayEventsDescription(struct soap* soap)
	{
		tt__MessageDescription * _MessageDescription_RelayEvt = soap_new_tt__MessageDescription(soap);
		_MessageDescription_RelayEvt->IsProperty = OnvifUtil::BoolValue(soap, true);

		_MessageDescription_RelayEvt->Source = soap_new_tt__ItemListDescription(soap);

        simple_item_description_type sourceDescr;
		sourceDescr.Name = "InputToken";
		sourceDescr.Type = "tt:ReferenceToken";
		_MessageDescription_RelayEvt->Source->SimpleItemDescription.push_back(sourceDescr);

		_MessageDescription_RelayEvt->Data = soap_new_tt__ItemListDescription(soap);

        simple_item_description_type elemDescr;
		elemDescr.Name = "LogicalState";
		elemDescr.Type = "xsd:boolean";
		_MessageDescription_RelayEvt->Data->SimpleItemDescription.push_back(elemDescr);

		soap_dom_element msgDescr(soap);
		msgDescr.set(_MessageDescription_RelayEvt, SOAP_TYPE_tt__MessageDescription);
		msgDescr.name = "tt:MessageDescription";

		soap_dom_element relayEvtDescrRelayEl = EventsBuildUtil::AddHolder(soap, msgDescr, "DigitalInput", true);
		soap_dom_element relayEvtDescrTriggerEl = EventsBuildUtil::AddHolder(soap, relayEvtDescrRelayEl, "Trigger", false);
		soap_dom_element relayEvtDescrDeviceEl = EventsBuildUtil::AddHolder(soap, relayEvtDescrTriggerEl, "tns1:Device", false);

		return relayEvtDescrDeviceEl;
	}
}

