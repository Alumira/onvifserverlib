#include "ImagingBindingServiceImpl.h"

namespace onvif_generated_ {
    ImagingBindingServiceImpl::ImagingBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data) :
        ImagingBindingService(soapObj->GetSoap()), IServiceImpl(data)
	{
	}

    int ImagingBindingServiceImpl::GetServiceCapabilities(_timg2__GetServiceCapabilities* timg2__GetServiceCapabilities, _timg2__GetServiceCapabilitiesResponse& timg2__GetServiceCapabilitiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }
        
        timg2__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities(this->soap);
        return SOAP_OK;
    }

    int ImagingBindingServiceImpl::GetImagingSettings(_timg2__GetImagingSettings* timg2__GetImagingSettings, _timg2__GetImagingSettingsResponse& timg2__GetImagingSettingsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto vSource = GetConfiguration()->GetVSource(timg2__GetImagingSettings->VideoSourceToken);

        if(!vSource)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoSource");
            return SOAP_FAULT;
        }

        timg2__GetImagingSettingsResponse.ImagingSettings = GetProxy()->CreateImagingSettings20(this->soap, vSource);

        return SOAP_OK;
    }

    int ImagingBindingServiceImpl::SetImagingSettings(_timg2__SetImagingSettings* timg2__SetImagingSettings, _timg2__SetImagingSettingsResponse& timg2__SetImagingSettingsResponse)
    {
        std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);
        
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto vSource = GetConfiguration()->GetVSource(timg2__SetImagingSettings->VideoSourceToken);

        if (!vSource)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoSource");
            return SOAP_FAULT;
        }

        if(timg2__SetImagingSettings->ImagingSettings->BacklightCompensation)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:SettingsInvalid");
            return SOAP_FAULT;
        }

		VideoSourceSP newVSource(new VideoSource(*vSource.get()));
		bool changed = false;

		if(timg2__SetImagingSettings->ImagingSettings->Brightness 
			&& *timg2__SetImagingSettings->ImagingSettings->Brightness != newVSource->GetBrightness())
		{
			if(newVSource->SetBrightness(*timg2__SetImagingSettings->ImagingSettings->Brightness))
			{
				changed = true;
			}
			else
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:SettingsInvalid");
				return SOAP_FAULT;
			}
		}

		if (timg2__SetImagingSettings->ImagingSettings->Contrast
			&& *timg2__SetImagingSettings->ImagingSettings->Contrast != newVSource->GetContrast())
		{
			if (newVSource->SetContrast(*timg2__SetImagingSettings->ImagingSettings->Contrast))
			{
				changed = true;
			}
			else
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:SettingsInvalid");
				return SOAP_FAULT;
			}
		}

		if (timg2__SetImagingSettings->ImagingSettings->ColorSaturation
			&& *timg2__SetImagingSettings->ImagingSettings->ColorSaturation != newVSource->GetColorSaturation())
		{
			if (newVSource->SetColorSaturation(*timg2__SetImagingSettings->ImagingSettings->ColorSaturation))
			{
				changed = true;
			}
			else
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:SettingsInvalid");
				return SOAP_FAULT;
			}
		}

		if(changed)
		{
			GetProcessorFactory()->createUpdateImagingSettingsProcessor(newVSource)->DoWork();
		}

        if(timg2__SetImagingSettings->ImagingSettings->Focus)
        {
            vSource->SetAutoFocus(timg2__SetImagingSettings->ImagingSettings->Focus->AutoFocusMode == tt__AutoFocusMode__AUTO);

            if(!vSource->IsAutoFocusOn() 
                && ((timg2__SetImagingSettings->ImagingSettings->Focus->FarLimit && *timg2__SetImagingSettings->ImagingSettings->Focus->FarLimit != 1.0)
                || (timg2__SetImagingSettings->ImagingSettings->Focus->NearLimit && *timg2__SetImagingSettings->ImagingSettings->Focus->NearLimit != 0.0)))
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:SettingsInvalid");
                return SOAP_FAULT;
            }

            if(vSource->IsAutoFocusOn())
            {
                auto vSourceConfig = GetConfiguration()->FindVideoSourceConfigurationByCamId(vSource->camId);
                if (!vSourceConfig || !vSourceConfig->getPtzConfig())
                {
                    //OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:NoImagingForSource");
                    return SOAP_OK;
                }
                
				auto imagingProc = GetProcessorFactory()->createImagingProcessor(vSource, user_id);
				imagingProc->FocusMove(IImagingProcessor::action_auto, 0);
            }
        }

        if(timg2__SetImagingSettings->ImagingSettings->Exposure)
        {
            vSource->SetAutoIris(timg2__SetImagingSettings->ImagingSettings->Exposure->Mode == tt__ExposureMode__AUTO);

            if (!vSource->IsAutoIrisOn()
                && ((timg2__SetImagingSettings->ImagingSettings->Exposure->MaxIris && *timg2__SetImagingSettings->ImagingSettings->Exposure->MaxIris != 1.0)
                || (timg2__SetImagingSettings->ImagingSettings->Exposure->MinIris && *timg2__SetImagingSettings->ImagingSettings->Exposure->MinIris != 0.0)))
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:SettingsInvalid");
                return SOAP_FAULT;
            }

            if (vSource->IsAutoIrisOn())
            {
                auto vSourceConfig = GetConfiguration()->FindVideoSourceConfigurationByCamId(vSource->camId);
                if (!vSourceConfig || !vSourceConfig->getPtzConfig())
                {
                    //OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:NoImagingForSource");
                    return SOAP_OK;
                }
                
				auto imagingProc = GetProcessorFactory()->createImagingProcessor(vSource, user_id);
				imagingProc->IrisMove(IImagingProcessor::action_auto, 0);
            }
        }

        return SOAP_OK;
    }

    int ImagingBindingServiceImpl::GetOptions(_timg2__GetOptions* timg2__GetOptions, _timg2__GetOptionsResponse& timg2__GetOptionsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto vSource = GetConfiguration()->GetVSource(timg2__GetOptions->VideoSourceToken);
        if (!vSource)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoSource");
            return SOAP_FAULT;
        }

		timg2__GetOptionsResponse.ImagingOptions = soap_new_tt__ImagingOptions20(this->soap);

		timg2__GetOptionsResponse.ImagingOptions->Brightness = soap_new_set_tt__FloatRange(this->soap, float(BrightnessRangeMin), float(BrightnessRangeMax));
		timg2__GetOptionsResponse.ImagingOptions->Contrast = soap_new_set_tt__FloatRange(this->soap, float(ContrastRangeMin), float(ContrastRangeMax));
		timg2__GetOptionsResponse.ImagingOptions->ColorSaturation = soap_new_set_tt__FloatRange(this->soap, float(ColorSaturationRangeMin), float(ColorSaturationRangeMax));

        auto vSourceConfig = GetConfiguration()->FindVideoSourceConfigurationByCamId(vSource->camId);
        if (!vSourceConfig || !vSourceConfig->getPtzConfig())
        {
            //OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:NoImagingForSource");
            return SOAP_OK;
        }
        
        timg2__GetOptionsResponse.ImagingOptions->Exposure = soap_new_tt__ExposureOptions20(this->soap);
        timg2__GetOptionsResponse.ImagingOptions->Exposure->Mode.push_back(tt__ExposureMode__MANUAL);
        timg2__GetOptionsResponse.ImagingOptions->Exposure->Mode.push_back(tt__ExposureMode__AUTO);
        timg2__GetOptionsResponse.ImagingOptions->Exposure->MinIris = soap_new_set_tt__FloatRange(this->soap, 0, 0);
        timg2__GetOptionsResponse.ImagingOptions->Exposure->MaxIris = soap_new_set_tt__FloatRange(this->soap, 1.0, 1.0);

        timg2__GetOptionsResponse.ImagingOptions->Focus = soap_new_tt__FocusOptions20(this->soap);
        timg2__GetOptionsResponse.ImagingOptions->Focus->AutoFocusModes.push_back(tt__AutoFocusMode__MANUAL);
        timg2__GetOptionsResponse.ImagingOptions->Focus->AutoFocusModes.push_back(tt__AutoFocusMode__AUTO);
        timg2__GetOptionsResponse.ImagingOptions->Focus->NearLimit = soap_new_set_tt__FloatRange(this->soap, 0, 0);
        timg2__GetOptionsResponse.ImagingOptions->Focus->FarLimit = soap_new_set_tt__FloatRange(this->soap, 1.0, 1.0);
		timg2__GetOptionsResponse.ImagingOptions->Focus->DefaultSpeed = soap_new_set_tt__FloatRange(this->soap, 1.0, 1.0);

        return SOAP_OK;
    }

    int ImagingBindingServiceImpl::Move(_timg2__Move* timg2__Move, _timg2__MoveResponse& timg2__MoveResponse)
    {
        std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);

        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto vSource = GetConfiguration()->GetVSource(timg2__Move->VideoSourceToken);

        if (!vSource)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoSource");
            return SOAP_FAULT;
        }

        auto vSourceConfig = GetConfiguration()->FindVideoSourceConfigurationByCamId(vSource->camId);
        if (!vSourceConfig || !vSourceConfig->getPtzConfig())
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:NoImagingForSource");
            return SOAP_FAULT;
        }

		IImagingProcessor::MoveAction _action = IImagingProcessor::action_stop;
        int _speed = 5;

        if(timg2__Move->Focus->Relative)
        {
            if(timg2__Move->Focus->Relative->Distance < -1 || timg2__Move->Focus->Relative->Distance > 1
                || (timg2__Move->Focus->Relative->Speed && *timg2__Move->Focus->Relative->Speed < 1) || (timg2__Move->Focus->Relative->Speed && *timg2__Move->Focus->Relative->Speed > 10))
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:SettingsInvalid");
                return SOAP_FAULT;
            }
            
            if(timg2__Move->Focus->Relative->Distance > 0)
            {
				_action = IImagingProcessor::action_in;
            }
            else if(timg2__Move->Focus->Relative->Distance < 0)
            {
				_action = IImagingProcessor::action_out;
            }

            _speed = timg2__Move->Focus->Relative->Speed ? int(*timg2__Move->Focus->Relative->Speed) : 0;
        }

        if(timg2__Move->Focus->Continuous)
        {
            if (timg2__Move->Focus->Continuous->Speed < -1 || timg2__Move->Focus->Continuous->Speed > 1)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:SettingsInvalid");
                return SOAP_FAULT;
            }
            
            if (timg2__Move->Focus->Continuous->Speed > 0)
            {
				_action = IImagingProcessor::action_in;
            }
            else if (timg2__Move->Focus->Continuous->Speed < 0)
            {
				_action = IImagingProcessor::action_out;
            }

            _speed = abs(int(timg2__Move->Focus->Continuous->Speed * 10));
        }

        if (timg2__Move->Focus->Absolute)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:SettingsInvalid");
            return SOAP_FAULT;
        }

		auto imagingProc = GetProcessorFactory()->createImagingProcessor(vSource, user_id);
		imagingProc->FocusMove(_action, _speed);
    	
        return SOAP_OK;
    }

    int ImagingBindingServiceImpl::Stop(_timg2__Stop* timg2__Stop, _timg2__StopResponse& timg2__StopResponse)
    {
        std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);

        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto vSource = GetConfiguration()->GetVSource(timg2__Stop->VideoSourceToken);

        if (!vSource)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoSource");
            return SOAP_FAULT;
        }

        auto vSourceConfig = GetConfiguration()->FindVideoSourceConfigurationByCamId(vSource->camId);
        if (!vSourceConfig || !vSourceConfig->getPtzConfig())
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:NoImagingForSource");
            return SOAP_FAULT;
        }

		auto imagingProc = GetProcessorFactory()->createImagingProcessor(vSource, user_id);
		imagingProc->FocusMove(IImagingProcessor::action_stop, 0);

        return SOAP_OK;
    }

	int ImagingBindingServiceImpl::GetStatus(_timg2__GetStatus* timg2__GetStatus, _timg2__GetStatusResponse& timg2__GetStatusResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		auto vSource = GetConfiguration()->GetVSource(timg2__GetStatus->VideoSourceToken);

		if (!vSource)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoSource");
			return SOAP_FAULT;
		}

		timg2__GetStatusResponse.Status = soap_new_tt__ImagingStatus20(this->soap);

		auto vSourceConfig = GetConfiguration()->FindVideoSourceConfigurationByCamId(vSource->camId);
		if (!vSourceConfig || !vSourceConfig->getPtzConfig())
		{
			//OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:NoImagingForSource");
			//return SOAP_FAULT;
			return SOAP_OK;
		}
    	
    	//TODO: fill the appropriate values
		timg2__GetStatusResponse.Status->FocusStatus20 = soap_new_tt__FocusStatus20(this->soap);
		timg2__GetStatusResponse.Status->FocusStatus20->MoveStatus = tt__MoveStatus__UNKNOWN;
		timg2__GetStatusResponse.Status->FocusStatus20->Position = 0;

		return SOAP_OK;
	}

	int ImagingBindingServiceImpl::GetMoveOptions(_timg2__GetMoveOptions* timg2__GetMoveOptions, _timg2__GetMoveOptionsResponse& timg2__GetMoveOptionsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto vSource = GetConfiguration()->GetVSource(timg2__GetMoveOptions->VideoSourceToken);

        if (!vSource)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoSource");
            return SOAP_FAULT;
        }

        auto vSourceConfig = GetConfiguration()->FindVideoSourceConfigurationByCamId(vSource->camId);
        if (!vSourceConfig || !vSourceConfig->getPtzConfig())
        {
            timg2__GetMoveOptionsResponse.MoveOptions = soap_new_tt__MoveOptions20(this->soap);
            return SOAP_OK;
        }

        timg2__GetMoveOptionsResponse.MoveOptions = soap_new_tt__MoveOptions20(this->soap);
        timg2__GetMoveOptionsResponse.MoveOptions->Continuous = soap_new_tt__ContinuousFocusOptions(this->soap);
        timg2__GetMoveOptionsResponse.MoveOptions->Continuous->Speed = soap_new_set_tt__FloatRange(this->soap, -1, 1);

        timg2__GetMoveOptionsResponse.MoveOptions->Relative = soap_new_tt__RelativeFocusOptions20(this->soap);
        timg2__GetMoveOptionsResponse.MoveOptions->Relative->Speed = soap_new_set_tt__FloatRange(this->soap, 1, 10);
        timg2__GetMoveOptionsResponse.MoveOptions->Relative->Distance = soap_new_set_tt__FloatRange(this->soap, -1, 1);

        return SOAP_OK;
    }

    timg2__Capabilities* ImagingBindingServiceImpl::GetServiceCapabilities(::soap* soapObj)
    {
        auto _capabilities = soap_new_timg2__Capabilities(soapObj);

        _capabilities->ImageStabilization = OnvifUtil::BoolValue(soapObj, false);
        _capabilities->Presets = OnvifUtil::BoolValue(soapObj, false);

        return _capabilities;
    }
}
