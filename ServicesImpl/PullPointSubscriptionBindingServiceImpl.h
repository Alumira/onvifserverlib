#pragma once
#include "onvif_generated_PullPointSubscriptionBindingService.h"
#include <Interface/IServiceImpl.h>
#include <BackgroundServices/TaskCommander.h>
#include <SoapProxy/SoapUtil.h>

namespace onvif_generated_ {
    class PullPointSubscriptionBindingServiceImpl : public PullPointSubscriptionBindingService, public IServiceImpl
    {
    public:
        PullPointSubscriptionBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander);
        PullPointSubscriptionBindingService* copy() override { return new PullPointSubscriptionBindingServiceImpl(*this); }

        int PullMessages(_tev__PullMessages* tev__PullMessages, _tev__PullMessagesResponse& tev__PullMessagesResponse) override;
        int Seek(_tev__Seek* tev__Seek, _tev__SeekResponse& tev__SeekResponse) override;
        int SetSynchronizationPoint(_tev__SetSynchronizationPoint* tev__SetSynchronizationPoint, _tev__SetSynchronizationPointResponse& tev__SetSynchronizationPointResponse) override;
        
        //IServiceImpl
        int Dispatch(struct soap* soapPtr) override
        {
            return PullPointSubscriptionBindingService::dispatch(soapPtr);
        }

        int Unsubscribe(_wsnt__Unsubscribe* wsnt__Unsubscribe,
            _wsnt__UnsubscribeResponse& wsnt__UnsubscribeResponse) override;
    private:
        TaskCommanderSP m_taskCommander;

        int defaultImplementation() override
        {
            if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
            {
                return auth_result;
            }

			OnvifUtil::ONVIF_Fault(this->soap, "ter:ActionNotSupported", "ter:NotImplemented", "This optinal method is not implemented", true);
            return SOAP_FAULT;
        }
    };

}
