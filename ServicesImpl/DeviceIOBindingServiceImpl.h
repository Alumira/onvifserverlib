#pragma once

#include "onvif_generated_DeviceIOBindingService.h"
#include <Interface/IServiceImpl.h>
#include <SoapProxy/SoapUtil.h>

namespace onvif_generated_ {
	class DeviceIOBindingServiceImpl : public DeviceIOBindingService, public IServiceImpl
	{
	public:
        DeviceIOBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data);

        DeviceIOBindingService* copy() override { return new DeviceIOBindingServiceImpl(*this); }

        int GetServiceCapabilities(_tmd__GetServiceCapabilities* tmd__GetServiceCapabilities, _tmd__GetServiceCapabilitiesResponse& tmd__GetServiceCapabilitiesResponse) override;
		int GetRelayOutputOptions(_tmd__GetRelayOutputOptions* tmd__GetRelayOutputOptions, _tmd__GetRelayOutputOptionsResponse& tmd__GetRelayOutputOptionsResponse) override;
        int GetAudioSources(tmd__Get* tmd__GetAudioSources, tmd__GetResponse& tmd__GetAudioSourcesResponse) override;
		int GetAudioOutputs(tmd__Get* tmd__GetAudioOutputs, tmd__GetResponse& tmd__GetAudioOutputsResponse) override;
        int GetVideoSources(tmd__Get* tmd__GetVideoSources, tmd__GetResponse& tmd__GetVideoSourcesResponse) override;
		int GetVideoOutputs(_tmd__GetVideoOutputs* tmd__GetVideoOutputs, _tmd__GetVideoOutputsResponse& tmd__GetVideoOutputsResponse) override;
        int GetVideoSourceConfiguration(_tmd__GetVideoSourceConfiguration* tmd__GetVideoSourceConfiguration, _tmd__GetVideoSourceConfigurationResponse& tmd__GetVideoSourceConfigurationResponse) override;
		int GetVideoOutputConfiguration(_tmd__GetVideoOutputConfiguration* tmd__GetVideoOutputConfiguration, _tmd__GetVideoOutputConfigurationResponse& tmd__GetVideoOutputConfigurationResponse) override;
        int GetAudioSourceConfiguration(_tmd__GetAudioSourceConfiguration* tmd__GetAudioSourceConfiguration, _tmd__GetAudioSourceConfigurationResponse& tmd__GetAudioSourceConfigurationResponse) override;
		int GetAudioOutputConfiguration(_tmd__GetAudioOutputConfiguration* tmd__GetAudioOutputConfiguration, _tmd__GetAudioOutputConfigurationResponse& tmd__GetAudioOutputConfigurationResponse) override;
		int SetVideoSourceConfiguration(_tmd__SetVideoSourceConfiguration* tmd__SetVideoSourceConfiguration, _tmd__SetVideoSourceConfigurationResponse& tmd__SetVideoSourceConfigurationResponse) override;
		int SetVideoOutputConfiguration(_tmd__SetVideoOutputConfiguration* tmd__SetVideoOutputConfiguration, _tmd__SetVideoOutputConfigurationResponse& tmd__SetVideoOutputConfigurationResponse) override;
		int SetAudioSourceConfiguration(_tmd__SetAudioSourceConfiguration* tmd__SetAudioSourceConfiguration, _tmd__SetAudioSourceConfigurationResponse& tmd__SetAudioSourceConfigurationResponse) override;
		int SetAudioOutputConfiguration(_tmd__SetAudioOutputConfiguration* tmd__SetAudioOutputConfiguration, _tmd__SetAudioOutputConfigurationResponse& tmd__SetAudioOutputConfigurationResponse) override;
        int GetVideoSourceConfigurationOptions(_tmd__GetVideoSourceConfigurationOptions* tmd__GetVideoSourceConfigurationOptions, _tmd__GetVideoSourceConfigurationOptionsResponse& tmd__GetVideoSourceConfigurationOptionsResponse) override;
		int GetVideoOutputConfigurationOptions(_tmd__GetVideoOutputConfigurationOptions* tmd__GetVideoOutputConfigurationOptions, _tmd__GetVideoOutputConfigurationOptionsResponse& tmd__GetVideoOutputConfigurationOptionsResponse) override;
        int GetAudioSourceConfigurationOptions(_tmd__GetAudioSourceConfigurationOptions* tmd__GetAudioSourceConfigurationOptions, _tmd__GetAudioSourceConfigurationOptionsResponse& tmd__GetAudioSourceConfigurationOptionsResponse) override;
		int GetAudioOutputConfigurationOptions(_tmd__GetAudioOutputConfigurationOptions* tmd__GetAudioOutputConfigurationOptions, _tmd__GetAudioOutputConfigurationOptionsResponse& tmd__GetAudioOutputConfigurationOptionsResponse) override;
        int GetRelayOutputs(_tds__GetRelayOutputs* tds__GetRelayOutputs, _tds__GetRelayOutputsResponse& tds__GetRelayOutputsResponse) override;
		int SetRelayOutputSettings(_tmd__SetRelayOutputSettings* tmd__SetRelayOutputSettings, _tmd__SetRelayOutputSettingsResponse& tmd__SetRelayOutputSettingsResponse) override;
		int SetRelayOutputState(_tds__SetRelayOutputState* tds__SetRelayOutputState, _tds__SetRelayOutputStateResponse& tds__SetRelayOutputStateResponse) override;
		int GetDigitalInputs(_tmd__GetDigitalInputs* tmd__GetDigitalInputs, _tmd__GetDigitalInputsResponse& tmd__GetDigitalInputsResponse) override;
		int GetDigitalInputConfigurationOptions(_tmd__GetDigitalInputConfigurationOptions* tmd__GetDigitalInputConfigurationOptions, _tmd__GetDigitalInputConfigurationOptionsResponse& tmd__GetDigitalInputConfigurationOptionsResponse) override;
		int SetDigitalInputConfigurations(_tmd__SetDigitalInputConfigurations* tmd__SetDigitalInputConfigurations, _tmd__SetDigitalInputConfigurationsResponse& tmd__SetDigitalInputConfigurationsResponse) override;
	    int GetSerialPorts(_tmd__GetSerialPorts* tmd__GetSerialPorts, _tmd__GetSerialPortsResponse& tmd__GetSerialPortsResponse) override { return defaultImplementation(); }
	    int GetSerialPortConfiguration(_tmd__GetSerialPortConfiguration* tmd__GetSerialPortConfiguration, _tmd__GetSerialPortConfigurationResponse& tmd__GetSerialPortConfigurationResponse) override { return defaultImplementation(); }
	    int SetSerialPortConfiguration(_tmd__SetSerialPortConfiguration* tmd__SetSerialPortConfiguration, _tmd__SetSerialPortConfigurationResponse& tmd__SetSerialPortConfigurationResponse) override { return defaultImplementation(); }
	    int GetSerialPortConfigurationOptions(_tmd__GetSerialPortConfigurationOptions* tmd__GetSerialPortConfigurationOptions, _tmd__GetSerialPortConfigurationOptionsResponse& tmd__GetSerialPortConfigurationOptionsResponse) override { return defaultImplementation(); }
	    int SendReceiveSerialCommand(_tmd__SendReceiveSerialCommand* tmd__SendReceiveSerialCommand, _tmd__SendReceiveSerialCommandResponse& tmd__SendReceiveSerialCommandResponse) override { return defaultImplementation(); }
	    //IServiceImpl
	    int Dispatch(struct soap* soapPtr) override
        {
            return DeviceIOBindingService::dispatch(soapPtr);
        }

        static _tmd__Capabilities* GetServiceCapabilities(struct soap* soapObj, OnvifDeviceConfigurationSP deviceConfig);
	    void updateEncoderParams(VideoSourceConfigurationSP vSourceConfig);
	    bool checkVideoSourceCodec(const VideoSourceConfigurationSP& vSourceConfig) const;
	    static MessageHolderTypeSP CreateRelayEvent(struct soap* soap, RelayOutputSP relayConfig, tt__PropertyOperation propOperation);
		static MessageHolderTypeSP CreateRayEvent(struct soap* soap, RayOutputSP rayConfig, tt__PropertyOperation propOperation);
		static soap_dom_element createRelayEventsDescription(::soap* soap);
		static soap_dom_element createRayEventsDescription(::soap* soap);

        static std::string GetUri()
        {
            return "/deviceio_service";
        }

	private:
        int defaultImplementation() override
        {
            if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
            {
                return auth_result;
            }

			OnvifUtil::ONVIF_Fault(this->soap, "ter:ActionNotSupported", "ter:NotImplemented", "This optinal method is not implemented", true);
            return SOAP_FAULT;
        }
	};
}
