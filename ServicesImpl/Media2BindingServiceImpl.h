#pragma once

#include "onvif_generated_Media2BindingService.h"
#include <Interface/IServiceImpl.h>
#include <SoapProxy/SoapUtil.h>
#include <BackgroundServices/TaskCommander.h>

namespace onvif_generated_ {
	class Media2BindingServiceImpl : public Media2BindingService, public IServiceImpl
	{
	public:
		Media2BindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander);

		Media2BindingService* copy() override { return new Media2BindingServiceImpl(*this); }

        int GetServiceCapabilities(_trt2__GetServiceCapabilities *trt2__GetServiceCapabilities, _trt2__GetServiceCapabilitiesResponse &trt2__GetServiceCapabilitiesResponse) override;
        int CreateProfile(_trt2__CreateProfile* trt2__CreateProfile, _trt2__CreateProfileResponse& trt2__CreateProfileResponse) override;
		int GetProfiles(_trt2__GetProfiles* trt2__GetProfiles, _trt2__GetProfilesResponse& trt2__GetProfilesResponse) override;
        int AddConfiguration(_trt2__AddConfiguration* trt2__AddConfiguration, _trt2__AddConfigurationResponse& trt2__AddConfigurationResponse) override;
        int RemoveConfiguration(_trt2__RemoveConfiguration* trt2__RemoveConfiguration, _trt2__RemoveConfigurationResponse& trt2__RemoveConfigurationResponse) override;
        int DeleteProfile(_trt2__DeleteProfile* trt2__DeleteProfile, _trt2__DeleteProfileResponse& trt2__DeleteProfileResponse) override;
        int GetVideoSourceConfigurations(trt2__GetConfiguration* trt2__GetVideoSourceConfigurations, _trt2__GetVideoSourceConfigurationsResponse& trt2__GetVideoSourceConfigurationsResponse) override;
        int GetVideoEncoderConfigurations(trt2__GetConfiguration* trt2__GetVideoEncoderConfigurations, _trt2__GetVideoEncoderConfigurationsResponse& trt2__GetVideoEncoderConfigurationsResponse) override;
        int GetAudioSourceConfigurations(trt2__GetConfiguration* trt2__GetAudioSourceConfigurations, _trt2__GetAudioSourceConfigurationsResponse& trt2__GetAudioSourceConfigurationsResponse) override;
        int GetAudioEncoderConfigurations(trt2__GetConfiguration* trt2__GetAudioEncoderConfigurations, _trt2__GetAudioEncoderConfigurationsResponse& trt2__GetAudioEncoderConfigurationsResponse) override;
		int GetAnalyticsConfigurations(trt2__GetConfiguration* trt2__GetAnalyticsConfigurations, _trt2__GetAnalyticsConfigurationsResponse& trt2__GetAnalyticsConfigurationsResponse) override;
        int GetMetadataConfigurations(trt2__GetConfiguration* trt2__GetMetadataConfigurations, _trt2__GetMetadataConfigurationsResponse& trt2__GetMetadataConfigurationsResponse) override;
		int GetAudioOutputConfigurations(trt2__GetConfiguration* trt2__GetAudioOutputConfigurations, _trt2__GetAudioOutputConfigurationsResponse& trt2__GetAudioOutputConfigurationsResponse) override;
		int GetAudioDecoderConfigurations(trt2__GetConfiguration* trt2__GetAudioDecoderConfigurations, _trt2__GetAudioDecoderConfigurationsResponse& trt2__GetAudioDecoderConfigurationsResponse) override;
        int SetVideoSourceConfiguration(_trt2__SetVideoSourceConfiguration* trt2__SetVideoSourceConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse) override;
        int SetVideoEncoderConfiguration(_trt2__SetVideoEncoderConfiguration* trt2__SetVideoEncoderConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse) override;
        int SetAudioSourceConfiguration(_trt2__SetAudioSourceConfiguration* trt2__SetAudioSourceConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse) override;
        int SetAudioEncoderConfiguration(_trt2__SetAudioEncoderConfiguration* trt2__SetAudioEncoderConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse) override;
		int SetMetadataConfiguration(_trt2__SetMetadataConfiguration* trt2__SetMetadataConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse) override;
		int SetAudioOutputConfiguration(_trt2__SetAudioOutputConfiguration* trt2__SetAudioOutputConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse) override;
		int SetAudioDecoderConfiguration(_trt2__SetAudioDecoderConfiguration* trt2__SetAudioDecoderConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse) override;
        int GetVideoSourceConfigurationOptions(trt2__GetConfiguration* trt2__GetVideoSourceConfigurationOptions, _trt2__GetVideoSourceConfigurationOptionsResponse& trt2__GetVideoSourceConfigurationOptionsResponse) override;
        int GetVideoEncoderConfigurationOptions(trt2__GetConfiguration* trt2__GetVideoEncoderConfigurationOptions, _trt2__GetVideoEncoderConfigurationOptionsResponse& trt2__GetVideoEncoderConfigurationOptionsResponse) override;
        int GetAudioSourceConfigurationOptions(trt2__GetConfiguration* trt2__GetAudioSourceConfigurationOptions, _trt2__GetAudioSourceConfigurationOptionsResponse& trt2__GetAudioSourceConfigurationOptionsResponse) override;
        int GetAudioEncoderConfigurationOptions(trt2__GetConfiguration* trt2__GetAudioEncoderConfigurationOptions, _trt2__GetAudioEncoderConfigurationOptionsResponse& trt2__GetAudioEncoderConfigurationOptionsResponse) override;
		int GetMetadataConfigurationOptions(trt2__GetConfiguration* trt2__GetMetadataConfigurationOptions, _trt2__GetMetadataConfigurationOptionsResponse& trt2__GetMetadataConfigurationOptionsResponse) override;
		int GetAudioOutputConfigurationOptions(trt2__GetConfiguration* trt2__GetAudioOutputConfigurationOptions, _trt2__GetAudioOutputConfigurationOptionsResponse& trt2__GetAudioOutputConfigurationOptionsResponse) override;
		int GetAudioDecoderConfigurationOptions(trt2__GetConfiguration* trt2__GetAudioDecoderConfigurationOptions, _trt2__GetAudioDecoderConfigurationOptionsResponse& trt2__GetAudioDecoderConfigurationOptionsResponse) override;
        int GetVideoEncoderInstances(_trt2__GetVideoEncoderInstances* trt2__GetVideoEncoderInstances, _trt2__GetVideoEncoderInstancesResponse& trt2__GetVideoEncoderInstancesResponse) override;
        int GetStreamUri(_trt2__GetStreamUri* trt2__GetStreamUri, _trt2__GetStreamUriResponse& trt2__GetStreamUriResponse) override;
        int StartMulticastStreaming(trt2__StartStopMulticastStreaming *trt2__StartMulticastStreaming, trt2__SetConfigurationResponse &trt2__StartMulticastStreamingResponse) override;
        int StopMulticastStreaming(trt2__StartStopMulticastStreaming *trt2__StopMulticastStreaming, trt2__SetConfigurationResponse &trt2__StopMulticastStreamingResponse) override;
        int SetSynchronizationPoint(_trt2__SetSynchronizationPoint* trt2__SetSynchronizationPoint, _trt2__SetSynchronizationPointResponse& trt2__SetSynchronizationPointResponse) override;
        int GetSnapshotUri(_trt2__GetSnapshotUri* trt2__GetSnapshotUri, _trt2__GetSnapshotUriResponse& trt2__GetSnapshotUriResponse) override;
		int GetVideoSourceModes(_trt2__GetVideoSourceModes* trt2__GetVideoSourceModes, _trt2__GetVideoSourceModesResponse& trt2__GetVideoSourceModesResponse) override;
		int SetVideoSourceMode(_trt2__SetVideoSourceMode* trt2__SetVideoSourceMode, _trt2__SetVideoSourceModeResponse& trt2__SetVideoSourceModeResponse) override;
		int GetOSDs(_trt2__GetOSDs* trt2__GetOSDs, _trt2__GetOSDsResponse& trt2__GetOSDsResponse) override;
		int GetOSDOptions(_trt2__GetOSDOptions* trt2__GetOSDOptions, _trt2__GetOSDOptionsResponse& trt2__GetOSDOptionsResponse) override;
		int SetOSD(_trt2__SetOSD* trt2__SetOSD, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse) override;
		int CreateOSD(_trt2__CreateOSD* trt2__CreateOSD, _trt2__CreateOSDResponse& trt2__CreateOSDResponse) override;
		int DeleteOSD(_trt2__DeleteOSD* trt2__DeleteOSD, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse) override;

		int GetMasks(_trt2__GetMasks* trt2__GetMasks, _trt2__GetMasksResponse& trt2__GetMasksResponse) override { return defaultImplementation(); }
		int GetMaskOptions(_trt2__GetMaskOptions* trt2__GetMaskOptions, _trt2__GetMaskOptionsResponse& trt2__GetMaskOptionsResponse) override { return defaultImplementation(); }
		int SetMask(_trt2__SetMask* trt2__SetMask, trt2__SetConfigurationResponse& trt2__SetMaskResponse) override { return defaultImplementation(); }
		int CreateMask(_trt2__CreateMask* trt2__CreateMask, _trt2__CreateMaskResponse& trt2__CreateMaskResponse) override { return defaultImplementation(); }
		int DeleteMask(_trt2__DeleteMask* trt2__DeleteMask, trt2__SetConfigurationResponse& trt2__DeleteMaskResponse) override { return defaultImplementation(); }

        //IServiceImpl
	    int Dispatch(struct soap* soapPtr) override
        {
            return Media2BindingService::dispatch(soapPtr);
        }

        static trt2__Capabilities2* GetServiceCapabilities(struct soap* soapObj, CoreSettingsSP coreSettings);
        static bool checkCodec(const std::string& codec);
	    bool checkVideoSourceCodec(const VideoSourceConfigurationSP& vSourceConfig) const;
	    static MessageHolderTypeSP CreateProfileChangedEvent(struct soap* soap, MediaProfileSP profile);
		static MessageHolderTypeSP CreateConfigurationChangedEvent(::soap* soap, const std::string& token, const std::string& type);
        static soap_dom_element createMedia2ProfileChangedDescription(::soap* soap);
        static soap_dom_element createMedia2ConfigurationChangedDescription(::soap* soap);

        static std::string GetUri()
        {
            return "/media_service2";
        }

	private:
		TaskCommanderSP m_taskCommander;
        void updateEncoderParams(VideoSourceConfigurationSP vSourceConfig);

	    int defaultImplementation() override
		{
			if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
			{
				return auth_result;
			}

			OnvifUtil::ONVIF_Fault(this->soap, "ter:ActionNotSupported", "ter:NotImplemented", "This optinal method is not implemented", true);
			return SOAP_FAULT;
		}
	};

	class CodecsCountMap
	{
	public:
		CodecsCountMap()
		{
			m_mapResult["H264"] = 0;
			m_mapResult["H265"] = 0;
			m_mapResult["MJPEG"] = 0;
			m_mapResult["MPEG4"] = 0;
		}

		void AddCodecs(const VideoSourceConfigurationSP& vSource)
		{
			BOOST_FOREACH(auto vEncoder, vSource->m_encoders)
			{
				if (m_mapResult.find(vEncoder.second->codec) != m_mapResult.end())
				{
					m_mapResult[vEncoder.second->codec] += 1;
				}
				else
				{
					m_mapResult[vEncoder.second->codec] = 1;
				}
			}
		}

		int GetTotalCount() const
		{
			int total = 0;
			BOOST_FOREACH(auto codec, m_mapResult)
			{
				total += codec.second;
			}

			return total;
		}

		const std::map<std::string, int>& GetMap() const
		{
			return m_mapResult;
		}

	private:
		std::map<std::string, int> m_mapResult;
	};
}
