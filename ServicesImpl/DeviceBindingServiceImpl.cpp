#include "onvif_generated_H.h"

#include <Util/NetworkUtil.h>
#include <SoapProxy/StructDefines.h>

#include "DeviceBindingServiceImpl.h"
#include "MediaBindingServiceImpl.h"
#include "ReceiverBindingServiceImpl.h"
#include "ReplayBindingServiceImpl.h"
#include "RecordingBindingServiceImpl.h"
#include "ImagingBindingServiceImpl.h"
#include "EventBindingServiceImpl.h"
#include "PtzBindingServiceImpl.h"
#include "Media2BindingServiceImpl.h"
#include "SearchBindingServiceImpl.h"
#include "DeviceIOBindingServiceImpl.h"
#include "AnalyticsEngineBindingServiceImpl.h"

#include <boost/asio.hpp>
#include <boost/foreach.hpp>
#include <boost/date_time/local_time/local_time.hpp>
#include <boost/chrono/system_clocks.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>
#include "Util/StringUtils.h"

namespace onvif_generated_ {
	DeviceBindingServiceImpl::DeviceBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, DiscoveryDownloaderPtr discovery) :
            DeviceBindingService(soapObj->GetSoap())
            , IServiceImpl(data)
			, m_discovery(discovery)
	{
	}

	int DeviceBindingServiceImpl::GetServices(_tds__GetServices* tds__GetServices, _tds__GetServicesResponse& tds__GetServicesResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }
	    
	    //Device service
        auto deviceSrv = soap_new_tds__Service(this->soap);
        deviceSrv->Namespace = "http://www.onvif.org/ver10/device/wsdl";
        deviceSrv->XAddr = GetCoreSettings()->GetServiceUri() + DeviceBindingServiceImpl::GetUri();
        deviceSrv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 5);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__tds__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = GetServiceCapabilities();

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__tds__Capabilities);

            deviceSrv->Capabilities = capabilities_list;
        }

        tds__GetServicesResponse.Service.push_back(deviceSrv);

        //Media service
        auto mediaSrv = soap_new_tds__Service(this->soap);

        mediaSrv->Namespace = "http://www.onvif.org/ver10/media/wsdl";
        mediaSrv->XAddr = GetCoreSettings()->GetServiceUri() + MediaBindingServiceImpl::GetUri();
        mediaSrv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 6);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__trt__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = MediaBindingServiceImpl::GetServiceCapabilities(this->soap, GetCoreSettings());

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__trt__Capabilities);

            mediaSrv->Capabilities = capabilities_list;
        }
        tds__GetServicesResponse.Service.push_back(mediaSrv);

        //Media2 service
        auto media2Srv = soap_new_tds__Service(this->soap);

        media2Srv->Namespace = "http://www.onvif.org/ver20/media/wsdl";
        media2Srv->XAddr = GetCoreSettings()->GetServiceUri() + Media2BindingServiceImpl::GetUri();
        media2Srv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 6);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__trt2__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = Media2BindingServiceImpl::GetServiceCapabilities(this->soap, GetCoreSettings());

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__trt2__Capabilities);

            media2Srv->Capabilities = capabilities_list;
        }
        
        if(GetCoreSettings()->IsVersion2())
        {
            //TODO: a problem with driver! Driver connect to Mediaservice instead of Media2 
            tds__GetServicesResponse.Service.push_back(media2Srv);            
        }

        //Ptz service
        auto ptzSrv = soap_new_tds__Service(this->soap);

        ptzSrv->Namespace = "http://www.onvif.org/ver20/ptz/wsdl";
        ptzSrv->XAddr = GetCoreSettings()->GetServiceUri() + PtzBindingServiceImpl::GetUri();
        ptzSrv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 6);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__tptz2__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = PtzBindingServiceImpl::GetServiceCapabilities(this->soap);

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__tptz2__Capabilities);

            ptzSrv->Capabilities = capabilities_list;
        }

        tds__GetServicesResponse.Service.push_back(ptzSrv);

		//Imaging service
        auto imagingSrv = soap_new_tds__Service(this->soap);

        imagingSrv->Namespace = "http://www.onvif.org/ver20/imaging/wsdl";
        imagingSrv->XAddr = GetCoreSettings()->GetServiceUri() + ImagingBindingServiceImpl::GetUri();
        imagingSrv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 5);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__timg2__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = ImagingBindingServiceImpl::GetServiceCapabilities(this->soap);

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__timg2__Capabilities);

            imagingSrv->Capabilities = capabilities_list;
        }

        tds__GetServicesResponse.Service.push_back(imagingSrv);

		//Events service
        auto eventsSrv = soap_new_tds__Service(this->soap);

        eventsSrv->Namespace = "http://www.onvif.org/ver10/events/wsdl";
        eventsSrv->XAddr = GetCoreSettings()->GetServiceUri() + EventBindingServiceImpl::GetUri();
        eventsSrv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 6);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__tev__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = EventBindingServiceImpl::GetServiceCapabilities(this->soap);

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__tev__Capabilities);

            eventsSrv->Capabilities = capabilities_list;
        }

        tds__GetServicesResponse.Service.push_back(eventsSrv);

		//Recording service
        auto recordingSrv = soap_new_tds__Service(this->soap);

        recordingSrv->Namespace = "http://www.onvif.org/ver10/recording/wsdl";
        recordingSrv->XAddr = GetCoreSettings()->GetServiceUri() + RecordingBindingServiceImpl::GetUri();
        recordingSrv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 5);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__trc__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = RecordingBindingServiceImpl::GetServiceCapabilities(this->soap);

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__trc__Capabilities);

            recordingSrv->Capabilities = capabilities_list;
        }

	    tds__GetServicesResponse.Service.push_back(recordingSrv);

		//Replay service
        auto replaySrv = soap_new_tds__Service(this->soap);

        replaySrv->Namespace = "http://www.onvif.org/ver10/replay/wsdl";
		replaySrv->XAddr = GetCoreSettings()->GetServiceUri() + ReplayBindingServiceImpl::GetUri();
        replaySrv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 2);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__trp__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = ReplayBindingServiceImpl::GetServiceCapabilities(this->soap);

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__trp__Capabilities);

            replaySrv->Capabilities = capabilities_list;
        }

        tds__GetServicesResponse.Service.push_back(replaySrv);

		//Search service
        auto serchSrv = soap_new_tds__Service(this->soap);

        serchSrv->Namespace = "http://www.onvif.org/ver10/search/wsdl";
        serchSrv->XAddr = GetCoreSettings()->GetServiceUri() + SearchBindingServiceImpl::GetUri();
        serchSrv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 4);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__tse__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = SearchBindingServiceImpl::GetServiceCapabilities(this->soap);

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__tse__Capabilities);

            serchSrv->Capabilities = capabilities_list;
        }

        tds__GetServicesResponse.Service.push_back(serchSrv);

        //DeviceIO service
        auto deviceIoSrv = soap_new_tds__Service(this->soap);

        deviceIoSrv->Namespace = "http://www.onvif.org/ver10/deviceIO/wsdl";
        deviceIoSrv->XAddr = GetCoreSettings()->GetServiceUri() + DeviceIOBindingServiceImpl::GetUri();
        deviceIoSrv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 6);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__tmd__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = DeviceIOBindingServiceImpl::GetServiceCapabilities(this->soap, GetConfiguration());

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__tmd__Capabilities);

            deviceIoSrv->Capabilities = capabilities_list;
        }

        tds__GetServicesResponse.Service.push_back(deviceIoSrv);

        //Analytics service
        auto analyticsSrv = soap_new_tds__Service(this->soap);

        analyticsSrv->Namespace = "http://www.onvif.org/ver20/analytics/wsdl";
        analyticsSrv->XAddr = GetCoreSettings()->GetServiceUri() + AnalyticsEngineBindingServiceImpl::GetUri();
        analyticsSrv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 6);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__tanl2__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = AnalyticsEngineBindingServiceImpl::GetServiceCapabilities(this->soap);

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__tanl2__Capabilities);

            analyticsSrv->Capabilities = capabilities_list;
        }

        tds__GetServicesResponse.Service.push_back(analyticsSrv);

		/*tds__GetServicesResponse.Service.push_back(soap_new_tds__Service(this->soap));
		tds__GetServicesResponse.Service.back()->Namespace = "http://www.onvif.org/ver10/display/wsdl";
		tds__GetServicesResponse.Service.back()->XAddr = ipv4 + "/display_service";
		tds__GetServicesResponse.Service.back()->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 2);*/

		//Receiver service
        auto receiverSrv = soap_new_tds__Service(this->soap);

        receiverSrv->Namespace = "http://www.onvif.org/ver10/receiver/wsdl";
        receiverSrv->XAddr = GetCoreSettings()->GetServiceUri() + ReceiverBindingServiceImpl::GetUri();
        receiverSrv->Version = soap_new_req_tt__OnvifVersion(this->soap, 2, 2);

        if (tds__GetServices->IncludeCapability)
        {
            auto rootType = soap_new__trv__GetServiceCapabilitiesResponse(this->soap);
            rootType->Capabilities = ReceiverBindingServiceImpl::GetServiceCapabilities(this->soap);

            auto capabilities_list = NEW_CAPABILITIES(this->soap);
            capabilities_list->__any.set(rootType->Capabilities, SOAP_TYPE__trv__Capabilities);

            receiverSrv->Capabilities = capabilities_list;
        }

        //TODO: implement Receiver service
	    //tds__GetServicesResponse.Service.push_back(receiverSrv);

		return SOAP_OK;
	}

    int DeviceBindingServiceImpl::GetServiceCapabilities(_tds__GetServiceCapabilities* tds__GetServiceCapabilities, _tds__GetServiceCapabilitiesResponse& tds__GetServiceCapabilitiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }

        tds__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities();
        
        return SOAP_OK;
    }

    tds__DeviceServiceCapabilities* DeviceBindingServiceImpl::GetServiceCapabilities() const
	{
        auto _Capabilities = soap_new_tds__DeviceServiceCapabilities(this->soap);

        _Capabilities->System = soap_new_tds__SystemCapabilities(this->soap);
        _Capabilities->System->SystemBackup = OnvifUtil::BoolValue(this->soap, false);

        _Capabilities->System = soap_new_tds__SystemCapabilities(this->soap);
        _Capabilities->System->FirmwareUpgrade = OnvifUtil::BoolValue(this->soap, false);

        _Capabilities->System->HttpFirmwareUpgrade = OnvifUtil::BoolValue(this->soap, false);

        _Capabilities->System->HttpSystemBackup = OnvifUtil::BoolValue(this->soap, false);

        _Capabilities->System->HttpSystemLogging = OnvifUtil::BoolValue(this->soap, false);

        _Capabilities->System->SystemBackup = OnvifUtil::BoolValue(this->soap, false);

        _Capabilities->System->SystemLogging = OnvifUtil::BoolValue(this->soap, false);

        _Capabilities->Network = soap_new_tds__NetworkCapabilities(this->soap);
        _Capabilities->Network->IPVersion6 = OnvifUtil::BoolValue(this->soap, false);

        _Capabilities->Network->ZeroConfiguration = OnvifUtil::BoolValue(this->soap, false);

        _Capabilities->Security = soap_new_tds__SecurityCapabilities(this->soap);
        _Capabilities->Security->RELToken = OnvifUtil::BoolValue(this->soap, true);

        _Capabilities->Security->HttpDigest = OnvifUtil::BoolValue(this->soap, true);

        _Capabilities->Security->AccessPolicyConfig = OnvifUtil::BoolValue(this->soap, false);

        _Capabilities->Security->DefaultAccessPolicy = OnvifUtil::BoolValue(this->soap, false);
        _Capabilities->Security->UsernameToken = OnvifUtil::BoolValue(this->soap, true);

        return _Capabilities;
	}

    int DeviceBindingServiceImpl::GetDeviceInformation(_tds__GetDeviceInformation* tds__GetDeviceInformation, _tds__GetDeviceInformationResponse& tds__GetDeviceInformationResponse)
    {
        if(auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
        {
            return auth_result;
        }
        
        tds__GetDeviceInformationResponse.FirmwareVersion = GetCoreSettings()->GetVersion();
        tds__GetDeviceInformationResponse.Manufacturer = GetCoreSettings()->GetBrand();
        tds__GetDeviceInformationResponse.Model = GetCoreSettings()->GetModel();
		tds__GetDeviceInformationResponse.SerialNumber = GetCoreSettings()->GetMainUuid();
        tds__GetDeviceInformationResponse.HardwareId = GetCoreSettings()->GetHardwareId();

        return SOAP_OK;
    }

    int DeviceBindingServiceImpl::SetSystemDateAndTime(_tds__SetSystemDateAndTime* tds__SetSystemDateAndTime, _tds__SetSystemDateAndTimeResponse& tds__SetSystemDateAndTimeResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
        {
            return auth_result;
        }

		if (!tds__SetSystemDateAndTime->UTCDateTime)
		{
			return SOAP_OK;
		}

		if(tds__SetSystemDateAndTime->TimeZone)
        {
        	try
            {
                //TODO: set timezone on PC
                boost::local_time::time_zone_ptr nyc_2(new boost::local_time::posix_time_zone(tds__SetSystemDateAndTime->TimeZone->TZ));

                boost::local_time::local_date_time phx_departure(boost::gregorian::date(tds__SetSystemDateAndTime->UTCDateTime->Date->Year, 
                    tds__SetSystemDateAndTime->UTCDateTime->Date->Month, tds__SetSystemDateAndTime->UTCDateTime->Date->Day), 
                    boost::posix_time::hours(tds__SetSystemDateAndTime->UTCDateTime->Time->Hour),
                    nyc_2,
                    boost::local_time::local_date_time::NOT_DATE_TIME_ON_ERROR);

                //std::string tzStr = phx_departure.zone_as_posix_string();

                //TODO: check timezone is correct?!
                if(phx_departure.is_not_a_date_time() || phx_departure.zone_as_posix_string() == "INVALIDTIMEZONE+00")
                {
                    OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidTimeZone");
                    return SOAP_FAULT;
                }
            }
            catch(...)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidDateTime");
                return SOAP_FAULT;
            }
        }
        
#ifdef _WINDOWS_DEF
        SYSTEMTIME lpSystemTime;
        memset(&lpSystemTime, 0, sizeof(lpSystemTime));
        lpSystemTime.wYear = tds__SetSystemDateAndTime->UTCDateTime->Date->Year;
        lpSystemTime.wDay = tds__SetSystemDateAndTime->UTCDateTime->Date->Day;
        lpSystemTime.wMonth = tds__SetSystemDateAndTime->UTCDateTime->Date->Month;

        lpSystemTime.wHour = tds__SetSystemDateAndTime->UTCDateTime->Time->Hour;
        lpSystemTime.wMinute = tds__SetSystemDateAndTime->UTCDateTime->Time->Minute;
        lpSystemTime.wSecond = tds__SetSystemDateAndTime->UTCDateTime->Time->Second;

        if(!::SetSystemTime(&lpSystemTime))
        {
			auto lMsg = (boost::format("SetSystemTime failed with error: %1%.") % ::GetLastError()).str();
			ONVIF_LOG_BASE(GetLogger(), ITV8::LOG_ERROR, lMsg.c_str());
                
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidDateTime");
            return SOAP_FAULT;
        }
#endif
// TODO Implement system time for Linux 
        return SOAP_OK;
    }

	boost::posix_time::time_duration get_utc_offset() 
	{
        using namespace boost::posix_time;

        typedef boost::date_time::c_local_adjustor<ptime> local_adj;

        const ptime utc_now = second_clock::universal_time();
        const ptime now = local_adj::utc_to_local(utc_now);

        return now - utc_now;
	}

	std::string get_utc_offset_string() 
	{
        std::stringstream out;

        using namespace boost::posix_time;
        time_facet* tf = new time_facet();
        tf->time_duration_format("%+%H:%M");
        out.imbue(std::locale(out.getloc(), tf));

        out << get_utc_offset();

        return "UTC" + out.str();
	}

    int DeviceBindingServiceImpl::GetSystemDateAndTime(_tds__GetSystemDateAndTime* tds__GetSystemDateAndTime, _tds__GetSystemDateAndTimeResponse& tds__GetSystemDateAndTimeResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }

        tds__GetSystemDateAndTimeResponse.SystemDateAndTime = soap_new_tt__SystemDateTime(this->soap);

	    {
            auto now = boost::posix_time::second_clock::universal_time();

            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->UTCDateTime = soap_new_tt__DateTime(this->soap);
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->UTCDateTime->Date = soap_new_tt__Date(this->soap);
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->UTCDateTime->Date->Day = now.date().day();
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->UTCDateTime->Date->Month = now.date().month();
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->UTCDateTime->Date->Year = now.date().year();

            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->UTCDateTime->Time = soap_new_tt__Time(this->soap);
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->UTCDateTime->Time->Hour = now.time_of_day().hours();
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->UTCDateTime->Time->Minute = now.time_of_day().minutes();
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->UTCDateTime->Time->Second = now.time_of_day().seconds();
        }

        {
            auto now_local = boost::posix_time::second_clock::local_time();

            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->LocalDateTime = soap_new_tt__DateTime(this->soap);
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->LocalDateTime->Date = soap_new_tt__Date(this->soap);
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->LocalDateTime->Date->Day = now_local.date().day();
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->LocalDateTime->Date->Month = now_local.date().month();
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->LocalDateTime->Date->Year = now_local.date().year();

            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->LocalDateTime->Time = soap_new_tt__Time(this->soap);
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->LocalDateTime->Time->Hour = now_local.time_of_day().hours();
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->LocalDateTime->Time->Minute = now_local.time_of_day().minutes();
            tds__GetSystemDateAndTimeResponse.SystemDateAndTime->LocalDateTime->Time->Second = now_local.time_of_day().seconds();
        }

        tds__GetSystemDateAndTimeResponse.SystemDateAndTime->DateTimeType = tt__SetDateTimeType__Manual;

        std::string nyc_string = get_utc_offset_string();

        tds__GetSystemDateAndTimeResponse.SystemDateAndTime->TimeZone = soap_new_tt__TimeZone(this->soap);
        tds__GetSystemDateAndTimeResponse.SystemDateAndTime->TimeZone->TZ = nyc_string;

        return SOAP_OK;
    }

    int DeviceBindingServiceImpl::SetSystemFactoryDefault(_tds__SetSystemFactoryDefault* tds__SetSystemFactoryDefault, _tds__SetSystemFactoryDefaultResponse& tds__SetSystemFactoryDefaultResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::UNRECOVERABLE))
        {
            return auth_result;
        }

        return SOAP_OK;
    }

    int DeviceBindingServiceImpl::SystemReboot(_tds__SystemReboot* tds__SystemReboot, _tds__SystemRebootResponse& tds__SystemRebootResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::UNRECOVERABLE))
        {
            return auth_result;
        }

	   GetProcessorFactory()->createRebootProcessor(GetCoreSettings())->DoWork();

        return SOAP_OK;
    }

    int DeviceBindingServiceImpl::GetScopes(_tds__GetScopes* tds__GetScopes, _tds__GetScopesResponse& tds__GetScopesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
        {
            return auth_result;
        }

		auto scopes = GetCoreSettings()->GetScopes();

		BOOST_FOREACH(auto sc, scopes)
		{
			addScope(sc, tds__GetScopesResponse.Scopes);
		}

        return SOAP_OK;
    }

	int DeviceBindingServiceImpl::SetScopes(_tds__SetScopes* tds__SetScopes, _tds__SetScopesResponse& tds__SetScopesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
		{
			return auth_result;
		}

		GetCoreSettings()->SetScopes(tds__SetScopes->Scopes);

		if(auto it = m_discovery.lock())
		{
			it->SetScopes(GetCoreSettings()->GetScopes());
		}

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::AddScopes(_tds__AddScopes* tds__AddScopes, _tds__AddScopesResponse& tds__AddScopesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
		{
			return auth_result;
		}

		GetCoreSettings()->SetScopes(tds__AddScopes->ScopeItem);

		if (auto it = m_discovery.lock())
		{
			it->SetScopes(GetCoreSettings()->GetScopes());
		}

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::RemoveScopes(_tds__RemoveScopes* tds__RemoveScopes, _tds__RemoveScopesResponse& tds__RemoveScopesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
		{
			return auth_result;
		}

		GetCoreSettings()->RemoveScopes(tds__RemoveScopes->ScopeItem);

		if (auto it = m_discovery.lock())
		{
			it->SetScopes(GetCoreSettings()->GetScopes());
		}

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::GetDiscoveryMode(_tds__GetDiscoveryMode* tds__GetDiscoveryMode, _tds__GetDiscoveryModeResponse& tds__GetDiscoveryModeResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
		{
			return auth_result;
		}

		tds__GetDiscoveryModeResponse.DiscoveryMode = GetCoreSettings()->IsDiscoveryEnabled() ?
			tt__DiscoveryMode__Discoverable : tt__DiscoveryMode__NonDiscoverable;

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::SetDiscoveryMode(_tds__SetDiscoveryMode* tds__SetDiscoveryMode, _tds__SetDiscoveryModeResponse& tds__SetDiscoveryModeResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
		{
			return auth_result;
		};

		CoreSettingsSP newSettings(new CoreSettings(*GetCoreSettings().get()));
		newSettings->SetDiscoveryMode(tds__SetDiscoveryMode->DiscoveryMode == tt__DiscoveryMode__Discoverable);

		if (auto it = m_discovery.lock())
		{
			it->Stop();
		}

		GetProcessorFactory()->createUpdateOnvifServerProcessor(newSettings)->DoWork();

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::GetEndpointReference(_tds__GetEndpointReference* tds__GetEndpointReference, _tds__GetEndpointReferenceResponse& tds__GetEndpointReferenceResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
		{
			return auth_result;
		};

		tds__GetEndpointReferenceResponse.GUID = m_data->m_coreSettings->GetUUID();

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::GetUsers(_tds__GetUsers* tds__GetUsers, _tds__GetUsersResponse& tds__GetUsersResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM_SECRET))
        {
            return auth_result;
        }
        
        BOOST_FOREACH(auto user, GetSecurity()->GetUsers())
        {
            auto userResponce = soap_new_tt__User(this->soap);
            userResponce->Username = user->GetUsername();
            userResponce->UserLevel = GetProxy()->CreateUserLevel(user->GetLevel());
            tds__GetUsersResponse.User.push_back(userResponce);
        }

        return SOAP_OK;
    }

	int DeviceBindingServiceImpl::CreateUsers(_tds__CreateUsers* tds__CreateUsers, _tds__CreateUsersResponse& tds__CreateUsersResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
		{
			return auth_result;
		}

		BOOST_FOREACH(auto user, tds__CreateUsers->User)
		{
			if (GetSecurity()->GetUserConfiguration(user->Username))
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:OperationProhibited", "ter:UsernameClash", NULL, false);
				return SOAP_FAULT;
			}
			
			UserConfigurationSP newUser(new UserConfiguration(user->Username, GetProxy()->GetUserLevel(user->UserLevel), 
				GetSecurity()->GetMD5ForUserInfo(user->Username.c_str(), user->Password->c_str()),
			user->Password->c_str()));

			GetProcessorFactory()->createCreateUserProcessor(newUser)->DoWork();
		}

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::DeleteUsers(_tds__DeleteUsers* tds__DeleteUsers, _tds__DeleteUsersResponse& tds__DeleteUsersResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
		{
			return auth_result;
		}

		BOOST_FOREACH(auto user, tds__DeleteUsers->Username)
		{
			auto deletingUser = GetSecurity()->GetUserConfiguration(user);
			if (!deletingUser)
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:UsernameMissing", NULL, false);
				return SOAP_FAULT;
			}
		}

		BOOST_FOREACH(auto user, tds__DeleteUsers->Username)
		{
			auto deletingUser = GetSecurity()->GetUserConfiguration(user);
			UserConfigurationSP newUser(deletingUser);

			GetProcessorFactory()->createDeleteUserProcessor(newUser)->DoWork();
		}

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::SetUser(_tds__SetUser* tds__SetUser, _tds__SetUserResponse& tds__SetUserResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
		{
			return auth_result;
		}

		BOOST_FOREACH(auto user, tds__SetUser->User)
		{
			auto updatingUser = GetSecurity()->GetUserConfiguration(user->Username);
			if (!updatingUser)
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:UsernameMissing", NULL, false);
				return SOAP_FAULT;
			}
		}

		BOOST_FOREACH(auto user, tds__SetUser->User)
		{
			auto updatingUser = GetSecurity()->GetUserConfiguration(user->Username);
			
			std::string passw = user->Password ? *user->Password : updatingUser->GetUserTextpassw();

			UserConfigurationSP newUser(new UserConfiguration(user->Username, GetProxy()->GetUserLevel(user->UserLevel),
				GetSecurity()->GetMD5ForUserInfo(user->Username.c_str(), passw.c_str()),
				passw.c_str()));

			newUser->SetPersonId(updatingUser->GetPersonId());
			newUser->SetRightsId(updatingUser->GetRightsId());

			GetProcessorFactory()->createUpdateUserProcessor(newUser)->DoWork();
		}

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::GetWsdlUrl(_tds__GetWsdlUrl* tds__GetWsdlUrl, _tds__GetWsdlUrlResponse& tds__GetWsdlUrlResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }
        
        tds__GetWsdlUrlResponse.WsdlUrl = "http://www.onvif.org/ver10/device/wsdl/devicemgmt.wsdl";
        return SOAP_OK;
    }

    int DeviceBindingServiceImpl::GetCapabilities(_tds__GetCapabilities* tds__GetCapabilities, _tds__GetCapabilitiesResponse& tds__GetCapabilitiesResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }

        auto _capabilities = soap_new_tt__Capabilities(this->soap);

        if(tds__GetCapabilities->Category.empty())
        {
            fillCapabilityCategory(_capabilities, tt__CapabilityCategory__All);
        }

        BOOST_FOREACH(const enum tt__CapabilityCategory c, tds__GetCapabilities->Category)
        {
            if(auto error = fillCapabilityCategory(_capabilities, c))
            {
                return error;
            }
        }

        tds__GetCapabilitiesResponse.Capabilities = _capabilities;
		return SOAP_OK;
	}

    int DeviceBindingServiceImpl::GetHostname(_tds__GetHostname* tds__GetHostname, _tds__GetHostnameResponse& tds__GetHostnameResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }

        tds__GetHostnameResponse.HostnameInformation = soap_new_tt__HostnameInformation(this->soap);
        tds__GetHostnameResponse.HostnameInformation->FromDHCP = false;
        tds__GetHostnameResponse.HostnameInformation->Name = OnvifUtil::StringValue(this->soap,GetConfiguration()->GetHostName());

        return SOAP_OK;
    }

    int DeviceBindingServiceImpl::SetHostname(_tds__SetHostname* tds__SetHostname, _tds__SetHostnameResponse& tds__SetHostnameResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
        {
            return auth_result;
        }

        if(!GetConfiguration()->SetHostName(tds__SetHostname->Name))
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidHostname", NULL, false);
            return SOAP_FAULT;
        }

        return SOAP_OK;
    }

    int DeviceBindingServiceImpl::GetDNS(_tds__GetDNS* tds__GetDNS, _tds__GetDNSResponse& tds__GetDNSResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
        {
            return auth_result;
        }

		auto adapters = NetworkUtil::GetAdapters();
		auto foundAdapterPtr = std::find_if(adapters.begin(), adapters.end(), [&](const AdapterDescriptorSP& ad) { return ad->m_ipv4 == StringUtils::getwstring(GetCoreSettings()->GetModuleIpAdress()); });
		auto adapterInfo = (foundAdapterPtr != adapters.end() ? *foundAdapterPtr : AdapterDescriptorSP());
		if (!adapterInfo) return SOAP_ERR;

		tds__GetDNSResponse.DNSInformation = soap_new_tt__DNSInformation(this->soap);

		BOOST_FOREACH(auto dns, adapterInfo->m_dns)
		{
			auto ipAddr = soap_new_tt__IPAddress(this->soap);
			ipAddr->IPv4Address = soap_new_std__string(soap);
			ipAddr->IPv4Address->assign(StringUtils::getstring(dns.dnsAddress));

			if (dns.isManual)
			{
				tds__GetDNSResponse.DNSInformation->DNSManual.push_back(ipAddr);
			}
			else
			{
				tds__GetDNSResponse.DNSInformation->DNSFromDHCP.push_back(ipAddr);
			}
		}

		tds__GetDNSResponse.DNSInformation->FromDHCP = !tds__GetDNSResponse.DNSInformation->DNSFromDHCP.empty();

 		auto searchDomains = NetworkUtil::GetSearchDomainsList();
		if (!searchDomains.empty()) {
			tds__GetDNSResponse.DNSInformation->SearchDomain = searchDomains;
		}

        return SOAP_OK;
    }

    int DeviceBindingServiceImpl::SetDNS(_tds__SetDNS* tds__SetDNS, _tds__SetDNSResponse& tds__SetDNSResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
        {
            return auth_result;
        }

        if(!GetCoreSettings()->IsDhcpConfigurationEnabled())
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:NotImplemented", "This action is not enabled for the device", false);
            return SOAP_FAULT;
        }

		auto adapters = NetworkUtil::GetAdapters();
		auto foundAdapterPtr = std::find_if(adapters.begin(), adapters.end(), [&](const AdapterDescriptorSP& ad) { return ad->m_ipv4 == StringUtils::getwstring(GetCoreSettings()->GetModuleIpAdress()); });
		auto adapterInfo = (foundAdapterPtr != adapters.end() ? *foundAdapterPtr : AdapterDescriptorSP());
		if (!adapterInfo) return SOAP_FAULT;

		NetworkUtil::SetSearchDomainsList(tds__SetDNS->SearchDomain);
        
		if(tds__SetDNS->FromDHCP)
        {
                NetworkUtil::SetAdapterDnsDhcp(GetLogger(), StringUtils::getstring(adapterInfo->m_regAdapterName));
        }
        else
        {
			std::vector<DnsInfo> dnsIps;
            BOOST_FOREACH(auto dns, tds__SetDNS->DNSManual)
            {
                dnsIps.push_back(DnsInfo(StringUtils::getwstring(*dns->IPv4Address), true));
            }

            if (!NetworkUtil::SetDNSAdapterInf(GetLogger(), StringUtils::getstring(adapterInfo->m_regAdapterName), dnsIps))
                return SOAP_FAULT;
        }

        return SOAP_OK;
    }

	int DeviceBindingServiceImpl::GetNTP(_tds__GetNTP* tds__GetNTP, _tds__GetNTPResponse& tds__GetNTPResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
		{
			return auth_result;
		}

		//TODO: get ntp mashine parameters
		tds__GetNTPResponse.NTPInformation = soap_new_tt__NTPInformation(this->soap);
		tds__GetNTPResponse.NTPInformation->FromDHCP = true;

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::GetNetworkInterfaces(_tds__GetNetworkInterfaces* tds__GetNetworkInterfaces, _tds__GetNetworkInterfacesResponse& tds__GetNetworkInterfacesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
        {
            return auth_result;
        }
        
		auto adapters = NetworkUtil::GetAdapters();
		BOOST_FOREACH (const auto &a, adapters)
		{
			if (a->m_ipv4.empty() || a->m_ipv4 != StringUtils::getwstring(GetCoreSettings()->GetModuleIpAdress())) continue;

			auto netwInterface = soap_new_tt__NetworkInterface(this->soap);
			netwInterface->Enabled = true;
			netwInterface->token = StringUtils::getstring(a->m_regAdapterName);
			netwInterface->IPv4 = soap_new_tt__IPv4NetworkInterface(this->soap);
			netwInterface->IPv4->Enabled = true;
			netwInterface->IPv4->Config = soap_new_tt__IPv4Configuration(this->soap);
            netwInterface->Info = soap_new_tt__NetworkInterfaceInfo(this->soap);
            netwInterface->Info->HwAddress = a->m_physicalAddress;

			if (a->dhcp_enabled)
			{
				netwInterface->IPv4->Config->FromDHCP = soap_new_tt__PrefixedIPv4Address(this->soap);
				netwInterface->IPv4->Config->FromDHCP->Address = StringUtils::getstring(a->m_ipv4);
			}
			else
			{
				
				auto add = soap_new_tt__PrefixedIPv4Address(this->soap);
				add->Address = StringUtils::getstring(a->m_ipv4);
				netwInterface->IPv4->Config->Manual.push_back(add);
			}
			
			netwInterface->IPv4->Config->DHCP = a->dhcp_enabled;
			tds__GetNetworkInterfacesResponse.NetworkInterfaces.push_back(netwInterface);
		}

        return SOAP_OK;
    }

	int DeviceBindingServiceImpl::SetNetworkInterfaces(_tds__SetNetworkInterfaces* tds__SetNetworkInterfaces, _tds__SetNetworkInterfacesResponse& tds__SetNetworkInterfacesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
		{
			return auth_result;
		}

        if (!GetCoreSettings()->IsDhcpConfigurationEnabled())
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:NotImplemented", "This action is not enabled for the device", false);
            return SOAP_FAULT;
        }

		tds__SetNetworkInterfacesResponse.RebootNeeded = true;
#ifdef _WINDOWS_DEF
		if (tds__SetNetworkInterfaces->NetworkInterface->IPv4)
		{
			CoreSettingsSP newSettings(new CoreSettings(*GetCoreSettings().get()));
			if (tds__SetNetworkInterfaces->NetworkInterface->IPv4->Manual.size() > 0)
			{
				std::string newIpAdress = tds__SetNetworkInterfaces->NetworkInterface->IPv4->Manual.front()->Address;
				newSettings->SetIpAdress(newIpAdress);
				
				m_finishCallback = [&, newSettings]()
				{
                    NetworkUtil::SetAdapterIpv4(GetLogger(), GetCoreSettings()->GetModuleIpAdress(), newSettings->GetModuleIpAdress());
					Sleep(2000);

					GetProcessorFactory()->createRebootProcessor(newSettings)->DoWork();
				};

				return SOAP_OK;
			}
			if (tds__SetNetworkInterfaces->NetworkInterface->IPv4->DHCP)
			{
				std::string interfaceToken = tds__SetNetworkInterfaces->InterfaceToken;
				m_finishCallback = [&, newSettings, interfaceToken]()
				{
                    NetworkUtil::EnableAdapterDhcp(GetLogger(), interfaceToken);
                    Sleep(2000);

					newSettings->SetIpAdress(StringUtils::getstring(NetworkUtil::GetAddress(interfaceToken)));
					GetProcessorFactory()->createRebootProcessor(newSettings)->DoWork();
				};
				return SOAP_OK;
			}
			
			m_finishCallback = [&, newSettings]()
			{
				GetProcessorFactory()->createRebootProcessor(newSettings)->DoWork();
			};
		}
#endif
// Implement network interface configuration for Linux
		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::GetNetworkProtocols(_tds__GetNetworkProtocols* tds__GetNetworkProtocols, _tds__GetNetworkProtocolsResponse& tds__GetNetworkProtocolsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
        {
            return auth_result;
        }

		BOOST_FOREACH(auto proto, GetCoreSettings()->GetNetworkProtocols())
        {
			auto soap_proto = soap_new_tt__NetworkProtocol(this->soap);
			soap_s2tt__NetworkProtocolType(this->soap, proto->name.c_str(), &soap_proto->Name);
			soap_proto->Port.push_back(proto->port);
			soap_proto->Enabled = proto->isEnabled;

			tds__GetNetworkProtocolsResponse.NetworkProtocols.push_back(soap_proto);
        }

        return SOAP_OK;
    }

    int DeviceBindingServiceImpl::SetNetworkProtocols(_tds__SetNetworkProtocols* tds__SetNetworkProtocols, _tds__SetNetworkProtocolsResponse& tds__SetNetworkProtocolsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
        {
            return auth_result;
        }

        BOOST_FOREACH(auto proto, tds__SetNetworkProtocols->NetworkProtocols)
        {
            if(proto->Name == tt__NetworkProtocolType__HTTPS)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ServiceNotSupported", NULL, false);
                return SOAP_FAULT;
            }

			GetCoreSettings()->SetNetworkProtocol(soap_tt__NetworkProtocolType2s(this->soap, proto->Name), proto->Enabled);
        }

        return SOAP_OK;
    }

    int DeviceBindingServiceImpl::GetNetworkDefaultGateway(_tds__GetNetworkDefaultGateway* tds__GetNetworkDefaultGateway, _tds__GetNetworkDefaultGatewayResponse& tds__GetNetworkDefaultGatewayResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
        {
            return auth_result;
        }

        tds__GetNetworkDefaultGatewayResponse.NetworkGateway = soap_new_tt__NetworkGateway(this->soap);
        auto adapterInfo = NetworkUtil::FindAdapter(GetCoreSettings()->GetModuleIpAdress());
        if (adapterInfo)
        {
            BOOST_FOREACH(auto gatew, adapterInfo->m_gateways)
            {
                tds__GetNetworkDefaultGatewayResponse.NetworkGateway->IPv4Address.push_back(gatew);
            }
        }

        return SOAP_OK;
    }

    int DeviceBindingServiceImpl::SetNetworkDefaultGateway(_tds__SetNetworkDefaultGateway* tds__SetNetworkDefaultGateway, _tds__SetNetworkDefaultGatewayResponse& tds__SetNetworkDefaultGatewayResponse)
    {
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
		{
			return auth_result;
		}

        if (!GetCoreSettings()->IsDhcpConfigurationEnabled())
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:NotImplemented", "This action is not enabled for the device", false);
            return SOAP_FAULT;
        }

		if (tds__SetNetworkDefaultGateway->IPv4Address.empty())
		{
			auto adapters = NetworkUtil::GetAdapters();
			auto foundAdapterPtr = std::find_if(adapters.begin(), adapters.end(), [&](const AdapterDescriptorSP& ad) { return ad->m_ipv4 == StringUtils::getwstring(GetCoreSettings()->GetModuleIpAdress()); });
			auto adapterInfo = (foundAdapterPtr != adapters.end() ? *foundAdapterPtr : AdapterDescriptorSP());
			if (!adapterInfo) return SOAP_ERR;
			
            NetworkUtil::EnableAdapterDhcp(GetLogger(), StringUtils::getstring(adapterInfo->m_regAdapterName));
		}
		else
		{
			//NetworkUtil::SetDefaultGatewayInf(GetCoreSettings()->GetModuleIpAdress(), tds__SetNetworkDefaultGateway->IPv4Address);
			//GetProcessorFactory()->createRebootProcessor(m_coreSettings)->DoWork();
		}
        return SOAP_OK;
    }

	int DeviceBindingServiceImpl::GetCertificates(_tds__GetCertificates* tds__GetCertificates,
		_tds__GetCertificatesResponse& tds__GetCertificatesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
		{
			return auth_result;
		}

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::GetCertificatesStatus(_tds__GetCertificatesStatus* tds__GetCertificatesStatus,
		_tds__GetCertificatesStatusResponse& tds__GetCertificatesStatusResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
		{
			return auth_result;
		}

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::GetRelayOutputs(_tds__GetRelayOutputs* tds__GetRelayOutputs, _tds__GetRelayOutputsResponse& tds__GetRelayOutputsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

		ReadLock_t l(GetConfiguration()->getLock());

		BOOST_FOREACH(auto relayConf,GetConfiguration()->GetRelaysConfigurations())
		{
			tds__GetRelayOutputsResponse.RelayOutputs.push_back(GetProxy()->createRelayOutput(this->soap, relayConf));
		}

        return SOAP_OK;
    }

	int DeviceBindingServiceImpl::SetRelayOutputSettings(_tds__SetRelayOutputSettings* tds__SetRelayOutputSettings, _tds__SetRelayOutputSettingsResponse& tds__SetRelayOutputSettingsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto conf = GetConfiguration()->findRelayConfiguration(tds__SetRelayOutputSettings->RelayOutputToken);

		if (!conf)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		if (tds__SetRelayOutputSettings->Properties->Mode == tt__RelayMode__Monostable)
		{
			conf->SetMode(RelayOutput::Monostable);
		}
		else if (tds__SetRelayOutputSettings->Properties->Mode == tt__RelayMode__Bistable)
		{
			conf->SetMode(RelayOutput::Bistable);
		}

		if (tds__SetRelayOutputSettings->Properties->IdleState == tt__RelayIdleState__open)
		{
			conf->SetIdleState(RelayOutput::on);
		}
		else if (tds__SetRelayOutputSettings->Properties->IdleState == tt__RelayIdleState__closed)
		{
			conf->SetIdleState(RelayOutput::off);
		}

		conf->SetDelayTime(tds__SetRelayOutputSettings->Properties->DelayTime);		

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::SetRelayOutputState(_tds__SetRelayOutputState* tds__SetRelayOutputState, _tds__SetRelayOutputStateResponse& tds__SetRelayOutputStateResponse)
	{
		std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);
		
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto conf = GetConfiguration()->findRelayConfiguration(tds__SetRelayOutputState->RelayOutputToken);

		if (!conf)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		IIOProcessor::ActionCommand action = tds__SetRelayOutputState->LogicalState == tt__RelayLogicalState__inactive ? IIOProcessor::action_off : IIOProcessor::action_on;

		auto releProc = GetProcessorFactory()->createIOProcessor(user_id);
		releProc->ReleCommand(conf, action);

		return SOAP_OK;
	}

	int DeviceBindingServiceImpl::fillCapabilityCategory(tt__Capabilities* _capabilities, const enum tt__CapabilityCategory c) const
    {
        switch (c)
        {
        case tt__CapabilityCategory__All:
            {
                fillMedia(_capabilities);
                fillPTZ(_capabilities);
                fillEvents(_capabilities);
                fillImaging(_capabilities);
                fillDevice(_capabilities);
                fillAnalytics(_capabilities);
                fillExtention(_capabilities);
            }
            break;
        case tt__CapabilityCategory__Analytics:
            fillAnalytics(_capabilities);
            break;
        case tt__CapabilityCategory__Device:
            fillDevice(_capabilities);
            break;
        case tt__CapabilityCategory__Events:
            fillEvents(_capabilities);
            break;
        case tt__CapabilityCategory__Imaging:
            fillImaging(_capabilities);
            break;
        case tt__CapabilityCategory__Media:
            fillMedia(_capabilities);
            break;
        case tt__CapabilityCategory__PTZ:
            fillPTZ(_capabilities);
            break;
        default:
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:NoSuchService", nullptr, true);
                return SOAP_FAULT;
            }
        }

        return SOAP_OK;
    }

    tt__DeviceCapabilities* DeviceBindingServiceImpl::GetDeviceCapabilities() const
    {
        auto _capabilities = soap_new_tt__DeviceCapabilities(this->soap);
        _capabilities->XAddr = GetCoreSettings()->GetServiceUri() + DeviceBindingServiceImpl::GetUri();

        _capabilities->Security = soap_new_tt__SecurityCapabilities(this->soap);
        _capabilities->Security->AccessPolicyConfig = false;
		_capabilities->Security->RELToken = true;
 
        _capabilities->Network = soap_new_tt__NetworkCapabilities(this->soap);
        _capabilities->Network->IPVersion6 = OnvifUtil::BoolValue(this->soap, false);

        _capabilities->System = soap_new_tt__SystemCapabilities(this->soap);
        _capabilities->System->FirmwareUpgrade = false;
        _capabilities->System->DiscoveryBye = false;
        _capabilities->System->DiscoveryResolve = false;
        _capabilities->System->RemoteDiscovery = false;
        _capabilities->System->SystemBackup = false;
        _capabilities->System->SystemLogging = false;

        _capabilities->System->SupportedVersions.push_back(createSupportedVersion(2, 1));
        _capabilities->System->SupportedVersions.push_back(createSupportedVersion(2, 2));
        _capabilities->System->SupportedVersions.push_back(createSupportedVersion(2, 4));
        _capabilities->System->SupportedVersions.push_back(createSupportedVersion(2, 5));
        _capabilities->System->SupportedVersions.push_back(createSupportedVersion(2, 6));

        _capabilities->IO = soap_new_tt__IOCapabilities(this->soap);
        _capabilities->IO->RelayOutputs = soap_new_int(this->soap);
        *_capabilities->IO->RelayOutputs = int(GetConfiguration()->GetRelaysConfigurations().size());

        _capabilities->IO->InputConnectors = soap_new_int(this->soap);
        *_capabilities->IO->InputConnectors = int(GetConfiguration()->GetRaysConfigurations().size());

        return _capabilities;
	}

	void DeviceBindingServiceImpl::fillMedia(tt__Capabilities *_capabilities) const
	{
        _capabilities->Media = soap_new_tt__MediaCapabilities(this->soap);
        _capabilities->Media->XAddr = GetCoreSettings()->GetServiceUri() + MediaBindingServiceImpl::GetUri();
        _capabilities->Media->StreamingCapabilities = soap_new_tt__RealTimeStreamingCapabilities(this->soap);
        _capabilities->Media->StreamingCapabilities->RTPMulticast = OnvifUtil::BoolValue(this->soap, GetCoreSettings()->IsMulticastEnabled());
        _capabilities->Media->StreamingCapabilities->RTP_USCORETCP = OnvifUtil::BoolValue(this->soap, true);
        _capabilities->Media->StreamingCapabilities->RTP_USCORERTSP_USCORETCP = OnvifUtil::BoolValue(this->soap, true);
	}

    void DeviceBindingServiceImpl::fillPTZ(tt__Capabilities *_capabilities) const
    {
        _capabilities->PTZ = soap_new_tt__PTZCapabilities(this->soap);
        _capabilities->PTZ->XAddr = GetCoreSettings()->GetServiceUri() + PtzBindingServiceImpl::GetUri();
    }

	void DeviceBindingServiceImpl::fillAnalytics(tt__Capabilities *_capabilities) const
	{
        _capabilities->Analytics = soap_new_tt__AnalyticsCapabilities(this->soap);
        _capabilities->Analytics->XAddr = GetCoreSettings()->GetServiceUri() + AnalyticsEngineBindingServiceImpl::GetUri();
        _capabilities->Analytics->RuleSupport = true;
        _capabilities->Analytics->AnalyticsModuleSupport = true;
	}

    void DeviceBindingServiceImpl::fillEvents(tt__Capabilities *_capabilities) const
    {
        _capabilities->Events = soap_new_tt__EventCapabilities(this->soap);
        _capabilities->Events->XAddr = GetCoreSettings()->GetServiceUri() + EventBindingServiceImpl::GetUri();
        _capabilities->Events->WSPausableSubscriptionManagerInterfaceSupport = false;
        _capabilities->Events->WSPullPointSupport = true;
        _capabilities->Events->WSSubscriptionPolicySupport = false;
    }

    void DeviceBindingServiceImpl::fillDevice(tt__Capabilities *_capabilities) const
    {
        _capabilities->Device = GetDeviceCapabilities();
    }

    void DeviceBindingServiceImpl::fillImaging(tt__Capabilities *_capabilities) const
    {
        _capabilities->Imaging = soap_new_tt__ImagingCapabilities(this->soap);
        _capabilities->Imaging->XAddr = GetCoreSettings()->GetServiceUri() + ImagingBindingServiceImpl::GetUri();
    }

    void DeviceBindingServiceImpl::fillExtention(tt__Capabilities *_capabilities) const
    {
        _capabilities->Extension = soap_new_tt__CapabilitiesExtension(this->soap);
        _capabilities->Extension->DeviceIO = soap_new_tt__DeviceIOCapabilities(this->soap);
        _capabilities->Extension->DeviceIO->XAddr = GetCoreSettings()->GetServiceUri() + DeviceIOBindingServiceImpl::GetUri();
        _capabilities->Extension->DeviceIO->VideoSources = GetConfiguration()->GetVSources().size();
        _capabilities->Extension->DeviceIO->VideoOutputs = 0;
        _capabilities->Extension->DeviceIO->AudioSources = GetConfiguration()->GetASources().size();
        _capabilities->Extension->DeviceIO->AudioOutputs = 0;
        _capabilities->Extension->DeviceIO->RelayOutputs = GetConfiguration()->GetRelaysConfigurations().size();

        _capabilities->Extension->Recording = soap_new_tt__RecordingCapabilities(this->soap);
        _capabilities->Extension->Recording->XAddr = GetCoreSettings()->GetServiceUri() + RecordingBindingServiceImpl::GetUri();
        _capabilities->Extension->Recording->DynamicRecordings = false;
        _capabilities->Extension->Recording->DynamicTracks = false;
        _capabilities->Extension->Recording->MaxStringLength = 1024;
        _capabilities->Extension->Recording->MediaProfileSource = true;
        _capabilities->Extension->Recording->ReceiverSource = false;

        _capabilities->Extension->Search = soap_new_tt__SearchCapabilities(this->soap);
        _capabilities->Extension->Search->XAddr = GetCoreSettings()->GetServiceUri() + SearchBindingServiceImpl::GetUri();
        _capabilities->Extension->Search->MetadataSearch = false;

        _capabilities->Extension->Replay = soap_new_tt__ReplayCapabilities(this->soap);
        _capabilities->Extension->Replay->XAddr = GetCoreSettings()->GetServiceUri() + ReplayBindingServiceImpl::GetUri();

        _capabilities->Extension->AnalyticsDevice = soap_new_tt__AnalyticsDeviceCapabilities(this->soap);
        _capabilities->Extension->AnalyticsDevice->XAddr = GetCoreSettings()->GetServiceUri() + AnalyticsEngineBindingServiceImpl::GetUri();
        _capabilities->Extension->AnalyticsDevice->RuleSupport = OnvifUtil::BoolValue(this->soap, false);
    }

    void DeviceBindingServiceImpl::addScope(const std::string& scope, std::vector<tt__Scope *>& scopes)
    {
        auto sk1 = soap_new_tt__Scope(this->soap);
        sk1->ScopeItem = scope;
        scopes.push_back(sk1);
    }

    tt__OnvifVersion* DeviceBindingServiceImpl::createSupportedVersion(int _major, int _minor) const
    {
        auto supportedVer = soap_new_tt__OnvifVersion(this->soap);
        supportedVer->Major = _major;
        supportedVer->Minor = _minor;

        return supportedVer;
    }
}
