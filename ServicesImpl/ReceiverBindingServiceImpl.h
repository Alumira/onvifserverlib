#pragma once

#include "onvif_generated_ReceiverBindingService.h"
#include <Interface/IServiceImpl.h>

namespace onvif_generated_ {
	class ReceiverBindingServiceImpl : public ReceiverBindingService, public IServiceImpl
	{
	public:
        ReceiverBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data);

        ReceiverBindingService* copy() override { return new ReceiverBindingServiceImpl(*this); }


        int GetServiceCapabilities(_trv__GetServiceCapabilities* trv__GetServiceCapabilities, _trv__GetServiceCapabilitiesResponse& trv__GetServiceCapabilitiesResponse) override;
	    int GetReceivers(_trv__GetReceivers* trv__GetReceivers, _trv__GetReceiversResponse& trv__GetReceiversResponse) override { return defaultImplementation(); }
	    int GetReceiver(_trv__GetReceiver* trv__GetReceiver, _trv__GetReceiverResponse& trv__GetReceiverResponse) override { return defaultImplementation(); }
	    int CreateReceiver(_trv__CreateReceiver* trv__CreateReceiver, _trv__CreateReceiverResponse& trv__CreateReceiverResponse) override { return defaultImplementation(); }
	    int DeleteReceiver(_trv__DeleteReceiver* trv__DeleteReceiver, _trv__DeleteReceiverResponse& trv__DeleteReceiverResponse) override { return defaultImplementation(); }
	    int ConfigureReceiver(_trv__ConfigureReceiver* trv__ConfigureReceiver, _trv__ConfigureReceiverResponse& trv__ConfigureReceiverResponse) override { return defaultImplementation(); }
	    int SetReceiverMode(_trv__SetReceiverMode* trv__SetReceiverMode, _trv__SetReceiverModeResponse& trv__SetReceiverModeResponse) override { return defaultImplementation(); }
	    int GetReceiverState(_trv__GetReceiverState* trv__GetReceiverState, _trv__GetReceiverStateResponse& trv__GetReceiverStateResponse) override { return defaultImplementation(); }

        //IServiceImpl

        int Dispatch(struct soap* soapPtr) override
        {
            return ReceiverBindingService::dispatch(soapPtr);
        }

        static trv__Capabilities* GetServiceCapabilities(struct soap* soapObj);

        static std::string GetUri()
        {
            return "/receiver_service";
        }
	};
}
