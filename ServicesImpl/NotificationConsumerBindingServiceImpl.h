#pragma once

#include "onvif_generated_NotificationConsumerBindingService.h"
#include <Interface/IServiceImpl.h>
#include <BackgroundServices/TaskCommander.h>

namespace onvif_generated_
{
	class NotificationConsumerBindingServiceImpl : public NotificationConsumerBindingService, public IServiceImpl
	{
	public:
		NotificationConsumerBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander);
		NotificationConsumerBindingService* copy() override { return new NotificationConsumerBindingServiceImpl(*this); }

		//IServiceImpl
		int Dispatch(struct soap* soapPtr) override
		{
			return NotificationConsumerBindingService::dispatch(soapPtr);
		}

		int Notify(_wsnt__Notify* wsnt__Notify) override;

	private:
		TaskCommanderSP m_taskCommander;
	};
}
