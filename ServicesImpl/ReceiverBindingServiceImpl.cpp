#include "ReceiverBindingServiceImpl.h"

namespace onvif_generated_ {
    ReceiverBindingServiceImpl::ReceiverBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data) :
            ReceiverBindingService(soapObj->GetSoap())
            , IServiceImpl(data)
	{
	}

    int ReceiverBindingServiceImpl::GetServiceCapabilities(_trv__GetServiceCapabilities* trv__GetServiceCapabilities, _trv__GetServiceCapabilitiesResponse& trv__GetServiceCapabilitiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }
        
        trv__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities(this->soap);
        return SOAP_OK;
    }

    trv__Capabilities* ReceiverBindingServiceImpl::GetServiceCapabilities(struct soap* soapObj)
    {
        auto _capabilities = soap_new_trv__Capabilities(soapObj);
        _capabilities->SupportedReceivers = 1;

        return _capabilities;
    }
}

