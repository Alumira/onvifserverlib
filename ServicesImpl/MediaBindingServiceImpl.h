#pragma once

#include "onvif_generated_MediaBindingService.h"
#include <Interface/IServiceImpl.h>
#include <SoapProxy/SoapUtil.h>
#include <BackgroundServices/TaskCommander.h>

namespace onvif_generated_ 
{
	class MediaBindingServiceImpl : public MediaBindingService, public IServiceImpl
	{
	public:
		MediaBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander);

		MediaBindingService* copy() override { return new MediaBindingServiceImpl(*this); }

        int GetServiceCapabilities(_trt__GetServiceCapabilities* trt__GetServiceCapabilities, _trt__GetServiceCapabilitiesResponse& trt__GetServiceCapabilitiesResponse) override;
		int GetVideoSources(_trt__GetVideoSources* trt__GetVideoSources, _trt__GetVideoSourcesResponse& trt__GetVideoSourcesResponse) override;
        int GetAudioSources(_trt__GetAudioSources* trt__GetAudioSources, _trt__GetAudioSourcesResponse& trt__GetAudioSourcesResponse) override;
        int GetAudioOutputs(_trt__GetAudioOutputs* trt__GetAudioOutputs, _trt__GetAudioOutputsResponse& trt__GetAudioOutputsResponse) override;
		int CreateProfile(_trt__CreateProfile* trt__CreateProfile, _trt__CreateProfileResponse& trt__CreateProfileResponse) override;
		int GetProfile(_trt__GetProfile* trt__GetProfile, _trt__GetProfileResponse& trt__GetProfileResponse) override;
		int GetProfiles(_trt__GetProfiles* trt__GetProfiles, _trt__GetProfilesResponse& trt__GetProfilesResponse) override;
		int AddVideoEncoderConfiguration(_trt__AddVideoEncoderConfiguration* trt__AddVideoEncoderConfiguration, _trt__AddVideoEncoderConfigurationResponse& trt__AddVideoEncoderConfigurationResponse) override;
		int AddVideoSourceConfiguration(_trt__AddVideoSourceConfiguration* trt__AddVideoSourceConfiguration, _trt__AddVideoSourceConfigurationResponse& trt__AddVideoSourceConfigurationResponse) override;
        int AddAudioEncoderConfiguration(_trt__AddAudioEncoderConfiguration* trt__AddAudioEncoderConfiguration, _trt__AddAudioEncoderConfigurationResponse& trt__AddAudioEncoderConfigurationResponse) override;
        int AddAudioSourceConfiguration(_trt__AddAudioSourceConfiguration* trt__AddAudioSourceConfiguration, _trt__AddAudioSourceConfigurationResponse& trt__AddAudioSourceConfigurationResponse) override;
        int AddPTZConfiguration(_trt__AddPTZConfiguration* trt__AddPTZConfiguration, _trt__AddPTZConfigurationResponse& trt__AddPTZConfigurationResponse) override;
		int AddVideoAnalyticsConfiguration(_trt__AddVideoAnalyticsConfiguration* trt__AddVideoAnalyticsConfiguration, _trt__AddVideoAnalyticsConfigurationResponse& trt__AddVideoAnalyticsConfigurationResponse) override;
		int AddMetadataConfiguration(_trt__AddMetadataConfiguration* trt__AddMetadataConfiguration, _trt__AddMetadataConfigurationResponse& trt__AddMetadataConfigurationResponse) override;
		int AddAudioOutputConfiguration(_trt__AddAudioOutputConfiguration* trt__AddAudioOutputConfiguration, _trt__AddAudioOutputConfigurationResponse& trt__AddAudioOutputConfigurationResponse) override { return defaultImplementation(); }
		int AddAudioDecoderConfiguration(_trt__AddAudioDecoderConfiguration* trt__AddAudioDecoderConfiguration, _trt__AddAudioDecoderConfigurationResponse& trt__AddAudioDecoderConfigurationResponse) override { return defaultImplementation(); }
        int RemoveVideoEncoderConfiguration(_trt__RemoveVideoEncoderConfiguration* trt__RemoveVideoEncoderConfiguration, _trt__RemoveVideoEncoderConfigurationResponse& trt__RemoveVideoEncoderConfigurationResponse) override;
        int RemoveVideoSourceConfiguration(_trt__RemoveVideoSourceConfiguration* trt__RemoveVideoSourceConfiguration, _trt__RemoveVideoSourceConfigurationResponse& trt__RemoveVideoSourceConfigurationResponse) override;
        int RemoveAudioEncoderConfiguration(_trt__RemoveAudioEncoderConfiguration* trt__RemoveAudioEncoderConfiguration, _trt__RemoveAudioEncoderConfigurationResponse& trt__RemoveAudioEncoderConfigurationResponse) override;
        int RemoveAudioSourceConfiguration(_trt__RemoveAudioSourceConfiguration* trt__RemoveAudioSourceConfiguration, _trt__RemoveAudioSourceConfigurationResponse& trt__RemoveAudioSourceConfigurationResponse) override;
        int RemovePTZConfiguration(_trt__RemovePTZConfiguration* trt__RemovePTZConfiguration, _trt__RemovePTZConfigurationResponse& trt__RemovePTZConfigurationResponse) override;
		int RemoveVideoAnalyticsConfiguration(_trt__RemoveVideoAnalyticsConfiguration* trt__RemoveVideoAnalyticsConfiguration, _trt__RemoveVideoAnalyticsConfigurationResponse& trt__RemoveVideoAnalyticsConfigurationResponse) override;
		int RemoveMetadataConfiguration(_trt__RemoveMetadataConfiguration* trt__RemoveMetadataConfiguration, _trt__RemoveMetadataConfigurationResponse& trt__RemoveMetadataConfigurationResponse) override;
		int RemoveAudioOutputConfiguration(_trt__RemoveAudioOutputConfiguration* trt__RemoveAudioOutputConfiguration, _trt__RemoveAudioOutputConfigurationResponse& trt__RemoveAudioOutputConfigurationResponse) override { return defaultImplementation(); }
		int RemoveAudioDecoderConfiguration(_trt__RemoveAudioDecoderConfiguration* trt__RemoveAudioDecoderConfiguration, _trt__RemoveAudioDecoderConfigurationResponse& trt__RemoveAudioDecoderConfigurationResponse) override { return defaultImplementation(); }
        int DeleteProfile(_trt__DeleteProfile* trt__DeleteProfile, _trt__DeleteProfileResponse& trt__DeleteProfileResponse) override;
		int GetVideoSourceConfigurations(_trt__GetVideoSourceConfigurations* trt__GetVideoSourceConfigurations, _trt__GetVideoSourceConfigurationsResponse& trt__GetVideoSourceConfigurationsResponse) override;
        int GetVideoEncoderConfigurations(_trt__GetVideoEncoderConfigurations* trt__GetVideoEncoderConfigurations, _trt__GetVideoEncoderConfigurationsResponse& trt__GetVideoEncoderConfigurationsResponse) override;
        int GetAudioSourceConfigurations(_trt__GetAudioSourceConfigurations* trt__GetAudioSourceConfigurations, _trt__GetAudioSourceConfigurationsResponse& trt__GetAudioSourceConfigurationsResponse) override;
        int GetAudioEncoderConfigurations(_trt__GetAudioEncoderConfigurations* trt__GetAudioEncoderConfigurations, _trt__GetAudioEncoderConfigurationsResponse& trt__GetAudioEncoderConfigurationsResponse) override;
		int GetVideoAnalyticsConfigurations(_trt__GetVideoAnalyticsConfigurations* trt__GetVideoAnalyticsConfigurations, _trt__GetVideoAnalyticsConfigurationsResponse& trt__GetVideoAnalyticsConfigurationsResponse) override;
        int GetMetadataConfigurations(_trt__GetMetadataConfigurations* trt__GetMetadataConfigurations, _trt__GetMetadataConfigurationsResponse& trt__GetMetadataConfigurationsResponse) override;
		int GetAudioOutputConfigurations(_trt__GetAudioOutputConfigurations* trt__GetAudioOutputConfigurations, _trt__GetAudioOutputConfigurationsResponse& trt__GetAudioOutputConfigurationsResponse) override { return defaultImplementation(); }
		int GetAudioDecoderConfigurations(_trt__GetAudioDecoderConfigurations* trt__GetAudioDecoderConfigurations, _trt__GetAudioDecoderConfigurationsResponse& trt__GetAudioDecoderConfigurationsResponse) override { return defaultImplementation(); }
		int GetVideoSourceConfiguration(_trt__GetVideoSourceConfiguration* trt__GetVideoSourceConfiguration, _trt__GetVideoSourceConfigurationResponse& trt__GetVideoSourceConfigurationResponse) override;
        int GetVideoEncoderConfiguration(_trt__GetVideoEncoderConfiguration* trt__GetVideoEncoderConfiguration, _trt__GetVideoEncoderConfigurationResponse& trt__GetVideoEncoderConfigurationResponse) override;
        int GetAudioSourceConfiguration(_trt__GetAudioSourceConfiguration* trt__GetAudioSourceConfiguration, _trt__GetAudioSourceConfigurationResponse& trt__GetAudioSourceConfigurationResponse) override;
        int GetAudioEncoderConfiguration(_trt__GetAudioEncoderConfiguration* trt__GetAudioEncoderConfiguration, _trt__GetAudioEncoderConfigurationResponse& trt__GetAudioEncoderConfigurationResponse) override;
		int GetVideoAnalyticsConfiguration(_trt__GetVideoAnalyticsConfiguration* trt__GetVideoAnalyticsConfiguration, _trt__GetVideoAnalyticsConfigurationResponse& trt__GetVideoAnalyticsConfigurationResponse) override;
		int GetMetadataConfiguration(_trt__GetMetadataConfiguration* trt__GetMetadataConfiguration, _trt__GetMetadataConfigurationResponse& trt__GetMetadataConfigurationResponse) override;
		int GetAudioOutputConfiguration(_trt__GetAudioOutputConfiguration* trt__GetAudioOutputConfiguration, _trt__GetAudioOutputConfigurationResponse& trt__GetAudioOutputConfigurationResponse) override { return defaultImplementation(); }
		int GetAudioDecoderConfiguration(_trt__GetAudioDecoderConfiguration* trt__GetAudioDecoderConfiguration, _trt__GetAudioDecoderConfigurationResponse& trt__GetAudioDecoderConfigurationResponse) override { return defaultImplementation(); }
		int GetCompatibleVideoEncoderConfigurations(_trt__GetCompatibleVideoEncoderConfigurations* trt__GetCompatibleVideoEncoderConfigurations, _trt__GetCompatibleVideoEncoderConfigurationsResponse& trt__GetCompatibleVideoEncoderConfigurationsResponse) override;
		int GetCompatibleVideoSourceConfigurations(_trt__GetCompatibleVideoSourceConfigurations* trt__GetCompatibleVideoSourceConfigurations, _trt__GetCompatibleVideoSourceConfigurationsResponse& trt__GetCompatibleVideoSourceConfigurationsResponse) override;
        int GetCompatibleAudioEncoderConfigurations(_trt__GetCompatibleAudioEncoderConfigurations* trt__GetCompatibleAudioEncoderConfigurations, _trt__GetCompatibleAudioEncoderConfigurationsResponse& trt__GetCompatibleAudioEncoderConfigurationsResponse) override;
        int GetCompatibleAudioSourceConfigurations(_trt__GetCompatibleAudioSourceConfigurations* trt__GetCompatibleAudioSourceConfigurations, _trt__GetCompatibleAudioSourceConfigurationsResponse& trt__GetCompatibleAudioSourceConfigurationsResponse) override;
		int GetCompatibleVideoAnalyticsConfigurations(_trt__GetCompatibleVideoAnalyticsConfigurations* trt__GetCompatibleVideoAnalyticsConfigurations, _trt__GetCompatibleVideoAnalyticsConfigurationsResponse& trt__GetCompatibleVideoAnalyticsConfigurationsResponse) override;
		int GetCompatibleMetadataConfigurations(_trt__GetCompatibleMetadataConfigurations* trt__GetCompatibleMetadataConfigurations, _trt__GetCompatibleMetadataConfigurationsResponse& trt__GetCompatibleMetadataConfigurationsResponse) override;
		int GetCompatibleAudioOutputConfigurations(_trt__GetCompatibleAudioOutputConfigurations* trt__GetCompatibleAudioOutputConfigurations, _trt__GetCompatibleAudioOutputConfigurationsResponse& trt__GetCompatibleAudioOutputConfigurationsResponse) override { return defaultImplementation(); }
		int GetCompatibleAudioDecoderConfigurations(_trt__GetCompatibleAudioDecoderConfigurations* trt__GetCompatibleAudioDecoderConfigurations, _trt__GetCompatibleAudioDecoderConfigurationsResponse& trt__GetCompatibleAudioDecoderConfigurationsResponse) override { return defaultImplementation(); }
        int SetVideoSourceConfiguration(_trt__SetVideoSourceConfiguration* trt__SetVideoSourceConfiguration, _trt__SetVideoSourceConfigurationResponse& trt__SetVideoSourceConfigurationResponse) override;
        int SetVideoEncoderConfiguration(_trt__SetVideoEncoderConfiguration* trt__SetVideoEncoderConfiguration, _trt__SetVideoEncoderConfigurationResponse& trt__SetVideoEncoderConfigurationResponse) override;
        int SetAudioSourceConfiguration(_trt__SetAudioSourceConfiguration* trt__SetAudioSourceConfiguration, _trt__SetAudioSourceConfigurationResponse& trt__SetAudioSourceConfigurationResponse) override;
        int SetAudioEncoderConfiguration(_trt__SetAudioEncoderConfiguration* trt__SetAudioEncoderConfiguration, _trt__SetAudioEncoderConfigurationResponse& trt__SetAudioEncoderConfigurationResponse) override;
		int SetVideoAnalyticsConfiguration(_trt__SetVideoAnalyticsConfiguration* trt__SetVideoAnalyticsConfiguration, _trt__SetVideoAnalyticsConfigurationResponse& trt__SetVideoAnalyticsConfigurationResponse) override;
		int SetMetadataConfiguration(_trt__SetMetadataConfiguration* trt__SetMetadataConfiguration, _trt__SetMetadataConfigurationResponse& trt__SetMetadataConfigurationResponse) override;
		int SetAudioOutputConfiguration(_trt__SetAudioOutputConfiguration* trt__SetAudioOutputConfiguration, _trt__SetAudioOutputConfigurationResponse& trt__SetAudioOutputConfigurationResponse) override { return defaultImplementation(); }
		int SetAudioDecoderConfiguration(_trt__SetAudioDecoderConfiguration* trt__SetAudioDecoderConfiguration, _trt__SetAudioDecoderConfigurationResponse& trt__SetAudioDecoderConfigurationResponse) override { return defaultImplementation(); }
        int GetVideoSourceConfigurationOptions(_trt__GetVideoSourceConfigurationOptions* trt__GetVideoSourceConfigurationOptions, _trt__GetVideoSourceConfigurationOptionsResponse& trt__GetVideoSourceConfigurationOptionsResponse) override;
		int GetVideoEncoderConfigurationOptions(_trt__GetVideoEncoderConfigurationOptions* trt__GetVideoEncoderConfigurationOptions, _trt__GetVideoEncoderConfigurationOptionsResponse& trt__GetVideoEncoderConfigurationOptionsResponse) override;
        int GetAudioSourceConfigurationOptions(_trt__GetAudioSourceConfigurationOptions* trt__GetAudioSourceConfigurationOptions, _trt__GetAudioSourceConfigurationOptionsResponse& trt__GetAudioSourceConfigurationOptionsResponse) override;
        int GetAudioEncoderConfigurationOptions(_trt__GetAudioEncoderConfigurationOptions* trt__GetAudioEncoderConfigurationOptions, _trt__GetAudioEncoderConfigurationOptionsResponse& trt__GetAudioEncoderConfigurationOptionsResponse) override;
		int GetMetadataConfigurationOptions(_trt__GetMetadataConfigurationOptions* trt__GetMetadataConfigurationOptions, _trt__GetMetadataConfigurationOptionsResponse& trt__GetMetadataConfigurationOptionsResponse) override;
		int GetAudioOutputConfigurationOptions(_trt__GetAudioOutputConfigurationOptions* trt__GetAudioOutputConfigurationOptions, _trt__GetAudioOutputConfigurationOptionsResponse& trt__GetAudioOutputConfigurationOptionsResponse) override { return defaultImplementation(); }
		int GetAudioDecoderConfigurationOptions(_trt__GetAudioDecoderConfigurationOptions* trt__GetAudioDecoderConfigurationOptions, _trt__GetAudioDecoderConfigurationOptionsResponse& trt__GetAudioDecoderConfigurationOptionsResponse) override { return defaultImplementation(); }
        int GetGuaranteedNumberOfVideoEncoderInstances(_trt__GetGuaranteedNumberOfVideoEncoderInstances* trt__GetGuaranteedNumberOfVideoEncoderInstances, _trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse& trt__GetGuaranteedNumberOfVideoEncoderInstancesResponse) override;
		int GetStreamUri(_trt__GetStreamUri* trt__GetStreamUri, _trt__GetStreamUriResponse& trt__GetStreamUriResponse) override;
        int StartMulticastStreaming(_trt__StartMulticastStreaming* trt__StartMulticastStreaming, _trt__StartMulticastStreamingResponse& trt__StartMulticastStreamingResponse) override;
        int StopMulticastStreaming(_trt__StopMulticastStreaming* trt__StopMulticastStreaming, _trt__StopMulticastStreamingResponse& trt__StopMulticastStreamingResponse) override;
        int SetSynchronizationPoint(_trt__SetSynchronizationPoint* trt__SetSynchronizationPoint, _trt__SetSynchronizationPointResponse& trt__SetSynchronizationPointResponse) override;
		int GetSnapshotUri(_trt__GetSnapshotUri* trt__GetSnapshotUri, _trt__GetSnapshotUriResponse& trt__GetSnapshotUriResponse) override;
		int GetVideoSourceModes(_trt__GetVideoSourceModes* trt__GetVideoSourceModes, _trt__GetVideoSourceModesResponse& trt__GetVideoSourceModesResponse) override { return defaultImplementation(); }
		int SetVideoSourceMode(_trt__SetVideoSourceMode* trt__SetVideoSourceMode, _trt__SetVideoSourceModeResponse& trt__SetVideoSourceModeResponse) override { return defaultImplementation(); }
		int GetOSDs(_trt__GetOSDs* trt__GetOSDs, _trt__GetOSDsResponse& trt__GetOSDsResponse) override { return defaultImplementation(); }
		int GetOSD(_trt__GetOSD* trt__GetOSD, _trt__GetOSDResponse& trt__GetOSDResponse) override { return defaultImplementation(); }
		int GetOSDOptions(_trt__GetOSDOptions* trt__GetOSDOptions, _trt__GetOSDOptionsResponse& trt__GetOSDOptionsResponse) override { return defaultImplementation(); }
		int SetOSD(_trt__SetOSD* trt__SetOSD, _trt__SetOSDResponse& trt__SetOSDResponse) override { return defaultImplementation(); }
		int CreateOSD(_trt__CreateOSD* trt__CreateOSD, _trt__CreateOSDResponse& trt__CreateOSDResponse) override { return defaultImplementation(); }
		int DeleteOSD(_trt__DeleteOSD* trt__DeleteOSD, _trt__DeleteOSDResponse& trt__DeleteOSDResponse) override { return defaultImplementation(); }

        //IServiceImpl

        int Dispatch(struct soap* soapPtr) override
        {
            return MediaBindingService::dispatch(soapPtr);
        }

        static trt__Capabilities* GetServiceCapabilities(struct soap* soapObj, CoreSettingsSP coreSettings);
		static MessageHolderTypeSP CreateProfileChangedEvent(struct soap* soap, MediaProfileSP profile);
		static MessageHolderTypeSP CreateVEncoderConfigurationChangedEvent(struct soap* soap, EncoderConfigurationSP config);

	    static std::string GetUri()
        {
            return "/media_service";
        }

	private:
		TaskCommanderSP m_taskCommander;

        void updateEncoderParams(VideoSourceConfigurationSP vSourceConfig);
        std::string convertEncodingName(tt__VideoEncoding& soapEncodingName);

        int defaultImplementation() override
        {
            if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
            {
                return auth_result;
            }

			OnvifUtil::ONVIF_Fault(this->soap, "ter:ActionNotSupported", "ter:NotImplemented", "This optinal method is not implemented", true);
            return SOAP_FAULT;
        }

        static bool checkCodec(const std::string& codec);
	    bool checkVideoSourceCodec(const VideoSourceConfigurationSP& vSourceConfig) const;
	};
}
