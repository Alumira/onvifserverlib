#pragma once

#include "onvif_generated_RecordingBindingService.h"
#include <Interface/IServiceImpl.h>
#include <Configuration/RecordingConfigurationTypes.h>

namespace onvif_generated_ {
	class RecordingBindingServiceImpl : public RecordingBindingService, public IServiceImpl
	{
	public:
        RecordingBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data);

        RecordingBindingService* copy() override { return new RecordingBindingServiceImpl(*this); }


        int GetServiceCapabilities(_trc__GetServiceCapabilities* trc__GetServiceCapabilities, _trc__GetServiceCapabilitiesResponse& trc__GetServiceCapabilitiesResponse) override;
	    int CreateRecording(_trc__CreateRecording* trc__CreateRecording, _trc__CreateRecordingResponse& trc__CreateRecordingResponse) override { return defaultImplementation(); }
	    int DeleteRecording(_trc__DeleteRecording* trc__DeleteRecording, _trc__DeleteRecordingResponse& trc__DeleteRecordingResponse) override { return defaultImplementation(); }
        int GetRecordings(_trc__GetRecordings* trc__GetRecordings, _trc__GetRecordingsResponse& trc__GetRecordingsResponse) override;
        int SetRecordingConfiguration(_trc__SetRecordingConfiguration* trc__SetRecordingConfiguration, _trc__SetRecordingConfigurationResponse& trc__SetRecordingConfigurationResponse) override;
        int GetRecordingConfiguration(_trc__GetRecordingConfiguration* trc__GetRecordingConfiguration, _trc__GetRecordingConfigurationResponse& trc__GetRecordingConfigurationResponse) override;
        int GetRecordingOptions(_trc__GetRecordingOptions* trc__GetRecordingOptions, _trc__GetRecordingOptionsResponse& trc__GetRecordingOptionsResponse) override;
	    int CreateTrack(_trc__CreateTrack* trc__CreateTrack, _trc__CreateTrackResponse& trc__CreateTrackResponse) override { return defaultImplementation(); }
	    int DeleteTrack(_trc__DeleteTrack* trc__DeleteTrack, _trc__DeleteTrackResponse& trc__DeleteTrackResponse) override { return defaultImplementation(); }
        int GetTrackConfiguration(_trc__GetTrackConfiguration* trc__GetTrackConfiguration, _trc__GetTrackConfigurationResponse& trc__GetTrackConfigurationResponse) override;
        int SetTrackConfiguration(_trc__SetTrackConfiguration* trc__SetTrackConfiguration, _trc__SetTrackConfigurationResponse& trc__SetTrackConfigurationResponse) override;
        int CreateRecordingJob(_trc__CreateRecordingJob* trc__CreateRecordingJob, _trc__CreateRecordingJobResponse& trc__CreateRecordingJobResponse) override;
        int DeleteRecordingJob(_trc__DeleteRecordingJob* trc__DeleteRecordingJob, _trc__DeleteRecordingJobResponse& trc__DeleteRecordingJobResponse) override;
        int GetRecordingJobs(_trc__GetRecordingJobs* trc__GetRecordingJobs, _trc__GetRecordingJobsResponse& trc__GetRecordingJobsResponse) override;
	    void sendRecordingJobConfigurationEvent(const RecordingJobSP& job, tt__PropertyOperation propOperation) const;
        int SetRecordingJobConfiguration(_trc__SetRecordingJobConfiguration* trc__SetRecordingJobConfiguration, _trc__SetRecordingJobConfigurationResponse& trc__SetRecordingJobConfigurationResponse) override;
        int GetRecordingJobConfiguration(_trc__GetRecordingJobConfiguration* trc__GetRecordingJobConfiguration, _trc__GetRecordingJobConfigurationResponse& trc__GetRecordingJobConfigurationResponse) override;
        int SetRecordingJobMode(_trc__SetRecordingJobMode* trc__SetRecordingJobMode, _trc__SetRecordingJobModeResponse& trc__SetRecordingJobModeResponse) override;
        int GetRecordingJobState(_trc__GetRecordingJobState* trc__GetRecordingJobState, _trc__GetRecordingJobStateResponse& trc__GetRecordingJobStateResponse) override;
	    int ExportRecordedData(_trc__ExportRecordedData* trc__ExportRecordedData, _trc__ExportRecordedDataResponse& trc__ExportRecordedDataResponse) override { return defaultImplementation(); }
	    int StopExportRecordedData(_trc__StopExportRecordedData* trc__StopExportRecordedData, _trc__StopExportRecordedDataResponse& trc__StopExportRecordedDataResponse) override { return defaultImplementation(); }
	    int GetExportRecordedDataState(_trc__GetExportRecordedDataState* trc__GetExportRecordedDataState, _trc__GetExportRecordedDataStateResponse& trc__GetExportRecordedDataStateResponse) override { return defaultImplementation(); }

        //IServiceImpl

        int Dispatch(struct soap* soapPtr) override
        {
            return RecordingBindingService::dispatch(soapPtr);
        }

        static trc__Capabilities* GetServiceCapabilities(struct soap* soapObj);
	    void sendRecordingConfigurationSetEvent(RecordingConfigurationSP recordingConf) const;
        void sendTrackConfigurationSetEvent(RecordingConfigurationSP recordingConf, RecordingTrackSP Track) const;
	    void sendJobStateEvent(RecordingJobSP recordingJob, tt__PropertyOperation propOperation) const;
	    static _tt__Message * CreateJobStateEvent(struct soap* soap, RecordingJobSP recordingJob, tt__PropertyOperation propOperation);
        static soap_dom_element createRecordingConfigEventsDescription(struct soap* soap);

        static std::string GetUri()
        {
            return "/recording_service";
        }
	};
}
