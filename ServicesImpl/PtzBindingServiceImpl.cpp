#include "PtzBindingServiceImpl.h"
#include <boost/foreach.hpp>
#include "Media2BindingServiceImpl.h"

namespace onvif_generated_ {
	PtzBindingServiceImpl::PtzBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data) :
		PTZBindingService(soapObj->GetSoap()), IServiceImpl(data)
	{
	}

	int PtzBindingServiceImpl::GetServiceCapabilities(_tptz2__GetServiceCapabilities* tptz2__GetServiceCapabilities, _tptz2__GetServiceCapabilitiesResponse& tptz2__GetServiceCapabilitiesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
		{
			return auth_result;
		}

		tptz2__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities(this->soap);
		return SOAP_OK;
	}

	int PtzBindingServiceImpl::GetConfigurations(_tptz2__GetConfigurations* tptz2__GetConfigurations, _tptz2__GetConfigurationsResponse& tptz2__GetConfigurationsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		BOOST_FOREACH(auto src, IServiceImpl::GetConfiguration()->GetVSourceConfigurations())
		{
			if (auto ptzConf = src.second->getPtzConfig())
			{
				tptz2__GetConfigurationsResponse.PTZConfiguration.push_back(GetProxy()->createPtzConfiguration(this->soap, ptzConf));
			}
		}

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::GetPresets(_tptz2__GetPresets* tptz2__GetPresets, _tptz2__GetPresetsResponse& tptz2__GetPresetsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__GetPresets->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__GetPresets->ProfileToken);
		}

		if (!profile->GetVideoSourceConfiguration() || !profile->GetVideoSourceConfiguration()->getPtzConfig())
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoPTZProfile");
			return SOAP_FAULT;
		}

		auto presets = profile->GetVideoSourceConfiguration()->getPtzConfig()->GetPresets();
		BOOST_FOREACH(auto presetConfig, presets)
		{
			tt__PTZPreset* preset = soap_new_req_tt__PTZPreset(this->soap);
			preset->token = soap_new_std__string(this->soap);
			preset->token->assign(presetConfig.second->GetToken());

			preset->Name = soap_new_std__string(this->soap);
			preset->Name->assign(presetConfig.second->GetName());

			preset->PTZPosition = soap_new_tt__PTZVector(this->soap);
			preset->PTZPosition->PanTilt =
				soap_new_set_tt__Vector2D(this->soap, presetConfig.second->m_position.x, presetConfig.second->m_position.y,
					OnvifUtil::StringValue(this->soap, "http://www.onvif.org/ver10/tptz/PanTiltSpaces/PositionGenericSpace"));

			preset->PTZPosition->Zoom =
				soap_new_set_tt__Vector1D(this->soap, presetConfig.second->m_position.zoom,
					OnvifUtil::StringValue(this->soap, "http://www.onvif.org/ver10/tptz/ZoomSpaces/PositionGenericSpace"));

			tptz2__GetPresetsResponse.Preset.push_back(preset);
		}

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::SetPreset(_tptz2__SetPreset* tptz2__SetPreset, _tptz2__SetPresetResponse& tptz2__SetPresetResponse)
	{
		std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);

		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		if (tptz2__SetPreset->ProfileToken.empty())
		{
            return no_profile(this->soap, tptz2__SetPreset->ProfileToken);
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__SetPreset->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__SetPreset->ProfileToken);
		}

		if (!profile->GetVideoSourceConfiguration() || !profile->GetVideoSourceConfiguration()->getPtzConfig())
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoPTZProfile");
			return SOAP_FAULT;
		}

        std::string presetName;
	    if(tptz2__SetPreset->PresetName)
	    {
            presetName = *tptz2__SetPreset->PresetName;
	    }
        else
        {
            presetName = boost::uuids::to_string(boost::uuids::random_generator()());
        }
	    auto newPreset = profile->GetPtzConfiguration()->AddPreset(presetName);

		auto telemetryProc = GetProcessorFactory()->createTelemetryProcessor(profile, user_id);
		telemetryProc->SetPreset(newPreset);

		tptz2__SetPresetResponse.PresetToken = newPreset->GetToken();

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::RemovePreset(_tptz2__RemovePreset* tptz2__RemovePreset, _tptz2__RemovePresetResponse& tptz2__RemovePresetResponse)
	{
		std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);

		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		if (tptz2__RemovePreset->ProfileToken.empty())
		{
            return no_profile(this->soap, tptz2__RemovePreset->ProfileToken);
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__RemovePreset->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__RemovePreset->ProfileToken);
		}

		if (!profile->GetVideoSourceConfiguration() || !profile->GetVideoSourceConfiguration()->getPtzConfig())
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoPTZProfile");
			return SOAP_FAULT;
		}

		auto newPreset = profile->GetPtzConfiguration()->GetPreset(tptz2__RemovePreset->PresetToken);
		newPreset->SetName("");

		auto telemetryProc = GetProcessorFactory()->createTelemetryProcessor(profile, user_id);
		telemetryProc->SetPreset(newPreset);

		profile->GetPtzConfiguration()->RemovePreset(tptz2__RemovePreset->PresetToken);

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::GotoPreset(_tptz2__GotoPreset* tptz2__GotoPreset, _tptz2__GotoPresetResponse& tptz2__GotoPresetResponse)
	{
		std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);

		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		if (tptz2__GotoPreset->ProfileToken.empty())
		{
            return no_profile(this->soap, tptz2__GotoPreset->ProfileToken);
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__GotoPreset->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__GotoPreset->ProfileToken);
		}

		if (!profile->GetVideoSourceConfiguration() || !profile->GetVideoSourceConfiguration()->getPtzConfig())
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoPTZProfile");
			return SOAP_FAULT;
		}

		auto presets = profile->GetPtzConfiguration()->GetPresets();

		BOOST_FOREACH(auto presetConfig, presets)
		{
			if (presetConfig.second->GetToken() == tptz2__GotoPreset->PresetToken)
			{
				auto telemetryProc = GetProcessorFactory()->createTelemetryProcessor(profile, user_id);
				telemetryProc->GoPreset(presetConfig.second);				

				break;
			}
		}

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::GetStatus(_tptz2__GetStatus* tptz2__GetStatus, _tptz2__GetStatusResponse& tptz2__GetStatusResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__GetStatus->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__GetStatus->ProfileToken);
		}

		auto ptzConf = profile->GetPtzConfiguration();
		if (!ptzConf)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoPTZProfile");
			return SOAP_FAULT;
		}

		tptz2__GetStatusResponse.PTZStatus = soap_new_tt__PTZStatus(this->soap);

		auto curTime = SoapProxy::GetCurTime();

		tptz2__GetStatusResponse.PTZStatus->UtcTime = *SoapProxy::convertTime(this->soap, curTime);
		tptz2__GetStatusResponse.PTZStatus->MoveStatus = soap_new_tt__PTZMoveStatus(this->soap);
		tptz2__GetStatusResponse.PTZStatus->MoveStatus->PanTilt = soap_new_tt__MoveStatus(this->soap);
		*tptz2__GetStatusResponse.PTZStatus->MoveStatus->PanTilt = tt__MoveStatus__IDLE;

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::GetConfiguration(_tptz2__GetConfiguration* tptz2__GetConfiguration, _tptz2__GetConfigurationResponse& tptz2__GetConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		BOOST_FOREACH(auto src, IServiceImpl::GetConfiguration()->GetVSourceConfigurations())
		{
			if (auto ptzConf = src.second->getPtzConfig())
			{
				if (tptz2__GetConfiguration->PTZConfigurationToken == ptzConf->GetConfigurationToken())
				{
					tptz2__GetConfigurationResponse.PTZConfiguration = GetProxy()->createPtzConfiguration(this->soap, ptzConf);
				}
			}
		}

		if (!tptz2__GetConfigurationResponse.PTZConfiguration)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::GetNodes(_tptz2__GetNodes* tptz2__GetNodes, _tptz2__GetNodesResponse& tptz2__GetNodesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		BOOST_FOREACH(auto src, IServiceImpl::GetConfiguration()->GetVSourceConfigurations())
		{
			if (auto ptzConf = src.second->getPtzConfig())
			{
				tptz2__GetNodesResponse.PTZNode.push_back(GetProxy()->createPtzNode(this->soap, ptzConf));
			}
		}

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::GetNode(_tptz2__GetNode* tptz2__GetNode, _tptz2__GetNodeResponse& tptz2__GetNodeResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		BOOST_FOREACH(auto src, IServiceImpl::GetConfiguration()->GetVSourceConfigurations())
		{
			auto ptzConf = src.second->getPtzConfig();
			if (ptzConf && ptzConf->GetToken() == tptz2__GetNode->NodeToken)
			{
				tptz2__GetNodeResponse.PTZNode = GetProxy()->createPtzNode(this->soap, ptzConf);
				break;
			}
		}

		if (!tptz2__GetNodeResponse.PTZNode)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoEntity");
			return SOAP_FAULT;
		}

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::SetConfiguration(_tptz2__SetConfiguration* tptz2__SetConfiguration, _tptz2__SetConfigurationResponse& tptz2__SetConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		BOOST_FOREACH(auto src, IServiceImpl::GetConfiguration()->GetVSourceConfigurations())
		{
			auto ptzConf = src.second->getPtzConfig();
			if (ptzConf && ptzConf->GetConfigurationToken() == tptz2__SetConfiguration->PTZConfiguration->token)
			{
				if (tptz2__SetConfiguration->PTZConfiguration->DefaultPTZTimeout)
				{
					ptzConf->SetPtzTimeout(*tptz2__SetConfiguration->PTZConfiguration->DefaultPTZTimeout);
				}

				if (!tptz2__SetConfiguration->PTZConfiguration->Name.empty())
				{
					ptzConf->SetName(tptz2__SetConfiguration->PTZConfiguration->Name);
				}

                auto evt = Media2BindingServiceImpl::CreateConfigurationChangedEvent(this->soap, ptzConf->GetConfigurationToken(), soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__PTZ));
                GetSubscriptionManager()->PushMessage(evt);

				return SOAP_OK;
			}
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
		return SOAP_FAULT;
	}

	int PtzBindingServiceImpl::GetConfigurationOptions(_tptz2__GetConfigurationOptions* tptz2__GetConfigurationOptions, _tptz2__GetConfigurationOptionsResponse& tptz2__GetConfigurationOptionsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		BOOST_FOREACH(auto src, IServiceImpl::GetConfiguration()->GetVSourceConfigurations())
		{
			auto ptzConf = src.second->getPtzConfig();
			if (ptzConf && ptzConf->GetConfigurationToken() == tptz2__GetConfigurationOptions->ConfigurationToken)
			{
				tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions = soap_new_tt__PTZConfigurationOptions(this->soap);
				tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions->Spaces = soap_new_tt__PTZSpaces(this->soap);

				if (ptzConf->IsAbsoluteCoordsSupported())
				{
					tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions->Spaces->AbsolutePanTiltPositionSpace.push_back(
						GetProxy()->CreateSpace2D(this->soap, "http://www.onvif.org/ver10/tptz/PanTiltSpaces/PositionGenericSpace"));
					tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions->Spaces->AbsoluteZoomPositionSpace.push_back(
						GetProxy()->CreateSpace1D(this->soap, "http://www.onvif.org/ver10/tptz/ZoomSpaces/PositionGenericSpace"));
				}

				tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions->Spaces->RelativePanTiltTranslationSpace.push_back(
					GetProxy()->CreateSpace2D(this->soap, "http://www.onvif.org/ver10/tptz/PanTiltSpaces/TranslationGenericSpace"));
				tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions->Spaces->RelativeZoomTranslationSpace.push_back(
					GetProxy()->CreateSpace1D(this->soap, "http://www.onvif.org/ver10/tptz/ZoomSpaces/TranslationGenericSpace"));

				tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions->Spaces->ContinuousPanTiltVelocitySpace.push_back(
					GetProxy()->CreateSpace2D(this->soap, "http://www.onvif.org/ver10/tptz/PanTiltSpaces/VelocityGenericSpace"));
				tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions->Spaces->ContinuousZoomVelocitySpace.push_back(
					GetProxy()->CreateSpace1D(this->soap, "http://www.onvif.org/ver10/tptz/ZoomSpaces/VelocityGenericSpace"));

				tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions->PTZTimeout = soap_new_tt__DurationRange(this->soap);
				tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions->PTZTimeout->Min = 0;
				tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions->PTZTimeout->Max = 1000000;
			}
		}

		if (!tptz2__GetConfigurationOptionsResponse.PTZConfigurationOptions)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::GotoHomePosition(_tptz2__GotoHomePosition* tptz2__GotoHomePosition, _tptz2__GotoHomePositionResponse& tptz2__GotoHomePositionResponse)
	{
		std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);

		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__GotoHomePosition->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__GotoHomePosition->ProfileToken);
		}

		if (!profile->GetPtzConfiguration())
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoPTZProfile");
			return SOAP_FAULT;
		}

		auto telemetryProc = GetProcessorFactory()->createTelemetryProcessor(profile, user_id);
		telemetryProc->Home();

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::SetHomePosition(_tptz2__SetHomePosition* tptz2__SetHomePosition,
		_tptz2__SetHomePositionResponse& tptz2__SetHomePositionResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__SetHomePosition->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__SetHomePosition->ProfileToken);
		}

		if (!profile->GetPtzConfiguration())
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoPTZProfile");
			return SOAP_FAULT;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:Action", "ter:CannotOverwriteHome", nullptr, true);
		return SOAP_FAULT;
	}

	int PtzBindingServiceImpl::ContinuousMove(_tptz2__ContinuousMove* tptz2__ContinuousMove, _tptz2__ContinuousMoveResponse& tptz2__ContinuousMoveResponse)
	{
		std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);

		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__ContinuousMove->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__ContinuousMove->ProfileToken);
		}

		if (!profile->GetPtzConfiguration())
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoPTZProfile");
			return SOAP_FAULT;
		}

		float move_x = 0;
		float move_y = 0;

		if (tptz2__ContinuousMove->Velocity && tptz2__ContinuousMove->Velocity->PanTilt)
		{
			move_x = tptz2__ContinuousMove->Velocity->PanTilt->x;
			move_y = tptz2__ContinuousMove->Velocity->PanTilt->y;
		}

		float zoom = 0;
		if (tptz2__ContinuousMove->Velocity && tptz2__ContinuousMove->Velocity->Zoom)
		{
			zoom = tptz2__ContinuousMove->Velocity->Zoom->x;
		}

		auto telemetryProc = GetProcessorFactory()->createTelemetryProcessor(profile, user_id);
		telemetryProc->Move(move_x, move_y, zoom);

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::RelativeMove(_tptz2__RelativeMove* tptz2__RelativeMove, _tptz2__RelativeMoveResponse& tptz2__RelativeMoveResponse)
	{
		std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);

		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__RelativeMove->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__RelativeMove->ProfileToken);
		}

		if (!profile->GetPtzConfiguration())
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoPTZProfile");
			return SOAP_FAULT;
		}

		float move_x = 0;
		float move_y = 0;

		if (tptz2__RelativeMove->Translation && tptz2__RelativeMove->Translation->PanTilt)
		{
			move_x = tptz2__RelativeMove->Translation->PanTilt->x;
			move_y = tptz2__RelativeMove->Translation->PanTilt->y;
		}

		float zoom = 0;
		if (tptz2__RelativeMove->Translation && tptz2__RelativeMove->Translation->Zoom)
		{
			zoom = tptz2__RelativeMove->Translation->Zoom->x;
		}

		auto telemetryProc = GetProcessorFactory()->createTelemetryProcessor(profile, user_id);
		telemetryProc->RelativeMove(move_x, move_y, zoom);

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::AbsoluteMove(_tptz2__AbsoluteMove* tptz2__AbsoluteMove, _tptz2__AbsoluteMoveResponse& tptz2__AbsoluteMoveResponse)
	{
		std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);

		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__AbsoluteMove->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__AbsoluteMove->ProfileToken);
		}

		if (!profile->GetPtzConfiguration() || !profile->GetVideoSourceConfiguration())
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoPTZProfile");
			return SOAP_FAULT;
		}

		float move_x = 0;
		float move_y = 0;

		if (tptz2__AbsoluteMove->Position && tptz2__AbsoluteMove->Position->PanTilt)
		{
			move_x = tptz2__AbsoluteMove->Position->PanTilt->x;
			move_y = tptz2__AbsoluteMove->Position->PanTilt->y;
		}

		float zoom = 0;
		if (tptz2__AbsoluteMove->Position && tptz2__AbsoluteMove->Position->Zoom)
		{
			zoom = tptz2__AbsoluteMove->Position->Zoom->x;
		}

		if (!checkBounds(move_x) || !checkBounds(move_y) || !checkBounds(zoom))
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidPosition");
			return SOAP_FAULT;
		}

		profile->GetPtzConfiguration()->SetCurrentPosition(PtzPosition(move_x, move_y, zoom));

		auto telemetryProc = GetProcessorFactory()->createTelemetryProcessor(profile, user_id);
		telemetryProc->AbsoluteMove(move_x, move_y, zoom);

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::Stop(_tptz2__Stop* tptz2__Stop, _tptz2__StopResponse& tptz2__StopResponse)
	{
		std::string user_id = GetSecurity()->GetLoggedUserName(this->soap);

		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__Stop->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__Stop->ProfileToken);
		}

		if (!profile->GetPtzConfiguration())
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoPTZProfile");
			return SOAP_FAULT;
		}

		auto telemetryProc = GetProcessorFactory()->createTelemetryProcessor(profile, user_id);
		telemetryProc->Stop();

		return SOAP_OK;
	}

	int PtzBindingServiceImpl::GetCompatibleConfigurations(_tptz2__GetCompatibleConfigurations* tptz2__GetCompatibleConfigurations, _tptz2__GetCompatibleConfigurationsResponse& tptz2__GetCompatibleConfigurationsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		auto profile = IServiceImpl::GetConfiguration()->FindProfile(tptz2__GetCompatibleConfigurations->ProfileToken);
		if (!profile)
		{
            return no_profile(this->soap, tptz2__GetCompatibleConfigurations->ProfileToken);
		}

		if (profile->GetVideoSourceConfiguration() && profile->GetVideoSourceConfiguration()->getPtzConfig())
		{
			tptz2__GetCompatibleConfigurationsResponse.PTZConfiguration.push_back(GetProxy()->createPtzConfiguration(this->soap, profile->GetVideoSourceConfiguration()->getPtzConfig()));
		}

		return SOAP_OK;
	}

	tptz2__Capabilities* PtzBindingServiceImpl::GetServiceCapabilities(struct soap* soapObj)
	{
		auto _capabilities = soap_new_tptz2__Capabilities(soapObj);

		_capabilities->GetCompatibleConfigurations = OnvifUtil::BoolValue(soapObj, true);
		_capabilities->StatusPosition = OnvifUtil::BoolValue(soapObj, false);
		_capabilities->MoveStatus = OnvifUtil::BoolValue(soapObj, false);

		return _capabilities;
	}

	bool PtzBindingServiceImpl::checkBounds(float move_coord) const
	{
		return move_coord >= -1 && move_coord <= 1;
	}
}
