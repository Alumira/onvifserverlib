#include "NotificationConsumerBindingProxyImpl.h"
#include "Util/Log.h"
#include <ItvSdk/include/IErrorService.h>

NotificationConsumerBindingProxyImpl::NotificationConsumerBindingProxyImpl(char *endpoint, ITV8::ILogger* logger) :
	NotificationConsumerBindingProxy(endpoint), m_endpoint(endpoint), m_logger(logger)
{
	soap_set_recv_logfile(soap, nullptr);
	soap_set_sent_logfile(soap, nullptr);
	soap_set_test_logfile(soap, nullptr);
}

NotificationConsumerBindingProxyImpl::~NotificationConsumerBindingProxyImpl()
{
	SOAP_FREE(nullptr, m_endpoint);
}

void NotificationConsumerBindingProxyImpl::NotifyMessage(MessageHolderTypeSP message)
{
	_wsnt__Notify* _notification = soap_new__wsnt__Notify(this->soap);
	auto _msg = message->GetMsg(this->soap);
	_notification->NotificationMessage.push_back(_msg);

	if (NotificationConsumerBindingProxy::Notify(_notification) != SOAP_OK)
	{
		NotificationConsumerBindingProxy::soap_stream_fault(std::cerr);
        ONVIF_LOG_BASE(m_logger, ITV8::LOG_ERROR, "Failed to send notification event to " << m_endpoint << ". ");
	}
}
