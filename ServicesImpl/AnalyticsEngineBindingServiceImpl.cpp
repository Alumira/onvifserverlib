#include "AnalyticsEngineBindingServiceImpl.h"
#include "onvif_generated_H.h"
#include <boost/foreach.hpp>

namespace onvif_generated_ {
    AnalyticsEngineBindingServiceImpl::AnalyticsEngineBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data) :
        AnalyticsEngineBindingService(soapObj->GetSoap()), IServiceImpl(data)
    {
    }

    int AnalyticsEngineBindingServiceImpl::GetServiceCapabilities(_tanl2__GetServiceCapabilities* tanl2__GetServiceCapabilities, _tanl2__GetServiceCapabilitiesResponse& tanl2__GetServiceCapabilitiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }

        tanl2__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities(this->soap);
        return SOAP_OK;
    }

    int AnalyticsEngineBindingServiceImpl::GetSupportedAnalyticsModules(
        _tanl2__GetSupportedAnalyticsModules* tanl2__GetSupportedAnalyticsModules,
        _tanl2__GetSupportedAnalyticsModulesResponse& tanl2__GetSupportedAnalyticsModulesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto vAnalyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(tanl2__GetSupportedAnalyticsModules->ConfigurationToken);

        if (!vAnalyticsConfig)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        tanl2__GetSupportedAnalyticsModulesResponse.SupportedAnalyticsModules =
            soap_new_tt__SupportedAnalyticsModules(this->soap);

        BOOST_FOREACH(auto _module, vAnalyticsConfig->GetModules())
        {
            tanl2__GetSupportedAnalyticsModulesResponse.SupportedAnalyticsModules->AnalyticsModuleContentSchemaLocation.push_back(_module->GetSchema());
            tanl2__GetSupportedAnalyticsModulesResponse.SupportedAnalyticsModules->AnalyticsModuleDescription.push_back(GetProxy()->createVideoAnalyticsModuleDescription(this->soap, _module));
        }

        return SOAP_OK;
    }

    int AnalyticsEngineBindingServiceImpl::GetAnalyticsModules(_tanl2__GetAnalyticsModules* tanl2__GetAnalyticsModules, _tanl2__GetAnalyticsModulesResponse& tanl2__GetAnalyticsModulesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto vAnalyticsConfig =
            GetConfiguration()->FindVideoAnalyticsConfiguration(tanl2__GetAnalyticsModules->ConfigurationToken);

        if (!vAnalyticsConfig)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        BOOST_FOREACH(auto _module, vAnalyticsConfig->GetModules())
        {
            tanl2__GetAnalyticsModulesResponse.AnalyticsModule.push_back(GetProxy()->createVideoAnalyticsModule(this->soap, _module));
        }

        return SOAP_OK;
    }

    tanl2__Capabilities* AnalyticsEngineBindingServiceImpl::GetServiceCapabilities(struct soap* soapObj)
    {
        auto _capabilities = soap_new_tanl2__Capabilities(soapObj);

        _capabilities->AnalyticsModuleSupport = OnvifUtil::BoolValue(soapObj, true);
        _capabilities->RuleSupport = OnvifUtil::BoolValue(soapObj, true);
        _capabilities->SupportedMetadata = OnvifUtil::BoolValue(soapObj, true);

        _capabilities->CellBasedSceneDescriptionSupported = OnvifUtil::BoolValue(soapObj, false);

        return _capabilities;
    }

    int AnalyticsEngineBindingServiceImpl::GetSupportedMetadata(
        _tanl2__GetSupportedMetadata *tanl2__GetSupportedMetadata,
        _tanl2__GetSupportedMetadataResponse &
        tanl2__GetSupportedMetadataResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        return SOAP_OK;
    }
}

