#include "ReplayBindingServiceImpl.h"
#include <SoapProxy/SoapUtil.h>

namespace onvif_generated_ {
    ReplayBindingServiceImpl::ReplayBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data) :
            ReplayBindingService(soapObj->GetSoap())
            , IServiceImpl(data)
	{
	}

    int ReplayBindingServiceImpl::GetServiceCapabilities(_trp__GetServiceCapabilities* trp__GetServiceCapabilities, _trp__GetServiceCapabilitiesResponse& trp__GetServiceCapabilitiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }
        
        trp__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities(this->soap);

        return SOAP_OK;
    }

    int ReplayBindingServiceImpl::GetReplayUri(_trp__GetReplayUri* trp__GetReplayUri, _trp__GetReplayUriResponse& trp__GetReplayUriResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
        
        auto vRecording = GetConfiguration()->findRecordingConfig(trp__GetReplayUri->RecordingToken);
        if (vRecording)
        {
			trp__GetReplayUriResponse.Uri = GetCoreSettings()->GetRtspInfo()->GetReplayUri(vRecording);
        }
        else
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoRecording");
            return SOAP_FAULT;
        }

        return SOAP_OK;
    }

	int ReplayBindingServiceImpl::GetReplayConfiguration(_trp__GetReplayConfiguration* trp__GetReplayConfiguration, _trp__GetReplayConfigurationResponse& trp__GetReplayConfigurationResponse)
    {
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		trp__GetReplayConfigurationResponse.Configuration = soap_new_tt__ReplayConfiguration(this->soap);
		trp__GetReplayConfigurationResponse.Configuration->SessionTimeout = GetConfiguration()->GetRtspTimeout();

		return SOAP_OK;
    }

	int ReplayBindingServiceImpl::SetReplayConfiguration(_trp__SetReplayConfiguration* trp__SetReplayConfiguration, _trp__SetReplayConfigurationResponse& trp__SetReplayConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		if(!trp__SetReplayConfiguration->Configuration)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
			return SOAP_FAULT;
		}

		//TODO: send timeout to RTSP server
    	GetConfiguration()->SetRtspTimeout(trp__SetReplayConfiguration->Configuration->SessionTimeout);

		return SOAP_OK;
	}

	trp__Capabilities* ReplayBindingServiceImpl::GetServiceCapabilities(struct soap* soapObj)
    {
        auto _capabilities = soap_new_trp__Capabilities(soapObj);
        _capabilities->ReversePlayback = soap_new_bool(soapObj);
        *_capabilities->ReversePlayback = false;
        _capabilities->RTP_USCORERTSP_USCORETCP = OnvifUtil::BoolValue(soapObj, true);

        return _capabilities;
    }
}

