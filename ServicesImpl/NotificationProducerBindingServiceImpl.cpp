#include "NotificationProducerBindingServiceImpl.h"
#include "NotificationConsumerBindingProxyImpl.h"
#include "EventBindingServiceImpl.h"

namespace onvif_generated_ {
    NotificationProducerBindingServiceImpl::NotificationProducerBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander) :
        NotificationProducerBindingService(soapObj->GetSoap())
        , IServiceImpl(data)
        , m_taskCommander(taskCommander)
    {
    }
   
    int NotificationProducerBindingServiceImpl::Subscribe(_wsnt__Subscribe* wsnt__Subscribe, _wsnt__SubscribeResponse& wsnt__SubscribeResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

		auto cur_tssTime = SoapProxy::GetCurTime();

        LONG64 timeDr = 10000000000;
        if (wsnt__Subscribe->InitialTerminationTime)
        {
            soap_s2xsd__duration(this->soap, wsnt__Subscribe->InitialTerminationTime->c_str(), &timeDr);
        }

        auto termTime = cur_tssTime + timeDr;

		std::vector<FilterTypeSP> filter;
		if (wsnt__Subscribe->Filter)
		{
			BOOST_FOREACH(auto txt, wsnt__Subscribe->Filter->__any)
			{
                if (auto tpe = FilterType::ConvertType(txt.name))
                {
                    filter.push_back(FilterTypeSP(new FilterType(tpe, txt.text)));
                }
			}
		}

		NotificationConsumerBindingProxySP subcriberProxy;

		if (wsnt__Subscribe->ConsumerReference.Address)
		{
			subcriberProxy = NotificationConsumerBindingProxySP(new NotificationConsumerBindingProxyImpl(soap_strdup(nullptr, wsnt__Subscribe->ConsumerReference.Address), GetLogger()));
		}

        //TODO: Send messages as client to wsnt__Subscribe.wsa5__EndpointReference
		auto newTask = GetProcessorFactory()->createSubscriptionTask(GetLogger(), termTime, filter, subcriberProxy);

    	auto newTaskUid =
            m_taskCommander->AddTask(newTask, this->soap);

        if (newTaskUid.GUID.empty())
        {
            return SOAP_FAULT;
        }

        std::string addr = GetCoreSettings()->GetServiceUri() + EventBindingServiceImpl::GetUri() + "/subscription/" + newTaskUid.GUID;
        wsnt__SubscribeResponse.SubscriptionReference.Address = soap_strdup(this->soap, addr.c_str());

        wsnt__SubscribeResponse.CurrentTime = SoapProxy::convertTime(this->soap, cur_tssTime);
        wsnt__SubscribeResponse.TerminationTime = SoapProxy::convertTime(this->soap, termTime);

        return SOAP_OK;
    }

    int NotificationProducerBindingServiceImpl::GetCurrentMessage(_wsnt__GetCurrentMessage* wsnt__GetCurrentMessage, _wsnt__GetCurrentMessageResponse& wsnt__GetCurrentMessageResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        return SOAP_OK;
    }
}
