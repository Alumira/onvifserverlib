#pragma once
#include "onvif_generated_NotificationConsumerBindingProxy.h"
#include <Interface/MessageHolderType.h>

namespace ITV8 {
    struct ILogger;
}

class NotificationConsumerBindingProxyImpl : public NotificationConsumerBindingProxy
{
public:
	NotificationConsumerBindingProxyImpl(char *endpoint, ITV8::ILogger* logger);
	~NotificationConsumerBindingProxyImpl();
	void NotifyMessage(MessageHolderTypeSP message);
	NotificationConsumerBindingProxy* copy() override { return new NotificationConsumerBindingProxyImpl(*this); }

private:
	char * m_endpoint;
    ITV8::ILogger* m_logger;
};

typedef std::shared_ptr<NotificationConsumerBindingProxyImpl> NotificationConsumerBindingProxySP;
