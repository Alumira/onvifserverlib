#pragma once

#include "onvif_generated_SearchBindingService.h"
#include <Interface/IServiceImpl.h>
#include <SoapProxy/SoapUtil.h>
#include <BackgroundServices/TaskCommander.h>

namespace onvif_generated_ {
	class SearchBindingServiceImpl : public SearchBindingService, public IServiceImpl
	{
	public:
        SearchBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander);

        SearchBindingService* copy() override { return new SearchBindingServiceImpl(*this); }


        int GetServiceCapabilities(_tse__GetServiceCapabilities* tse__GetServiceCapabilities, _tse__GetServiceCapabilitiesResponse& tse__GetServiceCapabilitiesResponse) override;
        int GetRecordingSummary(_tse__GetRecordingSummary* tse__GetRecordingSummary, _tse__GetRecordingSummaryResponse& tse__GetRecordingSummaryResponse) override;
        int GetRecordingInformation(_tse__GetRecordingInformation* tse__GetRecordingInformation, _tse__GetRecordingInformationResponse& tse__GetRecordingInformationResponse) override;
	    int GetMediaAttributes(_tse__GetMediaAttributes* tse__GetMediaAttributes, _tse__GetMediaAttributesResponse& tse__GetMediaAttributesResponse) override { return defaultImplementation(); }
        int FindRecordings(_tse__FindRecordings* tse__FindRecordings, _tse__FindRecordingsResponse& tse__FindRecordingsResponse) override;
        int GetRecordingSearchResults(_tse__GetRecordingSearchResults* tse__GetRecordingSearchResults, _tse__GetRecordingSearchResultsResponse& tse__GetRecordingSearchResultsResponse) override;
        int FindEvents(_tse__FindEvents* tse__FindEvents, _tse__FindEventsResponse& tse__FindEventsResponse) override;
        int GetEventSearchResults(_tse__GetEventSearchResults* tse__GetEventSearchResults, _tse__GetEventSearchResultsResponse& tse__GetEventSearchResultsResponse) override;
	    int FindPTZPosition(_tse__FindPTZPosition* tse__FindPTZPosition, _tse__FindPTZPositionResponse& tse__FindPTZPositionResponse) override { return defaultImplementation(); }
	    int GetPTZPositionSearchResults(_tse__GetPTZPositionSearchResults* tse__GetPTZPositionSearchResults, _tse__GetPTZPositionSearchResultsResponse& tse__GetPTZPositionSearchResultsResponse) override { return defaultImplementation(); }
	    int GetSearchState(_tse__GetSearchState* tse__GetSearchState, _tse__GetSearchStateResponse& tse__GetSearchStateResponse) override { return defaultImplementation(); }
        int EndSearch(_tse__EndSearch* tse__EndSearch, _tse__EndSearchResponse& tse__EndSearchResponse) override;
	    int FindMetadata(_tse__FindMetadata* tse__FindMetadata, _tse__FindMetadataResponse& tse__FindMetadataResponse) override { return defaultImplementation(); }
	    int GetMetadataSearchResults(_tse__GetMetadataSearchResults* tse__GetMetadataSearchResults, _tse__GetMetadataSearchResultsResponse& tse__GetMetadataSearchResultsResponse) override { return defaultImplementation(); }
	
        //IServiceImpl

        int Dispatch(struct soap* soapPtr) override
        {
            return SearchBindingService::dispatch(soapPtr);
        }

        static tse__Capabilities* GetServiceCapabilities(struct soap* soapObj);

        static std::string GetUri()
        {
            return "/search_service";
        }

	private:
        TaskCommanderSP m_taskCommander;

		bool addTimeEvent(tt__FindEventResultList* resultList, const IntellectRecEvent& recEvt, ISearchTaskSP tsk);
		bool addRecordingEvent(tt__FindEventResultList* resultList, const IntellectRecEvent& recEvt, ISearchTaskSP tsk);
        _tt__Message * CreateRecordingMessage(const time_t& tssTime, bool isStart);
        wsnt__NotificationMessageHolderType* CreateEvent(std::string eventName) const;

        int defaultImplementation() override
        {
            if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
            {
                return auth_result;
            }

			OnvifUtil::ONVIF_Fault(this->soap, "ter:ActionNotSupported", "ter:NotImplemented", "This optinal method is not implemented", true);
            return SOAP_FAULT;
        }
	};
}
