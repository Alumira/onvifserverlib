#include "Media2BindingServiceImpl.h"
#include <boost/foreach.hpp>
#include "SoapProxy/EventsBuildUtil.h"

namespace onvif_generated_ {
	Media2BindingServiceImpl::Media2BindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander) :
	    Media2BindingService(soapObj->GetSoap()), IServiceImpl(data), m_taskCommander(taskCommander)
	{
	}

    int Media2BindingServiceImpl::GetServiceCapabilities(_trt2__GetServiceCapabilities *trt2__GetServiceCapabilities, _trt2__GetServiceCapabilitiesResponse &trt2__GetServiceCapabilitiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }
        
		trt2__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities(this->soap, GetCoreSettings());
        return SOAP_OK; 
    }

    int Media2BindingServiceImpl::CreateProfile(_trt2__CreateProfile* trt2__CreateProfile, _trt2__CreateProfileResponse& trt2__CreateProfileResponse)
    {
        return defaultImplementation();

        /*if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        MediaProfileSP coreprofile(new MediaProfile);
        coreprofile->SetName(trt2__CreateProfile->Name);

        auto uuid = boost::uuids::to_string(boost::uuids::random_generator()());
        coreprofile->SetToken(uuid);

        BOOST_FOREACH(auto config, trt2__CreateProfile->Configuration)
        {
            if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__VideoSource) && config->Token)
            {
                auto videoSrcConfig = GetConfiguration()->FindVideoSourceConfiguration(*config->Token);
                if (!videoSrcConfig)
                {
                    continue;
                }

                coreprofile->SetVideoSourceConfiguration(videoSrcConfig, false);
            }

			if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__VideoEncoder) && config->Token)
			{
				auto vEncoderConfig = GetConfiguration()->FindVideoEncoderConfig(*config->Token);
				if (!vEncoderConfig)
				{
					continue;
				}

				coreprofile->SetEncoderConfiguration(vEncoderConfig, false);
			}

			if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__AudioSource) && config->Token)
			{
				auto audioSrcConfig = GetConfiguration()->FindAudioSourceConfiguration(*config->Token);
				if (!audioSrcConfig)
				{
					continue;
				}

				coreprofile->SetAudioSourceConfiguration(audioSrcConfig, false);
			}

			if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__AudioEncoder) && config->Token)
			{
				auto audioEncoderConfig = GetConfiguration()->FindAudioEncoderConfig(*config->Token);
				if (!audioEncoderConfig)
				{
					continue;
				}

				coreprofile->SetAudioEncoderConfiguration(audioEncoderConfig, false);
			}

			if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__Analytics) && config->Token)
			{
				auto analyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(*config->Token);
				if (!analyticsConfig)
				{
					continue;
				}

				coreprofile->SetVideoAnalyticsConfiguration(analyticsConfig, false);
			}

			if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__PTZ) && config->Token)
			{
				auto vSourceConfig = GetConfiguration()->FindVideoSourceConfigurationByPTZ(*config->Token);
				if (!vSourceConfig || !vSourceConfig->getPtzConfig())
				{
					continue;
				}
				
				coreprofile->SetPtzConfiguration(vSourceConfig->getPtzConfig(), false);
			}

			if (config->Type == soap_trt2__ConfigurationEnumeration2s(this->soap, trt2__ConfigurationEnumeration__Metadata) && config->Token)
			{
				auto mConfig = GetConfiguration()->FindMetadataConfiguration(*config->Token);
				if (!mConfig)
				{
					continue;
				}

				coreprofile->SetMetadataConfiguration(mConfig, false);
			}
        }

		auto update_profile = GetProcessorFactory()->createCreateProfileProcessor(coreprofile, m_data->m_coreSettings);
		update_profile->DoWork();

        trt2__CreateProfileResponse.Token = coreprofile->GetToken();
        return SOAP_OK;*/
    }

    int Media2BindingServiceImpl::GetProfiles(_trt2__GetProfiles* trt2__GetProfiles, _trt2__GetProfilesResponse& trt2__GetProfilesResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        if(trt2__GetProfiles->Token)
        {
            auto profile = GetConfiguration()->FindProfile(*trt2__GetProfiles->Token);
            if (!profile)
            {
                return no_profile(this->soap, *trt2__GetProfiles->Token);
            }

            updateEncoderParams(profile->GetVideoSourceConfiguration());
            trt2__GetProfilesResponse.Profiles.push_back(GetProxy()->getProfile2(soap, profile, trt2__GetProfiles->Type));
        }
        else
        {
            BOOST_FOREACH(const auto vProfile,GetConfiguration()->GetProfiles())
            {
                updateEncoderParams(vProfile.second->GetVideoSourceConfiguration());

                if (vProfile.second->GetVideoSourceConfiguration() && !checkVideoSourceCodec(vProfile.second->GetVideoSourceConfiguration()))
                {
                    continue;
                }
                if (vProfile.second->GetEncoderConfiguration() && !checkCodec(vProfile.second->GetEncoderConfiguration()->codec))
                {
                    continue;
                }
                
                trt2__MediaProfile * prof = GetProxy()->getProfile2(soap, vProfile.second, trt2__GetProfiles->Type);
                trt2__GetProfilesResponse.Profiles.push_back(prof);
            }
        }

	    return SOAP_OK;
	}

    int Media2BindingServiceImpl::AddConfiguration(_trt2__AddConfiguration* trt2__AddConfiguration, _trt2__AddConfigurationResponse& trt2__AddConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt2__AddConfiguration->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt2__AddConfiguration->ProfileToken);
        }

		auto _profile = GetConfiguration()->FindProfile(trt2__AddConfiguration->ProfileToken);
		MediaProfileSP newProfile(new MediaProfile(*_profile.get()));

        BOOST_FOREACH(auto config, trt2__AddConfiguration->Configuration)
        {
            if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__VideoSource) && config->Token)
            {
                auto videoSrcConfig = GetConfiguration()->FindVideoSourceConfiguration(*config->Token);
                if (!videoSrcConfig)
                {
                    continue;
                }

                newProfile->SetVideoSourceConfiguration(videoSrcConfig, false);
            }
            if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__VideoEncoder) && config->Token)
            {
                EncoderConfigurationSP encConf;
                BOOST_FOREACH(auto vSourceConfig,GetConfiguration()->GetVSourceConfigurations())
                {
                    auto encConfIt = vSourceConfig.second->m_encoders.find(*config->Token);
                    if (encConfIt != vSourceConfig.second->m_encoders.end())
                    {
                        encConf = encConfIt->second;
                        break;
                    }
                }

                if (!encConf)
                {
                    continue;
                }

                newProfile->SetEncoderConfiguration(encConf, false);
            }
            if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__AudioSource) && config->Token)
            {
                auto audioSrcConfig = GetConfiguration()->FindAudioSourceConfiguration(*config->Token);
                if (!audioSrcConfig)
                {
                    continue;
                }

                newProfile->SetAudioSourceConfiguration(audioSrcConfig, false);
            }
            if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__AudioEncoder) && config->Token)
            {
                AudioEncoderConfigurationSP encConf = GetConfiguration()->FindAudioEncoderConfig(*config->Token);
                if (!encConf)
                {
                    continue;
                }

                newProfile->SetAudioEncoderConfiguration(encConf, false);
            }
			if (config->Type == soap_trt2__ConfigurationEnumeration2s(this->soap, trt2__ConfigurationEnumeration__Metadata))
			{
				MetadataConfigurationSP vMetadataConfig;
				if(config->Token)
				{
					vMetadataConfig = GetConfiguration()->FindMetadataConfiguration(*config->Token);
				}
				else
				{
					vMetadataConfig = _profile->GetVideoSourceConfiguration()->GetMetadataConfiguration();
				}
				
				if (!vMetadataConfig)
				{
					continue;
				}

				newProfile->SetMetadataConfiguration(vMetadataConfig, false);
			}
			if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__Analytics))
			{
				VideoAnalyticsConfigurationSP vAnalyticsConfig;

				if(config->Token)
				{
					vAnalyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(*config->Token);
				}
				else
				{
					vAnalyticsConfig = _profile->GetVideoSourceConfiguration()->GetVideoAnalyticsConfiguration();
				}

				if (!vAnalyticsConfig)
				{
					continue;
				}

				newProfile->SetVideoAnalyticsConfiguration(vAnalyticsConfig, false);
			}
			if (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__PTZ))
			{
				PtzConfigurationSP ptzConfig;

				if (config->Token)
				{
					ptzConfig = GetConfiguration()->FindPtzConfiguration(*config->Token);
				}
				else
				{
					ptzConfig = _profile->GetVideoSourceConfiguration()->getPtzConfig();
				}

				if (!ptzConfig)
				{
					continue;
				}

				newProfile->SetPtzConfiguration(ptzConfig, false);
			}
        }

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::RemoveConfiguration(_trt2__RemoveConfiguration* trt2__RemoveConfiguration, _trt2__RemoveConfigurationResponse& trt2__RemoveConfigurationResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt2__RemoveConfiguration->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt2__RemoveConfiguration->ProfileToken);
        }

		MediaProfileSP newProfile(new MediaProfile(*profile.get()));

        BOOST_FOREACH(auto config, trt2__RemoveConfiguration->Configuration)
        {
            bool remove_all = (config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__All));
            if (remove_all || config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__VideoSource))
            {
                newProfile->SetVideoSourceConfiguration(nullptr, false);
            }
            if (remove_all || config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__VideoEncoder))
            {
                newProfile->SetEncoderConfiguration(nullptr, false);
            }
            if (remove_all || config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__AudioSource))
            {
                newProfile->SetAudioSourceConfiguration(nullptr, false);
            }
            if (remove_all || config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__AudioEncoder))
            {
                newProfile->SetAudioEncoderConfiguration(nullptr, false);
            }
			if (remove_all || config->Type == soap_trt2__ConfigurationEnumeration2s(this->soap, trt2__ConfigurationEnumeration__Metadata))
			{
				newProfile->SetMetadataConfiguration(nullptr, false);
			}
			if (remove_all || config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__Analytics))
			{
				newProfile->SetVideoAnalyticsConfiguration(nullptr, false);
			}
			if (remove_all || config->Type == soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__PTZ))
			{
				newProfile->SetPtzConfiguration(nullptr, false);
			}
        }

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_OK;
	}

    int Media2BindingServiceImpl::DeleteProfile(_trt2__DeleteProfile* trt2__DeleteProfile, _trt2__DeleteProfileResponse& trt2__DeleteProfileResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt2__DeleteProfile->Token);
        if (!profile)
        {
            return no_profile(this->soap, trt2__DeleteProfile->Token);
        }

		auto delete_profile = GetProcessorFactory()->createDeleteProfileProcessor(profile);
		delete_profile->DoWork();

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::GetVideoSourceConfigurations(_trt2__GetVideoSourceConfigurations* trt2__GetVideoSourceConfigurations, _trt2__GetVideoSourceConfigurationsResponse& trt2__GetVideoSourceConfigurationsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

		if(trt2__GetVideoSourceConfigurations->ConfigurationToken)
		{
			auto conf = GetConfiguration()->FindVideoSourceConfiguration(*trt2__GetVideoSourceConfigurations->ConfigurationToken);
			if(!conf)
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
				return SOAP_FAULT;
			}
			
			updateEncoderParams(conf);

			auto sourcecfg = GetProxy()->createVideoSourceConfiguration(soap, conf);
			trt2__GetVideoSourceConfigurationsResponse.Configurations.push_back(sourcecfg);
			return SOAP_OK;
		}

		BOOST_FOREACH(auto src, GetConfiguration()->GetVSourceConfigurations())
		{
			updateEncoderParams(src.second);
		}

		ReadLock_t l(GetConfiguration()->getLock());

        BOOST_FOREACH(auto src,GetConfiguration()->GetVSourceConfigurations())
        {
            if (checkVideoSourceCodec(src.second))
            {
                tt__VideoSourceConfiguration* sourcecfg = GetProxy()->createVideoSourceConfiguration(soap, src.second);
                trt2__GetVideoSourceConfigurationsResponse.Configurations.push_back(sourcecfg);
            }
        }

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::GetVideoEncoderConfigurations(_trt2__GetVideoEncoderConfigurations* trt2__GetVideoEncoderConfigurations, _trt2__GetVideoEncoderConfigurationsResponse& trt2__GetVideoEncoderConfigurationsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        if(trt2__GetVideoEncoderConfigurations->ProfileToken)
        {
            VideoSourceConfigurationSP vSource;
            {
                ReadLock_t l(GetConfiguration()->getLock());

                auto profile = GetConfiguration()->FindProfile(*trt2__GetVideoEncoderConfigurations->ProfileToken);
                if (!profile)
                {
                    return no_profile(this->soap, *trt2__GetVideoEncoderConfigurations->ProfileToken);
                }

                vSource = profile->GetVideoSourceConfiguration();

                if (!vSource)
                {
                    return SOAP_OK;
                }
            }

            updateEncoderParams(vSource);

            BOOST_FOREACH(auto vEncoder, vSource->m_encoders)
            {
                if (vEncoder.second->IsSet() && checkCodec(vEncoder.second->codec))
                {
                    trt2__GetVideoEncoderConfigurationsResponse.Configurations.push_back(GetProxy()->createVideoEncoder2Configuration(soap, vEncoder.second));
                }
            }
        }
        else if(trt2__GetVideoEncoderConfigurations->ConfigurationToken)
        {
            auto encConfig = GetConfiguration()->FindVideoEncoderConfig(*trt2__GetVideoEncoderConfigurations->ConfigurationToken);
            if (!encConfig)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }

            trt2__GetVideoEncoderConfigurationsResponse.Configurations.push_back(GetProxy()->createVideoEncoder2Configuration(soap, encConfig));
        }
        else
        {
            BOOST_FOREACH(auto vSource,GetConfiguration()->GetVSourceConfigurations())
            {
                updateEncoderParams(vSource.second);

                BOOST_FOREACH(auto vEncoder, vSource.second->m_encoders)
                {
                    if (vEncoder.second->IsSet() && checkCodec(vEncoder.second->codec))
                    {
                        trt2__GetVideoEncoderConfigurationsResponse.Configurations.push_back(GetProxy()->createVideoEncoder2Configuration(soap, vEncoder.second));
                    }
                }
            }
        }

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::GetAudioSourceConfigurations(trt2__GetConfiguration* trt2__GetAudioSourceConfigurations, _trt2__GetAudioSourceConfigurationsResponse& trt2__GetAudioSourceConfigurationsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

		if(trt2__GetAudioSourceConfigurations->ConfigurationToken)
		{
			auto aSourceConfig = GetConfiguration()->FindAudioSourceConfiguration(*trt2__GetAudioSourceConfigurations->ConfigurationToken);
			if(!aSourceConfig)
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
				return SOAP_FAULT;
			}

			auto sourcecfg = GetProxy()->createAudioSourceConfiguration(soap, aSourceConfig);
			trt2__GetAudioSourceConfigurationsResponse.Configurations.push_back(sourcecfg);

			return SOAP_OK;
		}

        ReadLock_t l(GetConfiguration()->getLock());

        BOOST_FOREACH(auto src,GetConfiguration()->GetASourceConfigurations())
        {
            tt__AudioSourceConfiguration* sourcecfg = GetProxy()->createAudioSourceConfiguration(soap, src);
            trt2__GetAudioSourceConfigurationsResponse.Configurations.push_back(sourcecfg);
        }

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::GetAudioEncoderConfigurations(trt2__GetConfiguration* trt2__GetAudioEncoderConfigurations, _trt2__GetAudioEncoderConfigurationsResponse& trt2__GetAudioEncoderConfigurationsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());
        BOOST_FOREACH(auto aSource,GetConfiguration()->GetASourceConfigurations())
        {
            auto aEncoder = aSource->GetAudioEncoderConfig();
            if (aEncoder)
            {
                trt2__GetAudioEncoderConfigurationsResponse.Configurations.push_back(GetProxy()->createAudioEncoder2Configuration(soap, aEncoder));
            }
        }

        return SOAP_OK;
    }

	int Media2BindingServiceImpl::GetAnalyticsConfigurations(trt2__GetConfiguration* trt2__GetAnalyticsConfigurations, _trt2__GetAnalyticsConfigurationsResponse& trt2__GetAnalyticsConfigurationsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

        if(trt2__GetAnalyticsConfigurations->ProfileToken)
        {
            auto profile = GetConfiguration()->FindProfile(*trt2__GetAnalyticsConfigurations->ProfileToken);
            if (!profile)
            {
                return no_profile(this->soap, *trt2__GetAnalyticsConfigurations->ProfileToken);
            }

            auto vSourceConfig = profile->GetVideoSourceConfiguration();
            if(vSourceConfig)
            {
                auto config = vSourceConfig->GetVideoAnalyticsConfiguration();
                if (config)
                {
                    trt2__GetAnalyticsConfigurationsResponse.Configurations.push_back(GetProxy()->createVideoAnalyticsConfiguration(soap, config));
                }

                return SOAP_OK;
            }
        }

		ReadLock_t l(GetConfiguration()->getLock());
		BOOST_FOREACH(auto vSourceConfig, GetConfiguration()->GetVSourceConfigurations())
		{
			auto config = vSourceConfig.second->GetVideoAnalyticsConfiguration();
			if (config)
			{
				trt2__GetAnalyticsConfigurationsResponse.Configurations.push_back(GetProxy()->createVideoAnalyticsConfiguration(soap, config));
			}
		}

		return SOAP_OK;
	}

	int Media2BindingServiceImpl::GetMetadataConfigurations(trt2__GetConfiguration* trt2__GetMetadataConfigurations, _trt2__GetMetadataConfigurationsResponse& trt2__GetMetadataConfigurationsResponse)
    {
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

        if (trt2__GetMetadataConfigurations->ProfileToken)
        {
            auto profile = GetConfiguration()->FindProfile(*trt2__GetMetadataConfigurations->ProfileToken);
            if (!profile)
            {
                return no_profile(this->soap, *trt2__GetMetadataConfigurations->ProfileToken);
            }

            auto vSourceConfig = profile->GetVideoSourceConfiguration();
            if (vSourceConfig)
            {
                auto config = vSourceConfig->GetMetadataConfiguration();
                if (config)
                {
                    trt2__GetMetadataConfigurationsResponse.Configurations.push_back(GetProxy()->createMetadataConfiguration(soap, config));
                }

                return SOAP_OK;
            }
        }

		ReadLock_t l(GetConfiguration()->getLock());

		BOOST_FOREACH(auto vSourceConfig,GetConfiguration()->GetVSourceConfigurations())
		{
			auto config = vSourceConfig.second->GetMetadataConfiguration();
			if(config && trt2__GetMetadataConfigurations->ConfigurationToken && *trt2__GetMetadataConfigurations->ConfigurationToken == config->GetToken())
			{
				trt2__GetMetadataConfigurationsResponse.Configurations.push_back(GetProxy()->createMetadataConfiguration(soap, config));
				return SOAP_OK;
			}

			if (config && !(trt2__GetMetadataConfigurations->ConfigurationToken))
			{
				trt2__GetMetadataConfigurationsResponse.Configurations.push_back(GetProxy()->createMetadataConfiguration(soap, config));
			}
		}

		return SOAP_OK;
    }

	int Media2BindingServiceImpl::GetAudioOutputConfigurations(trt2__GetConfiguration* trt2__GetAudioOutputConfigurations,
		_trt2__GetAudioOutputConfigurationsResponse& trt2__GetAudioOutputConfigurationsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:AudioOutputNotSupported", nullptr, true);
		return SOAP_FAULT;
	}

	int Media2BindingServiceImpl::GetAudioDecoderConfigurations(
		trt2__GetConfiguration* trt2__GetAudioDecoderConfigurations,
		_trt2__GetAudioDecoderConfigurationsResponse& trt2__GetAudioDecoderConfigurationsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:AudioDecodingNotSupported", nullptr, true);
		return SOAP_FAULT;
	}

	int Media2BindingServiceImpl::SetVideoSourceConfiguration(_trt2__SetVideoSourceConfiguration* trt2__SetVideoSourceConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        VideoSourceConfigurationSP config;
        if (trt2__SetVideoSourceConfiguration->Configuration != nullptr)
        {
            config = GetConfiguration()->FindVideoSourceConfiguration(trt2__SetVideoSourceConfiguration->Configuration->token);
            if (!config)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }
        }

        tt__IntRectangle *bounds = trt2__SetVideoSourceConfiguration->Configuration->Bounds;
        if (bounds)
        {
            if (!config->videoSource->m_bounds->CompareBounds(bounds->width, bounds->height, bounds->x, bounds->y))
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
                return SOAP_FAULT;
            }
        }

		if(!trt2__SetVideoSourceConfiguration->Configuration->Name.empty())
		{
			config->videoSource->camName = trt2__SetVideoSourceConfiguration->Configuration->Name;
		}

		auto evt = CreateConfigurationChangedEvent(this->soap, config->GetToken(), soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__VideoSource));
		GetSubscriptionManager()->PushMessage(evt);

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::SetVideoEncoderConfiguration(_trt2__SetVideoEncoderConfiguration* trt2__SetVideoEncoderConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto vEncoderConfig = GetConfiguration()->FindVideoEncoderConfig(trt2__SetVideoEncoderConfiguration->Configuration->token);

        if (!vEncoderConfig)
        { 
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

	    BOOST_FOREACH(auto vSourceConfig, GetConfiguration()->GetVSourceConfigurations())
        {
            auto cfg = vSourceConfig.second->m_encoders.find(trt2__SetVideoEncoderConfiguration->Configuration->token);
            if (cfg != vSourceConfig.second->m_encoders.end())
            {
	            auto vSource = vSourceConfig.second;
                updateEncoderParams(vSourceConfig.second);
            }
        }

        if (GetProxy()->convertToMedia2EncodingName(this->soap, vEncoderConfig->codec) != trt2__SetVideoEncoderConfiguration->Configuration->Encoding)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
            return SOAP_FAULT;
        }

        bool error = false;
        if (trt2__SetVideoEncoderConfiguration->Configuration->Resolution
            && !vEncoderConfig->m_resolutionsInfo.Contains(trt2__SetVideoEncoderConfiguration->Configuration->Resolution->Width,
                trt2__SetVideoEncoderConfiguration->Configuration->Resolution->Height))
        {
            error = true;
        }

		EncoderConfigurationSP newEnc(new EncoderConfiguration(*vEncoderConfig.get()));

		if(trt2__SetVideoEncoderConfiguration->Configuration->GovLength 
			&& (vEncoderConfig->GetStreamStaticParams()->GetGovLength() != *trt2__SetVideoEncoderConfiguration->Configuration->GovLength))
        {
			error |= !newEnc->GetStreamStaticParams()->SetGovLength(*trt2__SetVideoEncoderConfiguration->Configuration->GovLength);
        }

		//TODO: send new params to core
		if (trt2__SetVideoEncoderConfiguration->Configuration->RateControl)
		{
			error |= !newEnc->SetBitrateLimit(trt2__SetVideoEncoderConfiguration->Configuration->RateControl->BitrateLimit);

			auto frameRate = int(trt2__SetVideoEncoderConfiguration->Configuration->RateControl->FrameRateLimit);
			if (frameRate && (vEncoderConfig->GetStreamStaticParams()->GetFrameRateLimit() != frameRate))
			{
				error |= !newEnc->GetStreamStaticParams()->SetFrameRateLimit(frameRate);
			}
		}

		float quality = trt2__SetVideoEncoderConfiguration->Configuration->Quality;
		if (quality && vEncoderConfig->GetStreamStaticParams()->GetQuality() != quality)
		{
			error |= !newEnc->GetStreamStaticParams()->SetQuality(quality);
		}

		if(!trt2__SetVideoEncoderConfiguration->Configuration->Name.empty())
		{
			newEnc->SetName(trt2__SetVideoEncoderConfiguration->Configuration->Name);
		}

		if (trt2__SetVideoEncoderConfiguration->Configuration->Multicast
			&& trt2__SetVideoEncoderConfiguration->Configuration->Multicast->Address->IPv4Address)
		{
			MulticastParamsPtr newParams(new MulticastParams(*trt2__SetVideoEncoderConfiguration->Configuration->Multicast->Address->IPv4Address
				, trt2__SetVideoEncoderConfiguration->Configuration->Multicast->Port));

			newEnc->multicastParams = newParams;
		}

		if(error)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
			return SOAP_FAULT;
		}

		auto vSourceConfig = GetConfiguration()->FindVideoSourceConfigurationByEncoder(vEncoderConfig->GetToken());
		bool isCam = vSourceConfig && vSourceConfig->videoSource->camId == vEncoderConfig->streamId;

		GetProcessorFactory()->createUpdateVideoEncoderProcessor(newEnc, isCam)->DoWork();

		auto evt = CreateConfigurationChangedEvent(this->soap, vEncoderConfig->GetToken(), soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__VideoEncoder));
		GetSubscriptionManager()->PushMessage(evt);

		return SOAP_OK;
    }

    int Media2BindingServiceImpl::SetAudioSourceConfiguration(_trt2__SetAudioSourceConfiguration* trt2__SetAudioSourceConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        AudioSourceSP aSource;
        if (trt2__SetAudioSourceConfiguration->Configuration != nullptr)
        {
            aSource = GetConfiguration()->GetASource(trt2__SetAudioSourceConfiguration->Configuration->SourceToken);
            if (!aSource)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }

            auto aSourceConfig = GetConfiguration()->FindAudioSourceConfiguration(trt2__SetAudioSourceConfiguration->Configuration->token);
            if (!trt2__SetAudioSourceConfiguration->Configuration->token.empty() && !aSourceConfig)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }

			//TODO: send name of object (microphone) to core 
        	if(!trt2__SetAudioSourceConfiguration->Configuration->Name.empty())
			{
				aSource->SetName(trt2__SetAudioSourceConfiguration->Configuration->Name);
			}

			auto evt = CreateConfigurationChangedEvent(this->soap, aSourceConfig->GetToken(), soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__AudioSource));
			GetSubscriptionManager()->PushMessage(evt);
        }

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::SetAudioEncoderConfiguration(_trt2__SetAudioEncoderConfiguration* trt2__SetAudioEncoderConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto aEncoderConfig = GetConfiguration()->FindAudioEncoderConfig(trt2__SetAudioEncoderConfiguration->Configuration->token);

        if (!aEncoderConfig)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
            return SOAP_FAULT;
        }

        //TODO: send new params to core
        if (!trt2__SetAudioEncoderConfiguration->Configuration->Encoding.empty() 
			&& trt2__SetAudioEncoderConfiguration->Configuration->Encoding != "PCMU")
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
            return SOAP_FAULT;
        }

        bool error = false;

        if (trt2__SetAudioEncoderConfiguration->Configuration->Bitrate && trt2__SetAudioEncoderConfiguration->Configuration->Bitrate != 128)
        {
            error = true;
        }

        if (trt2__SetAudioEncoderConfiguration->Configuration->SampleRate && !GetProxy()->CheckAudioSampleRate(aEncoderConfig, trt2__SetAudioEncoderConfiguration->Configuration->SampleRate))
        {
            error = true;
        }

        if (error)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:ConfigModify");
            return SOAP_FAULT;
        }

		aEncoderConfig->SetName(trt2__SetAudioEncoderConfiguration->Configuration->Name);

		auto evt = CreateConfigurationChangedEvent(this->soap, aEncoderConfig->GetToken(), soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__AudioEncoder));
		GetSubscriptionManager()->PushMessage(evt);

        return SOAP_OK;
    }

	int Media2BindingServiceImpl::SetMetadataConfiguration(_trt2__SetMetadataConfiguration* trt2__SetMetadataConfiguration, trt2__SetConfigurationResponse& trt2__SetConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto metadataConfig = GetConfiguration()->FindMetadataConfiguration(trt2__SetMetadataConfiguration->Configuration->token);
		if (!metadataConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, " ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		if (trt2__SetMetadataConfiguration->Configuration)
		{
            if (trt2__SetMetadataConfiguration->Configuration->Analytics)
            {
                metadataConfig->SetAnalyticsFlag(*trt2__SetMetadataConfiguration->Configuration->Analytics);
            }

            metadataConfig->SetEventsFlag(trt2__SetMetadataConfiguration->Configuration->Events != nullptr);
            if(trt2__SetMetadataConfiguration->Configuration->Events)
            {
                std::vector<FilterTypeSP> filter;
                if (trt2__SetMetadataConfiguration->Configuration->Events->Filter)
                {
                    BOOST_FOREACH(auto txt, trt2__SetMetadataConfiguration->Configuration->Events->Filter->__any)
                    {
                        if (auto tpe = FilterType::ConvertType(txt.name))
                        {
                            filter.push_back(FilterTypeSP(new FilterType(tpe, txt.text)));
                        }
                    }
                }
                metadataConfig->SetFilter(filter);
            }

            if(trt2__SetMetadataConfiguration->Configuration && 
                trt2__SetMetadataConfiguration->Configuration->Multicast)
            {
                auto vSourceConf = GetConfiguration()->FindVideoSourceConfigurationByMetadataConfigurationToken(trt2__SetMetadataConfiguration->Configuration->token);
                if(vSourceConf)
                {
                    VideoSourceConfigurationSP newVideoSourceConf(new  VideoSourceConfiguration(*vSourceConf.get()));

                    MulticastParamsPtr newParams(new MulticastParams(*trt2__SetMetadataConfiguration->Configuration->Multicast->Address->IPv4Address
                        , trt2__SetMetadataConfiguration->Configuration->Multicast->Port));

                    newVideoSourceConf->GetMetadataConfiguration()->multicastParams = newParams;

                    GetProcessorFactory()->createUpdateVideoSourceProcessor(newVideoSourceConf)->DoWork();
                }
            }

            if(trt2__SetMetadataConfiguration->Configuration && 
                !trt2__SetMetadataConfiguration->Configuration->Name.empty())
            {
                metadataConfig->SetName(trt2__SetMetadataConfiguration->Configuration->Name);
            }

			auto evt = CreateConfigurationChangedEvent(this->soap, metadataConfig->GetToken(), soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__Metadata));
			GetSubscriptionManager()->PushMessage(evt);
		}

		return SOAP_OK;
	}

	int Media2BindingServiceImpl::SetAudioOutputConfiguration(
		_trt2__SetAudioOutputConfiguration* trt2__SetAudioOutputConfiguration,
		trt2__SetConfigurationResponse& trt2__SetConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, " ter:ActionNotSupported", "ter:AudioOutputNotSupported", nullptr, true);
		return SOAP_FAULT;
	}

	int Media2BindingServiceImpl::SetAudioDecoderConfiguration(
		_trt2__SetAudioDecoderConfiguration* trt2__SetAudioDecoderConfiguration,
		trt2__SetConfigurationResponse& trt2__SetConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, " ter:ActionNotSupported", "ter:AudioDecodingNotSupported", nullptr, true);
		return SOAP_FAULT;
	}

	int Media2BindingServiceImpl::GetVideoSourceConfigurationOptions(_trt2__GetVideoSourceConfigurationOptions* trt2__GetVideoSourceConfigurationOptions, _trt2__GetVideoSourceConfigurationOptionsResponse& trt2__GetVideoSourceConfigurationOptionsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        VideoSourceConfigurationSP config;
        if (trt2__GetVideoSourceConfigurationOptions->ConfigurationToken != nullptr)
        {
            config = GetConfiguration()->FindVideoSourceConfiguration(*trt2__GetVideoSourceConfigurationOptions->ConfigurationToken);
            if (!config)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }
        }

        if (config == nullptr && trt2__GetVideoSourceConfigurationOptions->ProfileToken != nullptr)
        {
            auto profile = GetConfiguration()->FindProfile(*trt2__GetVideoSourceConfigurationOptions->ProfileToken);
            if (!profile)
            {
                return no_profile(this->soap, *trt2__GetVideoSourceConfigurationOptions->ProfileToken);
            }

            config = profile->GetVideoSourceConfiguration();
        }

        trt2__GetVideoSourceConfigurationOptionsResponse.Options = soap_new_tt__VideoSourceConfigurationOptions(this->soap);
		trt2__GetVideoSourceConfigurationOptionsResponse.Options->BoundsRange = soap_new_tt__IntRectangleRange(this->soap);
		auto bounds = BoundsRangeSP(new BoundsRange(0, 0));

		if (config != nullptr)
		{
			trt2__GetVideoSourceConfigurationOptionsResponse.Options->VideoSourceTokensAvailable.push_back(config->videoSource->GetToken());
			bounds = config->videoSource->m_bounds;
		}
		else
		{
			BOOST_FOREACH(auto vSourceConfig, GetConfiguration()->GetVSources())
			{
				trt2__GetVideoSourceConfigurationOptionsResponse.Options->VideoSourceTokensAvailable.push_back(vSourceConfig.second->GetToken());
			}
		}

		trt2__GetVideoSourceConfigurationOptionsResponse.Options->BoundsRange->WidthRange = soap_new_set_tt__IntRange(this->soap, bounds->_widthRange.first, bounds->_widthRange.second);
		trt2__GetVideoSourceConfigurationOptionsResponse.Options->BoundsRange->HeightRange = soap_new_set_tt__IntRange(this->soap, bounds->_heightRange.first, bounds->_heightRange.second);
		trt2__GetVideoSourceConfigurationOptionsResponse.Options->BoundsRange->XRange = soap_new_set_tt__IntRange(this->soap, bounds->_xRange.first, bounds->_xRange.second);
		trt2__GetVideoSourceConfigurationOptionsResponse.Options->BoundsRange->YRange = soap_new_set_tt__IntRange(this->soap, bounds->_yRange.first, bounds->_yRange.second);

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::GetVideoEncoderConfigurationOptions(trt2__GetConfiguration* trt2__GetVideoEncoderConfigurationOptions, _trt2__GetVideoEncoderConfigurationOptionsResponse& trt2__GetVideoEncoderConfigurationOptionsResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        ReadLock_t l(GetConfiguration()->getLock());

        EncoderConfigurationSP config;
        if (trt2__GetVideoEncoderConfigurationOptions->ProfileToken != nullptr)
        {
            auto profile = GetConfiguration()->FindProfile(*trt2__GetVideoEncoderConfigurationOptions->ProfileToken);
            if (!profile)
            {
                return no_profile(this->soap, *trt2__GetVideoEncoderConfigurationOptions->ProfileToken);
            }

            config = profile->GetEncoderConfiguration();
        }

        if (config == nullptr && trt2__GetVideoEncoderConfigurationOptions->ConfigurationToken != nullptr)
        {
            config = GetConfiguration()->FindVideoEncoderConfig(*trt2__GetVideoEncoderConfigurationOptions->ConfigurationToken);

            if (!config)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }
        }

        //Count available encoders
		CodecsCountMap codecsmap;
	    BOOST_FOREACH(auto vSource, GetConfiguration()->GetVSourceConfigurations())
        {
			codecsmap.AddCodecs(vSource.second);
        }

        BOOST_FOREACH(auto codec, codecsmap.GetMap())
        {
            if ((config && config->codec == codec.first) || (!config && codec.second))
            {
                auto options = soap_new_tt__VideoEncoder2ConfigurationOptions(this->soap);

                GetProxy()->fillResolutions2(this->soap, options->ResolutionsAvailable,
                    config ? config->m_resolutionsInfo : ResolutionInfo());

                options->QualityRange =
                    soap_new_set_tt__FloatRange(this->soap, (float)QualityRangeMin, (float)QualityRangeMax);

                options->FrameRatesSupported = soap_new_tt__FloatAttrList(this->soap);
                *options->FrameRatesSupported =
                    config ? config->GetStreamStaticParams()->GetSupportedFramerates() : boost::lexical_cast<std::string>(FpsRangeMax);

                options->Encoding = GetProxy()->convertToMedia2EncodingName(this->soap, codec.first);

                options->GovLengthRange = soap_new_std__string(this->soap);
                *options->GovLengthRange = boost::lexical_cast<std::string>(GovLengthRangeMin) + " " + boost::lexical_cast<std::string>(GovLengthRangeMax);

                options->ProfilesSupported = soap_new_std__string(this->soap);
                *options->ProfilesSupported = "Main";

                options->BitrateRange = soap_new_set_tt__IntRange(this->soap, BpsRangeMin, BpsRangeMax);

                trt2__GetVideoEncoderConfigurationOptionsResponse.Options.push_back(options);
            }
        }

        return SOAP_OK;
	}

    int Media2BindingServiceImpl::GetAudioSourceConfigurationOptions(trt2__GetConfiguration* trt2__GetAudioSourceConfigurationOptions, _trt2__GetAudioSourceConfigurationOptionsResponse& trt2__GetAudioSourceConfigurationOptionsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        AudioSourceConfigurationSP config;
        if (trt2__GetAudioSourceConfigurationOptions->ConfigurationToken != nullptr)
        {
            config = GetConfiguration()->FindAudioSourceConfiguration(*trt2__GetAudioSourceConfigurationOptions->ConfigurationToken);
            if (!config)
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
                return SOAP_FAULT;
            }
        }

        /*if (config == nullptr && trt__GetAudioSourceConfigurationOptions->ProfileToken == nullptr)
        {
        OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
        return SOAP_FAULT;
        }*/

        if (config == nullptr && trt2__GetAudioSourceConfigurationOptions->ProfileToken != nullptr)
        {
            auto profile = GetConfiguration()->FindProfile(*trt2__GetAudioSourceConfigurationOptions->ProfileToken);
            if (!profile)
            {
                return no_profile(this->soap, *trt2__GetAudioSourceConfigurationOptions->ProfileToken);
            }

            config = profile->GetAudioSourceConfiguration();
        }

        trt2__GetAudioSourceConfigurationOptionsResponse.Options = soap_new_tt__AudioSourceConfigurationOptions(this->soap);

        BOOST_FOREACH(auto aSourceConfig,GetConfiguration()->GetASources())
        {
            trt2__GetAudioSourceConfigurationOptionsResponse.Options->InputTokensAvailable.push_back(aSourceConfig->GetToken());
        }

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::GetAudioEncoderConfigurationOptions(trt2__GetConfiguration* trt2__GetAudioEncoderConfigurationOptions, _trt2__GetAudioEncoderConfigurationOptionsResponse& trt2__GetAudioEncoderConfigurationOptionsResponse)
	{
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

		AudioEncoderConfigurationSP encConf;
		if (trt2__GetAudioEncoderConfigurationOptions->ConfigurationToken)
		{
			encConf = GetConfiguration()->FindAudioEncoderConfig(*trt2__GetAudioEncoderConfigurationOptions->ConfigurationToken);
		}

		if (trt2__GetAudioEncoderConfigurationOptions->ProfileToken)
		{
			auto profile = GetConfiguration()->FindProfile(*trt2__GetAudioEncoderConfigurationOptions->ProfileToken);
			if (!profile)
			{
                return no_profile(this->soap, *trt2__GetAudioEncoderConfigurationOptions->ProfileToken);
			}

			encConf = profile->GetAudioEncoderConfiguration();
		}

		{
			auto opt = GetProxy()->CreateAudioEncoder2ConfigurationOption(this->soap, encConf);
			trt2__GetAudioEncoderConfigurationOptionsResponse.Options.push_back(opt);
		}

        return SOAP_OK;
	}

	int Media2BindingServiceImpl::GetMetadataConfigurationOptions(trt2__GetConfiguration* trt2__GetMetadataConfigurationOptions, _trt2__GetMetadataConfigurationOptionsResponse& trt2__GetMetadataConfigurationOptionsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		trt2__GetMetadataConfigurationOptionsResponse.Options = soap_new_tt__MetadataConfigurationOptions(this->soap);
		trt2__GetMetadataConfigurationOptionsResponse.Options->PTZStatusFilterOptions = soap_new_tt__PTZStatusFilterOptions(this->soap);
		trt2__GetMetadataConfigurationOptionsResponse.Options->PTZStatusFilterOptions->PanTiltPositionSupported = OnvifUtil::BoolValue(this->soap, false);
		trt2__GetMetadataConfigurationOptionsResponse.Options->PTZStatusFilterOptions->PanTiltStatusSupported = false;
		trt2__GetMetadataConfigurationOptionsResponse.Options->PTZStatusFilterOptions->ZoomPositionSupported = OnvifUtil::BoolValue(this->soap, false);
		trt2__GetMetadataConfigurationOptionsResponse.Options->PTZStatusFilterOptions->ZoomStatusSupported = false;

		return SOAP_OK;
	}

	int Media2BindingServiceImpl::GetAudioOutputConfigurationOptions(
		trt2__GetConfiguration* trt2__GetAudioOutputConfigurationOptions,
		_trt2__GetAudioOutputConfigurationOptionsResponse& trt2__GetAudioOutputConfigurationOptionsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:AudioOutputNotSupported", nullptr, true);
		return SOAP_FAULT;
	}

	int Media2BindingServiceImpl::GetAudioDecoderConfigurationOptions(
		trt2__GetConfiguration* trt2__GetAudioDecoderConfigurationOptions,
		_trt2__GetAudioDecoderConfigurationOptionsResponse& trt2__GetAudioDecoderConfigurationOptionsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:ActionNotSupported", "ter:AudioDecodingNotSupported", nullptr, true);
		return SOAP_FAULT;
	}

	int Media2BindingServiceImpl::GetVideoEncoderInstances(_trt2__GetVideoEncoderInstances* trt2__GetVideoEncoderInstances, _trt2__GetVideoEncoderInstancesResponse& trt2__GetVideoEncoderInstancesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto vSource = GetConfiguration()->FindVideoSourceConfiguration(trt2__GetVideoEncoderInstances->ConfigurationToken);

        if(!vSource)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoSource");
            return SOAP_FAULT;
        }
        
        updateEncoderParams(vSource);

		CodecsCountMap codecsmap;
		codecsmap.AddCodecs(vSource);

        trt2__GetVideoEncoderInstancesResponse.Info = soap_new_trt2__EncoderInstanceInfo(this->soap);

        BOOST_FOREACH(auto codec, codecsmap.GetMap())
        {
            auto codec_soap = soap_new_trt2__EncoderInstance(this->soap);
            codec_soap->Encoding = GetProxy()->convertToMedia2EncodingName(this->soap, codec.first);
            codec_soap->Number = codec.second;

            trt2__GetVideoEncoderInstancesResponse.Info->Codec.push_back(codec_soap);
        }

		trt2__GetVideoEncoderInstancesResponse.Info->Total = codecsmap.GetTotalCount();

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::GetStreamUri(_trt2__GetStreamUri* trt2__GetStreamUri, _trt2__GetStreamUriResponse& trt2__GetStreamUriResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt2__GetStreamUri->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt2__GetStreamUri->ProfileToken);
        }

        VideoSourceConfigurationSP vSource = profile->GetVideoSourceConfiguration();
		auto aSource = profile->GetAudioSourceConfiguration();
		if (!vSource && !aSource)
        {
            return incomplete_configuration(soap, profile);
        }

		if (!profile->GetEncoderConfiguration() && !profile->GetMetadataConfiguration()
			&& !profile->GetAudioEncoderConfiguration())
        {
            return incomplete_configuration(soap, profile);
        }

        bool isMulticast = trt2__GetStreamUri->Protocol == "RtspMulticast";

        if (isMulticast || profile->m_isMulticast)
        {
            MediaProfileSP newProfile(new MediaProfile(*profile.get()));
            newProfile->m_isMulticast = true;

			GetProcessorFactory()->createUpdateRtspProcessor(GetCoreSettings(), newProfile)->DoWork();
        }

        if(trt2__GetStreamUri->Protocol == "RtspOverHttp" && GetCoreSettings()->IsProxy())
        {
            trt2__GetStreamUriResponse.Uri = GetCoreSettings()->GetBaseUri() + "/rtspproxy/" +
                GetCoreSettings()->GetRtspInfo()->GetProfileUnicastUriName(profile);
        }
        else
        {
            trt2__GetStreamUriResponse.Uri = GetCoreSettings()->GetRtspInfo()->GetUri(profile, isMulticast);
        }

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::StartMulticastStreaming(trt2__StartStopMulticastStreaming *trt2__StartMulticastStreaming, trt2__SetConfigurationResponse &trt2__StartMulticastStreamingResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt2__StartMulticastStreaming->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt2__StartMulticastStreaming->ProfileToken);
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->m_isMulticast = true;

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_FAULT;
    }

    int Media2BindingServiceImpl::StopMulticastStreaming(trt2__StartStopMulticastStreaming *trt2__StopMulticastStreaming, trt2__SetConfigurationResponse &trt2__StopMulticastStreamingResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt2__StopMulticastStreaming->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt2__StopMulticastStreaming->ProfileToken);
        }

        MediaProfileSP newProfile(new MediaProfile(*profile.get()));
        newProfile->m_isMulticast = false;

		auto update_profile = GetProcessorFactory()->createProfileUpdater(newProfile, m_data->m_coreSettings);
		update_profile->DoWork();

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::SetSynchronizationPoint(_trt2__SetSynchronizationPoint* trt2__SetSynchronizationPoint, _trt2__SetSynchronizationPointResponse& trt2__SetSynchronizationPointResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

		GetSubscriptionManager()->SendPropertyEventsStates(this->soap, IMessageSubscriptionPtr());

        return SOAP_OK;
    }

    int Media2BindingServiceImpl::GetSnapshotUri(_trt2__GetSnapshotUri* trt2__GetSnapshotUri, _trt2__GetSnapshotUriResponse& trt2__GetSnapshotUriResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto profile = GetConfiguration()->FindProfile(trt2__GetSnapshotUri->ProfileToken);
        if (!profile)
        {
            return no_profile(this->soap, trt2__GetSnapshotUri->ProfileToken);
        }

        if (!profile->GetVideoSourceConfiguration() || !profile->GetEncoderConfiguration())
        {
            return incomplete_configuration(soap, profile);
        }
                
        std::string uri = "http://" + GetCoreSettings()->GetServicesAddress() + "/image.jpg?cam.id="
            + profile->GetVideoSourceConfiguration()->videoSource->camId
            + "&stream.id=" + profile->GetEncoderConfiguration()->streamId;

        trt2__GetSnapshotUriResponse.Uri = uri;

        return SOAP_OK;
    }

	const std::string DEFAULT_VIDEO_SOURCE_MODE_TOKEN = "{817642AA-F6F0-4F8F-8E36-8BDE9EB7ABCD}";

	int Media2BindingServiceImpl::GetVideoSourceModes(_trt2__GetVideoSourceModes* trt2__GetVideoSourceModes, _trt2__GetVideoSourceModesResponse& trt2__GetVideoSourceModesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_SYSTEM))
		{
			return auth_result;
		}

		auto defaultVSourceMode = soap_new_trt2__VideoSourceMode(this->soap);
		defaultVSourceMode->Description = OnvifUtil::StringValue(this->soap, "Default");
		defaultVSourceMode->Enabled = OnvifUtil::BoolValue(this->soap, true);
		defaultVSourceMode->Encodings = "H265 H264";
		defaultVSourceMode->token = DEFAULT_VIDEO_SOURCE_MODE_TOKEN;
		defaultVSourceMode->MaxResolution = soap_new_set_tt__VideoResolution(soap, 1920, 1080);

		trt2__GetVideoSourceModesResponse.VideoSourceModes.push_back(defaultVSourceMode);

		return SOAP_OK;
	}

	int Media2BindingServiceImpl::SetVideoSourceMode(_trt2__SetVideoSourceMode* trt2__SetVideoSourceMode, _trt2__SetVideoSourceModeResponse& trt2__SetVideoSourceModeResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
		{
			return auth_result;
		}

		return SOAP_OK;
	}

	int Media2BindingServiceImpl::GetOSDs(_trt2__GetOSDs* trt2__GetOSDs, _trt2__GetOSDsResponse& trt2__GetOSDsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		if(trt2__GetOSDs->OSDToken)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		if(trt2__GetOSDs->ConfigurationToken)
		{
			auto videoSrcConfig = GetConfiguration()->FindVideoSourceConfiguration(*trt2__GetOSDs->ConfigurationToken);
			if (!videoSrcConfig)
			{
				OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
				return SOAP_FAULT;
			}
		}

		return SOAP_OK;
	}

	int Media2BindingServiceImpl::GetOSDOptions(_trt2__GetOSDOptions* trt2__GetOSDOptions,
		_trt2__GetOSDOptionsResponse& trt2__GetOSDOptionsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		auto videoSrcConfig = GetConfiguration()->FindVideoSourceConfiguration(trt2__GetOSDOptions->ConfigurationToken);
		if (!videoSrcConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		return SOAP_OK;
	}

	int Media2BindingServiceImpl::SetOSD(_trt2__SetOSD* trt2__SetOSD,
		trt2__SetConfigurationResponse& trt2__SetConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
		return SOAP_FAULT;
	}

	int Media2BindingServiceImpl::CreateOSD(_trt2__CreateOSD* trt2__CreateOSD,
		_trt2__CreateOSDResponse& trt2__CreateOSDResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:Action", "ter:MaxOSDs");
		return SOAP_FAULT;
	}

	int Media2BindingServiceImpl::DeleteOSD(_trt2__DeleteOSD* trt2__DeleteOSD,
		trt2__SetConfigurationResponse& trt2__SetConfigurationResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
		return SOAP_FAULT;
	}

	trt2__Capabilities2* Media2BindingServiceImpl::GetServiceCapabilities(struct soap* soapObj, CoreSettingsSP coreSettings)
    {
        auto out_capabilities = soap_new_trt2__Capabilities2(soapObj);

        out_capabilities->ProfileCapabilities = soap_new_trt2__ProfileCapabilities(soapObj);
        out_capabilities->ProfileCapabilities->MaximumNumberOfProfiles = soap_new_int(soapObj);
        *out_capabilities->ProfileCapabilities->MaximumNumberOfProfiles = 10000;
		out_capabilities->ProfileCapabilities->ConfigurationsSupported = OnvifUtil::StringValue(soapObj, "VideoSource VideoEncoder AudioSource AudioEncoder Analytics PTZ Metadata");

        out_capabilities->StreamingCapabilities = soap_new_trt2__StreamingCapabilities(soapObj);
        out_capabilities->StreamingCapabilities->RTPMulticast = OnvifUtil::BoolValue(soapObj, coreSettings->IsMulticastEnabled());

		out_capabilities->StreamingCapabilities->RTP_USCORERTSP_USCORETCP = OnvifUtil::BoolValue(soapObj, true);

		out_capabilities->StreamingCapabilities->NonAggregateControl = OnvifUtil::BoolValue(soapObj, false);
		out_capabilities->StreamingCapabilities->AutoStartMulticast = OnvifUtil::BoolValue(soapObj, false);

		out_capabilities->StreamingCapabilities->RTSPStreaming = OnvifUtil::BoolValue(soapObj, true);

		out_capabilities->SnapshotUri = OnvifUtil::BoolValue(soapObj, true);

		out_capabilities->Rotation = OnvifUtil::BoolValue(soapObj, false);
		out_capabilities->VideoSourceMode = OnvifUtil::BoolValue(soapObj, true);
		out_capabilities->OSD = OnvifUtil::BoolValue(soapObj, false);
		out_capabilities->TemporaryOSDText = OnvifUtil::BoolValue(soapObj, false);
		out_capabilities->Mask = OnvifUtil::BoolValue(soapObj, false);
		out_capabilities->SourceMask = OnvifUtil::BoolValue(soapObj, false);

        return out_capabilities;
    }

    void Media2BindingServiceImpl::updateEncoderParams(VideoSourceConfigurationSP vSourceConfig)
    {
		if (!vSourceConfig)
		{
			return;
		}

		auto it = std::find_if(vSourceConfig->m_encoders.begin(), vSourceConfig->m_encoders.end(),
			[](const std::pair<std::string, EncoderConfigurationSP>& enc) { return enc.second->NeedsUpdate(); });

		if (it != vSourceConfig->m_encoders.end())
		{
			GetProcessorFactory()->createGetStreamParamsProcessor(vSourceConfig->videoSource)->DoWork();
		}
    }

    bool Media2BindingServiceImpl::checkCodec(const std::string& codec)
    {
		return codec == "H264" || codec == "MPEG4" || codec == "MJPEG" || codec == "H265";
    }

    bool Media2BindingServiceImpl::checkVideoSourceCodec(const VideoSourceConfigurationSP& vSourceConfig) const
    {
        BOOST_FOREACH(auto vEncoder, vSourceConfig->m_encoders)
        {
            if (vEncoder.second->IsSet() && Media2BindingServiceImpl::checkCodec(vEncoder.second->codec))
            {
                return true;
            }
        }

        return false;
    }

	MessageHolderTypeSP Media2BindingServiceImpl::CreateProfileChangedEvent(struct soap* soap, MediaProfileSP profile)
	{
		auto msg = soap_new_wsnt__NotificationMessageHolderType(soap);

		msg->Topic = soap_new_wsnt__TopicExpressionType(soap);
		msg->Topic->__any = "tns1:Media/ProfileChanged";
		msg->Topic->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;

		_tt__Message * tt_msg = soap_new__tt__Message(soap);

		tt_msg->PropertyOperation = soap_new_tt__PropertyOperation(soap);
		*tt_msg->PropertyOperation = tt__PropertyOperation__Changed;

		auto cur_tssTime = SoapProxy::GetCurTime();
		tt_msg->UtcTime = *SoapProxy::convertTime(soap, cur_tssTime);

		tt_msg->Source = soap_new_tt__ItemList(soap);

        simple_item_type prop_vToken;
		prop_vToken.Name = "Token";
		prop_vToken.Value = profile->GetToken();
		tt_msg->Source->SimpleItem.push_back(prop_vToken);

		msg->Message.__any.set(tt_msg, SOAP_TYPE__tt__Message);

		return std::make_shared<MessageHolderType>(msg, nullptr);
	}

	MessageHolderTypeSP Media2BindingServiceImpl::CreateConfigurationChangedEvent(struct soap* soap, const std::string& token, const std::string& type)
	{
		auto msg = soap_new_wsnt__NotificationMessageHolderType(soap);

		msg->Topic = soap_new_wsnt__TopicExpressionType(soap);
		msg->Topic->__any = "tns1:Media/ConfigurationChanged";
		msg->Topic->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;

		_tt__Message * tt_msg = soap_new__tt__Message(soap);

		auto cur_tssTime = SoapProxy::GetCurTime();
		tt_msg->UtcTime = *SoapProxy::convertTime(soap, cur_tssTime);

		tt_msg->Source = soap_new_tt__ItemList(soap);

        simple_item_type prop_vToken;
		prop_vToken.Name = "Token";
		prop_vToken.Value = token;
		tt_msg->Source->SimpleItem.push_back(prop_vToken);

        simple_item_type prop_vType;
		prop_vType.Name = "Type";
		prop_vType.Value = type;
		tt_msg->Source->SimpleItem.push_back(prop_vType); 

		msg->Message.__any.set(tt_msg, SOAP_TYPE__tt__Message);

		return std::make_shared<MessageHolderType>(msg, nullptr);
	}

    soap_dom_element Media2BindingServiceImpl::createMedia2ProfileChangedDescription(struct soap* soap)
    {
	    tt__MessageDescription * _MessageDescription_ProfileChangedEvt = soap_new_tt__MessageDescription(soap);
        _MessageDescription_ProfileChangedEvt->IsProperty = OnvifUtil::BoolValue(soap, true);

        _MessageDescription_ProfileChangedEvt->Source = soap_new_tt__ItemListDescription(soap);

        simple_item_description_type sourceDescr;
        sourceDescr.Name = "Token";
        sourceDescr.Type = "tt:ReferenceToken";
        _MessageDescription_ProfileChangedEvt->Source->SimpleItemDescription.push_back(sourceDescr);

        soap_dom_element msgDescr(soap);
        msgDescr.set(_MessageDescription_ProfileChangedEvt, SOAP_TYPE_tt__MessageDescription);
        msgDescr.name = "tt:MessageDescription";

        soap_dom_element evtDescrEl = EventsBuildUtil::AddHolder(soap, msgDescr, "ProfileChanged", true);
        soap_dom_element evtDescrMediaEl = EventsBuildUtil::AddHolder(soap, evtDescrEl, "tns1:Media", false);

        return evtDescrMediaEl;
    }

    soap_dom_element Media2BindingServiceImpl::createMedia2ConfigurationChangedDescription(struct soap* soap)
    {
        tt__MessageDescription * _MessageDescription_ConfigurationChangedEvt = soap_new_tt__MessageDescription(soap);
        _MessageDescription_ConfigurationChangedEvt->IsProperty = OnvifUtil::BoolValue(soap, true);

        _MessageDescription_ConfigurationChangedEvt->Source = soap_new_tt__ItemListDescription(soap);

        simple_item_description_type sourceDescr;
        sourceDescr.Name = "Token";
        sourceDescr.Type = "tt:ReferenceToken";
        _MessageDescription_ConfigurationChangedEvt->Source->SimpleItemDescription.push_back(sourceDescr);

        simple_item_description_type typeDescr;
        typeDescr.Name = "Type";
        typeDescr.Type = "xsd:string";
        _MessageDescription_ConfigurationChangedEvt->Source->SimpleItemDescription.push_back(typeDescr);

        soap_dom_element msgDescr(soap);
        msgDescr.set(_MessageDescription_ConfigurationChangedEvt, SOAP_TYPE_tt__MessageDescription);
        msgDescr.name = "tt:MessageDescription";

        soap_dom_element evtDescrEl = EventsBuildUtil::AddHolder(soap, msgDescr, "ConfigurationChanged", true);
        soap_dom_element evtDescrMediaEl = EventsBuildUtil::AddHolder(soap, evtDescrEl, "tns1:Media", false);

        return evtDescrMediaEl;
    }
}
