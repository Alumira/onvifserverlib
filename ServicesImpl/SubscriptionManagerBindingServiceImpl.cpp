#include "SubscriptionManagerBindingServiceImpl.h"
#include "Discovery/wsaapi.h"
#include <SoapProxy/SoapUtil.h>
#include <BackgroundServices/HttpService.h>
#include <BackgroundServices/EventsSubscriptionTask.h>

namespace onvif_generated_ {
    SubscriptionManagerBindingServiceImpl::SubscriptionManagerBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander) :
        SubscriptionManagerBindingService(soapObj->GetSoap())
        , IServiceImpl(data)
        , m_taskCommander(taskCommander)
    {
    }

    int SubscriptionManagerBindingServiceImpl::Renew(_wsnt__Renew* wsnt__Renew, _wsnt__RenewResponse& wsnt__RenewResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto httpUrl = HttpQuery(this->soap->endpoint);
        auto taskId = httpUrl.GetSubUri();

        auto tsk = std::dynamic_pointer_cast<EventsSubscriptionTask>(m_taskCommander->GetTask(taskId));
        if (!tsk)
        {
            OnvifUtil::ONVIF_Fault(soap, "ResourceUnknownFault", "wsrfr:ResourceUnknownFault");
            soap->header->SOAP_WSA(Action) = soap_strdup(soap, "http://docs.oasis-open.org/wsn/bw-2/SubscriptionManager/RenewResponse");
            return SOAP_FAULT;
        }

        LONG64 timeDr;
        soap_s2xsd__duration(this->soap, wsnt__Renew->TerminationTime->c_str(), &timeDr);

		auto cur_time = SoapProxy::GetCurTime();
		auto term_time = cur_time + timeDr;
    	
        tsk->SetTerminationTime(term_time);

        wsnt__RenewResponse.CurrentTime = SoapProxy::convertTime(this->soap, cur_time);
        wsnt__RenewResponse.TerminationTime = *SoapProxy::convertTime(this->soap, tsk->GetTerminationTime());

        return soap_wsa_reply(this->soap, nullptr, "http://docs.oasis-open.org/wsn/bw-2/SubscriptionManager/RenewResponse");
    }

    int SubscriptionManagerBindingServiceImpl::Unsubscribe(_wsnt__Unsubscribe* wsnt__Unsubscribe, _wsnt__UnsubscribeResponse& wsnt__UnsubscribeResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto httpUrl = HttpQuery(this->soap->endpoint);
        auto taskId = httpUrl.GetSubUri();

        auto tsk = std::dynamic_pointer_cast<EventsSubscriptionTask>(m_taskCommander->GetTask(taskId));
        if (!tsk)
        {
            OnvifUtil::ONVIF_Fault(soap, "ResourceUnknownFault", "wsrfr:ResourceUnknownFault");
            return SOAP_FAULT;
        }

        m_taskCommander->RemoveTask(tsk->GetGUID());

        return soap_wsa_reply(this->soap, nullptr, "http://docs.oasis-open.org/wsn/bw-2/SubscriptionManager/UnsubscribeResponse");
    }
}
