#include "SearchBindingServiceImpl.h"
#include <boost/uuid/random_generator.hpp>
#include <boost/foreach.hpp>
#include "SoapProxy/EventsBuildUtil.h"

namespace onvif_generated_ {
    SearchBindingServiceImpl::SearchBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander) :
            SearchBindingService(soapObj->GetSoap())
            , IServiceImpl(data)
            , m_taskCommander(taskCommander)
	{
	}

    int SearchBindingServiceImpl::GetServiceCapabilities(_tse__GetServiceCapabilities* tse__GetServiceCapabilities, _tse__GetServiceCapabilitiesResponse& tse__GetServiceCapabilitiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }
        
        tse__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities(this->soap);
        return SOAP_OK;
    }

    int SearchBindingServiceImpl::GetRecordingSummary(_tse__GetRecordingSummary* tse__GetRecordingSummary, _tse__GetRecordingSummaryResponse& tse__GetRecordingSummaryResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
        
        //Returns a structure containing: DataFrom specifying the first time when there is recorded data on the device; DataUntil specifying the
        //last point in time where there is data recorded on the device; the total number of recordings on the device.

        tse__GetRecordingSummaryResponse.Summary = soap_new_tt__RecordingSummary(this->soap);

        auto dataTo = SoapProxy::GetCurTime();

        tse__GetRecordingSummaryResponse.Summary->DataFrom = *SoapProxy::convertTime(this->soap, 0);
        tse__GetRecordingSummaryResponse.Summary->DataUntil = *SoapProxy::convertTime(this->soap, dataTo);

        tse__GetRecordingSummaryResponse.Summary->NumberRecordings = GetConfiguration()->GetRecordingConfigurations().size();

        return SOAP_OK;
    }

    int SearchBindingServiceImpl::GetRecordingInformation(_tse__GetRecordingInformation* tse__GetRecordingInformation, _tse__GetRecordingInformationResponse& tse__GetRecordingInformationResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
        
        auto recordingConfig = GetConfiguration()->findRecordingConfig(tse__GetRecordingInformation->RecordingToken);
        if (recordingConfig)
        {
			GetProcessorFactory()->createGetArchDepthProcessor(recordingConfig)->DoWork();
            
            tse__GetRecordingInformationResponse.RecordingInformation = GetProxy()->createRecordingInformation(this->soap, recordingConfig);
        }
        else
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidToken");
            return SOAP_FAULT;
        }

        return SOAP_OK;
    }

    int SearchBindingServiceImpl::FindRecordings(_tse__FindRecordings* tse__FindRecordings, _tse__FindRecordingsResponse& tse__FindRecordingsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
        
        int maxMathes = -1;
        if(tse__FindRecordings->MaxMatches)
        {
            maxMathes = *tse__FindRecordings->MaxMatches;
        }

        auto tracks = GetProxy()->GetTrackTypes(tse__FindRecordings->Scope->RecordingInformationFilter);

		auto newTask = GetProcessorFactory()->createRecordingSearchTask(maxMathes, tse__FindRecordings->KeepAliveTime, tracks);
    	auto newTaskUid = 
            m_taskCommander->AddTask(newTask, this->soap);
            
        if (!newTaskUid.GUID.empty())
        {
            tse__FindRecordingsResponse.SearchToken = newTaskUid.GUID;
        }
        else
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:Action", "ter:ResourceProblem");
            return SOAP_FAULT;
        }

        return SOAP_OK;
    }

    int SearchBindingServiceImpl::GetRecordingSearchResults(_tse__GetRecordingSearchResults* tse__GetRecordingSearchResults, _tse__GetRecordingSearchResultsResponse& tse__GetRecordingSearchResultsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
        
        tse__GetRecordingSearchResultsResponse.ResultList = soap_new_tt__FindRecordingResultList(this->soap);

        int maxResults = tse__GetRecordingSearchResults->MaxResults ? *tse__GetRecordingSearchResults->MaxResults : -1;
        //int minResults = tse__GetRecordingSearchResults->MinResults ? *tse__GetRecordingSearchResults->MinResults : 0;

        //task results
        auto tsk = std::dynamic_pointer_cast<IRecordingSearchTask>(m_taskCommander->GetTask(tse__GetRecordingSearchResults->SearchToken));
        if (!tsk)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidToken");
            return SOAP_FAULT;
        }

        BOOST_FOREACH(auto interval, tsk->PopRecordings(maxResults))
        {
			tt__RecordingInformation * recInfo = GetProxy()->createRecordingInformation(this->soap, interval);
			tse__GetRecordingSearchResultsResponse.ResultList->RecordingInformation.push_back(recInfo);
			
        	if (!interval->m_startArchTime && !interval->m_stopArchTime)
			{
				ONVIF_LOG_BASE(GetLogger(), ITV8::LOG_ERROR, "Found recording " << interval->GetToken() << "has empty dates.");
			}
        }

        if(tsk->GetStatus() == ISearchTask::TIMEOUT)
        {
			tse__GetRecordingSearchResultsResponse.ResultList->SearchState = tt__SearchState__Completed;
        }
    	if (tsk->GetStatus() != ISearchTask::PROGRESS && !tsk->GetRecordingsNum())
        {
            tse__GetRecordingSearchResultsResponse.ResultList->SearchState = tt__SearchState__Completed;
        }
        else if (tsk->GetStatus() == ISearchTask::PROGRESS || tsk->GetRecordingsNum())
        {
            tse__GetRecordingSearchResultsResponse.ResultList->SearchState = tt__SearchState__Searching;
        }
        
        return SOAP_OK;
    }

    int SearchBindingServiceImpl::FindEvents(_tse__FindEvents* tse__FindEvents, _tse__FindEventsResponse& tse__FindEventsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
        
        //new task
        if(tse__FindEvents->Scope && !tse__FindEvents->Scope->IncludedRecordings.size() && !tse__FindEvents->Scope->IncludedSources.size())
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:Action", "ter:ResourceProblem");
            return SOAP_FAULT;
        }

        int maxMathes = -1;
        if (tse__FindEvents->MaxMatches)
        {
            maxMathes = *tse__FindEvents->MaxMatches;
        }

        RecordingConfigurationSP recordingConfig;
        if(tse__FindEvents->Scope && tse__FindEvents->Scope->IncludedRecordings.size())
        {
            recordingConfig = GetConfiguration()->findRecordingConfig(tse__FindEvents->Scope->IncludedRecordings[0]);
        }

        else if(tse__FindEvents->Scope && tse__FindEvents->Scope->IncludedSources.size() && tse__FindEvents->Scope->IncludedSources[0])
        {
            if(SoapProxy::SchemaProfileUrl == tse__FindEvents->Scope->IncludedSources[0]->Type)
            {
				auto profile = GetConfiguration()->FindProfile(tse__FindEvents->Scope->IncludedSources[0]->Token);
            	if(profile != nullptr && profile->GetVideoSourceConfiguration())
            	{
					recordingConfig = GetConfiguration()->findRecordingBySourceId(profile->GetVideoSourceConfiguration()->videoSource->GetToken());
            	}
            }
			else
			{
				recordingConfig = GetConfiguration()->findRecordingBySourceId(tse__FindEvents->Scope->IncludedSources[0]->Token);
			}
        }

        if(!recordingConfig && !GetConfiguration()->GetRecordingConfigurations().empty())
        {
            //TODO: this is a brief fix for request that does not contain any source reference.
			//We will get only first recording for now. 
			//Actually, we need to search all events in all recordings (it should be checked also)
			recordingConfig = *GetConfiguration()->GetRecordingConfigurations().begin();
        }

		if (!recordingConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:Action", "ter:ResourceProblem");
			return SOAP_FAULT;
		}

        if(recordingConfig)
        {
            SearchTaskParams taskParams(recordingConfig->GetVSource()->slave_id, recordingConfig->GetVSource()->camId, tse__FindEvents->StartPoint,
                *tse__FindEvents->EndPoint, tse__FindEvents->KeepAliveTime, maxMathes);
            taskParams.m_includeVirtualEvents = tse__FindEvents->IncludeStartState;

            auto newTask = GetProcessorFactory()->createSearchTask(taskParams);
			
        	auto result = m_taskCommander->AddTask(newTask, this->soap);
            if(!result.GUID.empty())
            {
                tse__FindEventsResponse.SearchToken = result.GUID;
            }
            else
            {
                OnvifUtil::ONVIF_Fault(soap, "ter:Action", "ter:ResourceProblem");
                return SOAP_FAULT;
            }
        }

        return SOAP_OK;
    }

    int SearchBindingServiceImpl::GetEventSearchResults(_tse__GetEventSearchResults* tse__GetEventSearchResults, _tse__GetEventSearchResultsResponse& tse__GetEventSearchResultsResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }
        
        //task results
        auto tsk = std::dynamic_pointer_cast<ISearchTask>(m_taskCommander->GetTask(tse__GetEventSearchResults->SearchToken));
        if(!tsk)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidToken");
            return SOAP_FAULT;
        }

        tse__GetEventSearchResultsResponse.ResultList = soap_new_tt__FindEventResultList(this->soap);

        BOOST_FOREACH(auto interval, tsk->PopIntervals(200))
        {
            if(!addRecordingEvent(tse__GetEventSearchResultsResponse.ResultList, interval, tsk))
            {
				break;
            }

            addTimeEvent(tse__GetEventSearchResultsResponse.ResultList, interval, tsk);
        }

        if (tsk->GetStatus() != ISearchTask::PROGRESS && !tsk->GetRecordingsNum())
        {
            tse__GetEventSearchResultsResponse.ResultList->SearchState = tt__SearchState__Completed;
        }
        else if (tsk->GetStatus() == ISearchTask::PROGRESS || tsk->GetRecordingsNum())
        {
            tse__GetEventSearchResultsResponse.ResultList->SearchState = tt__SearchState__Searching;
        }

        return SOAP_OK;
    }

    int SearchBindingServiceImpl::EndSearch(_tse__EndSearch* tse__EndSearch, _tse__EndSearchResponse& tse__EndSearchResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto tsk = m_taskCommander->GetTask(tse__EndSearch->SearchToken);
        if (!tsk)
        {
            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidToken");
            return SOAP_FAULT;
        }

        /*if(tsk->GetStatus() == SearchTask::FINISHED)
        {
            m_taskCommander->RemoveTask(tse__EndSearch->SearchToken);

            OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidToken");
            return SOAP_FAULT;
        }*/

        if(auto stsk = std::dynamic_pointer_cast<ISearchTask>(tsk))
        {
            tse__EndSearchResponse.Endpoint = *SoapProxy::convertTime(this->soap, stsk->GetSearchPoint());
        }

        m_taskCommander->RemoveTask(tse__EndSearch->SearchToken);

        return SOAP_OK;
    }

    tse__Capabilities* SearchBindingServiceImpl::GetServiceCapabilities(struct soap* soapObj)
    {
        auto _capabilities = soap_new_tse__Capabilities(soapObj);
        _capabilities->MetadataSearch = OnvifUtil::BoolValue(soapObj, false);

        _capabilities->GeneralStartEvents = OnvifUtil::BoolValue(soapObj, false);

        return _capabilities;
    }

	bool SearchBindingServiceImpl::addTimeEvent(tt__FindEventResultList* resultList, const IntellectRecEvent& recEvt, ISearchTaskSP tsk)
    {
		auto recConfig = GetConfiguration()->findRecordingConfigByCam(tsk->GetInfo().m_recording_id);
		if(!recConfig)
		{
			return false;
		}

		BOOST_FOREACH(auto track, recConfig->GetTracks())
		{
			tt__FindEventResult * resIntervalEnd = soap_new_tt__FindEventResult(this->soap);
			resIntervalEnd->RecordingToken = recConfig->GetToken();
			resIntervalEnd->TrackToken = track->GetToken();
			resIntervalEnd->Time = *GetProxy()->convertTime(this->soap, recEvt.m_time);

			resIntervalEnd->StartStateEvent = recEvt.m_virtual;

		    resIntervalEnd->Event = CreateEvent("tns1:RecordingHistory/Track/State");

			_tt__Message * msg = CreateRecordingMessage(recEvt.m_time, recEvt.m_start);

            simple_item_type prop;
			prop.Name = "IsDataPresent";
			prop.Value = soap_bool2s(this->soap, recEvt.m_start);
			msg->Data->SimpleItem.push_back(prop);

			resIntervalEnd->Event->Message.__any.set(msg, SOAP_TYPE__tt__Message);

			if (tsk->GetInfo().m_maxCount > 0 && int(resultList->Result.size()) >= tsk->GetInfo().m_maxCount)
			{
				return false;
			}

			resultList->Result.push_back(resIntervalEnd);
		}
		return true;
    }

	bool SearchBindingServiceImpl::addRecordingEvent(tt__FindEventResultList* resultList, const IntellectRecEvent& recEvt, ISearchTaskSP tsk)
    {
		auto recConfig = GetConfiguration()->findRecordingConfigByCam(tsk->GetInfo().m_recording_id);
		if (!recConfig)
		{
			return false;
		}

		//BOOST_FOREACH(auto track, recConfig->GetTracks())
		{
			tt__FindEventResult * resIntervalEnd = soap_new_tt__FindEventResult(this->soap);
			resIntervalEnd->RecordingToken = recConfig->GetToken();
			//resIntervalEnd->TrackToken = track->GetToken();
			resIntervalEnd->Time = *GetProxy()->convertTime(this->soap, recEvt.m_time);

			resIntervalEnd->StartStateEvent = recEvt.m_virtual;

			resIntervalEnd->Event = CreateEvent("tns1:RecordingHistory/Recording/State");

			_tt__Message * msg = CreateRecordingMessage(recEvt.m_time, recEvt.m_start);

            simple_item_type prop;
			prop.Name = "IsRecording";
			prop.Value = soap_bool2s(this->soap, recEvt.m_start);
			msg->Data->SimpleItem.push_back(prop);

			resIntervalEnd->Event->Message.__any.set(msg, SOAP_TYPE__tt__Message);

			if (tsk->GetInfo().m_maxCount > 0 && int(resultList->Result.size()) >= tsk->GetInfo().m_maxCount)
			{
				return false;
			}

			resultList->Result.push_back(resIntervalEnd);
		}
		return true;
    }

    _tt__Message * SearchBindingServiceImpl::CreateRecordingMessage(const time_t& tssTime, bool isStart)
    {
        auto msg = soap_new__tt__Message(this->soap);

        msg->Data = soap_new_tt__ItemList(this->soap);

        msg->UtcTime = *GetProxy()->convertTime(this->soap, tssTime);

        msg->PropertyOperation = soap_new_tt__PropertyOperation(this->soap);
        *msg->PropertyOperation = isStart ? tt__PropertyOperation__Initialized : tt__PropertyOperation__Deleted;

        msg->Source = soap_new_tt__ItemList(this->soap);

        return msg;
    }

    wsnt__NotificationMessageHolderType* SearchBindingServiceImpl::CreateEvent(std::string eventName) const
    {
        auto evt = soap_new_wsnt__NotificationMessageHolderType(this->soap);
        
        evt->Topic = soap_new_wsnt__TopicExpressionType(this->soap);
        evt->Topic->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;
        evt->Topic->__any = eventName;
        evt->Topic->__mixed.name = "-mixed";

        return evt;
    }
}

