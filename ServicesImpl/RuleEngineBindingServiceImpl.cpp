#include "RuleEngineBindingServiceImpl.h"
#include "onvif_generated_H.h"

namespace onvif_generated_ {
    RuleEngineBindingServiceImpl::RuleEngineBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data) :
            RuleEngineBindingService(soapObj->GetSoap()), IServiceImpl(data)
	{
	}

	int RuleEngineBindingServiceImpl::GetSupportedRules(_tanl2__GetSupportedRules* tanl2__GetSupportedRules,
		_tanl2__GetSupportedRulesResponse& tanl2__GetSupportedRulesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		auto vAnalyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(tanl2__GetSupportedRules->ConfigurationToken);

		if (!vAnalyticsConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		return SOAP_OK;
	}

	int RuleEngineBindingServiceImpl::CreateRules(_tanl2__CreateRules* tanl2__CreateRules,
		_tanl2__CreateRulesResponse& tanl2__CreateRulesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto vAnalyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(tanl2__CreateRules->ConfigurationToken);

		if (!vAnalyticsConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:Action", "ter:TooManyRules", nullptr, true);
		return SOAP_FAULT;
	}

	int RuleEngineBindingServiceImpl::DeleteRules(_tanl2__DeleteRules* tanl2__DeleteRules,
		_tanl2__DeleteRulesResponse& tanl2__DeleteRulesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto vAnalyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(tanl2__DeleteRules->ConfigurationToken);

		if (!vAnalyticsConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgs", "ter:RuleNotExistent");
		return SOAP_FAULT;
	}

	int RuleEngineBindingServiceImpl::GetRules(_tanl2__GetRules* tanl2__GetRules,
		_tanl2__GetRulesResponse& tanl2__GetRulesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
		{
			return auth_result;
		}

		auto vAnalyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(tanl2__GetRules->ConfigurationToken);

		if (!vAnalyticsConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		BOOST_FOREACH(auto rule, vAnalyticsConfig->GetModules())
		{
			tanl2__GetRulesResponse.Rule.push_back(GetProxy()->createVideoAnalyticsModule(this->soap, rule));
		}

		return SOAP_OK;
	}

	int RuleEngineBindingServiceImpl::ModifyRules(_tanl2__ModifyRules* tanl2__ModifyRules,
		_tanl2__ModifyRulesResponse& tanl2__ModifyRulesResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto vAnalyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(tanl2__ModifyRules->ConfigurationToken);

		if (!vAnalyticsConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		return SOAP_OK;
	}

	int RuleEngineBindingServiceImpl::GetRuleOptions(_tanl2__GetRuleOptions* tanl2__GetRuleOptions,
		_tanl2__GetRuleOptionsResponse& tanl2__GetRuleOptionsResponse)
	{
		if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::ACTUATE))
		{
			return auth_result;
		}

		auto vAnalyticsConfig = GetConfiguration()->FindVideoAnalyticsConfiguration(tanl2__GetRuleOptions->ConfigurationToken);

		if (!vAnalyticsConfig)
		{
			OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:NoConfig");
			return SOAP_FAULT;
		}

		return SOAP_OK;
	}
}

