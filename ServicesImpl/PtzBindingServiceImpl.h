#pragma once

#include "onvif_generated_PTZBindingService.h"
#include <Interface/IServiceImpl.h>
#include <SoapProxy/SoapUtil.h>

namespace onvif_generated_ {
	class PtzBindingServiceImpl : public PTZBindingService, public IServiceImpl
	{
	public:
        PtzBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data);

        PTZBindingService* copy() override { return new PtzBindingServiceImpl(*this); }

        int GetServiceCapabilities(_tptz2__GetServiceCapabilities* tptz2__GetServiceCapabilities, _tptz2__GetServiceCapabilitiesResponse& tptz2__GetServiceCapabilitiesResponse) override;
        int GetConfigurations(_tptz2__GetConfigurations* tptz2__GetConfigurations, _tptz2__GetConfigurationsResponse& tptz2__GetConfigurationsResponse) override;
        int GetPresets(_tptz2__GetPresets* tptz2__GetPresets, _tptz2__GetPresetsResponse& tptz2__GetPresetsResponse) override;
        int SetPreset(_tptz2__SetPreset* tptz2__SetPreset, _tptz2__SetPresetResponse& tptz2__SetPresetResponse) override;
        int RemovePreset(_tptz2__RemovePreset* tptz2__RemovePreset, _tptz2__RemovePresetResponse& tptz2__RemovePresetResponse) override;
        int GotoPreset(_tptz2__GotoPreset* tptz2__GotoPreset, _tptz2__GotoPresetResponse& tptz2__GotoPresetResponse) override;
        int GetStatus(_tptz2__GetStatus* tptz2__GetStatus, _tptz2__GetStatusResponse& tptz2__GetStatusResponse) override;
        int GetConfiguration(_tptz2__GetConfiguration* tptz2__GetConfiguration, _tptz2__GetConfigurationResponse& tptz2__GetConfigurationResponse) override;
        int GetNodes(_tptz2__GetNodes* tptz2__GetNodes, _tptz2__GetNodesResponse& tptz2__GetNodesResponse) override;
        int GetNode(_tptz2__GetNode* tptz2__GetNode, _tptz2__GetNodeResponse& tptz2__GetNodeResponse) override;
        int SetConfiguration(_tptz2__SetConfiguration* tptz2__SetConfiguration, _tptz2__SetConfigurationResponse& tptz2__SetConfigurationResponse) override;
        int GetConfigurationOptions(_tptz2__GetConfigurationOptions* tptz2__GetConfigurationOptions, _tptz2__GetConfigurationOptionsResponse& tptz2__GetConfigurationOptionsResponse) override;
        int GotoHomePosition(_tptz2__GotoHomePosition* tptz2__GotoHomePosition, _tptz2__GotoHomePositionResponse& tptz2__GotoHomePositionResponse) override;
		int SetHomePosition(_tptz2__SetHomePosition* tptz2__SetHomePosition, _tptz2__SetHomePositionResponse& tptz2__SetHomePositionResponse) override;
        int ContinuousMove(_tptz2__ContinuousMove* tptz2__ContinuousMove, _tptz2__ContinuousMoveResponse& tptz2__ContinuousMoveResponse) override;
        int RelativeMove(_tptz2__RelativeMove* tptz2__RelativeMove, _tptz2__RelativeMoveResponse& tptz2__RelativeMoveResponse) override;
	    int SendAuxiliaryCommand(_tptz2__SendAuxiliaryCommand* tptz2__SendAuxiliaryCommand, _tptz2__SendAuxiliaryCommandResponse& tptz2__SendAuxiliaryCommandResponse) override { return defaultImplementation(); }
        int AbsoluteMove(_tptz2__AbsoluteMove* tptz2__AbsoluteMove, _tptz2__AbsoluteMoveResponse& tptz2__AbsoluteMoveResponse) override;
		int GeoMove(_tptz2__GeoMove *tptz2__GeoMove, _tptz2__GeoMoveResponse &tptz2__GeoMoveResponse) override { return defaultImplementation(); }
        int Stop(_tptz2__Stop* tptz2__Stop, _tptz2__StopResponse& tptz2__StopResponse) override;
	    
	    int GetPresetTours(_tptz2__GetPresetTours* tptz2__GetPresetTours, _tptz2__GetPresetToursResponse& tptz2__GetPresetToursResponse) override { return defaultImplementation(); }
	    int GetPresetTour(_tptz2__GetPresetTour* tptz2__GetPresetTour, _tptz2__GetPresetTourResponse& tptz2__GetPresetTourResponse) override { return defaultImplementation(); }
	    int GetPresetTourOptions(_tptz2__GetPresetTourOptions* tptz2__GetPresetTourOptions, _tptz2__GetPresetTourOptionsResponse& tptz2__GetPresetTourOptionsResponse) override { return defaultImplementation(); }
	    int CreatePresetTour(_tptz2__CreatePresetTour* tptz2__CreatePresetTour, _tptz2__CreatePresetTourResponse& tptz2__CreatePresetTourResponse) override { return defaultImplementation(); }
	    int ModifyPresetTour(_tptz2__ModifyPresetTour* tptz2__ModifyPresetTour, _tptz2__ModifyPresetTourResponse& tptz2__ModifyPresetTourResponse) override { return defaultImplementation(); }
	    int OperatePresetTour(_tptz2__OperatePresetTour* tptz2__OperatePresetTour, _tptz2__OperatePresetTourResponse& tptz2__OperatePresetTourResponse) override { return defaultImplementation(); }
	    int RemovePresetTour(_tptz2__RemovePresetTour* tptz2__RemovePresetTour, _tptz2__RemovePresetTourResponse& tptz2__RemovePresetTourResponse) override { return defaultImplementation(); }
        int GetCompatibleConfigurations(_tptz2__GetCompatibleConfigurations* tptz2__GetCompatibleConfigurations, _tptz2__GetCompatibleConfigurationsResponse& tptz2__GetCompatibleConfigurationsResponse) override;
	
	    
	    //IServiceImpl

	    int Dispatch(struct soap* soapPtr) override
        {
            return PTZBindingService::dispatch(soapPtr);
        }

        static tptz2__Capabilities* GetServiceCapabilities(struct soap* soapObj);

        static std::string GetUri()
        {
            return "/ptz_service";
        }

	private:
        bool checkBounds(float move_coord) const;

        int defaultImplementation() override
        {
            if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
            {
                return auth_result;
            }

			OnvifUtil::ONVIF_Fault(this->soap, "ter:ActionNotSupported", "ter:NotImplemented", "This optinal method is not implemented", true);
            return SOAP_FAULT;
        }
	};
}
