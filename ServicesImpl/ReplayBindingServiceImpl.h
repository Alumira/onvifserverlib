#pragma once

#include "onvif_generated_ReplayBindingService.h"
#include <Interface/IServiceImpl.h>

namespace onvif_generated_ {
	class ReplayBindingServiceImpl : public ReplayBindingService, public IServiceImpl
	{
	public:
        ReplayBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data);

        ReplayBindingService* copy() override { return new ReplayBindingServiceImpl(*this); }

        int GetServiceCapabilities(_trp__GetServiceCapabilities* trp__GetServiceCapabilities, _trp__GetServiceCapabilitiesResponse& trp__GetServiceCapabilitiesResponse) override;
        int GetReplayUri(_trp__GetReplayUri* trp__GetReplayUri, _trp__GetReplayUriResponse& trp__GetReplayUriResponse) override;
		int GetReplayConfiguration(_trp__GetReplayConfiguration* trp__GetReplayConfiguration, _trp__GetReplayConfigurationResponse& trp__GetReplayConfigurationResponse) override;
		int SetReplayConfiguration(_trp__SetReplayConfiguration* trp__SetReplayConfiguration, _trp__SetReplayConfigurationResponse& trp__SetReplayConfigurationResponse) override;

        //IServiceImpl

        int Dispatch(struct soap* soapPtr) override
        {
            return ReplayBindingService::dispatch(soapPtr);
        }

        static trp__Capabilities* GetServiceCapabilities(struct soap* soapObj);

        static std::string GetUri()
        {
            return "/replay_service";
        }
	};
}
