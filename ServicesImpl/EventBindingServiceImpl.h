#pragma once

#include "onvif_generated_EventBindingService.h"
#include <Interface/IServiceImpl.h>
#include <BackgroundServices/TaskCommander.h>
#include <SoapProxy/SoapUtil.h>

namespace onvif_generated_ {
    class EventBindingServiceImpl : public EventBindingService, public IServiceImpl
    {
    public:
        static const int MAX_REQUESTS = 50;

        EventBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander);

        EventBindingService* copy() override { return new EventBindingServiceImpl(*this); }


        //IServiceImpl 

        int GetServiceCapabilities(_tev__GetServiceCapabilities* tev__GetServiceCapabilities, _tev__GetServiceCapabilitiesResponse& tev__GetServiceCapabilitiesResponse) override;

        int CreatePullPointSubscription(_tev__CreatePullPointSubscription* tev__CreatePullPointSubscription, _tev__CreatePullPointSubscriptionResponse& tev__CreatePullPointSubscriptionResponse) override;

        int GetEventProperties(_tev__GetEventProperties* tev__GetEventProperties, _tev__GetEventPropertiesResponse& tev__GetEventPropertiesResponse) override;

        int Dispatch(struct soap* soapPtr) override
        {
            return EventBindingService::dispatch(soapPtr);
        }

        static tev__Capabilities* GetServiceCapabilities(struct soap* soapObj);

        static std::string GetUri()
        {
            return "/event_service";
        }

    private:
        int defaultImplementation() override
        {
            if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
            {
                return auth_result;
            }

            OnvifUtil::ONVIF_Fault(this->soap, "ter:ActionNotSupported", "ter:NotImplemented", "This optinal method is not implemented", true);
            return SOAP_FAULT;
        }

        TaskCommanderSP m_taskCommander;

        soap_dom_element createMotionAlarmDescription(struct soap* soap);
        soap_dom_element createLongInZoneDescription(struct soap* soap);
        soap_dom_element createLostObjectDescription(struct soap* soap);
    public:
        int AddEventBroker(_tev__AddEventBroker *tev__AddEventBroker,
            _tev__AddEventBrokerResponse &tev__AddEventBrokerResponse)
            override;
        int DeleteEventBroker(
            _tev__DeleteEventBroker *tev__DeleteEventBroker,
            _tev__DeleteEventBrokerResponse &
            tev__DeleteEventBrokerResponse) override;
        int GetEventBrokers(_tev__GetEventBrokers *tev__GetEventBrokers,
            _tev__GetEventBrokersResponse &tev__GetEventBrokersResponse)
            override;
    };
}
