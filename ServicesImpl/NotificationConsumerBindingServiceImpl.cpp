#include "NotificationConsumerBindingServiceImpl.h"

namespace onvif_generated_ {
    NotificationConsumerBindingServiceImpl::NotificationConsumerBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander) :
        NotificationConsumerBindingService(soapObj->GetSoap())
        , IServiceImpl(data)
        , m_taskCommander(taskCommander)
    {
    }

	int NotificationConsumerBindingServiceImpl::Notify(_wsnt__Notify* wsnt__Notify)
	{
		return SOAP_OK;
	}
}
