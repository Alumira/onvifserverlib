#include "EventBindingServiceImpl.h"
#include "Discovery/wsaapi.h"
#include "RecordingBindingServiceImpl.h"
#include "DeviceIOBindingServiceImpl.h"
#include "Media2BindingServiceImpl.h"
#include "SoapProxy/EventsBuildUtil.h"
#include "SSTMK/SSMTKevents.h"

namespace onvif_generated_ {
    EventBindingServiceImpl::EventBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data, TaskCommanderSP taskCommander) :
        EventBindingService(soapObj->GetSoap()), IServiceImpl(data), m_taskCommander(taskCommander)
    {
    }

    int EventBindingServiceImpl::GetServiceCapabilities(_tev__GetServiceCapabilities* tev__GetServiceCapabilities, _tev__GetServiceCapabilitiesResponse& tev__GetServiceCapabilitiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
        {
            return auth_result;
        }

        tev__GetServiceCapabilitiesResponse.Capabilities = GetServiceCapabilities(this->soap);
        return SOAP_OK;
    }

    int EventBindingServiceImpl::CreatePullPointSubscription(_tev__CreatePullPointSubscription* tev__CreatePullPointSubscription, _tev__CreatePullPointSubscriptionResponse& tev__CreatePullPointSubscriptionResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        auto cur_tssTime = SoapProxy::GetCurTime();

        LONG64 timeDr = 10000000000;
        if (tev__CreatePullPointSubscription->InitialTerminationTime)
        {
            soap_s2xsd__duration(this->soap, tev__CreatePullPointSubscription->InitialTerminationTime->c_str(), &timeDr);

            if (!timeDr)
            {
                auto lMsg = (boost::format("[%1%] Failed to parse termination time value: '%2%'") % this->soap->socket % *tev__CreatePullPointSubscription->InitialTerminationTime).str();
                ONVIF_LOG_ERROR(m_data->m_logger, lMsg);
            }
        }

        auto termTime = cur_tssTime + timeDr;

        std::vector<FilterTypeSP> filter;
        if (tev__CreatePullPointSubscription->Filter)
        {
            BOOST_FOREACH(auto txt, tev__CreatePullPointSubscription->Filter->__any)
            {
                if (auto tpe = FilterType::ConvertType(txt.name))
                {
                    filter.push_back(FilterTypeSP(new FilterType(tpe, txt.text)));
                }
            }
        }

        auto newTask = GetProcessorFactory()->createSubscriptionTask(GetLogger(), termTime, filter, NotificationConsumerBindingProxySP());
        auto newTaskUid =
            m_taskCommander->AddTask(newTask, this->soap);

        if (newTaskUid.GUID.empty())
        {
            return SOAP_FAULT;
        }

        std::string addr = GetCoreSettings()->GetServiceUri() + GetUri() + "/subscription/" + newTaskUid.GUID;
        tev__CreatePullPointSubscriptionResponse.SubscriptionReference.Address = soap_strdup(this->soap, addr.c_str());

        tev__CreatePullPointSubscriptionResponse.wsnt__CurrentTime = *SoapProxy::convertTime(this->soap, cur_tssTime);
        tev__CreatePullPointSubscriptionResponse.wsnt__TerminationTime = *SoapProxy::convertTime(this->soap, termTime);

        return soap_wsa_reply(this->soap, nullptr, "http://www.onvif.org/ver10/events/wsdl/EventPortType/CreatePullPointSubscriptionResponse");
    }

    int EventBindingServiceImpl::GetEventProperties(_tev__GetEventProperties* tev__GetEventProperties, _tev__GetEventPropertiesResponse& tev__GetEventPropertiesResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        tev__GetEventPropertiesResponse.TopicNamespaceLocation.push_back("http://www.onvif.org/onvif/ver10/topics/topicns.xml");
        tev__GetEventPropertiesResponse.wsnt__FixedTopicSet = false;
        tev__GetEventPropertiesResponse.wstop__TopicSet = soap_new_wstop__TopicSetType(this->soap);

        tev__GetEventPropertiesResponse.wstop__TopicSet->__any.push_back(createMotionAlarmDescription(this->soap));

        tev__GetEventPropertiesResponse.wstop__TopicSet->__any.push_back(createLongInZoneDescription(this->soap));

        tev__GetEventPropertiesResponse.wstop__TopicSet->__any.push_back(createLostObjectDescription(this->soap));

        //Media2 service Events
        tev__GetEventPropertiesResponse.wstop__TopicSet->__any.push_back(Media2BindingServiceImpl::createMedia2ProfileChangedDescription(this->soap));
        tev__GetEventPropertiesResponse.wstop__TopicSet->__any.push_back(Media2BindingServiceImpl::createMedia2ConfigurationChangedDescription(this->soap));

        //Recording service Events
        tev__GetEventPropertiesResponse.wstop__TopicSet->__any.push_back(RecordingBindingServiceImpl::createRecordingConfigEventsDescription(this->soap));

        //IO service Events
        tev__GetEventPropertiesResponse.wstop__TopicSet->__any.push_back(DeviceIOBindingServiceImpl::createRelayEventsDescription(this->soap));
        tev__GetEventPropertiesResponse.wstop__TopicSet->__any.push_back(DeviceIOBindingServiceImpl::createRayEventsDescription(this->soap));

        //SSMT events
        tev__GetEventPropertiesResponse.wstop__TopicSet->__any.push_back(SSMTKevents::CreateEventsDescription(this->soap));

        //Common topics
        tev__GetEventPropertiesResponse.wsnt__TopicExpressionDialect.push_back(EventsBuildUtil::TopicExpressionConcreteSetDialectUri);
        tev__GetEventPropertiesResponse.wsnt__TopicExpressionDialect.push_back(EventsBuildUtil::TopicExpressionConcreteDialectUri);

        tev__GetEventPropertiesResponse.MessageContentFilterDialect.push_back(EventsBuildUtil::MessageContentDialectUri);
        tev__GetEventPropertiesResponse.MessageContentSchemaLocation.push_back("http://www.onvif.org/ver10/schema/onvif.xsd");

        return soap_wsa_reply(this->soap, nullptr, "http://www.onvif.org/ver10/events/wsdl/EventPortType/GetEventPropertiesResponse");
    }

    tev__Capabilities* EventBindingServiceImpl::GetServiceCapabilities(struct soap* soapObj)
    {
        auto _capabilities = soap_new_tev__Capabilities(soapObj);

        _capabilities->WSSubscriptionPolicySupport = OnvifUtil::BoolValue(soapObj, false);
        _capabilities->WSPullPointSupport = OnvifUtil::BoolValue(soapObj, true);
        _capabilities->WSPausableSubscriptionManagerInterfaceSupport = OnvifUtil::BoolValue(soapObj, false);

        _capabilities->MaxNotificationProducers = soap_new_int(soapObj);
        *_capabilities->MaxNotificationProducers = MAX_REQUESTS;

        _capabilities->MaxPullPoints = soap_new_int(soapObj);
        *_capabilities->MaxPullPoints = MAX_REQUESTS;

        _capabilities->PersistentNotificationStorage = OnvifUtil::BoolValue(soapObj, false);

        return _capabilities;
    }

    soap_dom_element EventBindingServiceImpl::createMotionAlarmDescription(struct soap* soap)
    {
        tt__MessageDescription * _MessageDescription_MotionAlarm = soap_new_tt__MessageDescription(soap);
        _MessageDescription_MotionAlarm->IsProperty = OnvifUtil::BoolValue(soap, true);

        _MessageDescription_MotionAlarm->Source = soap_new_tt__ItemListDescription(soap);

        simple_item_description_type sourceDescr;
        sourceDescr.Name = "Source";
        sourceDescr.Type = "tt:ReferenceToken";
        _MessageDescription_MotionAlarm->Source->SimpleItemDescription.push_back(sourceDescr);

        _MessageDescription_MotionAlarm->Data = soap_new_tt__ItemListDescription(soap);

        simple_item_description_type elemDescr;
        elemDescr.Name = "State";
        elemDescr.Type = "xsd:boolean";
        _MessageDescription_MotionAlarm->Data->SimpleItemDescription.push_back(elemDescr);

        soap_dom_element msgDescr(soap);
        msgDescr.set(_MessageDescription_MotionAlarm, SOAP_TYPE_tt__MessageDescription);
        msgDescr.name = "tt:MessageDescription";

        soap_dom_element evtDescrMotionAlarmEl = EventsBuildUtil::AddHolder(soap, msgDescr, "MotionAlarm", true);
        soap_dom_element evtDescrVideoSourceEl = EventsBuildUtil::AddHolder(soap, evtDescrMotionAlarmEl, "tns1:VideoSource", false);

        return evtDescrVideoSourceEl;
    }

    soap_dom_element EventBindingServiceImpl::createLongInZoneDescription(struct soap* soap)
    {
        tt__MessageDescription * _MessageDescription_MotionAlarm = soap_new_tt__MessageDescription(soap);
        _MessageDescription_MotionAlarm->IsProperty = OnvifUtil::BoolValue(soap, true);

        _MessageDescription_MotionAlarm->Source = soap_new_tt__ItemListDescription(soap);

        simple_item_description_type sourceDescr;
        sourceDescr.Name = "Source";
        sourceDescr.Type = "tt:ReferenceToken";
        _MessageDescription_MotionAlarm->Source->SimpleItemDescription.push_back(sourceDescr);

        _MessageDescription_MotionAlarm->Data = soap_new_tt__ItemListDescription(soap);

        simple_item_description_type elemDescr;
        elemDescr.Name = "State";
        elemDescr.Type = "xsd:boolean";
        _MessageDescription_MotionAlarm->Data->SimpleItemDescription.push_back(elemDescr);

        simple_item_description_type objIdDescr;
        objIdDescr.Name = "ObjectID";
        objIdDescr.Type = "xsd:string";
        _MessageDescription_MotionAlarm->Data->SimpleItemDescription.push_back(objIdDescr);

        soap_dom_element msgDescr(soap);
        msgDescr.set(_MessageDescription_MotionAlarm, SOAP_TYPE_tt__MessageDescription);
        msgDescr.name = "tt:MessageDescription";

        soap_dom_element evtDescrMotionAlarmEl = EventsBuildUtil::AddHolder(soap, msgDescr, "longInZone", true);
        soap_dom_element evtDescrVideoSourceEl = EventsBuildUtil::AddHolder(soap, evtDescrMotionAlarmEl, "tns1:VideoSource", false);

        return evtDescrVideoSourceEl;
    }

    soap_dom_element EventBindingServiceImpl::createLostObjectDescription(struct soap* soap)
    {
        tt__MessageDescription * _MessageDescription_MotionAlarm = soap_new_tt__MessageDescription(soap);
        _MessageDescription_MotionAlarm->IsProperty = OnvifUtil::BoolValue(soap, true);

        _MessageDescription_MotionAlarm->Source = soap_new_tt__ItemListDescription(soap);

        simple_item_description_type sourceDescr;
        sourceDescr.Name = "Source";
        sourceDescr.Type = "tt:ReferenceToken";
        _MessageDescription_MotionAlarm->Source->SimpleItemDescription.push_back(sourceDescr);

        soap_dom_element msgDescr(soap);
        msgDescr.set(_MessageDescription_MotionAlarm, SOAP_TYPE_tt__MessageDescription);
        msgDescr.name = "tt:MessageDescription";

        soap_dom_element evtDescrMotionAlarmEl = EventsBuildUtil::AddHolder(soap, msgDescr, "lostObject", true);
        soap_dom_element evtDescrVideoSourceEl = EventsBuildUtil::AddHolder(soap, evtDescrMotionAlarmEl, "tns1:VideoSource", false);

        return evtDescrVideoSourceEl;
    }

    int EventBindingServiceImpl::AddEventBroker(
        _tev__AddEventBroker *tev__AddEventBroker,
        _tev__AddEventBrokerResponse &tev__AddEventBrokerResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
        {
            return auth_result;
        }

        OnvifUtil::ONVIF_Fault(soap, "ter:Action", "ter:TooManyEventBrokers");
        return SOAP_FAULT;
    }

    int EventBindingServiceImpl::DeleteEventBroker(
        _tev__DeleteEventBroker *tev__DeleteEventBroker,
        _tev__DeleteEventBrokerResponse &tev__DeleteEventBrokerResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::WRITE_SYSTEM))
        {
            return auth_result;
        }

        OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidAddress");
        return SOAP_FAULT;
    }

    int EventBindingServiceImpl::GetEventBrokers(
        _tev__GetEventBrokers *tev__GetEventBrokers,
        _tev__GetEventBrokersResponse &tev__GetEventBrokersResponse)
    {
        if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::READ_MEDIA))
        {
            return auth_result;
        }

        OnvifUtil::ONVIF_Fault(soap, "ter:InvalidArgVal", "ter:InvalidAddress");
        return SOAP_FAULT;
    }
}

