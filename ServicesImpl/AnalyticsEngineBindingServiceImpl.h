#pragma once

#include "onvif_generated_AnalyticsEngineBindingService.h"
#include <Interface/IServiceImpl.h>
#include <SoapProxy/SoapUtil.h>

namespace onvif_generated_ {
	class AnalyticsEngineBindingServiceImpl : public AnalyticsEngineBindingService, public IServiceImpl
	{
	public:
        AnalyticsEngineBindingServiceImpl(SoapWrapperSP soapObj, const IntellectDataSP& data);

        AnalyticsEngineBindingService* copy() override { return new AnalyticsEngineBindingServiceImpl(*this); }

        int GetServiceCapabilities(_tanl2__GetServiceCapabilities* tanl2__GetServiceCapabilities, _tanl2__GetServiceCapabilitiesResponse& tanl2__GetServiceCapabilitiesResponse) override;
		int GetSupportedAnalyticsModules(_tanl2__GetSupportedAnalyticsModules* tanl2__GetSupportedAnalyticsModules, _tanl2__GetSupportedAnalyticsModulesResponse& tanl2__GetSupportedAnalyticsModulesResponse) override;
	    int CreateAnalyticsModules(_tanl2__CreateAnalyticsModules* tanl2__CreateAnalyticsModules, _tanl2__CreateAnalyticsModulesResponse& tanl2__CreateAnalyticsModulesResponse) override { return defaultImplementation(); }
	    int DeleteAnalyticsModules(_tanl2__DeleteAnalyticsModules* tanl2__DeleteAnalyticsModules, _tanl2__DeleteAnalyticsModulesResponse& tanl2__DeleteAnalyticsModulesResponse) override { return defaultImplementation(); }
		int GetAnalyticsModules(_tanl2__GetAnalyticsModules* tanl2__GetAnalyticsModules, _tanl2__GetAnalyticsModulesResponse& tanl2__GetAnalyticsModulesResponse) override;
		int GetAnalyticsModuleOptions(_tanl2__GetAnalyticsModuleOptions *tanl2__GetAnalyticsModuleOptions, _tanl2__GetAnalyticsModuleOptionsResponse &tanl2__GetAnalyticsModuleOptionsResponse) override { return defaultImplementation(); }
	    int ModifyAnalyticsModules(_tanl2__ModifyAnalyticsModules* tanl2__ModifyAnalyticsModules, _tanl2__ModifyAnalyticsModulesResponse& tanl2__ModifyAnalyticsModulesResponse) override { return defaultImplementation(); }

        //IServiceImpl
	    int Dispatch(struct soap* soapPtr) override
        {
            return AnalyticsEngineBindingService::dispatch(soapPtr);
        }

        static tanl2__Capabilities* GetServiceCapabilities(struct soap* soapObj);

        static std::string GetUri()
        {
            return "/analytics_service";
        }

	private:
        int defaultImplementation() override
        {
            if (auto auth_result = GetSecurity()->CheckAuthentication(this->soap, ISecurityServer::PRE_AUTH))
            {
                return auth_result;
            }

			OnvifUtil::ONVIF_Fault(this->soap, "ter:ActionNotSupported", "ter:NotImplemented", "This optinal method is not implemented", true);
            return SOAP_FAULT;
        }

      public:
        int GetSupportedMetadata(
            _tanl2__GetSupportedMetadata *tanl2__GetSupportedMetadata,
            _tanl2__GetSupportedMetadataResponse &
            tanl2__GetSupportedMetadataResponse) override;
	};
}
