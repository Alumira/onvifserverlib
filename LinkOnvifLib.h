#pragma once

#ifdef _MSC_VER
# ifdef _DEBUG
#  pragma comment(lib, "ONVIFServerLib-D.lib")
# else
#  pragma comment(lib, "ONVIFServerLib.lib")
# endif    
#endif
