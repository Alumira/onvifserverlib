#include "Discovery/wsaapi.h"
#include "Discovery/wsddapi.h"

#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include <boost/thread/locks.hpp>

#include "DiscoveryService.h"

DiscoveryService::DiscoveryService(std::string ip, std::string nat_ip, std::string port, std::string uuid, const std::vector<std::string>& newScopes) :
			m_oldFlags(0), m_stopped(false), m_scopes(newScopes)
{
    soapObj1 = soap_new();

    auto service_ip = !nat_ip.empty() ? nat_ip : ip;

    m_serviceURI = "http://" + service_ip + ":" + port + "/onvif/device_service";
    m_localInterface = ip;
    m_GUID = uuid; // soap_wsa_rand_uuid(soapObj1);
    
    m_multicastAddr = "239.255.255.250";
    udpPort = 3702;

    localInterface.s_addr = inet_addr(m_localInterface.c_str());
}

bool DiscoveryService::InitSoap()
{
    soap_init1(soapObj1, SOAP_IO_UDP);
    soap_register_plugin(soapObj1, soap_wsa);
    soap_set_imode(soapObj1, SOAP_XML_IGNORENS | SOAP_IO_FLUSH);
    soap_set_omode(soapObj1, SOAP_IO_DEFAULT);

	/*
	* Set local interface for outbound multicast datagrams.
	* The IP address specified must be associated with a local,
	* multicast-capable interface.
	*/

	soapObj1->ipv4_multicast_if = reinterpret_cast<char *>(&localInterface);
	soapObj1->ipv4_multicast_ttl = 20; 

    return true;
}

const std::string& DiscoveryService::GetDeviceURI()
{
    return m_serviceURI;
}

bool DiscoveryService::OpenUdpPortForReading() const
{
    soapObj1->connect_flags = m_oldFlags;
    soapObj1->bind_flags |= SO_REUSEADDR;
    if (!soap_valid_socket(soap_bind(soapObj1, NULL, udpPort, 0)))
    {
        return false; 
    }

    struct ip_mreq groupMulticast;
    memset(&groupMulticast, 0, sizeof(groupMulticast));
    groupMulticast.imr_multiaddr.s_addr = inet_addr(m_multicastAddr.c_str());

    groupMulticast.imr_interface.s_addr = localInterface.s_addr;
    if (setsockopt(soapObj1->master, IPPROTO_IP, IP_ADD_MEMBERSHIP, reinterpret_cast<char *>(&groupMulticast), sizeof(groupMulticast)))
    {
        return false; 
    }

    return true;
}

void DiscoveryService::Run()
{
    th.reset(new boost::thread(&DiscoveryService::process, this));
}

void DiscoveryService::process()
{
    while (!m_stopped)
    {
		auto tsoap = soap_copy(soapObj1);

    	soap_wsdd_listen(tsoap, 5);

		soap_destroy(tsoap);
		soap_end(tsoap);
		soap_free(tsoap);
    }
}

std::string DiscoveryService::ConvertScopes(const std::vector<std::string>& scopesVector)
{
	std::string scopes;
	BOOST_FOREACH(auto scope, scopesVector)
	{
		scopes += scope + " ";
	}

	return scopes;
}

std::string DiscoveryService::GetScopes() const
{
	boost::unique_lock<boost::shared_mutex> l(m_scopes_Lock);
	return ConvertScopes(m_scopes);
}

bool DiscoveryService::CheckScopes(std::string scopes) const
{
	std::vector<std::string> strs;
	boost::split(strs, scopes, boost::is_any_of(" "));

	if(strs.empty())
	{
		return true;
	}

	boost::unique_lock<boost::shared_mutex> l(m_scopes_Lock);
	BOOST_FOREACH(auto findscope, strs)
	{
		if (std::find_if(m_scopes.begin(), m_scopes.end(), [&](const std::string& sc) { return sc.find(findscope) != std::string::npos; }) != m_scopes.end())
		{
			return true;
		}
	}

	return false;
}

DiscoveryService::~DiscoveryService()
{
}

void DiscoveryService::InitNamespaces() const
{
    static const struct Namespace namespaces[] = {
        { "SOAP-ENV", "http://www.w3.org/2003/05/soap-envelope", NULL, NULL },
        { "SOAP-ENC", "http://www.w3.org/2003/05/soap-encoding", "http://schemas.xmlsoap.org/soap/encoding/", NULL },
        { "xsi", "http://www.w3.org/2001/XMLSchema-instance", "http://www.w3.org/*/XMLSchema-instance", NULL },
        { "xsd", "http://www.w3.org/2001/XMLSchema", "http://www.w3.org/*/XMLSchema", NULL },
        { "wsa", "http://schemas.xmlsoap.org/ws/2004/08/addressing", NULL, NULL },
        { "wsdd", "http://schemas.xmlsoap.org/ws/2005/04/discovery", NULL, NULL },
        { "wsdp", "http://schemas.xmlsoap.org/ws/2006/02/devprof", NULL, NULL },
        { "dn", "http://www.onvif.org/ver10/network/wsdl", NULL, NULL },
        { "tds", "http://www.onvif.org/ver10/device/wsdl", NULL, NULL },
        { NULL, NULL, NULL, NULL }
    };
    soap_set_namespaces(soapObj1, namespaces);
}

std::string DiscoveryService::getUdpURI() const
{
    std::string uri = (boost::format("soap.udp://%1%:%2%") % m_multicastAddr % udpPort).str();
    return uri;
}

void DiscoveryService::Stop()
{
    m_stopped = true;

    if (th)
    {
        th->join();
    }

	soap_destroy(soapObj1);
	soap_end(soapObj1);
	soap_free(soapObj1);
}

void DiscoveryService::SetScopes(const std::vector<std::string>& newScopes)
{
	{
		boost::unique_lock<boost::shared_mutex> l(m_scopes_Lock);

		m_scopes.clear();
		m_scopes = newScopes;
	}

	SendHello(newScopes);
}

bool DiscoveryService::Running()
{
    return m_stopped;
}

bool DiscoveryService::SendHello(const std::vector<std::string>& newScopes)
{
    m_oldFlags = soapObj1->connect_flags;
    soapObj1->connect_flags |= SO_BROADCAST;
    
    for (int i = 0; i < 3; i++)
    {
        if (soap_wsdd_Hello(soapObj1, SOAP_WSDD_ADHOC, getUdpURI().c_str(),
            soap_wsa_rand_uuid(soapObj1),
            NULL,
            soap_strdup(soapObj1, m_GUID.c_str()),
            soap_strdup(soapObj1, GetTypes().c_str()),
            soap_strdup(soapObj1, ConvertScopes(newScopes).c_str()),
            NULL,
            soap_strdup(soapObj1, m_serviceURI.c_str()),
            1) != SOAP_OK)
        {
            return false; 
        }

		//Sleep(100);
    }

    return true;
}

