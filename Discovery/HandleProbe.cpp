#include <Discovery/wsddapi.h>
#include "DiscoveryService.h"

extern DiscoveryServiceSP _gDeviceParams;

	void wsdd_event_Hello(struct soap *soap, unsigned int InstanceId, const char *SequenceId, unsigned int MessageNumber, const char *MessageID, const char *RelatesTo, const char *EndpointReference, const char *Types, const char *Scopes, const char *MatchBy, const char *XAddrs, unsigned int MetadataVersion)
	{
		
	}

	void wsdd_event_Bye(struct soap *soap, unsigned int InstanceId, const char *SequenceId, unsigned int MessageNumber, const char *MessageID, const char *RelatesTo, const char *EndpointReference, const char *Types, const char *Scopes, const char *MatchBy, const char *XAddrs, unsigned int *MetadataVersion)
	{
		
	}

	soap_wsdd_mode wsdd_event_Probe(struct soap *soap, const char *MessageID, const char *ReplyTo, const char *Types, const char *Scopes, const char *MatchBy, struct wsdd__ProbeMatchesType *matches)
	{
    	//LLOG("wsdd_event_Probe\tid=%s replyTo=%s types=%s scopes=%s\n", MessageID, ReplyTo, Types, Scopes);
		soap_wsdd_init_ProbeMatches(soap, matches);
        std::string _types = _gDeviceParams->GetTypes();
        std::string _scopes = _gDeviceParams->GetScopes();

		soap_wsdd_add_ProbeMatch(soap, matches, 
		                    soap_strdup(soap, _gDeviceParams->GetGuid().c_str()),
		                    soap_strdup(soap, _types.c_str()),
		                    soap_strdup(soap, _scopes.c_str()),
		                    NULL, 
		                    soap_strdup(soap, _gDeviceParams->GetDeviceURI().c_str()),
		                    1);

		std::string searchScopes(Scopes ? Scopes : "");
		std::string types(Types ? Types : "");

		if((searchScopes.empty() && types.empty()) || _gDeviceParams->CheckScopes(searchScopes))
		{
			if (soap_wsdd_ProbeMatches(soap, nullptr, soap_wsa_rand_uuid(soap), MessageID, ReplyTo, matches) != SOAP_OK)
			{
				soap_print_fault(soap, stderr);
			}
		}

		return SOAP_WSDD_ADHOC;
	}
	
	void wsdd_event_ProbeMatches(struct soap *soap, unsigned int InstanceId, const char *SequenceId, unsigned int MessageNumber, const char *MessageID, const char *RelatesTo, struct wsdd__ProbeMatchesType *matches)
	{
	}

	soap_wsdd_mode wsdd_event_Resolve(struct soap *soap, const char *MessageID, const char *ReplyTo, const char *EndpointReference, struct wsdd__ResolveMatchType *match)
	{
		return SOAP_WSDD_ADHOC;
	}

	void wsdd_event_ResolveMatches(struct soap *soap, unsigned int InstanceId, const char *SequenceId, unsigned int MessageNumber, const char *MessageID, const char *RelatesTo, struct wsdd__ResolveMatchType *match)
	{
		
	}

	SOAP_FMAC5 int SOAP_FMAC6 SOAP_ENV__Fault(struct soap *pSoap, char *faultcode, char *faultstring, char *faultactor, struct SOAP_ENV__Detail *detail, struct SOAP_ENV__Code *SOAP_ENV__Code, struct SOAP_ENV__Reason *SOAP_ENV__Reason, char *SOAP_ENV__Node, char *SOAP_ENV__Role, struct SOAP_ENV__Detail *SOAP_ENV__Detail)
	{
		return 0;
	}
