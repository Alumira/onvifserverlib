#include <Discovery/soapH.h>
#include "Discovery/wsaapi.h"
#include "Discovery/wsddapi.h"
#include <boost/thread/thread.hpp>
#include "DiscoveryService.h"

#define DISCOVERY_API extern "C" __declspec(dllexport)

DiscoveryServiceSP _gDeviceParams;

void disovery_init(std::string ip, std::string nat_ip, std::string port, std::string uuid, std::vector<std::string> newScopes)
{
    if(!_gDeviceParams)
    {
        _gDeviceParams.reset(new DiscoveryService(ip, nat_ip, port, uuid, newScopes));
    }
    
    if(_gDeviceParams->Running())
    {
        return;
    }

    _gDeviceParams->InitSoap();
    _gDeviceParams->InitNamespaces();
    
	if(!_gDeviceParams->SendHello(newScopes))
	{
        return;
	}

    if(!_gDeviceParams->OpenUdpPortForReading())
    {
        return;
    }

    _gDeviceParams->Run();
}

void disovery_stop()
{
    if(_gDeviceParams)
    {
        _gDeviceParams->Stop();
		_gDeviceParams.reset();
    }
}

void disovery_setScopes(const std::vector<std::string>& newScopes)
{
	if (_gDeviceParams)
	{
		_gDeviceParams->SetScopes(newScopes);
	}
}

DISCOVERY_API void InitDiscovery(std::string ip, std::string nat_ip, std::string port, std::string uuid, std::vector<std::string> newScopes)
{
	disovery_init(ip, nat_ip, port, uuid, newScopes);
}

DISCOVERY_API void StopDiscovery()
{
	disovery_stop();
}

DISCOVERY_API void SetDiscoveryScopes(const std::vector<std::string>& newScopes)
{
	disovery_setScopes(newScopes);
}
