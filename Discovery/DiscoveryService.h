#pragma once

#include <vector>
#include <inaddr.h>
#include <boost/thread/thread.hpp>

class DiscoveryService
{
public:
    DiscoveryService(std::string ip, std::string nat_ip, std::string port, std::string uuid, const std::vector<std::string>& newScopes);
    bool InitSoap();
    bool OpenUdpPortForReading() const;
    void Run();
    void process();
	static std::string ConvertScopes(const std::vector<std::string>& scopesVector);

	std::string GetScopes() const;

    static std::string GetTypes()
    {
        return "\"http://www.onvif.org/ver10/network/wsdl\":NetworkVideoTransmitter " "\"http://www.onvif.org/ver10/device/wsdl\":Device";
    }

    std::string GetGuid() const
    {
        return m_GUID;
    }

    const std::string& GetDeviceURI();
    void Stop();
	void SetScopes(const std::vector<std::string>& newScopes);
	bool Running();

    void InitNamespaces() const;
    std::string getUdpURI() const;
    bool SendHello(const std::vector<std::string>& newScopes);
    ~DiscoveryService();

	bool CheckScopes(std::string scopes) const;

private:
    std::string m_GUID;
    struct soap *soapObj1;
    std::string m_serviceURI;
    std::string m_multicastAddr;
    int udpPort;
    std::string m_localInterface;
    struct in_addr localInterface;
	std::vector<std::string> m_scopes;

    int m_oldFlags;
    mutable bool m_stopped;

    boost::shared_ptr<boost::thread> th;
	mutable boost::shared_mutex m_scopes_Lock;
};

typedef boost::shared_ptr<DiscoveryService> DiscoveryServiceSP;