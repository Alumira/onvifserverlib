#pragma once
#include "onvif_generated_Stub.h"
#include <Configuration/VideoConfigurationTypes.h>
#include <Configuration/PtzConfigurationTypes.h>
#include <Configuration/RecordingConfigurationTypes.h>
#include <Configuration/MediaProfile.h>
#include <Configuration/IOConfigurationTypes.h>
#include <Configuration/UserConfiguration.h>
#include "StructDefines.h"

class SoapProxy
{
public:
    SoapProxy()
    {
    }

	static const std::string SchemaProfileUrl;

	static tt__VideoSourceConfiguration* createVideoSourceConfiguration(soap* soap, VideoSourceConfigurationSP vSource);
	static tt__VideoEncoderConfiguration* createVideoEncoderConfiguration(soap* soap, EncoderConfigurationSP vSource);
    static tt__Profile * getProfile(soap* soap, const MediaProfileSP& vProfile);
	static tt__VideoSource* createVideoSource(soap* soap, VideoSourceSP vSource, VideoSourceConfigurationSP vSourceConfig);
	static tt__PTZConfiguration* createPtzConfiguration(soap* soap, PtzConfigurationSP ptzConf);
	static tt__PTZNode* createPtzNode(soap* soap, PtzConfigurationSP ptzConf);
	static tt__Space2DDescription* CreateSpace2D(soap* soap, const std::string& spaceName);
	static tt__Space1DDescription* CreateSpace1D(soap* soap, const std::string& spaceName);
    tt__GetRecordingsResponseItem* createRecordingItemInfo(soap* soap, const RecordingConfigurationSP& recordingConfig) const;
	static tt__RecordingConfiguration* createRecordingConfiguration(soap* soap, const RecordingConfigurationSP& recordingConfig);
    tt__RecordingInformation* createRecordingInformation(soap* soap, const RecordingConfigurationSP& vSourceConfig) const;
	static tt__TrackConfiguration* createTrackConfiguration(soap* soap, RecordingTrackSP trackConfig, RecordingConfigurationSP recordingConfig);
	static tt__RecordingSourceInformation* CreateRecordingSourceInformation(soap* soap, RecordingConfigurationSP vSource);
    void fillResolutions(soap* soap, std::vector<tt__VideoResolution*>& resolutionsAvailableResp, const ResolutionInfo& resolutions) const;
    void fillResolutions2(soap* soap, std::vector<tt__VideoResolution2*>& resolutionsAvailableResp, const ResolutionInfo& resolutions) const;
	static std::vector<RecordingTrackType> GetTrackTypes(const std::string* RecordingInformationFilter);
	static tt__RecordingJobConfiguration* CreateRecordingJobConfiguration(soap* soap, const RecordingJobSP& job);
	static tt__GetRecordingJobsResponseItem* CreateRecordingJobConfigurationItem(soap* soap, const RecordingJobSP& job);
	static tt__VideoEncoder2Configuration* createVideoEncoder2Configuration(soap* soap, const EncoderConfigurationSP& shared);
    static std::string convertToMedia2EncodingName(soap* soap, const std::string& internalEncodingName);
    static tt__AudioEncoder2Configuration* createAudioEncoder2Configuration(soap* soap, const AudioEncoderConfigurationSP& aEncoder);
	static bool ContainsType(const std::vector<std::string>& types, const std::string& type);
	static trt2__MediaProfile* getProfile2(soap* soap, const MediaProfileSP& profile, const std::vector<std::string>& types);
	static tt__ImagingSettings20* CreateImagingSettings20(soap* soap, const VideoSourceSP& vSource);
	static tt__ImagingSettings* CreateImagingSettings(soap* soap, const VideoSourceSP& vSource);
	static tt__AudioSource* createAudioSource(soap* soap, const AudioSourceSP& src, const AudioSourceConfigurationSP& sSource);
	static tt__AudioSourceConfiguration* createAudioSourceConfiguration(soap* soap, const AudioSourceConfigurationSP& src);
	static tt__AudioEncoderConfiguration* createAudioEncoderConfiguration(soap* soap, const AudioEncoderConfigurationSP& aEncoder);
	static void addSimpleItem(std::vector<simple_item_type>* parameters, const std::string& itemName, const std::string& itemValue);
	static void addSimpleItemDescription(std::vector<simple_item_description_type>* parameters, const std::string& itemName,
	                              const std::string& type);
	static tt__Config* createVideoAnalyticsModule(soap* soap, const TrackerModuleSP& module);
	static tt__ConfigDescription* createVideoAnalyticsModuleDescription(soap* soap, const TrackerModuleSP& module);
	static tt__VideoAnalyticsConfiguration* createVideoAnalyticsConfiguration(soap* soap, const VideoAnalyticsConfigurationSP& vAnalyticsConfiguration);
	static tt__MetadataConfiguration* createMetadataConfiguration(soap* soap, const MetadataConfigurationSP& config);
	static tt__RelayOutput* createRelayOutput(soap* soap, const RelayOutputSP& relayConf);
	static tt__DigitalInput* createRayOutput(soap* soap, const RayOutputSP& rayConf);
	static tt__RecordingJobStateInformation * createRecordingJobStateInformation(soap* soap, const RecordingJobSP& recordingJob);
	static timeval* convertTime(soap* soap, time_t time);
	static time_t GetCurTime();

	static UserLevel GetUserLevel(tt__UserLevel level);
	static tt__UserLevel CreateUserLevel(UserLevel level);

	bool CheckAudioSampleRate(AudioEncoderConfigurationSP aEncoderConfig, int _samplerate) const;
	tt__AudioEncoderConfigurationOption* CreateAudioEncoderConfigurationOption(soap* soap, AudioEncoderConfigurationSP aEncoderConfig) const;
	tt__AudioEncoder2ConfigurationOptions* CreateAudioEncoder2ConfigurationOption(soap* soap, AudioEncoderConfigurationSP aEncoderConfig) const;
};

typedef std::shared_ptr<SoapProxy> SoapProxySP;
