#pragma once
#include <string>
#include "BackgroundServices/EventsSubscriptionTask.h"

class EventsBuildUtil
{
public:
    static const std::string TopicExpressionConcreteDialectUri;
    static const std::string TopicExpressionConcreteSetDialectUri;
    static const std::string MessageContentDialectUri;

    static soap_dom_element AddHolder(struct soap* soap, soap_dom_element& child, const char* holderName, bool isTopic)
	{
		auto holder = soap_new_xsd__anyType(soap); 
		holder->add(child);

		if (isTopic)
		{
            auto attributes = soap_new_xsd__anyAttribute(soap);
            attributes->name = "wstop:topic";
            attributes->text = soap_bool2s(soap, true);

            holder->adds(attributes);
		}

		soap_dom_element holderEl(soap);
		holderEl.set(holder, SOAP_TYPE_xsd__anyType);
		holderEl.name = holderName;

		return holderEl;
	}

    static MessageHolderTypeSP CreateMessageEvent(soap* soapObj, const std::string& topic, const std::string& producer, VideoSourceSP vSource, _tt__Message * ttMsg)
    {
        auto msg = soap_new_wsnt__NotificationMessageHolderType(soapObj);

        msg->Topic = soap_new_wsnt__TopicExpressionType(soapObj);
        msg->Topic->__any = topic;
        msg->Topic->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;

        //ODM fails to parse messages with ProducerReference
            //msg->ProducerReference = soap_new_wsa5__EndpointReferenceType(soapObj);
            //msg->ProducerReference->Address = soap_strdup(soapObj, producer.c_str());

        msg->Message.__any.set(ttMsg, SOAP_TYPE__tt__Message);

        return std::make_shared<MessageHolderType>(msg, vSource);
    }
};

