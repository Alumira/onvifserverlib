#include "EventsBuildUtil.h"

const std::string EventsBuildUtil::TopicExpressionConcreteDialectUri = "http://docs.oasis-open.org/wsn/t-1/TopicExpression/Concrete";
const std::string EventsBuildUtil::TopicExpressionConcreteSetDialectUri = "http://www.onvif.org/ver10/tev/topicExpression/ConcreteSet";
const std::string EventsBuildUtil::MessageContentDialectUri = "http://www.onvif.org/ver10/tev/messageContentFilter/ItemFilter";