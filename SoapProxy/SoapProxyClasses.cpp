#include "SoapProxyClasses.h"
#include "onvif_generated_H.h"
#include <boost/foreach.hpp>
#include "SoapUtil.h"
#include <regex>
#include <boost/chrono/duration.hpp>
#include <boost/chrono/system_clocks.hpp>
#include "EventsBuildUtil.h"
#include "StructDefines.h"

const std::string SoapProxy::SchemaProfileUrl = "http://www.onvif.org/ver10/schema/Profile";

tt__VideoSourceConfiguration* SoapProxy::createVideoSourceConfiguration(soap* soap, VideoSourceConfigurationSP video_src)
{
    auto videoSourceConfiguration = soap_new_tt__VideoSourceConfiguration(soap);

    videoSourceConfiguration->token = video_src->GetToken();
    videoSourceConfiguration->SourceToken = video_src->videoSource->GetToken();
    videoSourceConfiguration->Bounds = soap_new_req_tt__IntRectangle(soap, 0, 0, video_src->videoSource->GetWidth(), video_src->videoSource->GetHeight());

    videoSourceConfiguration->UseCount = video_src->GetUseCount();
    videoSourceConfiguration->Name = video_src->videoSource->camName;

	videoSourceConfiguration->Extension = soap_new_tt__VideoSourceConfigurationExtension(soap);

    return videoSourceConfiguration;
}

tt__VideoEncoderConfiguration* SoapProxy::createVideoEncoderConfiguration(soap* soap, EncoderConfigurationSP vEncoder)
{
    tt__VideoEncoderConfiguration* cfg = soap_new_tt__VideoEncoderConfiguration(soap);
    if(vEncoder->m_resolutionsInfo.CurrentResolution())
    {
        cfg->Resolution = soap_new_req_tt__VideoResolution(soap, vEncoder->m_resolutionsInfo.CurrentResolution()->width, vEncoder->m_resolutionsInfo.CurrentResolution()->height);
    }

    cfg->Name = vEncoder->GetName();

    cfg->Multicast = soap_new_tt__MulticastConfiguration(soap);
    cfg->Multicast->Address = soap_new_tt__IPAddress(soap);
    cfg->Multicast->Address->IPv4Address = soap_new_std__string(soap);

    auto multicastParams = vEncoder->multicastParams;
    cfg->Multicast->TTL = 10000;
    cfg->Multicast->AutoStart = false;

    if(multicastParams)
    {
        *cfg->Multicast->Address->IPv4Address = multicastParams->GetDestinationIp();
        cfg->Multicast->Port = multicastParams->GetRealPort();
    }

    //cfg->SessionTimeout = "PT10S";

    if(vEncoder->codec == "H264")
    {
        cfg->Encoding = tt__VideoEncoding__H264;
        cfg->H264 = soap_new_tt__H264Configuration(soap);
        cfg->H264->H264Profile = tt__H264Profile__Main;
		cfg->H264->GovLength = vEncoder->GetStreamStaticParams()->GetGovLength();
    }

    if(vEncoder->codec == "MPEG4")
    {
        cfg->Encoding = tt__VideoEncoding__MPEG4;
        cfg->MPEG4 = soap_new_tt__Mpeg4Configuration(soap);
        cfg->MPEG4->Mpeg4Profile = tt__Mpeg4Profile__SP;
		cfg->MPEG4->GovLength = vEncoder->GetStreamStaticParams()->GetGovLength();
    }

    if (vEncoder->codec == "MJPEG")
    {
        cfg->Encoding = tt__VideoEncoding__JPEG;
    }

    cfg->RateControl = soap_new_tt__VideoRateControl(soap);
    cfg->RateControl->FrameRateLimit = vEncoder->GetStreamStaticParams()->GetFrameRateLimit();
    cfg->RateControl->BitrateLimit = vEncoder->GetBitrateLimit();
	cfg->RateControl->EncodingInterval = EncodingIntervalDefault;

    cfg->token = vEncoder->GetToken();
    cfg->UseCount = vEncoder->GetUseCount();

    cfg->Quality = vEncoder->GetStreamStaticParams()->GetQuality();

    return cfg;
}

tt__Profile * SoapProxy::getProfile(soap* soap, const MediaProfileSP& profile)
{
	auto soapProfile = soap_new_tt__Profile(soap);

	soapProfile->Name = profile->GetName();
	soapProfile->token = profile->GetToken();

	if (profile->GetVideoSourceConfiguration())
	{
		soapProfile->VideoSourceConfiguration = createVideoSourceConfiguration(soap, profile->GetVideoSourceConfiguration());

		if (profile->GetEncoderConfiguration() && profile->GetEncoderConfiguration()->IsSet())
		{
			soapProfile->VideoEncoderConfiguration = createVideoEncoderConfiguration(soap, profile->GetEncoderConfiguration());
		}
	}
	if (profile->GetAudioSourceConfiguration())
	{
		soapProfile->AudioSourceConfiguration = createAudioSourceConfiguration(soap, profile->GetAudioSourceConfiguration());

		if (profile->GetAudioEncoderConfiguration())
		{
			soapProfile->AudioEncoderConfiguration = createAudioEncoderConfiguration(soap, profile->GetAudioEncoderConfiguration());
		}
	}
	soapProfile->fixed = OnvifUtil::BoolValue(soap, false);

	if (profile->GetPtzConfiguration())
	{
		soapProfile->PTZConfiguration = createPtzConfiguration(soap, profile->GetPtzConfiguration());
	}

	if (profile->GetVideoAnalyticsConfiguration())
	{
		soapProfile->VideoAnalyticsConfiguration = createVideoAnalyticsConfiguration(soap, profile->GetVideoAnalyticsConfiguration());
	}

	if (profile->GetMetadataConfiguration())
	{
		soapProfile->MetadataConfiguration = createMetadataConfiguration(soap, profile->GetMetadataConfiguration());
	}

	return soapProfile;
}

tt__VideoSource* SoapProxy::createVideoSource(soap* soap, VideoSourceSP vSource, VideoSourceConfigurationSP vSourceConfig)
{
    tt__VideoSource* _tVideoSource = soap_new_tt__VideoSource(soap);
    _tVideoSource->token = vSource->GetToken();
    _tVideoSource->Resolution = soap_new_req_tt__VideoResolution(soap, vSource->GetWidth(), vSource->GetHeight());

    if(vSourceConfig->getPtzConfig())
    {
        _tVideoSource->Imaging = CreateImagingSettings(soap, vSource);
    }

    return _tVideoSource;
}

tt__PTZConfiguration* SoapProxy::createPtzConfiguration(soap* soap, PtzConfigurationSP ptzConf)
{
    auto conf = soap_new_tt__PTZConfiguration(soap);
    conf->NodeToken = ptzConf->GetToken();
    conf->token = ptzConf->GetConfigurationToken();
    conf->Name = ptzConf->GetName();
    conf->UseCount = ptzConf->GetUseCount();

    if (ptzConf->IsAbsoluteCoordsSupported())
    {
        conf->DefaultAbsolutePantTiltPositionSpace = OnvifUtil::StringValue(soap, "http://www.onvif.org/ver10/tptz/PanTiltSpaces/PositionGenericSpace");
        conf->DefaultAbsoluteZoomPositionSpace = OnvifUtil::StringValue(soap, "http://www.onvif.org/ver10/tptz/ZoomSpaces/PositionGenericSpace");
    }

    conf->DefaultRelativePanTiltTranslationSpace = OnvifUtil::StringValue(soap, "http://www.onvif.org/ver10/tptz/PanTiltSpaces/TranslationGenericSpace");
    conf->DefaultRelativeZoomTranslationSpace = OnvifUtil::StringValue(soap, "http://www.onvif.org/ver10/tptz/ZoomSpaces/TranslationGenericSpace");

    conf->DefaultContinuousPanTiltVelocitySpace = OnvifUtil::StringValue(soap, "http://www.onvif.org/ver10/tptz/PanTiltSpaces/VelocityGenericSpace");
    conf->DefaultContinuousZoomVelocitySpace = OnvifUtil::StringValue(soap, "http://www.onvif.org/ver10/tptz/ZoomSpaces/VelocityGenericSpace");

    conf->DefaultPTZTimeout = soap_new_xsd__duration(soap);
    *conf->DefaultPTZTimeout = ptzConf->GetPtzTimeout();

    return conf;
}

tt__PTZNode * SoapProxy::createPtzNode(soap* soap, PtzConfigurationSP ptzConf)
{
    tt__PTZNode * node = soap_new_tt__PTZNode(soap);
    node->token = ptzConf->GetToken();

    node->Name = soap_new_std__string(soap);
    node->Name->assign(ptzConf->GetName());

    node->HomeSupported = true;
    node->MaximumNumberOfPresets = 255;
    node->SupportedPTZSpaces = soap_new_tt__PTZSpaces(soap);

    if (ptzConf->IsAbsoluteCoordsSupported())
    {
        node->SupportedPTZSpaces->AbsolutePanTiltPositionSpace.push_back(
            CreateSpace2D(soap, "http://www.onvif.org/ver10/tptz/PanTiltSpaces/PositionGenericSpace"));

        node->SupportedPTZSpaces->AbsoluteZoomPositionSpace.push_back(
            CreateSpace1D(soap, "http://www.onvif.org/ver10/tptz/ZoomSpaces/PositionGenericSpace"));
    }
    
    node->SupportedPTZSpaces->RelativePanTiltTranslationSpace.push_back(
        CreateSpace2D(soap, "http://www.onvif.org/ver10/tptz/PanTiltSpaces/TranslationGenericSpace"));
    node->SupportedPTZSpaces->RelativeZoomTranslationSpace.push_back(
        CreateSpace1D(soap, "http://www.onvif.org/ver10/tptz/ZoomSpaces/TranslationGenericSpace"));

    node->SupportedPTZSpaces->ContinuousPanTiltVelocitySpace.push_back(
        CreateSpace2D(soap, "http://www.onvif.org/ver10/tptz/PanTiltSpaces/VelocityGenericSpace"));
    node->SupportedPTZSpaces->ContinuousZoomVelocitySpace.push_back(
        CreateSpace1D(soap, "http://www.onvif.org/ver10/tptz/ZoomSpaces/VelocityGenericSpace"));

    node->FixedHomePosition = OnvifUtil::BoolValue(soap, true);

    return node;
}

tt__Space2DDescription* SoapProxy::CreateSpace2D(soap* soap, const std::string& spaceName)
{
    auto space = soap_new_tt__Space2DDescription(soap);
    space->XRange = soap_new_tt__FloatRange(soap);
    space->XRange->Min = -1.0;
    space->XRange->Max = 1.0;

    space->YRange = soap_new_tt__FloatRange(soap);
    space->YRange->Min = -1.0;
    space->YRange->Max = 1.0;

    space->URI = spaceName;

    return space;
}

tt__Space1DDescription* SoapProxy::CreateSpace1D(soap* soap, const std::string& spaceName)
{
    auto space = soap_new_tt__Space1DDescription(soap);
    space->XRange = soap_new_tt__FloatRange(soap);
    space->XRange->Min = -1.0;
    space->XRange->Max = 1.0;

    space->URI = spaceName;

    return space;
}

tt__GetRecordingsResponseItem* SoapProxy::createRecordingItemInfo(soap* soap, const RecordingConfigurationSP& recordingConfig) const
{
    tt__GetRecordingsResponseItem* item = soap_new_tt__GetRecordingsResponseItem(soap);
    item->RecordingToken = recordingConfig->GetToken();

    item->Configuration = createRecordingConfiguration(soap, recordingConfig);

    item->Tracks = soap_new_tt__GetTracksResponseList(soap);

    BOOST_FOREACH(auto tr, recordingConfig->GetTracks())
    {
        tt__GetTracksResponseItem * track = soap_new_tt__GetTracksResponseItem(soap);
        track->TrackToken = tr->GetToken();

        track->Configuration = createTrackConfiguration(soap, tr, recordingConfig);
        item->Tracks->Track.push_back(track);
    }

    return item;
}

tt__TrackConfiguration* SoapProxy::createTrackConfiguration(soap* soap, RecordingTrackSP trackConfig, RecordingConfigurationSP recordingConfig)
{
    auto trackConf = soap_new_tt__TrackConfiguration(soap);

    switch (trackConfig->GetType())
    {
    case videoTrackType:
        trackConf->TrackType = tt__TrackType__Video;
        break;
    case audioTrackType:
        trackConf->TrackType = tt__TrackType__Audio;
        break;
    case metadataTrackType:
        trackConf->TrackType = tt__TrackType__Metadata;
        break;
    default:
        trackConf->TrackType = tt__TrackType__Video;
        break;
    }

    trackConf->Description = recordingConfig->GetVSource()->camId;

    return trackConf;
}

tt__RecordingConfiguration* SoapProxy::createRecordingConfiguration(soap* soap, const RecordingConfigurationSP& recordingConfig)
{
    auto recConf = soap_new_tt__RecordingConfiguration(soap);

    recConf->Source = CreateRecordingSourceInformation(soap, recordingConfig);
    recConf->Content = recordingConfig->GetVSource()->camId;
    recConf->MaximumRetentionTime = recordingConfig->GetRetentionTime();

    return recConf;
}

tt__RecordingInformation * SoapProxy::createRecordingInformation(soap* soap, const RecordingConfigurationSP& recordingConfig) const
{
    tt__RecordingInformation * recInfo = soap_new_tt__RecordingInformation(soap);
    recInfo->Content = recordingConfig->GetVSource()->camId;
    recInfo->RecordingToken = recordingConfig->GetToken();

    recInfo->Source = CreateRecordingSourceInformation(soap, recordingConfig);

    recInfo->EarliestRecording = convertTime(soap, recordingConfig->m_startArchTime);
    recInfo->LatestRecording = convertTime(soap, recordingConfig->m_stopArchTime);

    //recInfo->RecordingStatus = tt__RecordingStatus__Stopped;

    BOOST_FOREACH(auto tr, recordingConfig->GetTracks())
    {
        auto track = soap_new_tt__TrackInformation(soap);
        track->TrackToken = tr->GetToken();

        switch (tr->GetType())
        {
        case videoTrackType:
            track->TrackType = tt__TrackType__Video;
            break;
        case audioTrackType:
            track->TrackType = tt__TrackType__Audio;
            break;
        case metadataTrackType:
            track->TrackType = tt__TrackType__Metadata;
            break;
        default:
            track->TrackType = tt__TrackType__Video;
            break;
        }

        track->Description = recordingConfig->GetVSource()->camId;

        track->DataFrom = *SoapProxy::convertTime(soap, recordingConfig->m_startArchTime);
        track->DataTo = *SoapProxy::convertTime(soap, recordingConfig->m_stopArchTime);

        recInfo->Track.push_back(track);
    }
    
    return recInfo;
}

tt__RecordingSourceInformation * SoapProxy::CreateRecordingSourceInformation(soap* soap, RecordingConfigurationSP recConf)
{
    auto recSourceInfo = soap_new_tt__RecordingSourceInformation(soap);
    recSourceInfo->Name = recConf->m_sourceInfo.Name;
    recSourceInfo->SourceId = recConf->m_sourceInfo.SourceId;
    recSourceInfo->Description = recConf->m_sourceInfo.Description;
    recSourceInfo->Location = recConf->m_sourceInfo.Location;
    recSourceInfo->Address = recConf->m_sourceInfo.Address;

    return recSourceInfo;
}

void SoapProxy::fillResolutions(soap* soap, std::vector<tt__VideoResolution*>& resolutionsAvailableResp, const ResolutionInfo& resolutions) const
{
    BOOST_FOREACH(auto resolution, resolutions.m_resolutions)
    {
        resolutionsAvailableResp.push_back(soap_new_tt__VideoResolution(soap));
        resolutionsAvailableResp.back()->Width = resolution->width;
        resolutionsAvailableResp.back()->Height = resolution->height;
    }
}

void SoapProxy::fillResolutions2(soap* soap, std::vector<tt__VideoResolution2*>& resolutionsAvailableResp, const ResolutionInfo& resolutions) const
{
    BOOST_FOREACH(auto resolution, resolutions.m_resolutions)
    {
        resolutionsAvailableResp.push_back(soap_new_tt__VideoResolution2(soap));
        resolutionsAvailableResp.back()->Width = resolution->width;
        resolutionsAvailableResp.back()->Height = resolution->height;
    }
}

std::vector<RecordingTrackType> SoapProxy::GetTrackTypes(const std::string * RecordingInformationFilter)
{
    std::vector<RecordingTrackType> tracks;

    if(!RecordingInformationFilter)
    {
        tracks.push_back(allTrackType);
        return tracks;
    }

    if ((*RecordingInformationFilter).find("TrackType = \"Video\"") != std::string::npos)
    {
        tracks.push_back(videoTrackType);
    }
    if ((*RecordingInformationFilter).find("TrackType = \"Audio\"") != std::string::npos)
    {
        tracks.push_back(audioTrackType);
    }
    if ((*RecordingInformationFilter).find("TrackType = \"Metadata\"") != std::string::npos)
    {
        tracks.push_back(metadataTrackType);
    }

    if(!tracks.size())
    {
        tracks.push_back(allTrackType);
    }

    return tracks;
}

tt__RecordingJobConfiguration* SoapProxy::CreateRecordingJobConfiguration(soap* soap, const RecordingJobSP& job)
{
    auto jobConfig = soap_new_tt__RecordingJobConfiguration(soap);
    jobConfig->RecordingToken = job->m_recordingConfig->GetToken();

    jobConfig->Mode = job->m_state;
    jobConfig->Priority = job->priority;

	auto recSource = soap_new_tt__RecordingJobSource(soap);
	recSource->SourceToken = soap_new_tt__SourceReference(soap);

	if (job->m_profile)
    {
		recSource->SourceToken->Type = SoapProxy::SchemaProfileUrl;
		recSource->SourceToken->Token = job->m_profile->GetToken();
    }
	else
	{
		recSource->SourceToken->Token = job->m_recordingConfig->GetVSource()->GetToken();
	}

	jobConfig->Source.push_back(recSource);

    return jobConfig;
}

tt__GetRecordingJobsResponseItem* SoapProxy::CreateRecordingJobConfigurationItem(soap* soap, const RecordingJobSP& job)
{
    auto recJob = soap_new_tt__GetRecordingJobsResponseItem(soap);
    recJob->JobToken = job->GetToken();
    recJob->JobConfiguration = CreateRecordingJobConfiguration(soap, job);

    return recJob;
}

tt__VideoEncoder2Configuration* SoapProxy::createVideoEncoder2Configuration(soap* soap, const EncoderConfigurationSP& vEncoder)
{
    auto cfg = soap_new_tt__VideoEncoder2Configuration(soap);
    cfg->Resolution = soap_new_req_tt__VideoResolution2(soap, vEncoder->m_resolutionsInfo.CurrentResolution()->width, vEncoder->m_resolutionsInfo.CurrentResolution()->height);
    cfg->Multicast = soap_new_tt__MulticastConfiguration(soap);
    cfg->Multicast->Address = soap_new_tt__IPAddress(soap);
    cfg->Name = vEncoder->GetName();

    auto multicastParams = vEncoder->multicastParams;

    if (multicastParams)
    {
        cfg->Multicast->TTL = 10000;
        cfg->Multicast->AutoStart = false;

        cfg->Multicast->Address->IPv4Address = soap_new_std__string(soap);
        *cfg->Multicast->Address->IPv4Address = multicastParams->GetDestinationIp();
        cfg->Multicast->Port = multicastParams->GetRealPort();
    }
    //cfg->SessionTimeout = "PT10S";

    cfg->Encoding = convertToMedia2EncodingName(soap, vEncoder->codec);

    if (vEncoder->codec == "H264")
    {
        cfg->Profile = soap_new_std__string(soap);
        *cfg->Profile = "Main";
    }

    cfg->RateControl = soap_new_tt__VideoRateControl2(soap);
    cfg->RateControl->FrameRateLimit = float(vEncoder->GetStreamStaticParams()->GetFrameRateLimit());
    cfg->RateControl->BitrateLimit = vEncoder->GetBitrateLimit();

    cfg->token = vEncoder->GetToken();
    cfg->UseCount = vEncoder->GetUseCount();

    cfg->Quality = vEncoder->GetStreamStaticParams()->GetQuality();

	cfg->GovLength = soap_new_int(soap);
	*cfg->GovLength = vEncoder->GetStreamStaticParams()->GetGovLength();

    return cfg;
}

std::string SoapProxy::convertToMedia2EncodingName(soap * soap, const std::string& internalEncodingName)
{
    if (internalEncodingName == "H265")
        return soap_tt__VideoEncodingMimeNames2s(soap, tt__VideoEncodingMimeNames__H265);
    if (internalEncodingName == "H264")
        return soap_tt__VideoEncodingMimeNames2s(soap, tt__VideoEncodingMimeNames__H264);
    if (internalEncodingName == "MJPEG")
        return soap_tt__VideoEncodingMimeNames2s(soap, tt__VideoEncodingMimeNames__JPEG);
    if (internalEncodingName == "MPEG4")
        return soap_tt__VideoEncodingMimeNames2s(soap, tt__VideoEncodingMimeNames__MPV4_ES);

    return "";
}

tt__AudioEncoder2Configuration* SoapProxy::createAudioEncoder2Configuration(soap* soap, const AudioEncoderConfigurationSP& aEncoder)
{
    auto enc = soap_new_tt__AudioEncoder2Configuration(soap);
    enc->SampleRate = aEncoder->GetSamplerate();
    enc->Bitrate = 128;

    enc->token = aEncoder->GetToken();

    enc->Multicast = soap_new_tt__MulticastConfiguration(soap);
    enc->Multicast->Address = soap_new_tt__IPAddress(soap);
    enc->Multicast->Address->IPv4Address = soap_new_std__string(soap);

    auto multicastParams = aEncoder->multicastParams;
    enc->Multicast->TTL = 10000;
    enc->Multicast->AutoStart = false;

    if (multicastParams)
    {
        *enc->Multicast->Address->IPv4Address = multicastParams->GetDestinationIp();
        enc->Multicast->Port = multicastParams->GetRealPort();
    }

	enc->Encoding = soap_tt__AudioEncodingMimeNames2s(soap, tt__AudioEncodingMimeNames__PCMU);

    enc->UseCount = aEncoder->GetUseCount();
	enc->Name = aEncoder->GetName();

    return enc;
}

bool SoapProxy::ContainsType(const std::vector<std::string>& types, const std::string& type)
{
	return std::find_if(types.begin(), types.end(), [&](const std::string& tp) { return tp == type; }) != types.end();
}

trt2__MediaProfile* SoapProxy::getProfile2(soap* soap, const MediaProfileSP& profile, const std::vector<std::string>& types)
{
    auto soapProfile = soap_new_trt2__MediaProfile(soap);

    soapProfile->Name = profile->GetName();
    soapProfile->token = profile->GetToken(); 

    soapProfile->Configurations = soap_new_trt2__ConfigurationSet(soap);

    if (profile->GetVideoSourceConfiguration() && (ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__All)) 
		|| ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__VideoSource))))
    {
        soapProfile->Configurations->VideoSource = createVideoSourceConfiguration(soap, profile->GetVideoSourceConfiguration());
    }

    if (profile->GetEncoderConfiguration() && profile->GetEncoderConfiguration()->IsSet() && (ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__All))
        || ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__VideoEncoder))))
    {
        soapProfile->Configurations->VideoEncoder = createVideoEncoder2Configuration(soap, profile->GetEncoderConfiguration());
    }

    if (profile->GetAudioSourceConfiguration() && (ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__All)) 
		|| ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__AudioSource))))
    {
        soapProfile->Configurations->AudioSource = createAudioSourceConfiguration(soap, profile->GetAudioSourceConfiguration());
    }

    if (profile->GetAudioEncoderConfiguration() && (ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__All))
        || ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__AudioEncoder))))
    {
        soapProfile->Configurations->AudioEncoder = createAudioEncoder2Configuration(soap, profile->GetAudioEncoderConfiguration());
    }

    soapProfile->fixed = OnvifUtil::BoolValue(soap, false);

    if (profile->GetPtzConfiguration() && (ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__All))
		|| ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__PTZ))))
    {
        soapProfile->Configurations->PTZ = createPtzConfiguration(soap, profile->GetPtzConfiguration()); 
    }

	if (profile->GetVideoAnalyticsConfiguration() && (ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__All)) 
		|| ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__Analytics))))
	{
		soapProfile->Configurations->Analytics = createVideoAnalyticsConfiguration(soap, profile->GetVideoAnalyticsConfiguration());
	}

	if (profile->GetMetadataConfiguration() && (ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__All))
		|| ContainsType(types, soap_trt2__ConfigurationEnumeration2s(soap, trt2__ConfigurationEnumeration__Metadata))))
	{
		soapProfile->Configurations->Metadata = createMetadataConfiguration(soap, profile->GetMetadataConfiguration());
	}

    return soapProfile;
}

tt__ImagingSettings20* SoapProxy::CreateImagingSettings20(soap* soap, const VideoSourceSP& vSource)
{
    auto ImagingSettings = soap_new_tt__ImagingSettings20(soap);

    ImagingSettings->Focus = soap_new_tt__FocusConfiguration20(soap);
    ImagingSettings->Focus->AutoFocusMode = vSource->IsAutoFocusOn() ? tt__AutoFocusMode__AUTO : tt__AutoFocusMode__MANUAL;
    ImagingSettings->Focus->NearLimit = soap_new_float(soap);
    *ImagingSettings->Focus->NearLimit = 0;
    ImagingSettings->Focus->FarLimit = soap_new_float(soap);
    *ImagingSettings->Focus->FarLimit = 1;
	ImagingSettings->Focus->DefaultSpeed = soap_new_float(soap);
	*ImagingSettings->Focus->DefaultSpeed = 1.0;

    ImagingSettings->Exposure = soap_new_tt__Exposure20(soap);
    ImagingSettings->Exposure->Mode = vSource->IsAutoIrisOn() ? tt__ExposureMode__AUTO : tt__ExposureMode__MANUAL;
    ImagingSettings->Exposure->MinIris = soap_new_float(soap);
    *ImagingSettings->Exposure->MinIris = 0;
    ImagingSettings->Exposure->MaxIris = soap_new_float(soap);
    *ImagingSettings->Exposure->MaxIris = 1;
    ImagingSettings->Exposure->Window = soap_new_set_tt__Rectangle(soap, OnvifUtil::FloatValue(soap, 0), 
		OnvifUtil::FloatValue(soap, (float)vSource->GetHeight()), OnvifUtil::FloatValue(soap, 0.0), OnvifUtil::FloatValue(soap, (float)vSource->GetWidth()));

	ImagingSettings->Brightness = OnvifUtil::FloatValue(soap, vSource->GetBrightness());
	ImagingSettings->Contrast = OnvifUtil::FloatValue(soap, vSource->GetContrast());
	ImagingSettings->ColorSaturation = OnvifUtil::FloatValue(soap, vSource->GetColorSaturation());

	return ImagingSettings;
}

tt__ImagingSettings* SoapProxy::CreateImagingSettings(soap* soap, const VideoSourceSP& vSource)
{
    auto ImagingSettings = soap_new_tt__ImagingSettings(soap);
    ImagingSettings->Focus = soap_new_tt__FocusConfiguration(soap);
    ImagingSettings->Focus->AutoFocusMode = vSource->IsAutoFocusOn() ? tt__AutoFocusMode__AUTO : tt__AutoFocusMode__MANUAL;
    ImagingSettings->Focus->NearLimit = 0;
    ImagingSettings->Focus->FarLimit = 1;

    ImagingSettings->Exposure = soap_new_tt__Exposure(soap);
    ImagingSettings->Exposure->Mode = vSource->IsAutoIrisOn() ? tt__ExposureMode__AUTO : tt__ExposureMode__MANUAL;
    ImagingSettings->Exposure->MinIris = 0;
    ImagingSettings->Exposure->MaxIris = 1;

    ImagingSettings->Exposure->Window = soap_new_set_tt__Rectangle(soap, OnvifUtil::FloatValue(soap, 0),
		OnvifUtil::FloatValue(soap, (float)vSource->GetHeight()), OnvifUtil::FloatValue(soap, 0.0), OnvifUtil::FloatValue(soap, (float)vSource->GetWidth()));

    return ImagingSettings;
}

tt__AudioSource* SoapProxy::createAudioSource(soap* soap, const AudioSourceSP& src, const AudioSourceConfigurationSP& aSource)
{
    auto audioSource = soap_new_tt__AudioSource(soap);
    audioSource->Channels = 1;
    audioSource->token = src->GetToken();

    return audioSource;
}

tt__AudioSourceConfiguration* SoapProxy::createAudioSourceConfiguration(soap* soap, const AudioSourceConfigurationSP& src)
{
    auto aSourceConfig = soap_new_tt__AudioSourceConfiguration(soap);
    aSourceConfig->SourceToken = src->GetAudioSource()->GetToken();
    aSourceConfig->Name = src->GetAudioSource()->GetName();
    aSourceConfig->token = src->GetToken();
    aSourceConfig->UseCount = src->GetUseCount();
    return aSourceConfig;
}

tt__AudioEncoderConfiguration* SoapProxy::createAudioEncoderConfiguration(soap* soap, const AudioEncoderConfigurationSP& aEncoder)
{
    auto enc = soap_new_tt__AudioEncoderConfiguration(soap);
    enc->SampleRate = aEncoder->GetSamplerate();
    enc->Bitrate = 128;

    enc->token = aEncoder->GetToken();

    enc->Multicast = soap_new_tt__MulticastConfiguration(soap);
    enc->Multicast->Address = soap_new_tt__IPAddress(soap);
    enc->Multicast->Address->IPv4Address = soap_new_std__string(soap);

    auto multicastParams = aEncoder->multicastParams;

    if (multicastParams)
    {
        enc->Multicast->TTL = 10000;
        enc->Multicast->AutoStart = false;

        *enc->Multicast->Address->IPv4Address = multicastParams->GetDestinationIp();
        enc->Multicast->Port = multicastParams->GetRealPort();
    }

    enc->UseCount = aEncoder->GetUseCount();

    return enc;
}

void SoapProxy::addSimpleItem(std::vector<simple_item_type>* parameters, const std::string& itemName, const std::string& itemValue)
{
    simple_item_type prop_vSource;
	prop_vSource.Name = itemName;
	prop_vSource.Value = itemValue;
	parameters->push_back(prop_vSource);
}

void SoapProxy::addSimpleItemDescription(std::vector<simple_item_description_type>* parameters, const std::string& itemName, const std::string& type)
{
    simple_item_description_type prop_description;
	prop_description.Name = itemName;
	prop_description.Type = type;
	parameters->push_back(prop_description);
}

tt__Config* SoapProxy::createVideoAnalyticsModule(soap* soap, const TrackerModuleSP& module)
{
	auto md = soap_new_tt__Config(soap);
	md->Name = module->GetName();
	md->Type = module->GetType();
	md->Parameters = soap_new_tt__ItemList(soap);

	addSimpleItem(&md->Parameters->SimpleItem, "MinObjectWidth", boost::lexical_cast<std::string>(module->GetMinObjectWidth()));
	addSimpleItem(&md->Parameters->SimpleItem, "MaxObjectWidth", boost::lexical_cast<std::string>(module->GetMaxObjectWidth()));
	addSimpleItem(&md->Parameters->SimpleItem, "MinObjectHeight", boost::lexical_cast<std::string>(module->GetMinObjectHeight()));
	addSimpleItem(&md->Parameters->SimpleItem, "MaxObjectHeight", boost::lexical_cast<std::string>(module->GetMaxObjectHeight()));

	return md;
}

tt__ConfigDescription* SoapProxy::createVideoAnalyticsModuleDescription(soap* soap, const TrackerModuleSP& module)
{
	auto configDescr = soap_new_tt__ConfigDescription(soap);
	configDescr->Name = module->GetName();
	configDescr->Parameters = soap_new_tt__ItemListDescription(soap);

	addSimpleItemDescription(&configDescr->Parameters->SimpleItemDescription, "MinObjectWidth", "xsd:float");
	addSimpleItemDescription(&configDescr->Parameters->SimpleItemDescription, "MaxObjectWidth", "xsd:float");
	addSimpleItemDescription(&configDescr->Parameters->SimpleItemDescription, "MinObjectHeight", "xsd:float");
	addSimpleItemDescription(&configDescr->Parameters->SimpleItemDescription, "MaxObjectHeight", "xsd:float");

	return configDescr;
}

tt__VideoAnalyticsConfiguration* SoapProxy::createVideoAnalyticsConfiguration(soap* soap, const VideoAnalyticsConfigurationSP& vAnalyticsConfiguration)
{
	auto vAnalyticsConf = soap_new_tt__VideoAnalyticsConfiguration(soap);
	vAnalyticsConf->AnalyticsEngineConfiguration = soap_new_tt__AnalyticsEngineConfiguration(soap);

	vAnalyticsConf->token = vAnalyticsConfiguration->GetToken();
	vAnalyticsConf->Name = vAnalyticsConfiguration->GetName();
	vAnalyticsConf->UseCount = vAnalyticsConfiguration->GetUseCount();

	BOOST_FOREACH(auto md, vAnalyticsConfiguration->GetModules())
	{
		vAnalyticsConf->AnalyticsEngineConfiguration->AnalyticsModule.push_back(createVideoAnalyticsModule(soap, md));
	}

	return vAnalyticsConf;
}

tt__MetadataConfiguration* SoapProxy::createMetadataConfiguration(soap* soap, const MetadataConfigurationSP& config)
{
	auto metadataConf = soap_new_tt__MetadataConfiguration(soap);

	metadataConf->token = config->GetToken();
	metadataConf->Name = config->GetName();
	metadataConf->UseCount = config->GetUseCount();
	metadataConf->Analytics = OnvifUtil::BoolValue(soap, config->GetAnalyticsFlag());

	metadataConf->Multicast = soap_new_tt__MulticastConfiguration(soap);
	metadataConf->Multicast->Address = soap_new_tt__IPAddress(soap);
	metadataConf->Multicast->Address->IPv4Address = soap_new_std__string(soap);

	auto multicastParams = config->multicastParams;
	metadataConf->Multicast->TTL = 10000;
	metadataConf->Multicast->AutoStart = false;

	if (multicastParams)
	{
		*metadataConf->Multicast->Address->IPv4Address = multicastParams->GetDestinationIp();
		metadataConf->Multicast->Port = multicastParams->GetRealPort();
	}

    if(config->GetEventsFlag())
    {
        metadataConf->Events = soap_new_tt__EventSubscription(soap);

        if(config->GetFilter().size())
        {
            metadataConf->Events->Filter = soap_new_wsnt__FilterType(soap);

            BOOST_FOREACH(auto filter, config->GetFilter())
            {
                if (filter->m_typeName == SOAP_TYPE_wsnt__TopicExpressionType)
                {
                    auto topicExpr = soap_new_wsnt__TopicExpressionType(soap);
                    topicExpr->__any = filter->m_filterString;
                    topicExpr->Dialect = EventsBuildUtil::TopicExpressionConcreteSetDialectUri;
                    
                    soap_dom_element domElem(soap, "wsnt:TopicExpression");
                    domElem.set(topicExpr, filter->m_typeName);

                    metadataConf->Events->Filter->__any.push_back(domElem);
                }
                else if(filter->m_typeName == SOAP_TYPE__wsnt__MessageContent)
                {
                    auto contentExpr = soap_new__wsnt__MessageContent(soap);
                    contentExpr->__any = filter->m_filterString;
                    contentExpr->Dialect = EventsBuildUtil::MessageContentDialectUri;

                    soap_dom_element domElem(soap, "wsnt:MessageContent");
                    domElem.set(contentExpr, filter->m_typeName);
                    metadataConf->Events->Filter->__any.push_back(domElem);
                }
            }
        }
    }

	return metadataConf;
}

tt__RelayOutput* SoapProxy::createRelayOutput(soap* soap, const RelayOutputSP& relayConf)
{
	auto relay = soap_new_tt__RelayOutput(soap);
	relay->token = relayConf->GetToken();
	relay->Properties = soap_new_tt__RelayOutputSettings(soap);

	if(relayConf->GetMode() == RelayOutput::Monostable)
	{
		relay->Properties->Mode = tt__RelayMode__Monostable;
	}
	else if(relayConf->GetMode() == RelayOutput::Bistable)
	{
		relay->Properties->Mode = tt__RelayMode__Bistable;
	}

	if(relayConf->GetIdleState() == RelayOutput::on)
	{
		relay->Properties->IdleState = tt__RelayIdleState__open;
	}
	else if(relayConf->GetIdleState() == RelayOutput::off)
	{
		relay->Properties->IdleState = tt__RelayIdleState__closed;
	}

	relay->Properties->DelayTime = relayConf->GetDelayTime();

	return relay;
}

tt__DigitalInput* SoapProxy::createRayOutput(soap* soap, const RayOutputSP& rayConf)
{
	auto ray = soap_new_tt__DigitalInput(soap);
	ray->token = rayConf->GetToken();
	ray->IdleState = soap_new_tt__DigitalIdleState(soap);
	*ray->IdleState = rayConf->GetIdleState() ? tt__DigitalIdleState__open : tt__DigitalIdleState__closed;

	return ray;
}

tt__RecordingJobStateInformation* SoapProxy::createRecordingJobStateInformation(soap* soap, const RecordingJobSP& recordingJob)
{
    auto recJobInfo = soap_new_tt__RecordingJobStateInformation(soap);
    recJobInfo->RecordingToken = recordingJob->m_recordingConfig->GetToken();
    if (recordingJob->m_profile)
    {
        auto profileSource = soap_new_tt__RecordingJobStateSource(soap);
        profileSource->SourceToken = soap_new_tt__SourceReference(soap);
        profileSource->SourceToken->Type = SoapProxy::SchemaProfileUrl;
        profileSource->SourceToken->Token = recordingJob->m_profile->GetToken();

        profileSource->Tracks = soap_new_tt__RecordingJobStateTracks(soap);

        tt__RecordingJobStateTrack* track = soap_new_tt__RecordingJobStateTrack(soap);
        track->Destination = recordingJob->m_recordingConfig->GetToken();
        track->SourceTag = recordingJob->m_profile->GetVideoSourceConfiguration()->GetToken();

        profileSource->Tracks->Track.push_back(track);

        recJobInfo->Sources.push_back(profileSource);
    }
    recJobInfo->State = recordingJob->m_state;

    return recJobInfo;
}

timeval* SoapProxy::convertTime(soap* soap, std::time_t time)
{
    timeval* tm = soap_new_xsd__dateTime(soap);

    unsigned long long int uts = time * 1000;
    tm->tv_sec = static_cast<long>(uts / 1000000);
    tm->tv_usec = static_cast<long>(uts % 1000000);

    return tm;
}

time_t SoapProxy::GetCurTime()
{
	boost::posix_time::ptime time_t_epoch(boost::gregorian::date(1970, 1, 1));
	auto now = boost::posix_time::microsec_clock::universal_time();
	auto diff = now - time_t_epoch;
	auto tm = diff.total_milliseconds();
	
	return tm;
}

UserLevel SoapProxy::GetUserLevel(tt__UserLevel level)
{
	switch (level)
	{
	case tt__UserLevel__Administrator:
		return Administrator;
	case tt__UserLevel__Operator:
		return Operator;
	case tt__UserLevel__User: 
		return User;
	case tt__UserLevel__Anonymous:
		return Anonymous;
	case tt__UserLevel__Extended:
		return Extended;
	default:
		return Anonymous;
	}
}

tt__UserLevel SoapProxy::CreateUserLevel(UserLevel level)
{
	switch(level)
	{
	case Administrator:
		return tt__UserLevel__Administrator;
	case Operator:
		return tt__UserLevel__Operator;
	case User:
		return tt__UserLevel__User;
	case Anonymous:
		return tt__UserLevel__Anonymous;
	case Extended:
		return tt__UserLevel__Extended;
	default:
		return tt__UserLevel__Anonymous;
	}
}

tt__AudioEncoderConfigurationOption* SoapProxy::CreateAudioEncoderConfigurationOption(soap* soap, AudioEncoderConfigurationSP aEncoderConfig) const
{
	auto opt = soap_new_tt__AudioEncoderConfigurationOption(soap);
	opt->Encoding = tt__AudioEncoding__G711;
	opt->SampleRateList = soap_new_tt__IntItems(soap);

	if(aEncoderConfig)
	{
		opt->SampleRateList->Items.push_back(aEncoderConfig->GetSamplerate());
	}
	else
	{
		opt->SampleRateList->Items.push_back(8);
		opt->SampleRateList->Items.push_back(48);
	}

	opt->BitrateList = soap_new_tt__IntItems(soap);
	opt->BitrateList->Items.push_back(128);

	return opt;
}

tt__AudioEncoder2ConfigurationOptions* SoapProxy::CreateAudioEncoder2ConfigurationOption(soap* soap, AudioEncoderConfigurationSP aEncoderConfig) const
{
	auto opt = soap_new_tt__AudioEncoder2ConfigurationOptions(soap);
	opt->Encoding = "PCMU";
	opt->SampleRateList = soap_new_tt__IntItems(soap);

	if (aEncoderConfig)
	{
		opt->SampleRateList->Items.push_back(aEncoderConfig->GetSamplerate());
	}
	else
	{
		opt->SampleRateList->Items.push_back(8);
		opt->SampleRateList->Items.push_back(48);
	}

	opt->BitrateList = soap_new_tt__IntItems(soap);
	opt->BitrateList->Items.push_back(128);

	return opt;
}

bool SoapProxy::CheckAudioSampleRate(AudioEncoderConfigurationSP aEncoderConfig, int _samplerate) const
{
	return _samplerate == aEncoderConfig->GetSamplerate();
}