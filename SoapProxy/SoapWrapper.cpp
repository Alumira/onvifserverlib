#include "SoapWrapper.h"
#include "stdsoap2.h"

SoapWrapper::SoapWrapper(int type) 
{
	if (type == 1)
	{
		m_soap = soap_new1(SOAP_XML_INDENT);

		soap_set_test_logfile(m_soap, nullptr);
		soap_set_sent_logfile(m_soap, nullptr);
		soap_set_recv_logfile(m_soap, nullptr);
	}
	else
	{
		m_soap = soap_new2(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT | SOAP_XML_CANONICAL | SOAP_XML_NOTYPE);

		soap_set_test_logfile(m_soap, nullptr);
		//soap_set_sent_logfile(m_soap, nullptr);
		//soap_set_recv_logfile(m_soap, nullptr);
	}

	setNamespaces(m_soap);
}

SoapWrapper::SoapWrapper(soap* sourceSoap)
{
	m_soap = soap_copy(sourceSoap);
}

void SoapWrapper::setNamespaces(soap* soap)
{
	static const struct Namespace namespaces[] = {
		{"SOAP-ENV", "http://www.w3.org/2003/05/soap-envelope", "http://schemas.xmlsoap.org/soap/envelope/", NULL},
		{"SOAP-ENC", "http://www.w3.org/2003/05/soap-encoding", "http://schemas.xmlsoap.org/soap/encoding/", NULL},
		{ "tt", "http://www.onvif.org/ver10/schema", nullptr, nullptr },
		{ "wsnt", "http://docs.oasis-open.org/wsn/b-2", nullptr, nullptr },
		{ "tns1", "http://www.onvif.org/ver10/topics", nullptr, nullptr },
		{nullptr, nullptr, nullptr, nullptr }
	};
	soap_set_namespaces(soap, namespaces);
}

SoapWrapper::~SoapWrapper()
{
	soap_destroy(m_soap);
	soap_end(m_soap);
	soap_free(m_soap);
}

soap* SoapWrapper::GetSoap() const
{
	return m_soap;
}

