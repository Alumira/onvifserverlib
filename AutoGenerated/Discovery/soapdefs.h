#pragma once
#include <stdio.h>
#include <assert.h>

#define VER_SOAP_STRING "Onvif 2.X"
//#define SOAP_WSA_2005

#ifndef SOAP_H_FILE
#define SOAP_H_FILE onvif_generated_H.h
#endif

#ifndef DBGMSG

#define DBGMSG(DBGFILE, MSG, LEN) \
{ if (soap) \
{ if (!soap->fdebug[SOAP_INDEX_##DBGFILE]) \
      soap_open_logfile((struct soap*)soap, SOAP_INDEX_##DBGFILE); \
    if (soap->fdebug[SOAP_INDEX_##DBGFILE]) \
    { \
      soapLogMessage(soap, SOAP_INDEX_##DBGFILE, MSG, LEN); \
    } \
  } \
} 
#endif

#ifndef SOAP_MESSAGE
void SOAP_LOG(_Inout_ FILE * _File, _In_z_ _Printf_format_string_ const char * _Format, ...);
#define SOAP_MESSAGE SOAP_LOG
#endif

struct soap;
#ifdef __cplusplus
extern "C"
#endif
void soapLogMessage(const struct soap*, int, const char*, size_t);

//#define SOAP_DOM_EXTERNAL_NAMESPACE  