set GSOAP_VER=2.8.91
rem GSOAP_PATH=%ITV_THIRD%\gSOAP\%GSOAP_VER%\

set GSOAP_PATH=d:\SOURCE\gSOAP_git\gsoap\

set GSOAP_SOURCE_PATH=%GSOAP_PATH%gsoap\
set DEST_FOLDER=%CD%\

%GSOAP_SOURCE_PATH%bin\win32\soapcpp2 -x -L -2 -Iimport -I%GSOAP_SOURCE_PATH%;%GSOAP_SOURCE_PATH%import %GSOAP_SOURCE_PATH%import\wsdd10.h

copy "%GSOAP_SOURCE_PATH%stdsoap2.cpp" "%DEST_FOLDER%stdsoap2.cpp" /Y
copy "%GSOAP_SOURCE_PATH%stdsoap2.h" "%DEST_FOLDER%stdsoap2.h" /Y

copy "%GSOAP_SOURCE_PATH%plugin\threads.c" "%DEST_FOLDER%threads.cpp" /Y
copy "%GSOAP_SOURCE_PATH%plugin\threads.h" "%DEST_FOLDER%threads.h" /Y

copy "%GSOAP_SOURCE_PATH%plugin\wsaapi.c" "%DEST_FOLDER%wsaapi.cpp" /Y
copy "%GSOAP_SOURCE_PATH%plugin\wsaapi.h" "%DEST_FOLDER%wsaapi.h" /Y

copy "%GSOAP_SOURCE_PATH%plugin\wsddapi.c" "%DEST_FOLDER%wsddapi.cpp" /Y
copy "%GSOAP_SOURCE_PATH%plugin\wsddapi.h" "%DEST_FOLDER%wsddapi.h" /Y