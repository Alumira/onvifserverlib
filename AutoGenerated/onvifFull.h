// SOAP HEADER

#import "GeneratedOnvifCode.h"

/******************************************************************************\
*                                                                            *
* FUNCTIONS                                            *
*                                                                            *
*                                                                            *
\******************************************************************************/

#import "wsse.h"	// wsse = <http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd>

// Theses directives adds http://www.onvif.org/ver10/network/wsdl to namespace map table.
//gsoap dn schema namespace:	http://www.onvif.org/ver10/network/wsdl
//gsoap dn schema elementForm:	qualified

// gsoap does not push to namespace map namespaces with empty definitions,
// so we define dummy type to prevent this behavior.
typedef std::string dn__dummy;

// Theses directives adds http://www.onvif.org/ver10/topics to namespace map table.
//gsoap tns1 schema namespace: http://www.onvif.org/ver10/topics
//gsoap tns1 schema elementForm:	qualified

// gsoap does not push to namespace map namespaces with empty definitions,
// so we define dummy type to prevent this behavior.
typedef std::string tns1__dummy;

// gsoap does not push to namespace map namespaces with empty definitions,
// so we define dummy type to prevent this behavior.
typedef std::string ter__dummy;

typedef std::string axn__someType;

//typedef std::string wsaw__someType;
//gsoap xmime schema namespace:	 http://www.w3.org/2005/05/xmlmime
//gsoap xmime schema elementForm:	qualified

