rem http://www.onvif.org/onvif/ver10/device/wsdl/devicemgmt.wsdl http://www.onvif.org/onvif/ver10/events/wsdl/event.wsdl http://www.onvif.org/onvif/ver10/display.wsdl http://www.onvif.org/onvif/ver10/deviceio.wsdl http://www.onvif.org/onvif/ver20/imaging/wsdl/imaging.wsdl http://www.onvif.org/onvif/ver10/media/wsdl/media.wsdl http://www.onvif.org/onvif/ver20/media/wsdl/media.wsdl http://www.onvif.org/onvif/ver20/ptz/wsdl/ptz.wsdl http://www.onvif.org/onvif/ver10/receiver.wsdl http://www.onvif.org/onvif/ver10/recording.wsdl http://www.onvif.org/onvif/ver10/search.wsdl http://www.onvif.org/onvif/ver10/replay.wsdl 

set GSOAP_PATH=d:\SOURCE\gSOAP_git\gsoap\

set GSOAP_VER=2.8.91
rem GSOAP_PATH=%ITV_THIRD%\gSOAP\%GSOAP_VER%\

del onvif_generated_*.*
del GeneratedOnvifCode*.h

set GSOAP_SOURCE_PATH=%GSOAP_PATH%gsoap\
set DEST_FOLDER=%CD%\

%GSOAP_SOURCE_PATH%bin\ssl\wsdl2h -u -g -O1 -P -j -a -d -nOnvifServer -NOnvifServerService -y -ttypemap.dat -oGeneratedOnvifCode.h ..\SSTMK\sensors.xsd https://www.onvif.org/ver10/schema/onvif.xsd https://www.w3.org/2006/05/addressing/wsdl/ws-addr-wsdl.xsd http://www.onvif.org/onvif/ver10/deviceio.wsdl http://www.onvif.org/onvif/ver10/media/wsdl/media.wsdl http://www.onvif.org/onvif/ver20/media/wsdl/media.wsdl http://www.onvif.org/onvif/ver20/ptz/wsdl/ptz.wsdl http://www.onvif.org/onvif/ver10/events/wsdl/event.wsdl http://www.onvif.org/onvif/ver10/recording.wsdl http://www.onvif.org/onvif/ver10/replay.wsdl http://www.onvif.org/onvif/ver10/search.wsdl http://www.onvif.org/onvif/ver20/imaging/wsdl/imaging.wsdl http://www.onvif.org/onvif/ver10/receiver.wsdl  http://www.onvif.org/ver20/analytics/wsdl/analytics.wsdl http://www.onvif.org/ver10/schema/metadatastream.xsd http://docs.oasis-open.org/wsn/t-1.xsd 

%GSOAP_SOURCE_PATH%bin\win32\soapcpp2 -a -Ec -Ed -x -w -j -Iimport -2 -n -f200 -ponvif_generated_ -I%GSOAP_SOURCE_PATH%;%GSOAP_SOURCE_PATH%import onvifFull.h 

copy /Y "%GSOAP_SOURCE_PATH%stdsoap2.cpp" "%DEST_FOLDER%stdsoap2.cpp"
copy "%GSOAP_SOURCE_PATH%stdsoap2.h" "%DEST_FOLDER%stdsoap2.h" /Y
copy "%GSOAP_SOURCE_PATH%dom.cpp" "%DEST_FOLDER%OnvifSource\dom.cpp" /Y

copy "%GSOAP_SOURCE_PATH%custom\duration.c" "%DEST_FOLDER%\OnvifSource\duration.cpp" /Y
copy "%GSOAP_SOURCE_PATH%custom\duration.h" "%DEST_FOLDER%\OnvifSource\duration.h" /Y

copy "%GSOAP_SOURCE_PATH%custom\struct_timeval.c" "%DEST_FOLDER%OnvifSource\struct_timeval.cpp" /Y
copy "%GSOAP_SOURCE_PATH%custom\struct_timeval.h" "%DEST_FOLDER%OnvifSource\struct_timeval.h" /Y

copy "%GSOAP_SOURCE_PATH%plugin\httpda.c" "%DEST_FOLDER%OnvifSource\httpda.cpp" /Y
copy "%GSOAP_SOURCE_PATH%plugin\httpda.h" "%DEST_FOLDER%OnvifSource\httpda.h" /Y

copy "%GSOAP_SOURCE_PATH%plugin\mecevp.c" "%DEST_FOLDER%OnvifSource\mecevp.cpp" /Y
copy "%GSOAP_SOURCE_PATH%plugin\mecevp.h" "%DEST_FOLDER%OnvifSource\mecevp.h" /Y

copy "%GSOAP_SOURCE_PATH%plugin\smdevp.c" "%DEST_FOLDER%OnvifSource\smdevp.cpp" /Y
copy "%GSOAP_SOURCE_PATH%plugin\smdevp.h" "%DEST_FOLDER%OnvifSource\smdevp.h" /Y

copy "%GSOAP_SOURCE_PATH%plugin\threads.c" "%DEST_FOLDER%OnvifSource\threads.cpp" /Y
copy "%GSOAP_SOURCE_PATH%plugin\threads.h" "%DEST_FOLDER%OnvifSource\threads.h" /Y

copy "%GSOAP_SOURCE_PATH%plugin\wsaapi.c" "%DEST_FOLDER%OnvifSource\wsaapi.cpp" /Y
copy "%GSOAP_SOURCE_PATH%plugin\wsaapi.h" "%DEST_FOLDER%OnvifSource\wsaapi.h" /Y

copy "%GSOAP_SOURCE_PATH%plugin\wsseapi.cpp" "%DEST_FOLDER%OnvifSource\wsseapi.cpp" /Y
copy "%GSOAP_SOURCE_PATH%plugin\wsseapi.h" "%DEST_FOLDER%OnvifSource\wsseapi.h" /Y






