// Customize wsnt_TopicExpressionType to prevent ignoring message topic.
class wsnt__TopicExpressionType
{ public:
	// was:
	// struct soap_dom_element __any;	/* external */
	xsd__QName	    __any                         ;
	@xsd__anyURI        Dialect                        1;
	@xsd__anyAttribute  __anyAttribute                ;
	xsd__anyType        __mixed                       0;
	struct soap         *soap                          ;
};

//Video analytics Events
class tt__MotionDetection
{ public:
	tt__MessageDescription* MessageDescription  0;

	xsd__QName	    __any                            ;
	@xsd__anyAttribute  __anyAttribute               ;
	xsd__anyType        __mixed                      0;
	struct soap         *soap                        ;
};

class _VideoAnalyticsType
{ public:
	tt__MotionDetection MotionDetection  			1;
	
	xsd__QName	    __any                            ;
	@xsd__anyAttribute  __anyAttribute               ;
	xsd__anyType        __mixed                      0;
	struct soap         *soap                        ;
};

class tns1__VideoAnalyticsEventDescription
{ public:
	_VideoAnalyticsType  	VideoAnalytics  		1;
	
	xsd__QName	    __any                            ;
	@xsd__anyAttribute  __anyAttribute               ;
	xsd__anyType        __mixed                      0;
	struct soap         *soap                        ;
};

//Recording Events
class tt__RecordingConfigurationType
{ public:
	tt__MessageDescription* MessageDescription  0;
	
	xsd__QName	    __any                            ;
	@xsd__anyAttribute  __anyAttribute               ;
	xsd__anyType        __mixed                      0;
	struct soap         *soap                        ;
};

class tt__TrackConfigurationType
{ public:
	tt__MessageDescription* MessageDescription  0;
	
	xsd__QName	    __any                            ;
	@xsd__anyAttribute  __anyAttribute               ;
	xsd__anyType        __mixed                      0;
	struct soap         *soap                        ;
};

class tt__JobStateType
{ public:
	tt__MessageDescription* MessageDescription  0;
	
	xsd__QName	    __any                            ;
	@xsd__anyAttribute  __anyAttribute               ;
	xsd__anyType        __mixed                      0;
	struct soap         *soap                        ;
};

class tt__RecordingJobConfigurationType
{ public:
	tt__MessageDescription* MessageDescription  0;
	
	xsd__QName	    __any                            ;
	@xsd__anyAttribute  __anyAttribute               ;
	xsd__anyType        __mixed                      0;
	struct soap         *soap                        ;
};

class _RecordingConfigType
{ public:
	tt__RecordingConfigurationType  RecordingConfiguration  1;
	tt__TrackConfigurationType  	TrackConfiguration  	1;
	tt__JobStateType  				JobState  				1;
	tt__RecordingJobConfigurationType  	RecordingJobConfiguration  	1;

	xsd__QName	    __any                            ;
	@xsd__anyAttribute  __anyAttribute               ;
	xsd__anyType        __mixed                      0;
	struct soap         *soap                        ;
};

class tns1__RecordingConfigEventDescription
{ public:
	_RecordingConfigType       RecordingConfig  1;

	xsd__QName	    __any                            ;
	@xsd__anyAttribute  __anyAttribute               ;
	xsd__anyType        __mixed                      0;
	struct soap         *soap                        ;
};

//here you should add any type, that could be suppressed due to -O3 option (that is not used for any of wsdl), but you need it
/*class tt__AdditionalClasses
{ public:
	enum tt__VideoEncodingMimeNames VideoEncodingMimeNames  ;

	xsd__QName	    __any                            ;
	@xsd__anyAttribute  __anyAttribute               ;
	xsd__anyType        __mixed                      0;
	struct soap         *soap                        ;
};*/
