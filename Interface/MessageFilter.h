#pragma once
#include "AutoGenerated/onvif_generated_Stub.h"

class FilterType
{
public:
    explicit FilterType(int _typeName, const std::string& _filterString) :
        m_typeName(_typeName), m_filterString(_filterString) {}

    static int ConvertType(const std::string& typeName)
    {
        if (typeName.find("TopicExpression") != std::string::npos)
        {
            return SOAP_TYPE_wsnt__TopicExpressionType;
        }
        else if (typeName.find("MessageContent") != std::string::npos)
        {
            return SOAP_TYPE__wsnt__MessageContent;
        }

        return 0;
    }

    int m_typeName;
    std::string m_filterString;
};
typedef std::shared_ptr<FilterType> FilterTypeSP;
