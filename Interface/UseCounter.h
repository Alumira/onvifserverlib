#pragma once

#ifndef _WINDOWS_DEF
#include <cstddef>
#endif

class UseCounter
{
public:
    UseCounter() : use_count(0)
    {}

    void IncUseCount()
    {
        use_count++;
    }

    void DecUseCount()
    {
        use_count--;
    }

    size_t GetUseCount() const
    {
        return use_count;
    }


private:
    size_t use_count;
};