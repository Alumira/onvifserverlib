﻿#pragma once

class AbstractProcessor
{
public:
	virtual ~AbstractProcessor() {}
	virtual int DoWork() = 0;
};

typedef std::shared_ptr<AbstractProcessor> AbstractProcessorSP;
