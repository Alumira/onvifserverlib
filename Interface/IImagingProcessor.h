﻿#pragma once

class IImagingProcessor
{
public:
	enum MoveAction
	{
		action_in,
		action_out,
		action_stop,
		action_auto
	};

	virtual ~IImagingProcessor() {}
	virtual int FocusMove(MoveAction action, int speed) = 0;
	virtual int IrisMove(MoveAction action, int speed) = 0;
};

typedef std::shared_ptr<IImagingProcessor> IImagingProcessorSP;
