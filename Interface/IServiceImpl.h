#pragma once

#include <Configuration/CoreSettings.h>
#include <SoapProxy/SoapProxyClasses.h>
#include "ISecurityServer.h"
#include "IProcessorFactory.h"
#include "ISubscriptionManager.h"

#include <ItvSdk/include/IErrorService.h>
#include <Configuration/VideoConfiguration.h>
#include <Util/Log.h>

namespace onvif_generated_ {
    class IntellectData
    {
    public:
        IntellectData(CoreSettingsSP coreSettings, SoapProxySP cfg, 
					OnvifDeviceConfigurationSP deviceConfiguration, ISecurityServerSP securityServer, 
					IProcessorFactorySP processorFactory, 
					ISubscriptionManagerSP subscriptionManager, ITV8::ILogger* logger) :
		    m_coreSettings(coreSettings)
		    , m_cfg(cfg)
		    , m_deviceConfiguration(deviceConfiguration)
		    , m_security(securityServer)
    		, processorFactory(processorFactory)
			, m_subscriptionManager(subscriptionManager)
			, m_logger(logger)
	    {
	    }

        CoreSettingsSP m_coreSettings;
        SoapProxySP m_cfg;
        OnvifDeviceConfigurationSP m_deviceConfiguration;
        ISecurityServerSP m_security;
		IProcessorFactorySP processorFactory;
		ISubscriptionManagerSP m_subscriptionManager;
		ITV8::ILogger* m_logger;
    };
    typedef std::shared_ptr<IntellectData> IntellectDataSP;

	class IServiceImpl
	{
	public:
        IServiceImpl(const IntellectDataSP& data) :
             m_data(data)
        {
            
        }

	    virtual ~IServiceImpl()
	    {
	    }

		OnvifDeviceConfigurationSP GetConfiguration() const
        {
			return m_data->m_deviceConfiguration;
        }

		ISecurityServerSP GetSecurity() const
		{
			return m_data->m_security;
		}

		SoapProxySP GetProxy() const
        {
			return m_data->m_cfg;
        }

		CoreSettingsSP GetCoreSettings() const
        {
			return m_data->m_coreSettings;
        }

		IProcessorFactorySP GetProcessorFactory() const
		{
			return m_data->processorFactory;
		}

		ISubscriptionManagerSP GetSubscriptionManager() const
		{
			return m_data->m_subscriptionManager;
		}

		ITV8::ILogger* GetLogger() const
        {
			return m_data->m_logger;
        }

	    virtual int Dispatch(struct soap *soap) = 0; 

	protected:
		IntellectDataSP m_data;

        virtual int defaultImplementation()
        {
			ONVIF_LOG_BASE(GetLogger(), ITV8::LOG_ERROR, "The requested command is not implemented in service " << typeid(*this).name());
        	return SOAP_NO_METHOD;
        }

        int no_profile(soap* soapObj, const std::string& profile_token) const;
	    int incomplete_configuration(soap* soapObj, MediaProfileSP profile) const;
	};

    typedef std::shared_ptr<IServiceImpl> IServiceImplSP;
}
