﻿#pragma once
#include <Configuration/IOConfigurationTypes.h>

class IIOProcessor
{
public:
	enum ActionCommand
	{
		action_on,
		action_off
	};

	virtual ~IIOProcessor() {}
	virtual int ReleCommand(RelayOutputSP relaySp, ActionCommand _action) = 0;
};

typedef std::shared_ptr<IIOProcessor> IIOProcessorSP;
