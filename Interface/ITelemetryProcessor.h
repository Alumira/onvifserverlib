﻿#pragma once
#include <Configuration/PtzConfigurationTypes.h>

class ITelemetryProcessor
{
public:
	virtual ~ITelemetryProcessor() {}
	virtual int Move(float x, float y, float z) = 0;
	virtual int RelativeMove(float x, float y, float z) = 0;
	virtual int AbsoluteMove(float x, float y, float z) = 0;
	virtual int Stop() = 0;
	virtual int Home() = 0;
	virtual int GoPreset(PresetConfigurationSP preset) = 0;
	virtual int SetPreset(PresetConfigurationSP preset) = 0;
};

typedef std::shared_ptr<ITelemetryProcessor> ITelemetryProcessorSP;
