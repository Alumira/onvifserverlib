#pragma once
#include <Interface/IMessageSubscription.h>

class ISubscriptionManager;
typedef std::shared_ptr<ISubscriptionManager> ISubscriptionManagerSP;
typedef std::weak_ptr<ISubscriptionManager> ISubscriptionManagerWP;

class ISubscriptionManager
{
public:
	virtual ~ISubscriptionManager() {};
	
	virtual size_t attach(IMessageSubscriptionSP receiver) = 0;
    virtual size_t detach(IMessageSubscriptionSP receiver) = 0;

	virtual void PushMessage(MessageHolderTypeSP message) = 0;
	virtual void SendPropertyEventsStates(soap* soap_local, IMessageSubscriptionPtr newReceiver) = 0;

    virtual void Start() = 0;
    virtual void Stop() = 0;
};

