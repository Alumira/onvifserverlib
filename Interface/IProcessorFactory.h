﻿#pragma once
#include <Interface/AbstractProcessor.h>

#include <Configuration/MediaProfile.h>
#include <Configuration/IOConfigurationTypes.h>
#include <Configuration/RecordingConfigurationTypes.h>
#include <Configuration/AudioConfigurationTypes.h>
#include <Interface/ITelemetryProcessor.h>
#include <Interface/IImagingProcessor.h>
#include <Interface/IBackgroundTask.h>
#include <Interface/SearchTaskParams.h>
#include <ServicesImpl/NotificationConsumerBindingProxyImpl.h>
#include <Interface/IIOProcessor.h>
#include <Configuration/UserConfiguration.h>
#include <Configuration/CoreSettings.h>

namespace ITV8 {
	struct ILogger;
}

class IProcessorFactory
{
public:
	virtual ~IProcessorFactory()
	{
	}

	virtual AbstractProcessorSP createProfileUpdater(MediaProfileSP profile, CoreSettingsSP _settings) const = 0;
	virtual AbstractProcessorSP createDeleteProfileProcessor(MediaProfileSP profile) const = 0;
	virtual AbstractProcessorSP createCreateProfileProcessor(MediaProfileSP profile, CoreSettingsSP _settings) const = 0;
	virtual AbstractProcessorSP createCreateUserProcessor(UserConfigurationSP userConfig) const = 0;
	virtual AbstractProcessorSP createDeleteUserProcessor(UserConfigurationSP userConfig) const = 0;
	virtual AbstractProcessorSP createUpdateUserProcessor(UserConfigurationSP userConfig) const = 0;
	virtual AbstractProcessorSP createUpdateOnvifServerProcessor(CoreSettingsSP _settings) const = 0;
	virtual AbstractProcessorSP createUpdateRtspProcessor(CoreSettingsSP coreSettings, MediaProfileSP profile) const = 0;
	virtual AbstractProcessorSP createGetStreamParamsProcessor(VideoSourceSP vSource) const = 0;
	virtual AbstractProcessorSP createUpdateRayProcessor(RayOutputSP rayConf) const = 0;
	virtual AbstractProcessorSP createUpdateVideoEncoderProcessor(EncoderConfigurationSP vEncoder, bool isCam) const = 0;
    virtual AbstractProcessorSP createUpdateVideoSourceProcessor(VideoSourceConfigurationSP vSource) const = 0;
	virtual AbstractProcessorSP createUpdateImagingSettingsProcessor(VideoSourceSP vSource) const = 0;
	virtual AbstractProcessorSP createUpdateAudioEncoderProcessor(AudioEncoderConfigurationSP aEncoder) const = 0;
	virtual AbstractProcessorSP createAddJobProcessor(RecordingJobSP job) const = 0;
	virtual AbstractProcessorSP createDeleteJobProcessor(RecordingJobSP job) const = 0;
	virtual AbstractProcessorSP createGetArchDepthProcessor(RecordingConfigurationSP vRecording) const = 0;
	virtual AbstractProcessorSP createGetSnapshotProcessor(const std::string& camId, const std::string& chan, soap* soap) const = 0;
	virtual AbstractProcessorSP createRebootProcessor(CoreSettingsSP coreSettings) const = 0;
	virtual IBackgroundTaskSP createSearchTask(const SearchTaskParams& taskParams) const = 0;
	virtual IBackgroundTaskSP createRecordingSearchTask(int requestRecordsMaxCount, time_t requestDurationTime, const std::vector<RecordingTrackType>& tracks) const = 0;
	virtual IBackgroundTaskSP createSubscriptionTask(ITV8::ILogger* logger, const time_t& terminationTime, const std::vector<FilterTypeSP>& filter, NotificationConsumerBindingProxySP consumerProxy) const = 0;
	virtual ITelemetryProcessorSP createTelemetryProcessor(MediaProfileSP mProfile, const std::string& user_id) const = 0;
	virtual IImagingProcessorSP createImagingProcessor(VideoSourceSP vSource, const std::string& user_id) const = 0;
	virtual IIOProcessorSP createIOProcessor(const std::string& user_id) const = 0;
};

typedef std::shared_ptr<IProcessorFactory> IProcessorFactorySP;
