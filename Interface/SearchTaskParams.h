#pragma once

struct SearchTaskParams
{
	std::string m_slave_id;
	timeval m_timeFrom;
	timeval m_timeTo;
	std::string m_recording_id;
	time_t m_timeout;
	int m_maxCount;
	bool m_includeVirtualEvents;

	SearchTaskParams(std::string slave_id, std::string recording_id, timeval timeFrom, timeval timeTo, time_t timeout, int maxCount) :
		m_slave_id(slave_id), m_timeFrom(timeFrom), m_timeTo(timeTo), m_recording_id(recording_id), m_timeout(timeout), m_maxCount(maxCount)
		, m_includeVirtualEvents(false)
	{
	}
};
