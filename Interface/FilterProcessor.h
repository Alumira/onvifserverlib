#pragma once
#include <vector>
#include<memory>

class TopicConverter
{
public:
    TopicConverter(const std::string topic);

    std::string GetAsString() const;

    bool Contains(const TopicConverter& topic) const;

private:
    std::vector<std::string> m_topics;
    const std::string m_topic;
    bool m_root;
};

class MessageHolderType;

class FilterType;
typedef std::shared_ptr<FilterType> FilterTypePtr;

class FilterProcessor
{
public:
    FilterProcessor(const std::vector<FilterTypePtr>& fltVector) : m_filter(fltVector)
    {
    }

    bool Filter(const std::shared_ptr<MessageHolderType> filteringmessage) const;

private:
    std::vector<FilterTypePtr> m_filter;

    bool FilterByTopic(const std::string& messagetopic, const std::string& filterString) const;
    bool FilterByContent(const std::string& messageXmlText, const std::string& contentFilterString) const;
};