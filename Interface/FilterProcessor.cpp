#include "FilterProcessor.h"
#include <boost/foreach.hpp>
#include <PugiXml/pugixml.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/regex/v4/regex.hpp>
#include <Interface/MessageHolderType.h>
#include <Interface/MessageFilter.h>

TopicConverter::TopicConverter(const std::string topic) : m_topic(topic), m_root(false)
{
    try
    {
        std::vector<std::string> _topics;
        boost::split(_topics, m_topic, boost::is_any_of("/"), boost::token_compress_on);
        BOOST_FOREACH(auto tns, _topics)
        {
            if (tns == ".")
            {
                m_root = true;
                continue;
            }

            std::vector<std::string> _parts;
            boost::split(_parts, tns, boost::is_any_of(":"), boost::token_compress_on);

            if (_parts.size() == 2)
            {
                m_topics.push_back(_parts[1]);
            }
            else if (_parts.size() == 1)
            {
                m_topics.push_back(_parts[0]);
            }
        }
    }
    catch (...)
    {
    }
}

std::string TopicConverter::GetAsString() const
{
    std::string retStr;
    BOOST_FOREACH(auto tns, m_topics)
    {
        retStr += tns + "/";
    }
    return retStr;
}

bool TopicConverter::Contains(const TopicConverter& topic) const
{
    auto filter = GetAsString();
    auto tc = topic.GetAsString();

    if (m_root && (tc.find(filter) != std::string::npos))
    {
        return true;
    }

    return tc == filter;
}

bool FilterProcessor::FilterByTopic(const std::string& messageTopic, const std::string& topicFilterString) const
{
    auto messageTopicFilter = TopicConverter(messageTopic);

    std::vector<std::string> _topics;
    boost::split(_topics, topicFilterString, boost::is_any_of("|"), boost::token_compress_on);

    if (topicFilterString.empty())
    {
        return true;
    }

    BOOST_FOREACH(auto tns, _topics)
    {
        if (TopicConverter(tns).Contains(messageTopicFilter))
        {
            return true;
        }
    }

    return false;
}

bool FilterProcessor::FilterByContent(const std::string& messageXmlText, const std::string& contentFilterString) const
{
    pugi::xml_document doc;

    if (!doc.load_string(messageXmlText.c_str()))
    {
        //LERR
        return false;
    }

    try
    {
        std::string contentNSFilterString = contentFilterString;

        {
            //TODO: just for ODTT. Need to parse namespaces and change to our names
            boost::replace_all(contentNSFilterString, "//tt1:", "//tt:");
        }

        pugi::xpath_query qry(contentNSFilterString.c_str());
        auto resp = qry.evaluate_boolean(doc);

        if (resp)
        {
            return true;
        }
    }
    catch (...)
    {
        //LERR
        return false;
    }

    return false;
}

bool FilterProcessor::Filter(const std::shared_ptr<MessageHolderType> filteringmessage) const
{
    bool found = true;
    BOOST_FOREACH(auto fl, m_filter)
    {
        if (fl->m_typeName == SOAP_TYPE_wsnt__TopicExpressionType)
        {
            found &= FilterByTopic(filteringmessage->GetTopic(), fl->m_filterString);
        }
        if (fl->m_typeName == SOAP_TYPE__wsnt__MessageContent)
        {
            found &= FilterByContent(filteringmessage->GetXml(), fl->m_filterString);
        }
    }
    return found;
}
