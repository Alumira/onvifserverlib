#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include "IBackgroundTask.h"

IBackgroundTask::~IBackgroundTask()
{
}

std::string IBackgroundTask::GetGUID() const
{
    return m_GUID;
}

void IBackgroundTask::Start(soap* soap)
{
    m_startTimer.restart();
    m_status = PROGRESS;
}

void IBackgroundTask::Stop()
{
    doInterrupt(STOPPED);
}

IBackgroundTask::SearchResult IBackgroundTask::GetStatus() const
{
    return m_status;
}

std::string IBackgroundTask::generateNewUuid()
{
    return boost::uuids::to_string(boost::uuids::random_generator()());
}

void IBackgroundTask::doInterrupt(SearchResult reason)
{
    m_interrupt = true;
    m_status = reason;
}
