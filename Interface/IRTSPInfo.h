﻿#pragma once
#include <boost/lexical_cast.hpp>
#include "Configuration/MediaProfile.h"
#include "Configuration/RecordingConfigurationTypes.h"

class IRTSPInfo
{
public:
	virtual ~IRTSPInfo() {}
	
	IRTSPInfo(int port, const std::string& ip, const std::string& nat_ip, const std::string& multicast_rtsp_port) :
				m_rtsp_port(port), m_ip(ip), m_multicast_rtsp_port(multicast_rtsp_port), m_nat_ip(nat_ip)
	{}
	
	std::string GetRtspPort() const
	{
		return boost::lexical_cast<std::string>(m_rtsp_port);
	}

	std::string GetMulicastRtspPort() const
	{
		return m_multicast_rtsp_port;
	}
	
	std::string GetIp() const
	{
		return m_ip;
	}

    std::string GetNatIp() const
    {
        if(!m_nat_ip.empty())
	        return m_nat_ip;

        return m_ip;
    }

	virtual std::string GetUri(MediaProfileSP mediaProfile, bool isMulticast) const = 0;
	virtual std::string GetReplayUri(const RecordingConfigurationSP& recConfig) const = 0;
    virtual std::string GetProfileUnicastUriName(const MediaProfileSP& mediaProfile) const = 0;

protected:
	int m_rtsp_port;
	std::string m_ip;
	std::string m_multicast_rtsp_port;
	std::string m_nat_ip;
};

typedef std::shared_ptr<IRTSPInfo> IRTSPInfoSP;
