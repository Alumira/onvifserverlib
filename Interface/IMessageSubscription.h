#pragma once
#include "Interface/MessageHolderType.h"
#include "Interface/MessageFilter.h"

class IMessageSubscription
{
public:
    explicit IMessageSubscription(std::vector<FilterTypeSP> filter) : m_filter(filter) {}
    virtual void OnReceive(MessageHolderTypeSP message) = 0;
    virtual void Cancel() {};

    virtual ~IMessageSubscription()
    {
    }

protected:
    std::vector<FilterTypeSP> m_filter;
};

typedef std::shared_ptr<IMessageSubscription> IMessageSubscriptionSP;
typedef std::weak_ptr<IMessageSubscription> IMessageSubscriptionPtr;
