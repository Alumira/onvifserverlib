#pragma once
#include <Configuration/UserConfiguration.h>

class ISecurityServer
{
public:
    enum AccessClasses
    {
        PRE_AUTH,
        READ_SYSTEM,
		READ_SYSTEM_SENSITIVE,
        READ_SYSTEM_SECRET,
        WRITE_SYSTEM,
        UNRECOVERABLE,
        READ_MEDIA, 
        ACTUATE
    };
	
	virtual ~ISecurityServer() {} 

	virtual std::string GetMD5ForUserInfo(const std::string& login, const std::string& password) const = 0;
	virtual int CheckAuthentication(struct soap* soap_ptr, const AccessClasses& accessClass) const = 0;
	virtual UserConfigurationList GetUsers() const = 0;
    virtual UserConfigurationSP GetUserConfiguration(const std::string& userid) const = 0;
    virtual std::string GetLoggedUserName(struct soap* soap_ptr) const = 0;
};

typedef std::shared_ptr<ISecurityServer> ISecurityServerSP;
