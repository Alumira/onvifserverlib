#include "IServiceImpl.h"
#include "SoapProxy/SoapUtil.h"

namespace onvif_generated_ 
{
    int IServiceImpl::no_profile(soap* soapObj, const std::string& profile_token) const
    {
        std::string lMsg = (boost::format("[%1%] Failed to find requested profile <%2%> in profiles list: %3%\t")
            % soapObj->socket % profile_token % GetConfiguration()->PrintProfiles()).str();
        ONVIF_LOG_WARN(m_data->m_logger, lMsg);

        OnvifUtil::ONVIF_Fault(soapObj, "ter:InvalidArgVal", "ter:NoProfile");
        return SOAP_FAULT;
    }

    int IServiceImpl::incomplete_configuration(soap* soapObj, const MediaProfileSP profile) const
    {
        std::string lMsg = (boost::format("[%1%] IncompleteConfiguration: <%2%>\t")
            % soapObj->socket % profile->PrintConfiguration()).str();
        ONVIF_LOG_WARN(m_data->m_logger, lMsg);
        
        OnvifUtil::ONVIF_Fault(soapObj, "ter:Action", "ter:IncompleteConfiguration");
        return SOAP_FAULT;
    }

}
